package test;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.HttpConfiguration;
import org.eclipse.jetty.server.HttpConnectionFactory;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ErrorPageErrorHandler;
import org.eclipse.jetty.servlet.ServletContextHandler;

public class Test {
	public static void main(String[] args) throws Exception {
		Server server = new Server(8080);
		//create http channel 
		HttpConfiguration http_config = new HttpConfiguration();
		http_config.setSecureScheme("https");
		http_config.setSecurePort(8080);
		http_config.setOutputBufferSize(30000);

		ServerConnector http = new ServerConnector(server,new HttpConnectionFactory(http_config));
		http.setPort(8080);
		http.setIdleTimeout(30000);
		server.addConnector(http);

		ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);

		context.setContextPath("/");
		context.addServlet(Server1.class, "/");

		server.setHandler(context);
		server.addBean(new ErrorHandler());
		server.start();
		server.join();
	}

	//error handler 
	static class ErrorHandler extends ErrorPageErrorHandler {
		@Override
		public void handle(String target, Request baseRequest,
				HttpServletRequest request, HttpServletResponse response)
						throws IOException {
			response.getWriter()
			.append("{\"status\":\"ERROR\",\"message\":\"HTTP ")
			.append(String.valueOf(response.getStatus())).append("\"}");
		}
	}
}
