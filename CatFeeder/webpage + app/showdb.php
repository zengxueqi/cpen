<?php

$servername = "localhost";
$username = "root";
$password = "group7";
$dbname = "catDataBase";

// Create & check connection
$conn = new mysqli($servername, $username, $password, $dbname);
if($conn->connect_error) {
    die($conn->connect_error);
}

// select data into db
$sql = "SELECT cat_id, date, amount_food FROM feed_history";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    echo "<center><h3>Database Record:</h3></center>";
    $totalsum = 0;

    while($row = $result->fetch_assoc()) {
      echo "<center>ID: " . $row["cat_id"]. " ---------- Date(YYYY-MM-DD HH/MM/SS): " .
      $row["date"]. " ---------- Amount of food(grams): " . $row["amount_food"].
      "<br></center>";
      $totalsum += $row["amount_food"];
    }
    echo "<br>";
    echo "<br>";
    // output the total food fed to the cat from last reset
    // the date we last fed the cat

    echo "<center>Total food used since last refill: " . $totalsum . " (grams) <br></center>";
    echo "<br>";
    // go back to main page
    echo "<center><button onclick=\"history.go(-1);\">Go back to main page</button></center>";

} else {
    echo "<center>0 results. You have never fed your cat!</center><br>";
    echo "<br>";
    // go back to main page
    echo "<center><button onclick=\"history.go(-1);\">Go back to main page</button></center>";
}

$conn->close();
?>
