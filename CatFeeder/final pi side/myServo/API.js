var http = require('http');
var express = require('express');
var piblaster = require('pi-blaster.js');
 
var app = express();
// ------------------------------------------------------------------------
// configure Express to serve index.html and any other static pages stored 
// in the home directory
app.use(express.static(__dirname));
 

//spin feeder
app.get('/unlock', function(req, res) { 
       res.header("Access-Control-Allow-Origin", "*");//set allow access header
       piblaster.setPwm(22, 0.1);//move in full speed
       setTimeout(function(){
       piblaster.setPwm(22, 0);//turn of the servo after 0.6 seconds
       },600);
       res.end('Box is unlocked');
 });

//spin camera control to right
 app.get('/unlockr', function(req, res) { 
       res.header("Access-Control-Allow-Origin", "*");
       piblaster.setPwm(27, 0.125);//move in half speed
       setTimeout(function(){
       piblaster.setPwm(27, 0);//stop after 0.1 second
       },100);
       res.end('Box is unlocked');
 });

//spin camera control to left
 app.get('/unlockl', function(req, res) {
       res.header("Access-Control-Allow-Origin", "*");
       piblaster.setPwm(27, 0.175);//move in half speed to the other direction
       setTimeout(function(){
       piblaster.setPwm(27, 0);//stop of 0.1 seconds
       },100);
       res.end('Box is unlocked');
 });


var flag = true;//flag to check whether to spin lief or right (the servo can only turn 90 degress)
 app.get('/unlockp', function(req, res) {
       res.header("Access-Control-Allow-Origin", "*");
       if(flag){//if flag is true turn one way
              piblaster.setPwm(23, 0.1);
              setTimeout(function(){
              piblaster.setPwm(23, 0);
              },1000);
              flag = false;//set flag false
       }
       else{//if flag is false turn the other way
             piblaster.setPwm(23, 0.2);
              setTimeout(function(){
              piblaster.setPwm(23, 0);
              },1000); 
              flag = true;//set flag true
       }
       
       res.end('Box is unlocked');
 });


 
// Express route for any other unrecognised incoming requests
app.get('*', function (req, res) {
       res.status(404).send('Unrecognised API call');
});
 
// Express route to handle errors
app.use(function (err, req, res, next) {
 if (req.xhr) {
       res.status(500).send('Oops, Something went wrong!');
 } else {
       next(err);
 }
}); // apt.use()
 
 
//------------------------------------------------------------------------
//on clrl-c, put stuff here to execute before closing your server with ctrl-c
process.on('SIGINT', function() {
 var i;
 console.log("\nGracefully shutting down from SIGINT (Ctrl+C)");
 process.exit();
});
 
// ------------------------------------------------------------------------
// Start Express App Server
//
app.listen(3000);
console.log('App Server is listening on port 3000');
