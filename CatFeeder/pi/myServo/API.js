var http = require('http');
var express = require('express');
var piblaster = require('pi-blaster.js');
 
var app = express();
// ------------------------------------------------------------------------
// configure Express to serve index.html and any other static pages stored 
// in the home directory
app.use(express.static(__dirname));
 
//try a simpler rest get call
app.get('/hello', function(req, res) { 
       console.log("hello");
 });
 
//lock rest get call
// app.get('/lock', function(req, res) { 
//        piblaster.setPwm(22, 0);

//        res.end('Box is locked');
//  });
 
//unlock rest get call
app.get('/unlock', function(req, res) { 
       res.header("Access-Control-Allow-Origin", "*");
       piblaster.setPwm(22, 0.1);
       setTimeout(function(){
       piblaster.setPwm(22, 0);
       },600);
       res.end('Box is unlocked');
 });

 app.get('/unlockr', function(req, res) { 
       res.header("Access-Control-Allow-Origin", "*");
       piblaster.setPwm(27, 0.125);
       setTimeout(function(){
       piblaster.setPwm(27, 0);
       },100);
       res.end('Box is unlocked');
 });

 app.get('/unlockl', function(req, res) {
       res.header("Access-Control-Allow-Origin", "*");
       piblaster.setPwm(27, 0.175);
       setTimeout(function(){
       piblaster.setPwm(27, 0);
       },100);
       res.end('Box is unlocked');
 });

var flag = true;
 app.get('/unlockp', function(req, res) {
       res.header("Access-Control-Allow-Origin", "*");
       if(flag){
              piblaster.setPwm(23, 0.1);
              setTimeout(function(){
              piblaster.setPwm(23, 0);
              },1000);
              flag = false;
       }
       else{
             piblaster.setPwm(23, 0.2);
              setTimeout(function(){
              piblaster.setPwm(23, 0);
              },1000); 
              flag = true;
       }
       
       res.end('Box is unlocked');
 });


 
// Express route for any other unrecognised incoming requests
app.get('*', function (req, res) {
       res.status(404).send('Unrecognised API call');
});
 
// Express route to handle errors
app.use(function (err, req, res, next) {
 if (req.xhr) {
       res.status(500).send('Oops, Something went wrong!');
 } else {
       next(err);
 }
}); // apt.use()
 
 
//------------------------------------------------------------------------
//on clrl-c, put stuff here to execute before closing your server with ctrl-c
process.on('SIGINT', function() {
 var i;
 console.log("\nGracefully shutting down from SIGINT (Ctrl+C)");
 process.exit();
});
 
// ------------------------------------------------------------------------
// Start Express App Server
//
app.listen(3000);
console.log('App Server is listening on port 3000');
