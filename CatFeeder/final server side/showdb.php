<?php

$servername = "localhost";
$username = "root";
$password = "group7";
$dbname = "catDataBase";

// Create & check connection
$conn = new mysqli($servername, $username, $password, $dbname);
if($conn->connect_error) {
    die($conn->connect_error);
}

// select data into db
$sql = "SELECT cat_id, date, amount_food FROM feed_history";
$result = $conn->query($sql);

// style for database page
echo "<style> body{ background-image: linear-gradient(rgba(255,255,255,0.5),rgba(255,255,255,0.5)), url('../img/cat.jpg');
    background-repeat: no-repeat;
    background-size: cover;
    background-attachment: fixed;
    background-position: center;}
    button{font-size:30px; border-radius:30px; height:60px;}
    center{
        background:linear-gradient(rgba(255,255,255,0.5),rgba(255,255,255,0.5));
        font-size:18px;
    } 
    </style>";

if ($result->num_rows > 0) {
    echo "<center><h3>Database Record:</h3></center>";
    // counters for totalfood and id
    $totalsum = 0;
    $id = 1;
    //display each row
    while($row = $result->fetch_assoc()) {
      echo "<center>ID: " . $id. " ---------- Date(YYYY-MM-DD HH/MM/SS): " .
      $row["date"]. "</br> Amount of food(grams): " . $row["amount_food"].
      "<br><br></center>";
      $totalsum += $row["amount_food"];
      $id++;
    }
    echo "<br>";
    echo "<br>";
    // output the total food fed to the cat from last reset
    // the date we last fed the cat

    echo "<center>Total food used since last refill: " . $totalsum . " (grams) <br></center>";
    echo "<br>";
    // go back to main page
    echo "<center><button onclick=\"history.go(-1);\">Go back to main page</button></center>";

} else {
    echo "<center>0 results. You have never fed your cat!</center><br>";
    echo "<br>";
    // go back to main page
    echo "<center><button onclick=\"history.go(-1);\">Go back to main page</button></center>";
}

$conn->close();
?>
