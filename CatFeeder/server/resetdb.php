<?php

$servername = "localhost";
$username = "root";
$password = "group7";
$dbname = "catDataBase";

// Create & check connection
$conn = new mysqli($servername, $username, $password, $dbname);
if($conn->connect_error) {
    die($conn->connect_error);
}

// clear data into db
$sql = "TRUNCATE TABLE feed_history";
$result = $conn->query($sql);

if ($result===TRUE) {
    header("Location: index.html?database=cleared");
} else {
    header("Location: index.html?database=notcleared");
}

$conn->close();
?>
