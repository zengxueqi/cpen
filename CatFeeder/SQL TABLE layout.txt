CREATE TABLE cat (name VARCHAR(30) NOT NULL, sex ENUM('M','F') NOT NULL, birth_date DATE NOT NULL, age TINYINT UNSIGNED NOT NULL, cat_id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY);

+------------+---------------------+------+-----+---------+----------------+
| Field      | Type                | Null | Key | Default | Extra          |
+------------+---------------------+------+-----+---------+----------------+
| name       | varchar(30)         | NO   |     | NULL    |                |
| sex        | enum('M','F')       | NO   |     | NULL    |                |
| birth_date | date                | NO   |     | NULL    |                |
| age        | tinyint(3) unsigned | NO   |     | NULL    |                |
| cat_id     | int(10) unsigned    | NO   | PRI | NULL    | auto_increment |
+------------+---------------------+------+-----+---------+----------------+

CREATE TABLE feed_history (cat_id INT UNSIGNED NOT NULL, date DATE NOT NULL, amount_food SMALLINT NOT NULL);

+-------------+------------------+------+-----+---------+-------+
| Field       | Type             | Null | Key | Default | Extra |
+-------------+------------------+------+-----+---------+-------+
| cat_id      | int(10) unsigned | NO   |     | NULL    |       |
| date        | date             | NO   |     | NULL    |       |
| amount_food | smallint(6)      | NO   |     | NULL    |       |
+-------------+------------------+------+-----+---------+-------+