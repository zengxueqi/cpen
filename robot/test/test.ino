#include <Servo.h>
#include <SoftwareSerial.h>
#include <math.h>

double arr[] = {170.25,108.5,83,195.25,263.25,252.75,259.5,82.5}; // assume this is an array with x,y coordinates of points

int E1 = 5;
int M1 = 4;
int E2 = 6;
int M2 = 7;

int echo = 2;
int trig = 3;                                                                                                                                                                                                                                                                                                      
int servo = A0;
int tempSensor = A1;
int hallPin1 = 8; // the number of the hall effect sensor pins //right
int hallPin2 = 9;//left
int pretime=0;
int p1 = 0;
int p2 =0;
int p1s=1;
int p2s = 1;

//PART4 VARIABLES
int numArgs = 0;
int data[32]; //data array containing x,y, coordinates for points
bool haveParsed = false; //condition whether things have been parsed

int switch4 = 13;
SoftwareSerial mySerial(0, 1);
Servo myservo;
unsigned long temp;
unsigned long duration;
unsigned long distance;

void setup()
{
  pinMode(M1, OUTPUT);   // set pin input mode or output mode
  pinMode(M2, OUTPUT);
  Serial.begin(9600);
//  Serial.println("Type AT commands!");
  mySerial.begin(38400);

      pinMode(echo,INPUT);
      pinMode(trig,OUTPUT);
      pinMode(tempSensor, INPUT);
      pinMode(hallPin1, INPUT);
      pinMode(hallPin2, INPUT);
  myservo.attach(servo); // set servo
}

void loop()
{
remoteControl();
}

void remoteControl(){
    if (mySerial.available()) {
    char control = mySerial.read();
    //forward
    while (control == '1') {
      digitalWrite(M1, HIGH);
      digitalWrite(M2, HIGH);
      Serial.println("forward");


      forward(200);
      control = mySerial.read();
    }

    //backward
    while (control == '2') {
      digitalWrite(M1, LOW);
      digitalWrite(M2, LOW);

      Serial.println("forward");

      forward(200);
      control = mySerial.read();
    }

    //left
    while (control == '3') {
      digitalWrite(M1, HIGH);
      digitalWrite(M2, HIGH);

      Serial.println("forward");

      turnleft(1);
      control = mySerial.read();
    }

    //right
    while (control == '4') {
      digitalWrite(M1, HIGH);
      digitalWrite(M2, HIGH);
      Serial.println("forward");

      turnright(1);
      control = mySerial.read();
    }

    //stop
    while (control == '0') {
      digitalWrite(M1, HIGH);
      digitalWrite(M2, HIGH);
      Serial.println("forward");

      stopm();
      control = mySerial.read();
    }

  }
}
//
//void part4() {
//  int count = 2;
//  int d90= 25;
//  double x1 = arr[0];
//  double y1 = arr[1];
//  double x2;
//  double y2;
//  double dist;
//  double angle;
//  int up;
//  int left;
//  while(count-1 < sizeof(arr)) {
//    x2 = arr[count];
//    y2 = arr[count+1];  
//
//    if(y2-y1 < 0)
//      up = 1;
//    else if(y2-y1==0)
//      up = 0;
//    else
//      up = -1;
//      
//    if(x2-x1 < 0)
//      left = 1;
//    else if(x2-x1 == 0)
//      left = 0;
//    else
//      left = -1;
//
//    angle = fabs(180 / 3.14159265 * atan((y2-y1)/(x2-x1))); // in degree
//    dist = distanceFun(x1,y1,x2,y2);
//    Serial.println(dist);
//    Serial.println(left);
//    Serial.println(up);
//    if(dist ==  0){
//      while(true)
//      stopm();
//    }
//
//    if(left==0 && up==1) { // front
//      while(dist > 0) {
//        forward(150);
//        dist -= 10;
//      }
//      stopm();
//    }
//
//    else if(left==0 && up==-1) { // back
//      turnleft(d90*2);
//      while(dist > 0) {
//        forward(150);
//        dist -= 10;
//      }
//      stopm();
//      turnright(d90*2);
//    }
//    else if(up==0 && left==1) { // left
//      turnleft(d90);
//      while(dist > 0) {
//        forward(150);
//        dist -= 10;
//      }
//      stopm();
//      turnright(d90);
//    }
//    else if(up==0 && left==-1) { // right
//      turnright(d90);
//      while(dist > 0) {
//        forward(150);
//        dist -= 10;
//      }
//      stopm();
//      turnleft(d90);
//    }
//    else if(up==1 && left==-1) { // NE
//      turnright(d90 / 90 * (90 - angle));
//      while(dist > 0) {
//        forward(150);
//        dist -= 10;
//      }
//      stopm();
//      turnleft(d90 / 90 * (90 - angle));
//    }
//    else if(left==-1 && up==-1) { // SE
//
//      turnright(d90 / 90 * (90 + angle));
//      while(dist > 0) {
//        forward(150);
//        dist -= 10;
//      }
//      stopm();
//      turnleft(d90 / 90 * (90 + angle));
//    }
//    else if(left==1 && up==1) { // NW
//      turnleft(d90 / 90 * (90 - angle));
//      while(dist > 0) {
//        forward(150);
//        dist -= 10;
//      }
//      stopm();
//      turnright(d90 / 90 * (90 - angle));
//    }
//    else if(left==1 && up==-1) { // SW
//      turnleft(d90 / 90 * (90 + angle));
//      delay(30);
//      while(dist > 0) {
//        forward(150);
//        dist -= 10;
//      }
//      stopm();
//      turnright(d90 / 90 * (90 + angle));
//      delay(1000);
//    }
//    else
//      stopm();
//    x1 = x2;
//    y1 = y2;
//    count = count + 2;
//  }
//}
//
//double distanceFun(double a, double b, double c, double d) {
//  return sqrt((d-b)*(d-b) + (c-a)*(c-a));
//}

void part1() {
  digitalWrite(M1, HIGH);
  digitalWrite(M2, HIGH);

  while (dist() >= 10) {
    int spd = 255;
    if (dist() <=100 && dist() >= 50)
      spd = 200;
    else if (dist() < 50 && dist() >= 20)
      spd = 150;
    else if (dist() < 20)
      spd = 90;
    forward(spd);
    delay(100);
//    Serial.println(dist());
  }

  stopm();

  int left = sweep(10, 0);
  int right = sweep(10, 1);
  Serial.println("left ");
  Serial.println(left);
  Serial.println("right ");
  Serial.println(right);

  delay(500);
  Serial.println("stop");
  if (left > right) {
    turnleft(18);
  }
  else {
    turnright(18);
  }
}


// Stop the robot
void stopm() {
  analogWrite(E1, 0);   //PWM Speed Control
  analogWrite(E2, 0);
}

// turn the robot to the left by tme
void turnleft(int tme) {
  digitalWrite(M2, LOW);
  for (int i = 0; i < tme; i++) {
    analogWrite(E1, 150);   //PWM Speed Control
    analogWrite(E2, 150);   //PWM Speed Control
    Serial.println("left");
    delay(30);
  }
  digitalWrite(M2, HIGH);
}

// turn the robot to the right by tme
void turnright(int tme) {
    digitalWrite(M1, LOW);
  for (int i = 0; i < tme; i++) {
    analogWrite(E1, 150);   //PWM Speed Control
    analogWrite(E2, 150);  //PWM Speed Control
        Serial.println("left");
    delay(30);
  }
    digitalWrite(M1, HIGH);
}

// control the speed of robot moving forward
void forward(int spd) {
  analogWrite(E1, spd);
  analogWrite(E2, spd);
  double t = millis();
  while(millis() - t<30){
    Serial.println("sdjf");
    checkWheels(spd);
  }

   if(p1>500 || p2 >500){
    p1 = 0;
    p2 = 0;
  }
//  delay(30);
  
}

double velocity(){
   int dur = millis()-pretime/1000;//to sec
   return 1/dur/5 * 3.1415926 * 6; //cm/s 
}

//hall effect adjusting to straight line
void checkWheels(int spd){
  if(!digitalRead(hallPin1)&& p1s==1){
    p1++;
    p1s=0;
    pretime = millis();
  }
  else p1s=1;
  if(!digitalRead(hallPin2)&& p2s==1){
    p2++;
    p2s=0;
  } 
  else p2s = 1;
  if(p1>p2){
    analogWrite(E1,spd-80);
    while(digitalRead(hallPin2));
    p2++;
    p2s=0;
        analogWrite(E1,spd);
  }
  if(p2>p1){
        analogWrite(E2,spd-80);
    while(digitalRead(hallPin1));
    p1++;
    p1s=0;
        pretime = millis();
        analogWrite(E2,spd);
  }
//  Serial.println("p1: " + String(p1));
//  Serial.println("p2: " + String(p2));
}
 


// measure the distance of an object in front of the sensor
// return type, unsigned long
unsigned long dist() {
  temp = 5.0 * analogRead(tempSensor) * 100.0 / 1024; // use temperature sensor to get more accurate measurement
//    Serial.println("temp: "+String(temp)); //commented out for twitter
  //  Serial.print(String(temp) + ",");

  //code for triggerring the HC-SR04 sensor
  digitalWrite(trig, HIGH);
  delayMicroseconds(10);
  digitalWrite(trig, LOW);

  //calculate the duration
  duration = pulseIn(echo, HIGH);

  //calculation of the distance
  double speedOfSound = 331.5 + (0.6 * temp);


  //unit conversion
  speedOfSound = speedOfSound * 0.0001;
  distance = speedOfSound * duration / 2;

  return distance;
}

unsigned long sweep(int mode, int lr) {
  int pos;
  unsigned long dis;
  //sweep function that turns the servo motor back and forth
  if (lr == 0) {
    for (pos = 90; pos <= 170; pos += 1) { // goes from 0 degrees to 180 degrees
      // in steps of 1 degree
      myservo.write(pos);   // tell servo to go to position in variable 'pos'
            delay(mode);                       // waits 15ms for the servo to reach the position
    }
    dis = dist();
    Serial.println(dis);
    for (pos = 170; pos >= 91; pos -= 1) { // goes from 180 degrees to 0 degrees
      myservo.write(pos);       // tell servo to go to position in variable 'pos'
            delay(mode);                       // waits 15ms for the servo to reach the position
    }
    return dis;
  }
  else {
    for (pos = 90; pos >= 11; pos -= 1) { // goes from 180 degrees to 0 degrees
      myservo.write(pos);       // tell servo to go to position in variable 'pos'
            delay(mode);                       // waits 15ms for the servo to reach the position
    }
    dis = dist();
    Serial.println(dis);
    for (pos = 10; pos <= 90; pos += 1) { // goes from 0 degrees to 180 degrees
      // in steps of 1 degree
      myservo.write(pos);   // tell servo to go to position in variable 'pos'
            delay(mode);                       // waits 15ms for the servo to reach the position
    }

    return dis;
  }


}
