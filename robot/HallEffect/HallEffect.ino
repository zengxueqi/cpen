#include <Servo.h>

int E1 = 5;  
int M1 = 4; 
int E2 = 6;                      
int M2 = 7;   

int echo = 2;
int trig = 3;
int servo = A0;
int tempSensor = A1;

Servo myservo;   
unsigned long temp;
unsigned long duration;
unsigned long distance;                  


int hallPin1 = 8; // the number of the hall effect sensor pins
int hallPin2 = 9;

double duration1 = 0; // duration of 1 spin of the wheel in microseconds
double duration2 = 0; // ..

void setup() { 
  // initialize the hall effect sensor pin as an input:
  pinMode(hallPin1, INPUT);
  pinMode(hallPin2, INPUT);
  pinMode(M1, OUTPUT);   // set pin input mode or output mode
    pinMode(M2, OUTPUT); 
    pinMode(echo,INPUT);
    pinMode(trig,OUTPUT);
    pinMode(tempSensor, INPUT);
    myservo.attach(servo); // set servo
  Serial.begin(9600);
}

void loop(){ // if it is a forward motion, make it straight
  // read the duration of 1 spin of the hall effect sensors:
//  while(digitalRead(hallPin1)); // wait for it to finish HIGH
//  duration1 = pulseIn(hallPin1, HIGH); // measure duration of 1 revolution of the wheel
//
//  while(digitalRead(hallPin2));
//  duration2 = pulseIn(hallPin2, HIGH);
  digitalWrite(M1,HIGH);   
  digitalWrite(M2, HIGH); 
      analogWrite(E1,80); 
    analogWrite(E2, 80);
  delay(1000);
  
  while(digitalRead(hallPin1) && digitalRead(hallPin2));
  if(digitalRead(hallPin1)){
    analogWrite(E2,0);//stop pin2 motor
    while(digitalRead(hallPin1)){
      analogWrite(E1, 80);  
      delay(80);
    }
  }
  if(digitalRead(hallPin2)){
        analogWrite(E1,0);//stop pin1 motor
    while(digitalRead(hallPin2)){
      analogWrite(E2, 80);  
      delay(80);
    }
  }

Serial.print("1: ");
Serial.println(digitalRead(hallPin1));
Serial.print("2: ");
Serial.println(digitalRead(hallPin2));
      analogWrite(E1, 0); 
    analogWrite(E2, 0);
delay(1500);
  // if duration1 and duration2 have great difference
  // robot isnt moving in a straight line
  // thus need to do something:
}
