#include <Servo.h>
#include <SoftwareSerial.h>
//#include <ShiftedLCD.h>
#include <SPI.h>

//initialize lcd
//LiquidCrystal lcd(10);


int E1 = 5;
int M1 = 4;
int E2 = 6;
int M2 = 7;

int echo = 2;
int trig = 3;
int servo = A0;
int tempSensor = A1;
//int pin1s = 1;
//int pin2s = 1;
int hallPin1 = 8; // the number of the hall effect sensor pins
int hallPin2 = 9;

int switch4 = 13;
SoftwareSerial mySerial(0,1);
Servo myservo;
unsigned long temp;
unsigned long duration;
unsigned long distance;

void setup()
{
  pinMode(M1, OUTPUT);   // set pin input mode or output mode
  pinMode(M2, OUTPUT);
  Serial.begin(9600);
  Serial.println("Type AT commands!");
  mySerial.begin(38400);
  lcd.begin(16,2);

  //    pinMode(echo,INPUT);
  //    pinMode(trig,OUTPUT);
  //    pinMode(tempSensor, INPUT);
  //    pinMode(hallPin1, INPUT);
  //    pinMode(hallPin2, INPUT);
  myservo.attach(servo); // set servo
}

void loop()
{
  remoteControl();
}

void remoteControl(){
    if (mySerial.available()) {
      lcd.clear();
    char control = mySerial.read();
    //forward
    while (control == '1') {
      digitalWrite(M1, HIGH);
      digitalWrite(M2, HIGH);
      Serial.println("forward");
      lcd.setCursor(0,0);
      lcd.print("Forward");

      forward(200);
      control = mySerial.read();
    }

    //backward
    while (control == '2') {
      digitalWrite(M1, LOW);
      digitalWrite(M2, LOW);
      lcd.setCursor(0,0);
      lcd.print("Backward");

      forward(200);
      control = mySerial.read();
    }

    //left
    while (control == '3') {
      digitalWrite(M1, HIGH);
      digitalWrite(M2, HIGH);

      lcd.setCursor(0,0);
      lcd.print("Turn Left");

      turnleft(1);
      control = mySerial.read();
    }

    //right
    while (control == '4') {
      digitalWrite(M1, HIGH);
      digitalWrite(M2, HIGH);
      
      lcd.setCursor(0,0);
      lcd.print("Turn Right");

      turnright(1);
      control = mySerial.read();
    }

    //stop
    while (control == '0') {
      digitalWrite(M1, HIGH);
      digitalWrite(M2, HIGH);
      lcd.setCursor(0,0);
      lcd.print("Stop");

      stopm();
      control = mySerial.read();
    }

  }
}
void part1() {
  digitalWrite(M1, HIGH);
  digitalWrite(M2, HIGH);

  while (dist() >= 10) {
    int spd = 250;
    if (dist() < 50 && dist() >= 30)
      spd = 150;
    else if (dist() < 30 && dist() >= 20)
      spd = 120;
    else if (dist() < 20)
      spd = 80;
    forward(spd);
    delay(100);
    Serial.println(dist());
  }

  stopm();

  int left = sweep(10, 0);
  int right = sweep(10, 1);
  Serial.println("left ");
  Serial.println(left);
  Serial.println("right ");
  Serial.println(right);

  delay(500);
  Serial.println("stop");
  if (left > right) {
    turnleft(20);
  }
  else {
    turnright(20);
  }
}


// Stop the robot
void stopm() {
  analogWrite(E1, 0);   //PWM Speed Control
  analogWrite(E2, 0);
}

// turn the robot to the left by tme
void turnleft(int tme) {
  for (int i = 0; i < tme; i++) {
    analogWrite(E1, 150);   //PWM Speed Control
    analogWrite(E2, 0);   //PWM Speed Control
    delay(30);
  }
}

// turn the robot to the right by tme
void turnright(int tme) {
  for (int i = 0; i < tme; i++) {
    analogWrite(E1, 0);   //PWM Speed Control
    analogWrite(E2, 150);  //PWM Speed Control
    delay(30);
  }
}

// control the speed of robot moving forward
void forward(int spd) {
  analogWrite(E1, spd);
  analogWrite(E2, spd);
  delay(30);
  //    double t = millis();
  //     pin1s= digitalRead(hallPin1);
  //      pin2s = digitalRead(hallPin2);
  //     while(pin1s && pin2s && millis()-t < 50){
  //      pin1s= digitalRead(hallPin1);
  //      pin2s = digitalRead(hallPin2);
  //      delay(5);
  //     }
  //  if(!pin2s){
  //    analogWrite(E2,0);//stop pin2 motor
  //    while(pin1s&& millis() - t < 50){
  //      analogWrite(E1, 80);
  //      pin1s = digitalRead(hallPin1);
  //      delay(5);
  //    }
  //  }
  //  if(!pin1s){
  //        analogWrite(E1,0);//stop pin1 motor
  ////    while(digitalRead(hallPin2) && ){
  ////      analogWrite(E2, 80);
  ////      pin2s = digitalRead(hallPin2);
  ////      delay(80);
  ////    }
  //  }
}

// measure the distance of an object in front of the sensor
// return type, unsigned long
unsigned long dist() {
  temp = 5.0 * analogRead(tempSensor) * 100.0 / 1024; // use temperature sensor to get more accurate measurement
  //  Serial.println("temp: "+String(temp)); //commented out for twitter
  //  Serial.print(String(temp) + ",");

  //code for triggerring the HC-SR04 sensor
  digitalWrite(trig, HIGH);
  delayMicroseconds(10);
  digitalWrite(trig, LOW);

  //calculate the duration
  duration = pulseIn(echo, HIGH);

  //calculation of the distance
  double speedOfSound = 331.5 + (0.6 * temp);


  //unit conversion
  speedOfSound = speedOfSound * 0.0001;
  distance = speedOfSound * duration / 2;

  return distance;
}

unsigned long sweep(int mode, int lr) {
  int pos;
  unsigned long dis;
  //sweep function that turns the servo motor back and forth
  if (lr == 0) {
    for (pos = 90; pos <= 180; pos += 1) { // goes from 0 degrees to 180 degrees
      // in steps of 1 degree
      myservo.write(pos);   // tell servo to go to position in variable 'pos'
      //      delay(mode);                       // waits 15ms for the servo to reach the position
    }
    dis = dist();
    Serial.println(dis);
    for (pos = 180; pos >= 91; pos -= 1) { // goes from 180 degrees to 0 degrees
      myservo.write(pos);       // tell servo to go to position in variable 'pos'
      //      delay(mode);                       // waits 15ms for the servo to reach the position
    }
    return dis;
  }
  else {
    for (pos = 90; pos >= 1; pos -= 1) { // goes from 180 degrees to 0 degrees
      myservo.write(pos);       // tell servo to go to position in variable 'pos'
      //      delay(mode);                       // waits 15ms for the servo to reach the position
    }
    dis = dist();
    Serial.println(dis);
    for (pos = 0; pos <= 90; pos += 1) { // goes from 0 degrees to 180 degrees
      // in steps of 1 degree
      myservo.write(pos);   // tell servo to go to position in variable 'pos'
      //      delay(mode);                       // waits 15ms for the servo to reach the position
    }

    return dis;
  }


}
