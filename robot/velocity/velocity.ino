
#include <Servo.h>
#include <SoftwareSerial.h>
#include <math.h>

double arr[] = {170.25,108.5,83,195.25,263.25,252.75,259.5,82.5}; // assume this is an array with x,y coordinates of points

int E1 = 5;
int M1 = 4;
int E2 = 6;
int M2 = 7;
int r = 0;

int echo = 2;
int trig = 3;                                                                                                                                                                                                                                                                                                      
int servo = A0;
int tempSensor = A1;
int hallPin1 = 8; // the number of the hall effect sensor pins //right
int hallPin2 = 9;//left
int pretime=0;
int p1 = 0;
int p2 =0;
int p1s=1;
int p2s = 1;

//PART4 VARIABLES
int numArgs = 0;
int data[32]; //data array containing x,y, coordinates for points
bool haveParsed = false; //condition whether things have been parsed

int switch4 = 13;
SoftwareSerial mySerial(10, 8);
Servo myservo;
unsigned long temp;
unsigned long duration;
unsigned long distance;



void setup()
{
  pinMode(M1, OUTPUT);   // set pin input mode or output mode
  pinMode(M2, OUTPUT);
  Serial.begin(9600);
//  Serial.println("Type AT commands!");
  mySerial.begin(38400);

      pinMode(echo,INPUT);
      pinMode(trig,OUTPUT);
      pinMode(tempSensor, INPUT);
      pinMode(hallPin1, INPUT);
      pinMode(hallPin2, INPUT);
  myservo.attach(servo); // set servo
}

void loop() {
  // put your main code here, to run repeatedly:
    digitalWrite(M1, HIGH);
  digitalWrite(M2, HIGH);
  double t1 = millis();
  while(millis()-t1 <1000){
  forward(200);

  }
//  delay(1000);
  Serial.println(velocity());
}

void forward(int spd) {
//  Serial.println(p1);
  if(p1>20 &&p2 >20){
    p1 -=20;
    p2 -= 20;
    pretime = millis();
  }
  if(p1==0) pretime = millis();
  analogWrite(E1, spd);
  analogWrite(E2, spd);
  double t = millis();
  while(millis() - t<30){
    checkWheels(spd);
  }
//  delay(30);
  
}

double velocity(){
   double dur = (millis()-pretime)/1000.0;//to sec
   Serial.println("dur: " + String(dur) + " p1: " + String(p1));
   return p1/dur/5 * 3.1415926 * 6; //cm/s 
}

//hall effect adjusting to straight line
void checkWheels(int spd){
  if((!digitalRead(hallPin1))&& p1s==1){
    p1++;
//    Serial.println(digitalRead(hallPin1));
    p1s=0;
  }
  else if(p1s==0 &&(!digitalRead(hallPin1)));
  else p1s=1;
  if((!digitalRead(hallPin2))&& p2s==1){
    p2++;
    p2s=0;
  } 
  else if(p2s==0&&(!digitalRead(hallPin1)));
  else p2s = 1;
  if(p1>p2){
    analogWrite(E1,spd-80);
    while(digitalRead(hallPin2));
    p2++;
    p2s=0;
        analogWrite(E1,spd);
  }
  if(p2>p1){
        analogWrite(E2,spd-80);
    while(digitalRead(hallPin1));
    p1++;
    p1s=0;
        analogWrite(E2,spd);
  }
//  Serial.println("p1: " + String(p1));
//  Serial.println("p2: " + String(p2));
}
 
