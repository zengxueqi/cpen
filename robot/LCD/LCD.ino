// LCD pins
int RS = 13, E = 12, D4 = 11, D5 = 10, D6 = 9, D7 = 8;
int pin_swap[4] = {D4, D5, D6, D7}; //change initialization backwards

void setup(){
 // Set up the LCD pins
  pinMode(RS, OUTPUT);
  pinMode(E, OUTPUT);
  pinMode(D4, OUTPUT);
  pinMode(D5, OUTPUT);
  pinMode(D6, OUTPUT);
  pinMode(D7, OUTPUT);
  // Set up the serial monitor for debugging
  Serial.begin(9600);
  
  initializeLCD();
  delay(200);
  digitalWrite(RS, LOW); // clear LCD
  write_LCD(0x00);
  write_LCD(0x01);
  digitalWrite(RS, HIGH); // print CPEN 291
  str_2_LCD("CPEN 291");

  digitalWrite(RS, LOW); // next line
  write_LCD(0x0C);
  write_LCD(0x00);

  digitalWrite(RS, HIGH); // print L2A-7B
  str_2_LCD1("L2A-7-P1");
  delay(2000);


  for(int i = 0; i < 16; i++) { // a loop to shift till the right most
    digitalWrite(RS, LOW);
    write_LCD(0x00); // clear
    write_LCD(0x01);
    for(int j = 0; j < i+1; j++) {
      write_LCD(0x01); // set cursor to nx
      write_LCD(0x0C);
    }
    digitalWrite(RS, HIGH);
    str_2_LCD1("CPEN 291");

    digitalWrite(RS, LOW); // next line
    write_LCD(0x0C);
    write_LCD(0x00);

    digitalWrite(RS, HIGH);
    str_2_LCD("Team L2A-7-P1");
    delay(500);
  }
  digitalWrite(RS, LOW); // clear
  write_LCD(0x00);
  write_LCD(0x01);
}

void loop(){ 

}



void str_2_LCD(String str) { // print the string on LCD
  int ch; // ascii value of the character
  int leng = str.length();
  for(int i = 0; i < leng ; i++) {
    ch = str.charAt(i);
    digitalWrite(RS, HIGH);
    write_LCD(ch>>4); // upper bits
    write_LCD(ch & 0x0F); // lower bits
  }
  nextline();
  delay(1000);
  clear();
}

void initializeLCD() {
  digitalWrite(RS, LOW);
  digitalWrite(E, LOW);
  delay(50);
  write_LCD(0x03);
  delay(5);
  write_LCD(0x03);
  delay(1);
  write_LCD(0x03);
  delay(1);
  write_LCD(0x03);
  write_LCD(0x02);
  
  write_LCD(0x02); //N=1, F=1, XX = 00
  write_LCD(0x08);
  write_LCD(0x01);
  write_LCD(0x00);
  write_LCD(0x00);
  write_LCD(0x0C);
  write_LCD(0x00);
  write_LCD(0x06); // I/D and S
}

void write_LCD(uint8_t data)
{
  uint8_t cur_Bit; // the current bits reading
  for (int index = 3; index >= 0; index--){
    digitalWrite(pin_swap[index], (data>>index) & 0x01);
  }
  digitalWrite(E, LOW);
  delay(1);
  digitalWrite(E, HIGH);
  delay(1);
  digitalWrite(E, LOW);
  delay(1);
}

void clear() {
  digitalWrite(RS, LOW); // clear
  write_LCD(0x00);
  write_LCD(0x01);
}

void nextline() {
  digitalWrite(RS, LOW); // next line
  write_LCD(0x0C);
  write_LCD(0x00);
}
