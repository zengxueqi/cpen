#include <Servo.h>
#include <SoftwareSerial.h>

//============= PART 1 ===============
int E1 = 5;
int M1 = 4;
int E2 = 6;
int M2 = 7;

int echo = 2;
int trig = 3;                                                                                                                                                                                                                                                                                                      
int servo = A0;
int tempSensor = A1;
int hallPin1 = 8; // the number of the hall effect sensor pins //right
int hallPin2 = 9;//left
int pretime=0;
int p1 = 0;
int p2 =0;

int switch4 = 13;
Servo myservo;
unsigned long temp;
unsigned long duration;
unsigned long distance;


//============= PART 2 ================
const int opticalPin1 = A2;//left optical sensor
const int opticalPin2 = A3;
const int opticalPin3 = A4;//right optical sensor 
const int opticalPin4 = A5;

//define some global variables here 
const int speed = 225; //define the speed
const int turnTime = 20;//define the turning time 
const int TURNING_CONSTANT = 50;
const int threshold = 300; //the value to check optical sensor is on track or not 
int opticalValue1 = 0;
int opticalValue2 = 0;
int opticalValue3 = 0;
int opticalValue4 = 0;


//EXTRA FUNCTIONALITY 
//modes;    5: ultrasonic, 6: track, 7: remote
SoftwareSerial mySerial(10, 8);
char BTMODE = '5'; //default mode: ultrasonic


void setup()
{
  pinMode(M1, OUTPUT);   // set pin input mode or output mode
  pinMode(M2, OUTPUT);
  Serial.begin(9600);
//  Serial.println("Type AT commands!");
  mySerial.begin(38400);

      pinMode(echo,INPUT);
      pinMode(trig,OUTPUT);
      pinMode(tempSensor, INPUT);
      pinMode(hallPin1, INPUT);
      pinMode(hallPin2, INPUT);
  myservo.attach(servo); // set servo
}

void loop()
{
 //use bluetooth app to toggle between modes:
 //5: ultrasonic, 6: track, 7: remote
  if (mySerial.available()) { //read for change each loop to determine which mode to execute
    if(mySerial.read() == '5' || mySerial.read() == '6' || mySerial.read() == '7') { 
      BTMODE = mySerial.read();
    }
  }
  
  switch(BTMODE) {
    case 5:  //Part1: ULTRASONIC
      part1();
      break;
    case 6:
      func2(); //Part2: TRACK
      break;
    case 7:
      remoteControl(); //EXTRA FUNCTIONALITY
      break;
  }

}

//============= EXTRA FUNCTIONALITY ===============
/*
 * Additional Functionality: use blue tooth to control the robot
 */
void remoteControl(){
    if (mySerial.available()) {
    char control = mySerial.read();
    //forward
    while (control == '1') {
      digitalWrite(M1, HIGH);
      digitalWrite(M2, HIGH);
      Serial.println("forward");


      forward(200);
      control = mySerial.read();
    }

    //backward
    while (control == '2') {
      digitalWrite(M1, LOW);
      digitalWrite(M2, LOW);

      Serial.println("forward");

      forward(200);
      control = mySerial.read();
    }

    //left
    while (control == '3') {
      digitalWrite(M1, HIGH);
      digitalWrite(M2, HIGH);

      Serial.println("forward");

      turnleft(1);
      control = mySerial.read();
    }

    //right
    while (control == '4') {
      digitalWrite(M1, HIGH);
      digitalWrite(M2, HIGH);
      Serial.println("forward");

      turnright(1);
      control = mySerial.read();
    }

    //stop
    while (control == '0') {
      digitalWrite(M1, HIGH);
      digitalWrite(M2, HIGH);
      Serial.println("forward");

      stopm();
      control = mySerial.read();
    }

  }
}

//============= PART 1 ===============
/*
 * Functionality 1:
 * Robot walk at maximum speed until it detects an object in fornt of it
 * When robot detects an object at 50cm , it should gradually slow down 
 * Robot use servo motor to pick the directionthat has the longest free space
 */
void part1() {
  digitalWrite(M1, HIGH);
  digitalWrite(M2, HIGH);

  while (dist() >= 20) {
    int spd = 250;
    if (dist() <=100 && dist() >= 50)
      spd = 150;
    else if (dist() < 50 && dist() >= 20)
      spd = 80;
    else if (dist() < 20)
      spd = 80;
    forward(spd);
    delay(100);
    Serial.println(dist());
  }

  stopm();

  int left = sweep(10, 0);
  int right = sweep(10, 1);
  Serial.println("left ");
  Serial.println(left);
  Serial.println("right ");
  Serial.println(right);

  delay(500);
  Serial.println("stop");
  if (left > right) {
    turnleft(30);
  }
  else {
    turnright(30);
  }
}


/*
 * Stop the robot
 */
void stopm() {
  analogWrite(E1, 0);   //PWM Speed Control
  analogWrite(E2, 0);
}

// turn the robot to the left by tme
void turnleft(int tme) {
  for (int i = 0; i < tme; i++) {
    analogWrite(E1, 150);   //PWM Speed Control
    analogWrite(E2, 0);   //PWM Speed Control
    delay(30);
  }
}

// turn the robot to the right by tme
void turnright(int tme) {
  for (int i = 0; i < tme; i++) {
    analogWrite(E1, 0);   //PWM Speed Control
    analogWrite(E2, 150);  //PWM Speed Control
    delay(30);
  }
}

// control the speed of robot moving forward
void forward(int spd) {
  analogWrite(E1, spd);
  analogWrite(E2, spd);
  while(millis() - pretime<30){
    checkWheels(spd);
  }
      pretime = millis();
   if(p1>500 || p2 >500){
    p1-=490;
    p2-=490;
  } 
//  delay(30);
  
}

/*
 * Hall effect adjusting to straight line
 */
void checkWheels(int spd){
  if(!digitalRead(hallPin1))
    p1++;
  if(!digitalRead(hallPin2))
    p2++;
  if(p1>p2){
    analogWrite(E1,spd-42);
    while(digitalRead(hallPin2));
    p2++;
        analogWrite(E1,spd);
  }
  if(p2>p1){
        analogWrite(E2,spd-40);
    while(digitalRead(hallPin1));
    p1++;
        analogWrite(E2,spd);
  }
  Serial.println("p1: " + String(p1));
  Serial.println("p2: " + String(p2));
}
 


/* 
 *Measure the distance of an object in front of the sensor
 *Return type, unsigned long
 */
unsigned long dist() {
  temp = 5.0 * analogRead(tempSensor) * 100.0 / 1024; // use temperature sensor to get more accurate measurement
//    Serial.println("temp: "+String(temp)); //commented out for twitter
  //  Serial.print(String(temp) + ",");

  //code for triggerring the HC-SR04 sensor
  digitalWrite(trig, HIGH);
  delayMicroseconds(10);
  digitalWrite(trig, LOW);

  //calculate the duration
  duration = pulseIn(echo, HIGH);

  //calculation of the distance
  double speedOfSound = 331.5 + (0.6 * temp);


  //unit conversion
  speedOfSound = speedOfSound * 0.0001;
  distance = speedOfSound * duration / 2;

  return distance;
}

unsigned long sweep(int mode, int lr) {
  int pos;
  unsigned long dis;
  //sweep function that turns the servo motor back and forth
  if (lr == 0) {
    for (pos = 90; pos <= 170; pos += 1) { // goes from 0 degrees to 180 degrees
      // in steps of 1 degree
      myservo.write(pos);   // tell servo to go to position in variable 'pos'
            delay(mode);                       // waits 15ms for the servo to reach the position
    }
    dis = dist();
    Serial.println(dis);
    for (pos = 170; pos >= 91; pos -= 1) { // goes from 180 degrees to 0 degrees
      myservo.write(pos);       // tell servo to go to position in variable 'pos'
            delay(mode);                       // waits 15ms for the servo to reach the position
    }
    return dis;
  }
  else {
    for (pos = 90; pos >= 11; pos -= 1) { // goes from 180 degrees to 0 degrees
      myservo.write(pos);       // tell servo to go to position in variable 'pos'
            delay(mode);                       // waits 15ms for the servo to reach the position
    }
    dis = dist();
    Serial.println(dis);
    for (pos = 10; pos <= 90; pos += 1) { // goes from 0 degrees to 180 degrees
      // in steps of 1 degree
      myservo.write(pos);   // tell servo to go to position in variable 'pos'
            delay(mode);                       // waits 15ms for the servo to reach the position
    }

    return dis;
  }
}


//============= PART 2 ===============
/*
 * Implement functionality 2: robot follows the given track on the ground.
 * Robot should go forward when encountering the cross-over
 * If there is no track, the robot must stop 
 * List all possibilities of the value read from optical sensor compared to reference value 
 */
 void func2(){
  updateOptical();
  if(checkOnTrack()){
    //middle sensor 2 and 3 is on track,move forward 
    if(opticalValue1 < threshold && opticalValue2 > threshold && opticalValue3 > threshold && opticalValue4 < threshold){
    moveForward(speed);
    }
    //only sensor right 3 is on track, turn right slowly
    else if(opticalValue1 < threshold && opticalValue2 < threshold && opticalValue3 > threshold && opticalValue4 < threshold){
    slowTurnRight(speed,turnTime);
    }
    //only left sensor 2 is on track, turn left slowly
    else if(opticalValue1 < threshold && opticalValue2 > threshold && opticalValue3 < threshold && opticalValue4 < threshold){
    slowTurnLeft(speed,turnTime);
    }
    
    //only right sensor 1,2 is on track, turn left fast
    else if(opticalValue1 > threshold && opticalValue2 > threshold && opticalValue3 < threshold && opticalValue4 < threshold){
    sharpTurnLeft(speed,turnTime);
    }
    //only left sensor 3,4 in on track, turn right fast
    else if(opticalValue1 < threshold && opticalValue2 < threshold && opticalValue3 > threshold && opticalValue4 > threshold){
    fastTurnRight(speed,turnTime);
    }
    //only right sensor 1 is on track, turn left fast
    else if(opticalValue1 > threshold && opticalValue2 < threshold && opticalValue3 < threshold && opticalValue4 < threshold){
    sharpTurnLeft(speed,turnTime);
    }
    //only left sensor 4 in on track, turn right fast
    else if(opticalValue1 < threshold && opticalValue2 < threshold && opticalValue3 < threshold && opticalValue4 > threshold){
    sharpTurnRight(speed,turnTime);
    }
    //1,2,3 on track, turn left sharp(90 degree) by increasing the turning time 
    else if(opticalValue1 > threshold && opticalValue2 > threshold && opticalValue3 > threshold && opticalValue4 < threshold){
    sharpTurnLeft(speed,turnTime + 50);
    }
    //2,3,4 on track, turn right sharp(90 degree) by increasing the turning time 
    else if(opticalValue1 < threshold && opticalValue2 > threshold && opticalValue3 > threshold && opticalValue4 > threshold){
    sharpTurnRight(speed,turnTime + 50);
    }

    else if(opticalValue1 < threshold && opticalValue2 > threshold && opticalValue3 < threshold && opticalValue4 > threshold){
      slowTurnRight(speed,turnTime);
    }
    else if(opticalValue1 > threshold && opticalValue2 < threshold && opticalValue3 > threshold && opticalValue4 < threshold){
      slowTurnLeft(speed,turnTime);
    }
    else
    moveForward(speed);
  }
  else 
    moveStop();//stop the robot if it is not on track 
 }

 /*
  * return true if robot needs to move forward false otherwise 
  */
boolean checkForward(){
  return (opticalValue1 < threshold && opticalValue2 > threshold && opticalValue3 > threshold && opticalValue4 < threshold);
}

/*
 * Slowly turn left if only one sensor is on track
 */
void slowTurnLeft(int speed,int time){
  Serial.println("slow left");
unsigned long oldTime = millis();
while(millis() - oldTime < time){
  digitalWrite(M1,HIGH);
  digitalWrite(M2,HIGH);
  analogWrite(E1,speed);
  analogWrite(E2,speed - TURNING_CONSTANT);
  updateOptical(); //update the value read from optical sensor 

  if(checkForward() && checkOnTrack())//if robot move on the middle of track,stop turning 
    break;
  if(!checkOnTrack()){//if robot is not on track, stop the robot 
    moveStop();
   break;
   }
  }
}

/*
 * Fast turn left if 2 left sensor on track
 */
void fastTurnLeft(int speed,int time){
Serial.println("fast left");
unsigned long oldTime = millis();
while(millis() - oldTime < time){
  digitalWrite(M1,HIGH);
  digitalWrite(M2,HIGH);
  analogWrite(E1,speed);
  analogWrite(E2,0);
  updateOptical();

  if(checkForward() && checkOnTrack())
    break;
  if(!checkOnTrack()){
    moveStop();
   break;
   }
  }
}

/*
 * Slowly turn right if only one sensor is on track
 */
void slowTurnRight(int speed,int time){
Serial.println("slow right");
unsigned long oldTime = millis();
while(millis() - oldTime < time){
  digitalWrite(M1,HIGH);
  digitalWrite(M2,HIGH);
  analogWrite(E1,speed - TURNING_CONSTANT);
  analogWrite(E2,speed);
  
  updateOptical();

  if(checkForward() && checkOnTrack())
    break;
  if(!checkOnTrack()){
    moveStop();
    break;
   }
  }
}

/*
 * Fast turn right if 2 right sensor on track
 */
void fastTurnRight(int speed,int time){
Serial.println("fast right");
unsigned long oldTime = millis();
while(millis() - oldTime < time){
  digitalWrite(M1,HIGH);
  digitalWrite(M2,HIGH);
  analogWrite(E1,0);
  analogWrite(E2,speed);

  updateOptical();
  
  if(checkForward() && checkOnTrack()) //stop turning if robot needs to move forward
    break;
  if(!checkOnTrack()){
    moveStop();
    break;
   }
  }
}

/*
 * Robot truns left sharply
 */
void sharpTurnLeft(int speed, int time){
Serial.println("sharp_left");
unsigned long oldTime = millis();
while(millis() - oldTime < time){
  digitalWrite(M1,HIGH);
  digitalWrite(M2,LOW);
  analogWrite(E1,speed);
  analogWrite(E2,speed);

  updateOptical();

  if(checkForward() && checkOnTrack())
  break;
  }
}

/*
 * Robot turns right sharply
 */
void sharpTurnRight(int speed,int time){
unsigned long oldTime = millis();
Serial.println("sharp_right");
while(millis() - oldTime < time){
  digitalWrite(M1,LOW);
  digitalWrite(M2,HIGH);
  analogWrite(E1,speed);
  analogWrite(E2,speed);
  updateOptical();

  if(checkForward() && checkOnTrack())
  break;
  }
}

/*
 * Robot goes a straight line 
 */
void moveForward(int speed){
  Serial.println("forward");
  digitalWrite(M1,HIGH);
  digitalWrite(M2,HIGH);
  analogWrite(E1,speed);
  analogWrite(E2,speed);
}

/*
 * Stop the robot
 */
void moveStop(){
  Serial.println("stop");
  digitalWrite(M1,HIGH);
  digitalWrite(M2,HIGH);
  analogWrite(E1,0);
  analogWrite(E2,0);
}

/*
 * Check if robot is on track 
 * @return true if robot is on track, false otherwise 
 */
boolean checkOnTrack(){
  if(opticalValue1 < threshold && opticalValue2 < threshold && opticalValue3 < threshold && opticalValue4 < threshold)
  return false;
  else 
  return true;
}

/*
 * Update optical sensor value at 4 sensors together 
 */
void updateOptical(){
  opticalValue1 = analogRead(opticalPin1);
  opticalValue2 = analogRead(opticalPin2);
  opticalValue3 = analogRead(opticalPin3);
  opticalValue4 = analogRead(opticalPin4);
}

//test optical sensor,test only
void print(){
Serial.println(analogRead(A2));
Serial.println(analogRead(A3));
Serial.println(analogRead(A4));
Serial.println(analogRead(A5));
//delay(2000);
}





