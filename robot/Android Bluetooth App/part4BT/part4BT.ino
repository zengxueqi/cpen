 #include <Servo.h>
#include <SoftwareSerial.h>
#include <math.h>
SoftwareSerial mySerial(0, 1); // RX, TX


//PART4 VARIABLES
int numArgs = 0;
int data[32]; //data array containing x,y, coordinates for points
bool haveParsed = false; //condition whether things have been parsed


double arr[] = {170.25,108.5,83,195.25,263.25,252.75,259.5,82.5}; // assume this is an array with x,y coordinates of points

int E1 = 5;
int M1 = 4;
int E2 = 6;
int M2 = 7;

int echo = 2;
int trig = 3;                                                                                                                                                                                                                                                                                                      
int servo = A0;
int tempSensor = A1;
int hallPin1 = 8; // the number of the hall effect sensor pins //right
int hallPin2 = 9;//left
int pretime=0;
int p1 = 0;
int p2 =0;
int p1s=1;
int p2s = 1;
//SoftwareSerial mySerial(10, 8);
Servo myservo;
unsigned long temp;
unsigned long duration;
unsigned long distance;
void setup() 
{
   pinMode(M1, OUTPUT);   // set pin input mode or output mode
  pinMode(M2, OUTPUT);
  Serial.begin(9600);
//  Serial.println("Type AT commands!");
  mySerial.begin(38400);

      pinMode(echo,INPUT);
      pinMode(trig,OUTPUT);
      pinMode(tempSensor, INPUT);
      pinMode(hallPin1, INPUT);
      pinMode(hallPin2, INPUT);
  myservo.attach(servo); // set servo

}
 
void loop() 
{

  if(!haveParsed) { //wait for user to input data, then parse it into data array
   parse();
  }
  else {
    //using data[] and numArgs
    part4(); //using the data array coordinates, translate to robot movement those points
    haveParsed = false;

}

  
}


void parse() { //converts bluetooth data from serial into int array of coordinates
  String str;
if(mySerial.available()) {

str = mySerial.readString();// read the incoming data as string

//Serial.println(str); //DEBUG

int index = str.indexOf(","); //want to split string by comma
String val;
char charBuffer[3]; //canvas range of 0 to ~300, so only need 3 char positions
int beginIndex = 0;

while (index != -1) //keep check for commas until end reached
{
    val = str.substring(beginIndex, index);
    val.toCharArray(charBuffer, 16);
    data[numArgs++] = atoi(charBuffer); //convert string into int and store into data array
    beginIndex = index + 1;
    index = str.indexOf(",", beginIndex);
}

//Serial.println(numArgs); //DEBUG
for(int i = 0; i <numArgs; i++) {
  Serial.println(data[i]);

  haveParsed = true; //set condition to stop parse
}
  
}
}



void part4() {
  int count = 2;
  int d90= 25;
  double x1 = data[0];
  double y1 = data[1];
  double x2;
  double y2;
  double dist;
  double angle;
  int up;
  int left;
  for(count = 2; count< numArgs; count+=2) {
    x2 = data[count];
    y2 = data[count+1];  

    if(y2-y1 < 0)
      up = 1;
    else if(y2-y1==0)
      up = 0;
    else
      up = -1;
      
    if(x2-x1 < 0)
      left = 1;
    else if(x2-x1 == 0)
      left = 0;
    else
      left = -1;

    angle = fabs(180 / 3.14159265 * atan((y2-y1)/(x2-x1))); // in degree
    dist = distanceFun(x1,y1,x2,y2);
    Serial.println(dist);
    Serial.println(left);
    Serial.println(up);
    if(dist ==  0){
      while(true)
      stopm();
    }

    if(left==0 && up==1) { // front
      while(dist > 0) {
        forward(150);
        dist -= 10;
      }
      stopm();
    }

    else if(left==0 && up==-1) { // back
      turnleft(d90*2);
      while(dist > 0) {
        forward(150);
        dist -= 10;
      }
      stopm();
      turnright(d90*2);
    }
    else if(up==0 && left==1) { // left
      turnleft(d90);
      while(dist > 0) {
        forward(150);
        dist -= 10;
      }
      stopm();
      turnright(d90);
    }
    else if(up==0 && left==-1) { // right
      turnright(d90);
      while(dist > 0) {
        forward(150);
        dist -= 10;
      }
      stopm();
      turnleft(d90);
    }
    else if(up==1 && left==-1) { // NE
      turnright(d90 / 90 * (90 - angle));
      while(dist > 0) {
        forward(150);
        dist -= 10;
      }
      stopm();
      turnleft(d90 / 90 * (90 - angle));
    }
    else if(left==-1 && up==-1) { // SE

      turnright(d90 / 90 * (90 + angle));
      while(dist > 0) {
        forward(150);
        dist -= 10;
      }
      stopm();
      turnleft(d90 / 90 * (90 + angle));
    }
    else if(left==1 && up==1) { // NW
      turnleft(d90 / 90 * (90 - angle));
      while(dist > 0) {
        forward(150);
        dist -= 10;
      }
      stopm();
      turnright(d90 / 90 * (90 - angle));
    }
    else if(left==1 && up==-1) { // SW
      turnleft(d90 / 90 * (90 + angle));
      delay(30);
      while(dist > 0) {
        forward(150);
        dist -= 10;
      }
      stopm();
      turnright(d90 / 90 * (90 + angle));
      delay(1000);
    }
    else
      stopm();
    x1 = x2;
    y1 = y2;
  }
}

/*
 * Helper function to get the distance between 2 coordinates 
 */
double distanceFun(double a, double b, double c, double d) {
  return sqrt((d-b)*(d-b) + (c-a)*(c-a));
}



// Stop the robot
void stopm() {
  analogWrite(E1, 0);   //PWM Speed Control
  analogWrite(E2, 0);
}

// turn the robot to the left by tme
void turnleft(int tme) {
  digitalWrite(M2, LOW);
  for (int i = 0; i < tme; i++) {
    analogWrite(E1, 200);   //PWM Speed Control
    analogWrite(E2, 200);   //PWM Speed Control
    Serial.println("left");
    delay(30);
  }
  digitalWrite(M2, HIGH);
}

// turn the robot to the right by tme
void turnright(int tme) {
    digitalWrite(M1, LOW);
  for (int i = 0; i < tme; i++) {
    analogWrite(E1, 200);   //PWM Speed Control
    analogWrite(E2, 200);  //PWM Speed Control
        Serial.println("left");
    delay(30);
  }
    digitalWrite(M1, HIGH);
}

// control the speed of robot moving forward
void forward(int spd) {
  analogWrite(E1, 200);
  analogWrite(E2, 200);
  double t = millis();
  while(millis() - t<30){
//    checkWheels(spd);
  }

   if(p1>500 || p2 >500){
    p1 = 0;
    p2 = 0;
  }
//  delay(30);
  
}


