#include <Servo.h>
#include <ShiftedLCD.h>
#include <SPI.h>

//initialize lcd
LiquidCrystal lcd(10);


//server pin
const int E1 = 5;  
const int M1 = 4; 
const int E2 = 6;                      
const int M2 = 7; 



const int opticalPin1 = A2;//left optical sensor
const int opticalPin2 = A3;
const int opticalPin3 = A4;//right optical sensor 
const int opticalPin4 = A5;

//define some global variables here 
const int speed = 100; //define the speed
const int turnTime = 20;//define the turning time 
const int TURNING_CONSTANT = 50;
const int threshold = 300; //the value to check optical sensor is on track or not 
int opticalValue1 = 0;
int opticalValue2 = 0;
int opticalValue3 = 0;
int opticalValue4 = 0;
int ref = 0;

void setup(){
// Set up the serial monitor for debugging
Serial.begin(9600);
lcd.begin(16,2);
lcd.print("hello");

}

void loop(){
  print();
  func2();
}

/*
 * Implement functionality 2: robot follows the given track on the ground.
 * Robot should go forward when encountering the cross-over
 * If there is no track, the robot must stop 
 * List all possibilities of the value read from optical sensor compared to reference value 
 */
 void func2(){
  updateOptical();
  if(checkOnTrack()){
    //middle sensor 2 and 3 is on track,move forward 
    if(opticalValue1 < threshold && opticalValue2 > threshold && opticalValue3 > threshold && opticalValue4 < threshold){
    moveForward(speed);
    }
    //only sensor right 3 is on track, turn right slowly
    else if(opticalValue1 < threshold && opticalValue2 < threshold && opticalValue3 > threshold && opticalValue4 < threshold){
    slowTurnRight(speed,turnTime);
    }
    //only left sensor 2 is on track, turn left slowly
    else if(opticalValue1 < threshold && opticalValue2 > threshold && opticalValue3 < threshold && opticalValue4 < threshold){
    slowTurnLeft(speed,turnTime);
    }
    
    //only right sensor 1,2 is on track, turn left fast
    else if(opticalValue1 > threshold && opticalValue2 > threshold && opticalValue3 < threshold && opticalValue4 < threshold){
    sharpTurnLeft(speed,turnTime);
    }
    //only left sensor 3,4 in on track, turn right fast
    else if(opticalValue1 < threshold && opticalValue2 < threshold && opticalValue3 > threshold && opticalValue4 > threshold){
    fastTurnRight(speed,turnTime);
    }
    //only right sensor 1 is on track, turn left fast
    else if(opticalValue1 > threshold && opticalValue2 < threshold && opticalValue3 < threshold && opticalValue4 < threshold){
    sharpTurnLeft(speed,turnTime);
    }
    //only left sensor 4 in on track, turn right fast
    else if(opticalValue1 < threshold && opticalValue2 < threshold && opticalValue3 < threshold && opticalValue4 > threshold){
    sharpTurnRight(speed,turnTime);
    }
    //1,2,3 on track, turn left sharp(90 degree) by increasing the turning time 
    else if(opticalValue1 > threshold && opticalValue2 > threshold && opticalValue3 > threshold && opticalValue4 < threshold){
    sharpTurnLeft(speed,turnTime + 50);
    }
    //2,3,4 on track, turn right sharp(90 degree) by increasing the turning time 
    else if(opticalValue1 < threshold && opticalValue2 > threshold && opticalValue3 > threshold && opticalValue4 > threshold){
    sharpTurnRight(speed,turnTime + 50);
    }

    else if(opticalValue1 < threshold && opticalValue2 > threshold && opticalValue3 < threshold && opticalValue4 > threshold){
      slowTurnRight(speed,turnTime);
    }
    else if(opticalValue1 > threshold && opticalValue2 < threshold && opticalValue3 > threshold && opticalValue4 < threshold){
      slowTurnLeft(speed,turnTime);
    }
    /*
    //left > right, turn right
    else if(opticalValue1 + opticalValue2 > opticalValue3 + opticalValue4){
    slowTurnRight(80,50);
    }
    //right > left, turn left 
    else if(opticalValue1 + opticalValue2 < opticalValue3 + opticalValue4){
    slowTurnLeft(80,50);
    }
    */
    else
    moveForward(speed);
  }
  else 
    moveStop();//stop the robot if it is not on track 
 }

 /*
  * return true if robot needs to move forward false otherwise 
  */
boolean checkForward(){
  return (opticalValue1 < threshold && opticalValue2 > threshold && opticalValue3 > threshold && opticalValue4 < threshold);
}

/*
 * Slowly turn left if only one sensor is on track
 */
void slowTurnLeft(int speed,int time){
  Serial.println("slow left");
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Turning left");
unsigned long oldTime = millis();
while(millis() - oldTime < time){
  digitalWrite(M1,HIGH);
  digitalWrite(M2,HIGH);
  analogWrite(E1,speed);
  analogWrite(E2,speed - TURNING_CONSTANT);
  updateOptical();

//right sensor value is larger than left sensor, stop turning left
  if(checkForward() && checkOnTrack())
    break;
  if(!checkOnTrack()){
    moveStop();
   break;
   }
  }
}

/*
 * Fast turn left if 2 left sensor on track
 */
void fastTurnLeft(int speed,int time){
Serial.println("fast left");
lcd.clear();
lcd.setCursor(0,0);
lcd.print("Turning left");
unsigned long oldTime = millis();
while(millis() - oldTime < time){
  digitalWrite(M1,HIGH);
  digitalWrite(M2,HIGH);
  analogWrite(E1,speed);
  analogWrite(E2,0);
  
  updateOptical();

//left>right,stop turning left
  if(checkForward() && checkOnTrack())
    break;
  if(!checkOnTrack()){
    moveStop();
   break;
   }
  }
}

/*
 * Slowly turn right if only one sensor is on track
 */
void slowTurnRight(int speed,int time){
Serial.println("slow right");
lcd.clear();
lcd.setCursor(0,0);
lcd.print("Turning right");
unsigned long oldTime = millis();
while(millis() - oldTime < time){
  digitalWrite(M1,HIGH);
  digitalWrite(M2,HIGH);
  analogWrite(E1,speed - TURNING_CONSTANT);
  analogWrite(E2,speed);
  
  updateOptical();

//left <= right, stop turning right
  if(checkForward() && checkOnTrack())
    break;
  if(!checkOnTrack()){
    moveStop();
    break;
   }
  }
}

/*
 * Fast turn right if 2 right sensor on track
 */
void fastTurnRight(int speed,int time){
Serial.println("fast right");
lcd.clear();
lcd.setCursor(0,0);
lcd.print("Turning right");
unsigned long oldTime = millis();
while(millis() - oldTime < time){
  digitalWrite(M1,HIGH);
  digitalWrite(M2,HIGH);
  analogWrite(E1,0);
  analogWrite(E2,speed);
  
  updateOptical();
  
  if(checkForward() && checkOnTrack()) //stop turning if robot needs to move forward
    break;
  if(!checkOnTrack()){
    moveStop();
    break;
   }
  }
}

void sharpTurnLeft(int speed, int time){
Serial.println("sharp_left");
lcd.clear();
lcd.setCursor(0,0);
lcd.print("Turning left");
unsigned long oldTime = millis();
while(millis() - oldTime < time){
  digitalWrite(M1,HIGH);
  digitalWrite(M2,LOW);
  analogWrite(E1,speed);
  analogWrite(E2,speed);
  
  updateOptical();

  if(checkForward() && checkOnTrack())
  break;
  }
}

void sharpTurnRight(int speed,int time){
unsigned long oldTime = millis();
Serial.println("sharp_right");
lcd.clear();
lcd.setCursor(0,0);
lcd.print("Turning right");
while(millis() - oldTime < time){
  digitalWrite(M1,LOW);
  digitalWrite(M2,HIGH);
  analogWrite(E1,speed);
  analogWrite(E2,speed);
  updateOptical();

  if(checkForward() && checkOnTrack())
  break;
  }
}

/*
 * Robot goes a straight line 
 */
void moveForward(int speed){
  Serial.println("forward");
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("MOVING FORWARD");
  digitalWrite(M1,HIGH);
  digitalWrite(M2,HIGH);
  analogWrite(E1,speed);
  analogWrite(E2,speed);
}

/*
 * Stop the robot
 */
void moveStop(){
  Serial.println("stop");
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("STOP");
  digitalWrite(M1,HIGH);
  digitalWrite(M2,HIGH);
  analogWrite(E1,0);
  analogWrite(E2,0);
}

/*
 * Check if robot is on track 
 * @return true if robot is on track, false otherwise 
 */
boolean checkOnTrack(){
  if(opticalValue1 < threshold && opticalValue2 < threshold && opticalValue3 < threshold && opticalValue4 < threshold)
  return false;
  else 
  return true;
}
 
/*
 * Get the average reference optical value of the ground
 */
void prepareFollowing(){
  long sum = 0;
  for(int count = 0; count < 10; count++){
      sum += analogRead(A2);
      delay(2);
      sum += analogRead(A3);
      delay(2);
      sum += analogRead(A4);
      delay(2);
      sum += analogRead(A5);
      delay(2);
  }
    ref = sum/40; //set the reference value of optical sensor 
}


/*
 * Update optical sensor value at 4 sensors together 
 */
void updateOptical(){
  opticalValue1 = analogRead(opticalPin1);
  opticalValue2 = analogRead(opticalPin2);
  opticalValue3 = analogRead(opticalPin3);
  opticalValue4 = analogRead(opticalPin4);
}

//test optical sensor,test only
void print(){
Serial.println(analogRead(A2));
Serial.println(analogRead(A3));
Serial.println(analogRead(A4));
Serial.println(analogRead(A5));
//delay(2000);
}






