#include <Servo.h>

//server pin
const int E1 = 5;  
const int M1 = 4; 
const int E2 = 6;                      
const int M2 = 7;   

const int opticalPin1 = A2;//left optical sensor
const int opticalPin2 = A3;
const int opticalPin3 = A4;//right optical sensor 
const int opticalPin4 = A5;

//define some global variables here 
const int TURNING_CONSTANT = 40;
int opticalValue1 = 0;
int opticalValue2 = 0;
int opticalValue3 = 0;
int opticalValue4 = 0;
int ref = 0;

void setup(){
// Set up the serial monitor for debugging
Serial.begin(9600);
prepareFollowing();
Serial.print("reference value is" + ref);
}

void loop(){
  func2();
}

/*
 * Implement functionality 2: robot follows the given track on the ground.
 * Robot should go forward when encountering the cross-over
 * If there is no track, the robot must stop 
 */
 void func2(){
  updateOptical();
  if(checkOnTrack()){
    //middle sensor 2 and 3 is on track,move forward 
    if(opticalValue1 > ref - 100 && opticalValue2 < ref - 100 && opticalValue3 < ref - 200 && opticalValue4 > ref - 100){
    moveForward(75);
    }
    //only right sensor 1 is on track, turn right fast
    else if(opticalValue1 < ref - 100 && opticalValue2 > ref - 100 && opticalValue3 > ref - 100 && opticalValue4 > ref - 100){
    sharpTurnRight(75,50);
    }
    //only left sensor 4 in on track, turn left fast
    else if(opticalValue1 > ref - 100 && opticalValue2 > ref - 100 && opticalValue3 > ref - 100 && opticalValue4 < ref - 200){
    sharpTurnLeft(75,50);
    }
    //only sensor right 3 is on track, turn right slowly
    else if(opticalValue1 > ref - 100 && opticalValue2 > ref - 100 && opticalValue3 < ref - 200 && opticalValue4 > ref - 100){
    slowTurnRight(75,50);
    }
    //only left sensor 2 is on track, turn left slowly
    else if(opticalValue1 > ref - 100 && opticalValue2 < ref - 200 && opticalValue3 > ref - 100 && opticalValue4 > ref - 100){
    slowTurnLeft(75,50);
    }
    //only right sensor 1,2 is on track, turn right fast
    else if(opticalValue1 < ref - 100 && opticalValue2 < ref - 200 && opticalValue3 > ref - 100 && opticalValue4 > ref - 100){
    fastTurnRight(75,50);
    }
    //only left sensor 3,4 in on track, turn left fast
    else if(opticalValue1 > ref - 100 && opticalValue2 > ref - 100 && opticalValue3 < ref - 200 && opticalValue4 < ref - 200){
    fastTurnLeft(75,50);
    }
    else
    moveForward(75);
  }
  else 
    moveStop();//stop the robot if it is not on track 
 }

/*
 * Slowly turn left if only one sensor is on track
 */
void slowTurnLeft(int speed,int time){
  Serial.println("slow left");
unsigned long oldTime = millis();
while(millis() - oldTime < time){
  digitalWrite(M1,HIGH);
  digitalWrite(M2,HIGH);
  analogWrite(E1,speed);
  analogWrite(E2,speed - TURNING_CONSTANT);
  updateOptical();

//right sensor value is larger than left sensor, stop turning left
  if(((opticalValue1 + opticalValue2)/2 >= (opticalValue3 + opticalValue4)/2) && checkOnTrack())
    break;
  if(!checkOnTrack()){
    moveStop();
   break;
   }
  }
}

/*
 * Fast turn left if 2 left sensor on track
 */
void fastTurnLeft(int speed,int time){
  Serial.println("fast left");
unsigned long oldTime = millis();
while(millis() - oldTime < time){
  digitalWrite(M1,HIGH);
  digitalWrite(M2,HIGH);
  analogWrite(E1,speed);
  analogWrite(E2,0);
  
  updateOptical();

//left>right,stop turning left
  if(((opticalValue1 + opticalValue2)/2 >= (opticalValue3 + opticalValue4)/2) && checkOnTrack())
    break;
  if(!checkOnTrack()){
    moveStop();
   break;
   }
  }
}

/*
 * Slowly turn right if only one sensor is on track
 */
void slowTurnRight(int speed,int time){
  Serial.println("slow right");
unsigned long oldTime = millis();
while(millis() - oldTime < time){
  digitalWrite(M1,HIGH);
  digitalWrite(M2,HIGH);
  analogWrite(E1,speed - TURNING_CONSTANT);
  analogWrite(E2,speed);
  
  updateOptical();

//left <= right, stop turning right
  if(((opticalValue1 + opticalValue2)/2 <= (opticalValue3 + opticalValue4)/2) && checkOnTrack())
    break;
  if(!checkOnTrack()){
    moveStop();
    break;
   }
  }
}

/*
 * Fast turn right if 2 right sensor on track
 */
void fastTurnRight(int speed,int time){
  Serial.println("fast right");
unsigned long oldTime = millis();
while(millis() - oldTime < time){
  digitalWrite(M1,HIGH);
  digitalWrite(M2,HIGH);
  analogWrite(E1,0);
  analogWrite(E2,speed);

  updateOptical();
  
//left <= right, stop turning right
  if(((opticalValue1 + opticalValue2)/2 <= (opticalValue3 + opticalValue4)/2) && checkOnTrack())
    break;
  if(!checkOnTrack()){
    moveStop();
    break;
   }
  }
}

void sharpTurnLeft(int speed, int time){
    Serial.println("sharp_left");
  unsigned long oldTime = millis();
while(millis() - oldTime < time){
  digitalWrite(M1,HIGH);
  digitalWrite(M2,LOW);
  analogWrite(E1,speed);
  analogWrite(E2,speed);

  updateOptical();

  if(((opticalValue1 + opticalValue2)/2 >= (opticalValue3 + opticalValue4)/2) && checkOnTrack())
  break;
  }
}

void sharpTurnRight(int speed,int time){
unsigned long oldTime = millis();
Serial.println("sharp_right");
while(millis() - oldTime < time){
  digitalWrite(M1,LOW);
  digitalWrite(M2,HIGH);
  analogWrite(E1,speed);
  analogWrite(E2,speed);
  updateOptical();

  if(((opticalValue1 + opticalValue2)/2 <= (opticalValue3 + opticalValue4)/2) && checkOnTrack())
  break;
  }
}

  /*
 * Robot goes a straight line 
 */
void moveForward(int speed){
  digitalWrite(M1,HIGH);
  digitalWrite(M2,HIGH);
  analogWrite(E1,speed);
  analogWrite(E2,speed);
}

/*
 * Stop the robot
 */
void moveStop(){
  digitalWrite(M1,HIGH);
  digitalWrite(M2,HIGH);
  analogWrite(E1,0);
  analogWrite(E2,0);
}

/*
 * Check if robot is on track 
 * @return true if robot is on track, false otherwise 
 */
boolean checkOnTrack(){
  if(abs(opticalValue1 - ref) < 50 && abs(opticalValue2 - ref) < 50 && abs(opticalValue3 - ref) < 50 && abs(opticalValue4 - ref) < 50)
  return false;
  else 
  return true;
}
 
/*
 * Get the average reference optical value of the ground
 */
void prepareFollowing(){
  long sum = 0;
  for(int count = 0; count < 10; count++){
      sum += analogRead(A2);
      delay(2);
      sum += analogRead(A3);
      delay(2);
      sum += analogRead(A4);
      delay(2);
      sum += analogRead(A5);
      delay(2);
  }
    ref = sum/40; //set the reference value of optical sensor 
}

/*
 * Update optical sensor value at 4 sensors together 
 */
void updateOptical(){
  opticalValue1 = analogRead(opticalPin1);
  opticalValue2 = analogRead(opticalPin2);
  opticalValue3 = analogRead(opticalPin3);
  opticalValue4 = analogRead(opticalPin4);
  print();
}

//test optical sensor,test only
void print(){
Serial.println(analogRead(A2));
Serial.println(analogRead(A3));
Serial.println(analogRead(A4));
Serial.println(analogRead(A5));
}


