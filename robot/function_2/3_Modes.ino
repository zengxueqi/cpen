#include <SoftwareSerial.h>
SoftwareSerial BT(9, 10);
int switch_pin = 0;
int flag = 1;
char instr = '1';

void setup() {
  

}

void loop() {
 //use switch to control the robot 
if(digitalRead(switch_pin) == HIGH){
  switchMode(flag);
  int flag1 = changeFlag(instr);

  if(flag1 != 0 && flag1 != flag){
  Serial.println("change mode,stop");
  //stop 1 second when successfully change the mode 
  delay(1000);
    }
  }
}

/*
 * Functionality 1:
 * Robot walk at maximum speed until it detects an object in fornt of it
 * When robot detects an object at 50cm , it should gradually slow down 
 * Robot use servo motor to pick the directionthat has the longest free space
 */
void mode1(){
  while(flag == 1){
    
    readBT();
    if(flag != 1 || instr != '1' || digitalRead(switch_pin) == LOW)
    break;
  }
}

/*
 * Functionality 2: robot follows the track
 * Robot should go forward when encountering the cross-over
 * If there is no track, the robot must stop
 * Robot should work on any shape of track 
 */
void mode2(){
  while(flag == 2){
    
    readBT();
    if(flag != 2 || instr != '2' || digitalRead(switch_pin) == LOW)
    break;
  }
}

/*
 * Additional Functionality: use blue tooth to control the robot
 * 
 */
void mode3(){
  while(flag == 3){
    
    readBT();
    if(flag != 3 || instr != '3' || digitalRead(switch_pin) == LOW)
    break;
  }
}

/*
 * Read input from blue tooth
 */
void readBT(){
  if(BT.available()){
    String s = BT.readString();
    instr = s.charAt(0);
    Serial.println("update key");
  }
}

/*
 * Change the flag when input read from blue tooth changes
 */
int changeFlag(char input){
  if(input == '1'){
  flag = 1;
  return 1;
  }
  else if(input == '2'){
  flag = 2;
  return 2;
  }
  else if(input == '3'){
  flag = 3;
  return 3;
  }
  return 0;
}

/*
 * Switch to different mode when flag value changes 
 */
void switchMode(int flag){
  switch(flag){
    case1:
    mode1();
    break;
    case2:
    mode2();
    break;
    case 3:
    mode3();
    default:
    break;
  }
}


