#include<Servo.h>
Servo servo;

//robot motor pin
const int motorRight = 5;
const int motorLeft = 6;
const int M1 = 4;
const int M2 = 7;

//sonar sensor pins
const int echo = 11;
const int trig = 12;
const int servo_pin = 13;

//4 optical reflective sensors
const int pin1 = A1;
const int pin2 = A2;
const int pin3 = A3;
const int pin4 = A4;

const int moving_speed = 200;
const int turning_speed = 120;
const int turning_speed_max = 225;
const int turningTime = 500;

//optical sensor values
int opVal1;
int opVal2;
int opVal3;
int opVal4;

int sensorVal = 0;      //avg infrared color value
int infraValMin = 1023; //infrared color
int infraValMax = 0;    //infrared color


void setup(){
  pinMode(M1,OUTPUT);
  pinMode(M2,OUTPUT);
  pinMode(echo,INPUT);
  pinMode(trig,OUTPUT);
  pinMode(servo_pin,OUTPUT);

  //get the value of the optical sensors
  for(int i = 14; i < 20; i++){
    pinMode(i,INPUT);
  }
}

void loop(){

  robotmove();
  
}

void robotmove(){
  int turningTime = 500;
  setupOS();
  
  scan();
  delay(1000);

  //check whether the robot is on track
  moveForward(moving_speed);
  delay(1);

  if(((opVal1 + opVal2)/2) > ((opVal3 + opVal4)/2 + 200)){
    turnRight(turning_speed, turningTime);
  }

  else if(((opVal1 + opVal2)/2) < ((opVal3 + opVal4)/2 - 200)){
    turnLeft(turning_speed, turningTime);
  }

  else if(opVal1 > sensorVal && opVal4 > sensorVal){
    moveStop();
  }
 
}


void scan(){
  opVal1 = analogRead(pin1);
  opVal2 = analogRead(pin2);
  opVal3 = analogRead(pin3);
  opVal4 = analogRead(pin4);
}

void moveForward(long Speed){
  digitalWrite(M1,HIGH);
  digitalWrite(M2,HIGH);
  analogWrite(motorLeft,Speed);
  analogWrite(motorRight,Speed);
}

void moveStop(){
  digitalWrite(M1,HIGH);
  digitalWrite(M2,HIGH);
  analogWrite(motorLeft,0);
  analogWrite(motorRight,0);
}

void turnRight(int Speed, long Time){
  //get the previous time
  unsigned long preTime;
  preTime = millis();

  
  while (millis() - preTime <= Time){
    analogWrite(M1,HIGH);
    analogWrite(M2,HIGH);
    digitalWrite(motorLeft,Speed);
    digitalWrite(motorRight,0);    
  } 
}

void turnLeft(int Speed, long Time){
  //get the previous time
  unsigned long preTime;
  preTime = millis();

  while(millis() - preTime <= Time){
    analogWrite(M1,HIGH);
    analogWrite(M2,HIGH);
    digitalWrite(motorLeft,0);
    digitalWrite(motorRight,Speed);
  }
}

void setupOS(){

  int tempVal = 0;
  
  for(int i = 14; i < 20; i++){
    tempVal = analogRead(i);
  }

  if(tempVal > infraValMax){
    tempVal = infraValMax;
  }

  infraValMin = tempVal;
  //set temp
  if(tempVal < infraValMin){
    infraValMin = tempVal;
  }

  sensorVal = (infraValMax + infraValMin)/2;
}


