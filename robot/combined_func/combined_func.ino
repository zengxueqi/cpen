#include <Servo.h>
#include <SoftwareSerial.h>
#include <math.h>
#include <ShiftedLCD.h>
#include <SPI.h>

//initialize lcd
LiquidCrystal lcd(10);
//============= PART 1 ===============
const int switchPin1 = 12;

const int E1 = 5;
const int M1 = 4;
const int E2 = 6;
const int M2 = 7;

const int echo = 2;
const int trig = 3;                                                                                                                                                                                                                                                                                                      
const int servo = A0;
const int tempSensor = A1;
const int hallPin1 = 8; // the number of the hall effect sensor pins //right
const int hallPin2 = 9;//left

//define some global variables here 
int pretime=0;
int p1 = 0;
int p2 =0;
int p1s=1;
int p2s = 1;
Servo myservo;
unsigned long temp;
unsigned long duration;
unsigned long distance;
int vt=0;

//============= PART 2 ================
const int opticalPin1 = A2;//left optical sensor
const int opticalPin2 = A3;
const int opticalPin3 = A4;//right optical sensor 
const int opticalPin4 = A5;

//define some global variables here 
const int speed = 200; //define the speed
const int turnTime = 20;//define the turning time 
const int TURNING_CONSTANT = 50;
const int threshold = 300; //the value to check optical sensor is on track or not 
int opticalValue1 = 0;
int opticalValue2 = 0;
int opticalValue3 = 0;
int opticalValue4 = 0;

//============= PART 4 ================
int numArgs = 0;
int data[32]; //data array containing x,y, coordinates for points
bool haveParsed = false; //condition whether things have been parsed
SoftwareSerial mySerial(0, 1); // RX, TX



void setup() {
  pinMode(M1, OUTPUT);   
  // set pin input mode or output mode
  pinMode(M2, OUTPUT);
  Serial.begin(9600);
  mySerial.begin(38400);
  pinMode(echo,INPUT);
  pinMode(trig,OUTPUT);
  pinMode(tempSensor, INPUT);
  pinMode(hallPin1, INPUT);
  pinMode(hallPin2, INPUT);
  pinMode(switchPin1, INPUT_PULLUP);
  //pinMode(switchPin2, INPUT_PULLUP);
  lcd.begin(16,2);
  myservo.attach(servo); // set servo
}

void loop() {
  //ues switch to change modes 
  Serial.println(digitalRead(switchPin1));
  if(digitalRead(switchPin1) == HIGH)
  func1();
  else if(digitalRead(switchPin1) == LOW)
  func2();

}

//============= PART 1 ===============
/*
 * Functionality 1:
 * Robot walk at maximum speed until it detects an object in fornt of it
 * When robot detects an object at 50cm , it should gradually slow down 
 * Robot use servo motor to pick the directionthat has the longest free space
 */
void func1(){
  digitalWrite(M1, HIGH);
  digitalWrite(M2, HIGH);

  while (dist() >= 10) {
    int spd = 255;
    if (dist() <=100 && dist() >= 50)
      spd = 200;
    else if (dist() < 50 && dist() >= 20)
      spd = 150;
    else if (dist() < 20)
      spd = 90;
    forward(spd);
    delay(100);
//    Serial.println(dist());
    if (digitalRead(switchPin1)==LOW) return;
  }

  stopm();

  int left = sweep(10, 0);
  int right = sweep(10, 1);
  Serial.println("left ");
  Serial.println(left);
  Serial.println("right ");
  Serial.println(right);

  delay(500);
  Serial.println("stop");
  if (left > right) {
    turnleft(18);
  }
  else {
    turnright(18);
  }
}


/* 
 * Stop the robot
 */
void stopm() {
  analogWrite(E1, 0);   //PWM Speed Control
  analogWrite(E2, 0);
}

/* 
 * Turn the robot to the left by tme
 */
void turnleft(int tme) {
  digitalWrite(M2, LOW);
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Turning Left");
  for (int i = 0; i < tme; i++) {
    analogWrite(E1, 150);   //PWM Speed Control
    analogWrite(E2, 150);   //PWM Speed Control
    Serial.println("left");
    delay(30);
  }
  digitalWrite(M2, HIGH);
}

/* 
 * Turn the robot to the right by tme
 */
void turnright(int tme) {
    digitalWrite(M1, LOW);
      lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Turning Right");
  for (int i = 0; i < tme; i++) {
    analogWrite(E1, 150);   //PWM Speed Control
    analogWrite(E2, 150);  //PWM Speed Control
        Serial.println("left");
    delay(30);
  }
    digitalWrite(M1, HIGH);
}

/* 
 * control the speed of robot moving forward 
 */
void forward(int spd) {
//    lcd.clear();
lcd.print("                   ");
  lcd.setCursor(0,0);
  lcd.print("Forward");
  if(p1==0) pretime = millis();
  analogWrite(E1, spd);
  analogWrite(E2, spd);
  double t = millis();
  while(millis() - t<30){
    checkWheels(spd);
  }
   if(p1>5 && p2 >5){
    lcd.setCursor(0,0);
        lcd.print("               ");
    velocity();
    p1 -=5;
    p2 -=4;
//    if(p1>p2) p1++;
//    if(p2>p1) p2++;
    pretime = millis();
  }

//  if(millis()-vt>1000){
//
//    vt = millis();
//  }
}

double velocity(){
   double dur = (millis()-pretime)/1000.0;//to sec
   Serial.println(p1);

   double velo = p1/dur/5 * 3.1415926 * 6;
   Serial.println("dur: " + String(dur) + " p1: " + String(p1) +"velocity: " + String(velo));   
   lcd.setCursor(0,1);
   lcd.print("Velocity: "+String(velo));
   
   return p1/dur/5 * 3.1415926 * 6; //cm/s 
}


/*
 * hall effect adjusting to straight line
 */
void checkWheels(int spd){
  if((!digitalRead(hallPin1))&& p1s==1){
    p1++;
//    Serial.println(digitalRead(hallPin1));
    p1s=0;
  }
//  else if(p1s==0 &&(!digitalRead(hallPin1)));
  else p1s=1;
  if((!digitalRead(hallPin2))&& p2s==1){
    p2++;
    p2s=0;
  } 
//  else if(p2s==0&&(!digitalRead(hallPin1)));
  else p2s = 1;
  if(p1>p2){
    analogWrite(E1,0);
    while(digitalRead(hallPin2));
    p2++;
    p2s=0;
        analogWrite(E1,spd);
  }
  if(p2>p1){
        analogWrite(E2,0);
    while(digitalRead(hallPin1));
    p1++;
    p1s=0;
        analogWrite(E2,spd);
  }
  Serial.println("p1: " + String(p1));
  Serial.println("p2: " + String(p2));
}


/* 
 * Measure the distance of an object in front of the sensor
 * Return type, unsigned long
 */
unsigned long dist() {
  temp = 5.0 * analogRead(tempSensor) * 100.0 / 1024; // use temperature sensor to get more accurate measurement
//    Serial.println("temp: "+String(temp)); //commented out for twitter
  //  Serial.print(String(temp) + ",");

  //code for triggerring the HC-SR04 sensor
  digitalWrite(trig, HIGH);
  delayMicroseconds(10);
  digitalWrite(trig, LOW);

  //calculate the duration
  duration = pulseIn(echo, HIGH);

  //calculation of the distance
  double speedOfSound = 331.5 + (0.6 * temp);


  //unit conversion
  speedOfSound = speedOfSound * 0.0001;
  distance = speedOfSound * duration / 2;

  return distance;
}

/*
 * Control the turning degree of servo
 */
unsigned long sweep(int mode, int lr) {
  int pos;
  unsigned long dis;
  //sweep function that turns the servo motor back and forth
  if (lr == 0) {
    for (pos = 90; pos <= 170; pos += 1) { // goes from 0 degrees to 180 degrees
      // in steps of 1 degree
      myservo.write(pos);   // tell servo to go to position in variable 'pos'
            delay(mode);                       // waits 15ms for the servo to reach the position
    }
    dis = dist();
    Serial.println(dis);
    for (pos = 170; pos >= 91; pos -= 1) { // goes from 180 degrees to 0 degrees
      myservo.write(pos);       // tell servo to go to position in variable 'pos'
            delay(mode);                       // waits 15ms for the servo to reach the position
    }
    return dis;
  }
  else {
    for (pos = 90; pos >= 11; pos -= 1) { // goes from 180 degrees to 0 degrees
      myservo.write(pos);       // tell servo to go to position in variable 'pos'
            delay(mode);                       // waits 15ms for the servo to reach the position
    }
    dis = dist();
    Serial.println(dis);
    for (pos = 10; pos <= 90; pos += 1) { // goes from 0 degrees to 180 degrees
      // in steps of 1 degree
      myservo.write(pos);   // tell servo to go to position in variable 'pos'
            delay(mode);                       // waits 15ms for the servo to reach the position
    }

    return dis;
  }


}

//============= PART 2 ===============
/*
 * Functionality 2: robot follows the track
 * Robot should go forward when encountering the cross-over
 * If there is no track, the robot must stop
 * Robot should work on any shape of track 
 */
void func2(){
  updateOptical();
  if(checkOnTrack()){
    //middle sensor 2 and 3 is on track,move forward 
    if(opticalValue1 < threshold && opticalValue2 > threshold && opticalValue3 > threshold && opticalValue4 < threshold){
    moveForward(speed);
    }
    //only sensor right 3 is on track, turn right slowly
    else if(opticalValue1 < threshold && opticalValue2 < threshold && opticalValue3 > threshold && opticalValue4 < threshold){
    slowTurnRight(speed,turnTime);
    }
    //only left sensor 2 is on track, turn left slowly
    else if(opticalValue1 < threshold && opticalValue2 > threshold && opticalValue3 < threshold && opticalValue4 < threshold){
    slowTurnLeft(speed,turnTime);
    }
    
    //only right sensor 1,2 is on track, turn left fast
    else if(opticalValue1 > threshold && opticalValue2 > threshold && opticalValue3 < threshold && opticalValue4 < threshold){
    sharpTurnLeft(speed,turnTime);
    }
    //only left sensor 3,4 in on track, turn right fast
    else if(opticalValue1 < threshold && opticalValue2 < threshold && opticalValue3 > threshold && opticalValue4 > threshold){
    fastTurnRight(speed,turnTime);
    }
    //only right sensor 1 is on track, turn left fast
    else if(opticalValue1 > threshold && opticalValue2 < threshold && opticalValue3 < threshold && opticalValue4 < threshold){
    sharpTurnLeft(speed,turnTime);
    }
    //only left sensor 4 in on track, turn right fast
    else if(opticalValue1 < threshold && opticalValue2 < threshold && opticalValue3 < threshold && opticalValue4 > threshold){
    sharpTurnRight(speed,turnTime);
    }
    //1,2,3 on track, turn left sharp(90 degree) by increasing the turning time 
    else if(opticalValue1 > threshold && opticalValue2 > threshold && opticalValue3 > threshold && opticalValue4 < threshold){
    sharpTurnLeft(speed,turnTime + 50);
    }
    //2,3,4 on track, turn right sharp(90 degree) by increasing the turning time 
    else if(opticalValue1 < threshold && opticalValue2 > threshold && opticalValue3 > threshold && opticalValue4 > threshold){
    sharpTurnRight(speed,turnTime + 50);
    }

    else if(opticalValue1 < threshold && opticalValue2 > threshold && opticalValue3 < threshold && opticalValue4 > threshold){
      slowTurnRight(speed,turnTime);
    }
    else if(opticalValue1 > threshold && opticalValue2 < threshold && opticalValue3 > threshold && opticalValue4 < threshold){
      slowTurnLeft(speed,turnTime);
    }
    else
    moveForward(speed);
  }
  else 
    moveStop();//stop the robot if it is not on track 
}

//============= EXTRA FUNCTIONALITY1 ===============
/*
 * Additional Functionality: use blue tooth to control the robot
 * 
 */
//void func3(){
//    if (mySerial.available()) {
//    char control = mySerial.read();
//    //forward
//    while (control == '1') {
//      digitalWrite(M1, HIGH);
//      digitalWrite(M2, HIGH);
//      Serial.println("forward");
//
//
//      forward(200);
//      control = mySerial.read();
//    }
//
//    //backward
//    while (control == '2') {
//      digitalWrite(M1, LOW);
//      digitalWrite(M2, LOW);
//
//      Serial.println("forward");
//
//      forward(200);
//      control = mySerial.read();
//    }
//
//    //left
//    while (control == '3') {
//      digitalWrite(M1, HIGH);
//      digitalWrite(M2, HIGH);
//
//      Serial.println("forward");
//
//      turnleft(1);
//      control = mySerial.read();
//    }
//
//    //right
//    while (control == '4') {
//      digitalWrite(M1, HIGH);
//      digitalWrite(M2, HIGH);
//      Serial.println("forward");
//
//      turnright(1);
//      control = mySerial.read();
//    }
//
//    //stop
//    while (control == '0') {
//      digitalWrite(M1, HIGH);
//      digitalWrite(M2, HIGH);
//      Serial.println("forward");
//
//      stopm();
//      control = mySerial.read();
//    }
//
//  }
//}

//============= EXTRA FUNCTIONALITY2 ===============
/*
 * Use blue tooth to draw points on the ground
 * Robot can follow the track created by bluetooth
 */
//void func4(){
//  if(!haveParsed) { //wait for user to input data, then parse it into data array
//   parse();
//  }
//  else {
//    //using data[] and numArgs
//    part4(); //using the data array coordinates, translate to robot movement those points
//    haveParsed = false;
//    numArgs = 0;
//}
//
//  
//}
/*
 * Converts bluetooth data from serial into int array of coordinates
 */
//void parse() { 
//  String str;
//if(mySerial.available()) {
//
//str = mySerial.readString();// read the incoming data as string
//
////Serial.println(str); //DEBUG
//
//int index = str.indexOf(","); //want to split string by comma
//String val;
//char charBuffer[3]; //canvas range of 0 to ~300, so only need 3 char positions
//int beginIndex = 0;
//
//while (index != -1) //keep check for commas until end reached
//{
//    val = str.substring(beginIndex, index);
//    val.toCharArray(charBuffer, 16);
//    data[numArgs++] = atoi(charBuffer); //convert string into int and store into data array
//    beginIndex = index + 1;
//    index = str.indexOf(",", beginIndex);
//}
//
////Serial.println(numArgs); //DEBUG
//for(int i = 0; i <numArgs; i++) {
//  Serial.println(data[i]);
//
//  haveParsed = true; //set condition to stop parse
//}
//  
//}
//}
//
///*
// * Helper function for func4 
// * Let robot follow the track drawn by bluetooth 
// */
// void part4() {
//  int count = 2;
//  int d90= 25;
//  double x1 = data[0];
//  double y1 = data[1];
//  double x2;
//  double y2;
//  double dist;
//  double angle;
//  int up;
//  int left;
//  for(count = 2; count< numArgs; count+=2) {
//    x2 = data[count];
//    y2 = data[count+1];  
//
//    if(y2-y1 < 0)
//      up = 1;
//    else if(y2-y1==0)
//      up = 0;
//    else
//      up = -1;
//      
//    if(x2-x1 < 0)
//      left = 1;
//    else if(x2-x1 == 0)
//      left = 0;
//    else
//      left = -1;
//
//    angle = fabs(180 / 3.14159265 * atan((y2-y1)/(x2-x1))); // in degree
//    dist = distanceFun(x1,y1,x2,y2);
//    Serial.println(dist);
//    Serial.println(left);
//    Serial.println(up);
//    if(dist ==  0){
//      while(true)
//      stopm();
//    }
//
//    if(left==0 && up==1) { // front
//      while(dist > 0) {
//        forward(150);
//        dist -= 10;
//      }
//      stopm();
//    }
//
//    else if(left==0 && up==-1) { // back
//      turnleft(d90*2);
//      while(dist > 0) {
//        forward(150);
//        dist -= 10;
//      }
//      stopm();
//      turnright(d90*2);
//    }
//    else if(up==0 && left==1) { // left
//      turnleft(d90);
//      while(dist > 0) {
//        forward(150);
//        dist -= 10;
//      }
//      stopm();
//      turnright(d90);
//    }
//    else if(up==0 && left==-1) { // right
//      turnright(d90);
//      while(dist > 0) {
//        forward(150);
//        dist -= 10;
//      }
//      stopm();
//      turnleft(d90);
//    }
//    else if(up==1 && left==-1) { // NE
//      turnright(d90 / 90 * (90 - angle));
//      while(dist > 0) {
//        forward(150);
//        dist -= 10;
//      }
//      stopm();
//      turnleft(d90 / 90 * (90 - angle));
//    }
//    else if(left==-1 && up==-1) { // SE
//
//      turnright(d90 / 90 * (90 + angle));
//      while(dist > 0) {
//        forward(150);
//        dist -= 10;
//      }
//      stopm();
//      turnleft(d90 / 90 * (90 + angle));
//    }
//    else if(left==1 && up==1) { // NW
//      turnleft(d90 / 90 * (90 - angle));
//      while(dist > 0) {
//        forward(150);
//        dist -= 10;
//      }
//      stopm();
//      turnright(d90 / 90 * (90 - angle));
//    }
//    else if(left==1 && up==-1) { // SW
//      turnleft(d90 / 90 * (90 + angle));
//      delay(30);
//      while(dist > 0) {
//        forward(150);
//        dist -= 10;
//      }
//      stopm();
//      turnright(d90 / 90 * (90 + angle));
//      delay(1000);
//    }
//    else
//      stopm();
//    x1 = x2;
//    y1 = y2;
//  }
//}

/*
 * Helper function to get the distance between 2 coordinates 
 */
double distanceFun(double a, double b, double c, double d) {
  return sqrt((d-b)*(d-b) + (c-a)*(c-a));
}

/*
  * return true if robot needs to move forward false otherwise 
  */
boolean checkForward(){
  return (opticalValue1 < threshold && opticalValue2 > threshold && opticalValue3 > threshold && opticalValue4 < threshold);
}

/*
 * Slowly turn left if only one sensor is on track
 */
void slowTurnLeft(int speed,int time){
  Serial.println("slow left");
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("TURNING LEFT");
unsigned long oldTime = millis();
while(millis() - oldTime < time){
  digitalWrite(M1,HIGH);
  digitalWrite(M2,HIGH);
  analogWrite(E1,speed);
  analogWrite(E2,speed - TURNING_CONSTANT);
  updateOptical(); //update the value read from optical sensor 

  if(checkForward() && checkOnTrack())//if robot move on the middle of track,stop turning 
    break;
  if(!checkOnTrack()){//if robot is not on track, stop the robot 
    moveStop();
   break;
   }
  }
}

/*
 * Fast turn left if 2 left sensor on track
 */
void fastTurnLeft(int speed,int time){
Serial.println("fast left");
lcd.clear();
lcd.setCursor(0,0);
lcd.print("TURNING LEFT");
unsigned long oldTime = millis();
while(millis() - oldTime < time){
  digitalWrite(M1,HIGH);
  digitalWrite(M2,HIGH);
  analogWrite(E1,speed);
  analogWrite(E2,0);
  updateOptical();

  if(checkForward() && checkOnTrack())
    break;
  if(!checkOnTrack()){
    moveStop();
   break;
   }
  }
}

/*
 * Slowly turn right if only one sensor is on track
 */
void slowTurnRight(int speed,int time){
Serial.println("slow right");
lcd.clear();
lcd.setCursor(0,0);
lcd.print("TURNING RIGHT");
unsigned long oldTime = millis();
while(millis() - oldTime < time){
  digitalWrite(M1,HIGH);
  digitalWrite(M2,HIGH);
  analogWrite(E1,speed - TURNING_CONSTANT);
  analogWrite(E2,speed);
  
  updateOptical();

  if(checkForward() && checkOnTrack())
    break;
  if(!checkOnTrack()){
    moveStop();
    break;
   }
  }
}

/*
 * Fast turn right if 2 right sensor on track
 */
void fastTurnRight(int speed,int time){
Serial.println("fast right");
lcd.clear();
lcd.setCursor(0,0);
lcd.print("TURNING RIGHT");
unsigned long oldTime = millis();
while(millis() - oldTime < time){
  digitalWrite(M1,HIGH);
  digitalWrite(M2,HIGH);
  analogWrite(E1,0);
  analogWrite(E2,speed);

  updateOptical();
  
  if(checkForward() && checkOnTrack()) //stop turning if robot needs to move forward
    break;
  if(!checkOnTrack()){
    moveStop();
    break;
   }
  }
}

/*
 * Robot truns left sharply
 */
void sharpTurnLeft(int speed, int time){
Serial.println("sharp_left");
lcd.clear();
lcd.setCursor(0,0);
lcd.print("TURNING LEFT");
unsigned long oldTime = millis();
while(millis() - oldTime < time){
  digitalWrite(M1,HIGH);
  digitalWrite(M2,LOW);
  analogWrite(E1,speed);
  analogWrite(E2,speed);

  updateOptical();

  if(checkForward() && checkOnTrack())
  break;
  }
}

/*
 * Robot turns right sharply
 */
void sharpTurnRight(int speed,int time){
unsigned long oldTime = millis();
Serial.println("sharp_right");
lcd.clear();
lcd.setCursor(0,0);
lcd.print("TURNING RIGHT");
while(millis() - oldTime < time){
  digitalWrite(M1,LOW);
  digitalWrite(M2,HIGH);
  analogWrite(E1,speed);
  analogWrite(E2,speed);
  updateOptical();

  if(checkForward() && checkOnTrack())
  break;
  }
}

/*
 * Robot goes a straight line 
 */
void moveForward(int speed){
  Serial.println("forward");
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("MOVING FORWARD");
  lcd.setCursor(0,1);
  velocity();
  digitalWrite(M1,HIGH);
  digitalWrite(M2,HIGH);
  analogWrite(E1,speed);
  analogWrite(E2,speed);
  
}

/*
 * Stop the robot
 */
void moveStop(){
  Serial.println("stop");
  lcd.clear();
  lcd.setCursor(6,0);
  lcd.print("STOP");
  digitalWrite(M1,HIGH);
  digitalWrite(M2,HIGH);
  analogWrite(E1,0);
  analogWrite(E2,0);
}

/*
 * Check if robot is on track 
 * @return true if robot is on track, false otherwise 
 */
boolean checkOnTrack(){
  if(opticalValue1 < threshold && opticalValue2 < threshold && opticalValue3 < threshold && opticalValue4 < threshold)
  return false;
  else 
  return true;
}

/*
 * Update optical sensor value at 4 sensors together 
 */
void updateOptical(){
  opticalValue1 = analogRead(opticalPin1);
  opticalValue2 = analogRead(opticalPin2);
  opticalValue3 = analogRead(opticalPin3);
  opticalValue4 = analogRead(opticalPin4);
}

//test optical sensor,test only
void print(){
Serial.println(analogRead(A2));
Serial.println(analogRead(A3));
Serial.println(analogRead(A4));
Serial.println(analogRead(A5));
//delay(2000);
}


 
