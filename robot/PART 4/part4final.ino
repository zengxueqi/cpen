#include <SoftwareSerial.h>
#include <math.h>
SoftwareSerial mySerial(0, 1); // RX, TX

const int switchPin1 = 4;
const int switchPin2 = 10;
const int E1 = 5;
const int M1 = 4;
const int E2 = 6;
const int M2 = 7;
const int hallPin1 = 8; // the number of the hall effect sensor pins //right
const int hallPin2 = 9;//left
int pretime=0;
int p1 = 0;
int p2 =0;
int p1s=1;
int p2s = 1;

//PART4 VARIABLES
int numArgs = 0;
int data[32]; //data array containing x,y, coordinates for points
bool haveParsed = false; //condition whether things have been parsed

void setup() 
{
  Serial.begin(9600);
  pinMode(M1, OUTPUT);   // set pin input mode or output mode
  pinMode(M2, OUTPUT);
  mySerial.begin(38400);

}
 
void loop() 
{

  if(!haveParsed) { //wait for user to input data, then parse it into data array
   parse();
  }
  else {
    //using data[] and numArgs
    part4(); //using the data array coordinates, translate to robot movement those points
    haveParsed = false; //prepare for next parse
    numArgs = 0; //set data array index back to 0
}

  
}

/* 
 * Parses x,y coordinates sent through bluetooth in Serial and stores them in the global array Data[]
 */
void parse() { //converts bluetooth data from serial into int array of coordinates
  String str;
  if(mySerial.available()) {

  str = mySerial.readString();// read the incoming data as string

  //Serial.println(str); //DEBUG

  int index = str.indexOf(","); //want to split string by comma
  String val;
  char charBuffer[3]; //canvas range of 0 to ~300, so only need 3 char positions
  int beginIndex = 0;

  while (index != -1) //keep check for commas until end reached
  {
    val = str.substring(beginIndex, index);
    val.toCharArray(charBuffer, 16);
    data[numArgs++] = atoi(charBuffer); //convert string into int and store into data array
    beginIndex = index + 1;
    index = str.indexOf(",", beginIndex);
  }

  //Serial.println(numArgs); //DEBUG
  for(int i = 0; i <numArgs; i++) {
    Serial.println(data[i]);
  }
    haveParsed = true; //set condition to stop parse
  
  
  }
}


/* 
 * Given a data array containing x,y coors in the format (x1,y1,x2,y2...)
 * Control the robot to drive along those coordinates
 */
void part4() {
  digitalWrite(M1, HIGH);
  digitalWrite(M2, HIGH);
  int count = 2;
  int x1 = data[0];
  int y1 = data[1];
  
  int x2;
  int y2;

  double dist;
  double angle;
  int up;
  int left;
  double temp;
  double t1;
  
  for(count = 2; count<numArgs; count +=2) {
    x2 = data[count];
    y2 = data[count+1];  

    if(y2-y1 < 0)
      up = 1;
    else if(y2-y1==0)
      up = 0;
    else
      up = -1;

    if(x2-x1 < 0)
      left = 1;
    else if(x2-x1 == 0)
      left = 0;
    else
      left = -1;

    temp = (double) (y2-y1)/(x2-x1);
    temp = atan(temp);
    angle = fabs(180 / 3.14159265 * temp); // in degree
    Serial.println("angle: " + String(angle));
    dist = distance(x1,y1,x2,y2);
    Serial.println("dist: " + String(dist));
     
    if(left==0 && up==1) { // front
      while(dist > 0) {
        forward(150);
        dist -= 1;
        delay(30);
      }
      stopm();
            delay(150);
    }
    else if(left==0 && up==-1) { // back
      turnleft(36);
      stopm();
      delay(150);
      while(dist > 0) {
        forward(150);
        dist -= 1;
        delay(30);
      }
      stopm();
            delay(150);
      turnright(36);
      stopm();
            delay(150);
    }
    else if(up==0 && left==1) { // left
      turnleft(18);
      stopm();
      delay(150);
      while(dist > 0) {
        forward(150);
        dist -= 1;
        delay(30);
      }
      stopm();
            delay(150);
      turnright(18);
      stopm();
            delay(150);
    }
    else if(up==0 && left==-1) { // right
      turnright(18);
      stopm();
      delay(150);
      while(dist > 0) {
        forward(150);
        dist -= 1;
        delay(30);
      }
      stopm();
            delay(150);
      turnleft(18);
      stopm();
            delay(150);
    }
    else if(up==1 && left==-1) { // NE
      t1 = ((double) 18.0 / 90.0 * (90 - angle));
      turnright(t1);
      stopm();
      delay(150);
      while(dist > 0) {
        forward(150);
        dist -= 1;
        delay(30);
      }
      stopm();
      delay(150);
      turnleft(t1);
      stopm();
      delay(150);
    }
    else if(left==-1 && up==-1) { // SE
      t1 = ((double) 18.0 / 90.0 * (90 + angle));
      turnright(t1);
      stopm();
      delay(150);
      while(dist > 0) {
        forward(150);
        dist -= 1;
        delay(30);
      }
      stopm();
            delay(150);
      turnleft(t1);
      stopm();
            delay(150);
    }
    else if(left==1 && up==1) { // NW
      t1 = ((double) 18.0 / 90.0 * (90 - angle));
      turnleft(t1);
      stopm();
      delay(150);
      while(dist > 0) {
        forward(150);
        dist -= 1;
        delay(30);
      }
      stopm();
      delay(150);
      turnright(t1);
      stopm();
            delay(150);
    }
    else if(left==1 && up==-1) { // SW
      t1 = ((double)18.0 / 90.0 * (90 + angle));
      turnleft(t1);
      stopm();
      delay(150);
      while(dist > 0) {
        forward(150);
        dist -= 1;
        delay(30);
      }
      stopm();
      delay(150);
      turnright(t1);
      stopm();
      delay(150);
    }
    else
      stopm();
      

    x1 = x2;
    y1 = y2;

    stopm();
    delay(500);
  }
}

/* 
* Calculate the distance between points (a,b) and (c,d)
*/
double distance(int a, int b, int c, int d) {
  double sum = pow((d-b),2) + pow((c-a),2)  ;
//  Serial.println("SUM: " + String(sum));
  sum = sqrt(sum);
  return sum;
}


/* 
 * Stop the robot
 */
void stopm() {
  analogWrite(E1, 0);   //PWM Speed Control
  analogWrite(E2, 0);
}


/* 
 * Turn the robot to the left by tme
 */
void turnleft(double tme) {
  digitalWrite(M2, LOW);
//  for (int i = 0; i < tme; i++) {
    analogWrite(E1, 160);   //PWM Speed Control
    analogWrite(E2, 150);   //PWM Speed Control
    Serial.println("left");
    delay(30*tme);
//  }
  digitalWrite(M2, HIGH);
}

/* 
 * Turn the robot to the right by tme
 */
void turnright(double tme) {
    digitalWrite(M1, LOW);
//  for (int i = 0; i < tme; i++) {
    analogWrite(E1, 150);   //PWM Speed Control
    analogWrite(E2, 150);  //PWM Speed Control
    Serial.println("right");
    delay(tme*30);
//  }
    digitalWrite(M1, HIGH);
}

/* 
 * control the speed of robot moving forward 
 */
void forward(int spd) {
  digitalWrite(M1, HIGH);
  digitalWrite(M2, HIGH);
  analogWrite(E1, spd-16);
  analogWrite(E2, spd);
}


