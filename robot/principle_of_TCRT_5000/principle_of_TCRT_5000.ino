//Remove ambience noise from sensor data.
//IR LED Digital pin: 6
//IR diode analog input:A3

int noise_signal, noise, denoised_signal;
void setup() 
{
  Serial.begin(9600);
  pinMode(6,OUTPUT); //IR LED Digital pin: 6
}

void loop() {
  digitalWrite(6,HIGH);    // Turning ON LED
  delayMicroseconds(500);  //wait
  noise_signal = analogRead(A3);//take reading from photodiode(pin A3) :noise+signal
  digitalWrite(6,LOW);     //turn Off LED
  delayMicroseconds(500);  //wait
  noise = analogRead(A3);  // again take reading from photodiode :noise
  denoised_signal = noise_signal - noise;  //taking differnce:[ (noise+signal)-(noise)] just signal

  Serial.println(denoised_signal);  // denoised signal
 
}
