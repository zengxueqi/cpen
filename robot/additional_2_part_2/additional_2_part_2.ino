double arr[] = {1,2,3,4,5,6}; // assume this is an array with x,y coordinates of points

void setup() {
  // put your setup code here, to run once:

}

void loop() {
  // put your main code here, to run repeatedly:
  part4();

}

void part4() {
  int count = 2;
  double x1 = arr[0];
  double y1 = arr[1];
  
  double x2;
  double y2;

  double dist;
  double angle;
  int up;
  int left;
  
  while(count < sizeof(arr)) {
    x2 = arr[count];
    y2 = arr[count+1];  

    if(y2-y1 < 0)
      up = 1;
    else if(y2-y1==0)
      up = 0;
    else
      up = -1;

    if(x2-x1 < 0)
      left = 1;
    else if(x2-x1 == 0)
      left = 0;
    else
      left = -1;
      
    angle = fabs(180 / 3.14159265 * atan((y2-y1)/(x2-x1))); // in degree

    dist = distance(x1,y1,x2,y2);
     
    if(left==0 && up==1) { // front
      while(dist > 0) {
        forward(150);
        dist -= 50;
      }
      stopm();
    }
    else if(left==0 && up==-1) { // back
      turnleft(??*2);
      while(dist > 0) {
        forward(150);
        dist -= 50;
      }
      stopm();
      turnright(??*2);
    }
    else if(up==0 && left==1) { // left
      turnleft(??);
      while(dist > 0) {
        forward(150);
        dist -= 50;
      }
      stopm();
      turnright(??);
    }
    else if(up==0 && left==-1) { // right
      turnright(??);
      while(dist > 0) {
        forward(150);
        dist -= 50;
      }
      stopm();
      turnleft(??);
    }
    else if(up==1 && left==-1) { // NE
      turnright(?? / 90 * (90 - angle));
      while(dist > 0) {
        forward(150);
        dist -= 50;
      }
      stopm();
      turnleft(?? / 90 * (90 - angle));
    }
    else if(left==-1 && up==-1) { // SE
      turnright(?? / 90 * (90 + angle));
      while(dist > 0) {
        forward(150);
        dist -= 50;
      }
      stopm();
      turnleft(?? / 90 * (90 + angle));
    }
    else if(left==1 && up==1) { // NW
      turnleft(?? / 90 * (90 - angle));
      while(dist > 0) {
        forward(150);
        dist -= 50;
      }
      stopm();
      turnright(?? / 90 * (90 - angle));
    }
    else if(left==1 && up==-1) { // SW
      turnleft(?? / 90 * (90 + angle));
      while(dist > 0) {
        forward(150);
        dist -= 50;
      }
      stopm();
      turnright(?? / 90 * (90 + angle));
    }
    else
      stopm();

    x1 = x2;
    y1 = y2;
    count = count + 2;
  }
}

double distance(double a, double b, double c, double d) {
  return sqrt((d-b)*(d-b) + (c-a)*(c-a));
}

