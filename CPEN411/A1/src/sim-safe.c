/* sim-safe.c - sample functional simulator implementation */

/* SimpleScalar(TM) Tool Suite
 * Copyright (C) 1994-2003 by Todd M. Austin, Ph.D. and SimpleScalar, LLC.
 * All Rights Reserved. 
 * 
 * THIS IS A LEGAL DOCUMENT, BY USING SIMPLESCALAR,
 * YOU ARE AGREEING TO THESE TERMS AND CONDITIONS.
 * 
 * No portion of this work may be used by any commercial entity, or for any
 * commercial purpose, without the prior, written permission of SimpleScalar,
 * LLC (info@simplescalar.com). Nonprofit and noncommercial use is permitted
 * as described below.
 * 
 * 1. SimpleScalar is provided AS IS, with no warranty of any kind, express
 * or implied. The user of the program accepts full responsibility for the
 * application of the program and the use of any results.
 * 
 * 2. Nonprofit and noncommercial use is encouraged. SimpleScalar may be
 * downloaded, compiled, executed, copied, and modified solely for nonprofit,
 * educational, noncommercial research, and noncommercial scholarship
 * purposes provided that this notice in its entirety accompanies all copies.
 * Copies of the modified software can be delivered to persons who use it
 * solely for nonprofit, educational, noncommercial research, and
 * noncommercial scholarship purposes provided that this notice in its
 * entirety accompanies all copies.
 * 
 * 3. ALL COMMERCIAL USE, AND ALL USE BY FOR PROFIT ENTITIES, IS EXPRESSLY
 * PROHIBITED WITHOUT A LICENSE FROM SIMPLESCALAR, LLC (info@simplescalar.com).
 * 
 * 4. No nonprofit user may place any restrictions on the use of this software,
 * including as modified by the user, by any other authorized user.
 * 
 * 5. Noncommercial and nonprofit users may distribute copies of SimpleScalar
 * in compiled or executable form as set forth in Section 2, provided that
 * either: (A) it is accompanied by the corresponding machine-readable source
 * code, or (B) it is accompanied by a written offer, with no time limit, to
 * give anyone a machine-readable copy of the corresponding source code in
 * return for reimbursement of the cost of distribution. This written offer
 * must permit verbatim duplication by anyone, or (C) it is distributed by
 * someone who received only the executable form, and is accompanied by a
 * copy of the written offer of source code.
 * 
 * 6. SimpleScalar was developed by Todd M. Austin, Ph.D. The tool suite is
 * currently maintained by SimpleScalar LLC (info@simplescalar.com). US Mail:
 * 2395 Timbercrest Court, Ann Arbor, MI 48105.
 * 
 * Copyright (C) 1994-2003 by Todd M. Austin, Ph.D. and SimpleScalar, LLC.
 */


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>

#include "host.h"
#include "misc.h"
#include "machine.h"
#include "regs.h"
#include "memory.h"
#include "loader.h"
#include "syscall.h"
#include "options.h"
#include "stats.h"
#include "sim.h"

/*
 * This file implements a functional simulator.  This functional simulator is
 * the simplest, most user-friendly simulator in the simplescalar tool set.
 * Unlike sim-fast, this functional simulator checks for all instruction
 * errors, and the implementation is crafted for clarity rather than speed.
 */

/* simulated registers */
static struct regs_t regs;

/* simulated memory */
static struct mem_t *mem = NULL;

/* track number of refs */
static counter_t sim_num_refs = 0;

/*Task 1: Start*/
static counter_t g_total_dmiss = 0;
static counter_t g_total_setmiss = 0;
/*Task 1: End*/

/*Task 2: Start*/
static counter_t g_total_load = 0;
static counter_t g_total_store = 0;
static counter_t g_total_load_miss = 0;
static counter_t g_total_store_miss = 0;
static counter_t g_total_writeback = 0;
/*Task 2: End*/

/*Task 3: Start*/
static counter_t g_total_load_dmiss = 0;
static counter_t g_total_store_dmiss = 0;
static counter_t g_total_load_s2miss = 0;
static counter_t g_total_store_s2miss = 0;
static counter_t g_total_load_s4miss = 0;
static counter_t g_total_store_s4miss = 0;
/*Task 3: End*/

/*Task 4 Start*/
static counter_t g_total_prefetch = 0;
static counter_t g_total_pref_hit = 0;
static counter_t g_total_miss = 0;
/*Task 4 End*/

/* maximum number of inst's to execute */
static unsigned int max_insts;

/* register simulator-specific options */
void
sim_reg_options(struct opt_odb_t *odb)
{
  opt_reg_header(odb, 
"sim-safe: This simulator implements a functional simulator.  This\n"
"functional simulator is the simplest, most user-friendly simulator in the\n"
"simplescalar tool set.  Unlike sim-fast, this functional simulator checks\n"
"for all instruction errors, and the implementation is crafted for clarity\n"
"rather than speed.\n"
		 );

  /* instruction limit */
  opt_reg_uint(odb, "-max:inst", "maximum number of inst's to execute",
	       &max_insts, /* default */0,
	       /* print */TRUE, /* format */NULL);

}

/* check simulator-specific option values */
void
sim_check_options(struct opt_odb_t *odb, int argc, char **argv)
{
  /* nada */
}

/* register simulator-specific statistics */
void
sim_reg_stats(struct stat_sdb_t *sdb)
{
  stat_reg_counter(sdb, "sim_num_insn",
		   "total number of instructions executed",
		   &sim_num_insn, sim_num_insn, NULL);
  stat_reg_counter(sdb, "sim_num_refs",
		   "total number of loads and stores executed",
		   &sim_num_refs, 0, NULL);
  stat_reg_int(sdb, "sim_elapsed_time",
	       "total simulation time in seconds",
	       &sim_elapsed_time, 0, NULL);
  stat_reg_formula(sdb, "sim_inst_rate",
		   "simulation speed (in insts/sec)",
		   "sim_num_insn / sim_elapsed_time", NULL);

  /*Task 1 Start*/
  stat_reg_counter(sdb,"sim_num_dmiss",
          "total number of direct mapped cache misses",
          &g_total_dmiss,0,NULL);
  stat_reg_formula(sdb,"sim_dcache_miss_rate",
          "Task 1:miss rate for directed mapped cache",
          "100 * sim_num_dmiss / sim_num_insn",NULL);
  
  stat_reg_counter(sdb,"sim_num_smiss",
          "total number of 4-way set associative cache misses",
          &g_total_setmiss,0,NULL);
  stat_reg_formula(sdb,"sim_setcache_miss_rate",
          "Task 1:miss rate for 4-way set associative cache",
          "100 * sim_num_smiss / sim_num_insn",NULL);
  /*Task 1 End*/

  /*Task 2 Start*/
  stat_reg_counter(sdb,"sim_num_load",
          "total number of load instructions",
           &g_total_load,0,NULL);
  stat_reg_counter(sdb,"sim_num_store",
          "total number of store instructions",
           &g_total_store,0,NULL);

  stat_reg_counter(sdb,"sim_num_load_miss",
          "total number of load misses",
          &g_total_load_miss,0,NULL);
  stat_reg_formula(sdb,"sim_load_miss_rate",
          "Task 2:miss rate for load instruction",
          "100 * sim_num_load_miss / sim_num_load",NULL);

  stat_reg_counter(sdb,"sim_num_store_miss",
          "total number of store misses",
          &g_total_store_miss,0,NULL);
  stat_reg_formula(sdb,"sim_store_miss_rate",
          "Task 2: miss rate for store instruction",
          "100 * sim_num_store_miss / sim_num_store",NULL);

  stat_reg_counter(sdb,"sim_num_writeback",
          "total number of write back events",
          &g_total_writeback,0,NULL);
  stat_reg_formula(sdb,"sim_writeback_store_rate",
          "Task 2:ratio of writeback events of store instructions",
          "100 * sim_num_writeback / sim_num_store",NULL);
  /*Task 2 End*/

  /*Task 3 Start*/
  stat_reg_counter(sdb,"sim_num_ld_d",
          "total number of load miss using direct-mapped and victim",
          &g_total_load_dmiss,0,NULL);
  stat_reg_formula(sdb,"sim_num_ld_dvmiss",
          "Task 3:miss rate for load directed-mapped and victim",
          "100 * sim_num_ld_d / sim_num_load",NULL);
  
  stat_reg_counter(sdb,"sim_num_st_d",
          "total number of store miss using direct-mapped and victim",
          &g_total_store_dmiss,0,NULL);
  stat_reg_formula(sdb,"sim_num_st_dvmiss",
          "Task 3:miss rate for store with directed-mapped and victim",
          "100 * sim_num_st_d / sim_num_store",NULL);

  stat_reg_counter(sdb,"sim_num_ld_s2",
          "total number of load miss using 2 way set associative and victim",
          &g_total_load_s2miss,0,NULL);
  stat_reg_formula(sdb,"sim_num_ld_sv2miss",
          "Task 3:miss rate for load with 2 way set associative and victim",
          "100 * sim_num_ld_s2 / sim_num_load",NULL);
  
  stat_reg_counter(sdb,"sim_num_st_s2",
          "total number of store miss using 2 way set associative and victim",
          &g_total_store_s2miss,0,NULL);
  stat_reg_formula(sdb,"sim_num_st_sv2miss",
          "Task 3:miss rate for store with 2 way set associative and victim",
          "100 * sim_num_st_s2 / sim_num_store",NULL);

  stat_reg_counter(sdb,"sim_num_ld_s4",
          "total number of load miss using 4 way set associative and victim",
          &g_total_load_s4miss,0,NULL);
  stat_reg_formula(sdb,"sim_num_ld_sv4miss",
          "Task 3:miss rate for load with 4 way set associative and victim",
          "100 * sim_num_ld_s4 / sim_num_load",NULL);
  
  stat_reg_counter(sdb,"sim_num_st_s4",
          "total number of store miss using 4 way set associative and victim",
          &g_total_store_s4miss,0,NULL);
  stat_reg_formula(sdb,"sim_num_st_sv4miss",
          "Task 3:miss rate for store with 4 way set associative and victim",
          "100 * sim_num_st_s4 / sim_num_store",NULL);
  /*Task 3 End*/

  /*Task 4 Start*/
  stat_reg_counter(sdb,"sim_num_miss",
	  "Task 4: total number of miss",
	  &g_total_miss,0,NULL);
  stat_reg_formula(sdb,"sim_miss_rate",
	  "Task 4: actual miss rate of instruction cache",
	  "100 * sim_num_miss / sim_num_insn",NULL);
 
  stat_reg_counter(sdb,"sim_num_prefetch",
          "Task 4:total number of prefetch instructions",
          &g_total_prefetch,0,NULL);
  stat_reg_counter(sdb,"sim_num_pref_hit",
          "Task 4:total number of prefetch success",
          &g_total_pref_hit,0,NULL);
  stat_reg_formula(sdb,"sim_num_pref_rate",
          "Task 4:prefetching success rate",
          "100 * sim_num_pref_hit / sim_num_prefetch",NULL);
  /*Task 4 End*/

  ld_reg_stats(sdb);
  mem_reg_stats(mem, sdb);
}

/* initialize the simulator */
void
sim_init(void)
{
  sim_num_refs = 0;

  /* allocate and initialize register file */
  regs_init(&regs);

  /* allocate and initialize memory space */
  mem = mem_create("mem");
  mem_init(mem);
}

/* load program into simulated state */
void
sim_load_prog(char *fname,		/* program to load */
	      int argc, char **argv,	/* program arguments */
	      char **envp)		/* program environment */
{
  /* load program text and data, set up environment, memory, and regs */
  ld_load_prog(fname, argc, argv, envp, &regs, mem, TRUE);
}

/* print simulator-specific configuration information */
void
sim_aux_config(FILE *stream)		/* output stream */
{
  /* nothing currently */
}

/* dump simulator-specific auxiliary simulator statistics */
void
sim_aux_stats(FILE *stream)		/* output stream */
{
  /* nada */
}

/* un-initialize simulator-specific state */
void
sim_uninit(void)
{
  /* nada */
}


/*
 * configure the execution engine
 */

/*
 * precise architected register accessors
 */

/* next program counter */
#define SET_NPC(EXPR)		(regs.regs_NPC = (EXPR))

/* current program counter */
#define CPC			(regs.regs_PC)

/* general purpose registers */
#define GPR(N)			(regs.regs_R[N])
#define SET_GPR(N,EXPR)		(regs.regs_R[N] = (EXPR))

#if defined(TARGET_PISA)

/* floating point registers, L->word, F->single-prec, D->double-prec */
#define FPR_L(N)		(regs.regs_F.l[(N)])
#define SET_FPR_L(N,EXPR)	(regs.regs_F.l[(N)] = (EXPR))
#define FPR_F(N)		(regs.regs_F.f[(N)])
#define SET_FPR_F(N,EXPR)	(regs.regs_F.f[(N)] = (EXPR))
#define FPR_D(N)		(regs.regs_F.d[(N) >> 1])
#define SET_FPR_D(N,EXPR)	(regs.regs_F.d[(N) >> 1] = (EXPR))

/* miscellaneous register accessors */
#define SET_HI(EXPR)		(regs.regs_C.hi = (EXPR))
#define HI			(regs.regs_C.hi)
#define SET_LO(EXPR)		(regs.regs_C.lo = (EXPR))
#define LO			(regs.regs_C.lo)
#define FCC			(regs.regs_C.fcc)
#define SET_FCC(EXPR)		(regs.regs_C.fcc = (EXPR))

#elif defined(TARGET_ALPHA)

/* floating point registers, L->word, F->single-prec, D->double-prec */
#define FPR_Q(N)		(regs.regs_F.q[N])
#define SET_FPR_Q(N,EXPR)	(regs.regs_F.q[N] = (EXPR))
#define FPR(N)			(regs.regs_F.d[(N)])
#define SET_FPR(N,EXPR)		(regs.regs_F.d[(N)] = (EXPR))

/* miscellaneous register accessors */
#define FPCR			(regs.regs_C.fpcr)
#define SET_FPCR(EXPR)		(regs.regs_C.fpcr = (EXPR))
#define UNIQ			(regs.regs_C.uniq)
#define SET_UNIQ(EXPR)		(regs.regs_C.uniq = (EXPR))

#else
#error No ISA target defined...
#endif

/* precise architected memory state accessor macros */
#define READ_BYTE(SRC, FAULT)						\
  ((FAULT) = md_fault_none, addr = (SRC), MEM_READ_BYTE(mem, addr))
#define READ_HALF(SRC, FAULT)						\
  ((FAULT) = md_fault_none, addr = (SRC), MEM_READ_HALF(mem, addr))
#define READ_WORD(SRC, FAULT)						\
  ((FAULT) = md_fault_none, addr = (SRC), MEM_READ_WORD(mem, addr))
#ifdef HOST_HAS_QWORD
#define READ_QWORD(SRC, FAULT)						\
  ((FAULT) = md_fault_none, addr = (SRC), MEM_READ_QWORD(mem, addr))
#endif /* HOST_HAS_QWORD */

#define WRITE_BYTE(SRC, DST, FAULT)					\
  ((FAULT) = md_fault_none, addr = (DST), MEM_WRITE_BYTE(mem, addr, (SRC)))
#define WRITE_HALF(SRC, DST, FAULT)					\
  ((FAULT) = md_fault_none, addr = (DST), MEM_WRITE_HALF(mem, addr, (SRC)))
#define WRITE_WORD(SRC, DST, FAULT)					\
  ((FAULT) = md_fault_none, addr = (DST), MEM_WRITE_WORD(mem, addr, (SRC)))
#ifdef HOST_HAS_QWORD
#define WRITE_QWORD(SRC, DST, FAULT)					\
  ((FAULT) = md_fault_none, addr = (DST), MEM_WRITE_QWORD(mem, addr, (SRC)))
#endif /* HOST_HAS_QWORD */

/* system call handler macro */
#define SYSCALL(INST)	sys_syscall(&regs, mem_access, mem, INST, TRUE)

#define DNA         (0)

/* general register dependence decoders */
#define DGPR(N)         (N)
#define DGPR_D(N)       ((N) &~1)

/* floating point register dependence decoders */
#define DFPR_L(N)       (((N)+32)&~1)
#define DFPR_F(N)       (((N)+32)&~1)
#define DFPR_D(N)       (((N)+32)&~1)

/* miscellaneous register dependence decoders */
#define DHI         (0+32+32)
#define DLO         (1+32+32)
#define DFCC            (2+32+32)
#define DTMP            (3+32+32)

/*Task 1 Start*/
/*Instruction Cache Block*/
struct block
{
  md_addr_t tag;
  int valid;
  int timestamp;
  int dirty;
  int prefetched;//indicate if it is a prefetched block 
};

//Functions for directed mapped cache access
void dcache_access(struct block *cache,unsigned addr,int offsetbits,int indexbits)
{
  int index;//index for address
  int tag;// tag for address

  index = (addr >> offsetbits) & ((1 << indexbits) - 1);
  tag = addr >> (offsetbits + indexbits);

  assert(index < pow(2,indexbits));

  //cache hit 
  if(cache[index].tag == tag && cache[index].valid == 1)
  {
    return;
  }
  //Cache miss
  else 
  {
    cache[index].tag = tag;
    cache[index].valid = 1;
    g_total_dmiss++;
  }
}

//Functions for 4 way set associative cache access
void setcache_access(struct block **cache,unsigned addr,int offsetbits,int indexbits,int nways)
{
  int index;//index for address
  int tag;// tag for address
  int way_num;
  int cache_hit = 0;

  index = (addr >> offsetbits) & ((1 << indexbits) - 1);
  tag = addr >> (offsetbits + indexbits);

  //Check if it is a cache hit 
  for(way_num = 0; way_num < nways; way_num++)
  {
    if(cache[index][way_num].tag == tag && cache[index][way_num].valid == 1)
    {
      //set timestamp to maximum upon insert
      cache[index][way_num].timestamp = nways - 1; 
      cache_hit = 1;
    }
    else if(cache_hit == 0 && cache[index][way_num].valid == 0)//insert if there is available slots
    {
      g_total_setmiss++;
      cache[index][way_num].tag = tag;
      //set timestamp to maximum upon insert
      cache[index][way_num].timestamp = nways - 1;
      cache[index][way_num].valid = 1;
      return;
    }
    else if(cache[index][way_num].valid == 1)
    { //decrement timestamp value if it is not insert or hit
      cache[index][way_num].timestamp--;
    }
  }

  if(cache_hit == 1)
    return;
    

  int evict_line = 0;
  int min = cache[index][0].timestamp;

  //find cache line with lowest timestamp value, which is the most least used line
  for(way_num = 1; way_num < nways; way_num++)
  {
    assert(cache[index][way_num].valid == 1);
    if(cache[index][way_num].timestamp < min)
    {
      min = cache[index][way_num].timestamp;
      evict_line = way_num;
    }
  }

  //no free slot, evict least used cache lines
  cache[index][evict_line].tag = tag;
  cache[index][evict_line].timestamp = nways - 1;
  g_total_setmiss++;

}
/*Task 1 End*/

/*Task 2 Start*/
//8 way set associative cache with 6 bits offset, 6 bits index, flag = 0 is load, flag = 1 is store 
void setcache(struct block **cache,unsigned addr,int offsetbits,int indexbits,int nways,int flag)
{
  int index;//index for address
  int tag;// tag for address
  int way_num;
  int cache_hit = 0;

  index = (addr >> offsetbits) & ((1 << indexbits) - 1);
  tag = addr >> (offsetbits + indexbits);

  assert(index < pow(2,indexbits));

  //Check if it is a cache hit 
  for(way_num = 0; way_num < nways; way_num++)
  {
    if(cache[index][way_num].tag == tag && cache[index][way_num].valid == 1)
    {
      //set timestamp to maximum upon insert
      cache[index][way_num].timestamp = nways - 1; 
      cache_hit = 1;
      
      //store instructions, mark cache as dirty 
      if(flag == 1)
      {
        cache[index][way_num].dirty = 1;
      }
    }
    else if(cache_hit == 0 && cache[index][way_num].valid == 0)//insert if there is available slots
    {
      if(flag == 0)
      {
        g_total_load_miss++;
      }
      else
      { 
        g_total_store_miss++;
        cache[index][way_num].dirty = 1;
      }

      cache[index][way_num].tag = tag;
      //set timestamp to maximum upon insert
      cache[index][way_num].timestamp = nways - 1;
      cache[index][way_num].valid = 1;
      return;
    }
    else if(cache[index][way_num].valid == 1)
    { //decrement timestamp value if it is not insert or hit
      cache[index][way_num].timestamp--;
    }
  }

  if(cache_hit == 1)
    return;
    
  int evict_line = 0;
  int min = cache[index][0].timestamp;

  //find cache line with lowest timestamp value, which is the most least used line
  for(way_num = 1; way_num < nways; way_num++)
  {
    assert(cache[index][way_num].valid == 1);
    if(cache[index][way_num].timestamp < min)
    {
      min = cache[index][way_num].timestamp;
      evict_line = way_num;
    }
  }

  //no free slot, evict least used cache lines
  cache[index][evict_line].tag = tag;
  cache[index][evict_line].timestamp = nways - 1;
  
  //on evict, write back to memory if it is dirty 
  if(cache[index][evict_line].dirty == 1)
  {
    g_total_writeback++;
  }

  if(flag == 0)
  {
    g_total_load_miss++;
    cache[index][evict_line].dirty = 0;
  }
  else
  { 
    g_total_store_miss++;
    cache[index][evict_line].dirty = 1; //store instruction, mark cache line as dirty 
  }

}
/*Task 2 End*/

/*Task 3 Start*/
//Functions for directed mapped cache access with victim cache 
void dcache_victim_access
(struct block *cache,struct block *victim,unsigned addr,int offsetbits,int indexbits,counter_t *miss_count)
{
  int index;//index for address
  int tag;// tag for address
  int i;
  int victim_hit = 0; //victim cache hit 
  int vtag;//victim cache tag: fully-associative cache 

  index = (addr >> offsetbits) & ((1 << indexbits) - 1);
  tag = addr >> (offsetbits + indexbits);
  vtag = addr >> offsetbits;

  assert(index < pow(2,indexbits));
  
  //Cache hit
  if(cache[index].tag == tag && cache[index].valid)
    return;
  //cold miss
  else if(cache[index].valid == 0)
  {
    cache[index].valid = 1;
    cache[index].tag = tag;
    *miss_count += 1;
    return;
  }

  md_addr_t address = (cache[index].tag << indexbits) ^ index; //evicted cache line in main cache address

  //scan victim cache to check if there is a hit in victim cache  
  for(i = 0; i < 32;i++)
  {
    //a hit in victim cache, swap cache line from direct mapped cache and victim cache
    if(victim[i].tag == vtag && victim[i].valid == 1)
    {
      cache[index].tag = tag;
      victim[i].tag = address;
      victim[i].timestamp = 32;
      victim_hit = 1;
    }
    //cold miss in victim cache, swap cache line from direct mapped cache and victim cache
    else if(victim_hit == 0 && victim[i].valid == 0)
    {
      cache[index].tag = tag;
      victim[i].tag = address;
      victim[i].valid = 1;
      victim[i].timestamp = 32;
      *miss_count += 1;//a miss 
      return;
    }
    else if(victim[i].valid == 1)
    {
      victim[i].timestamp--;
    }
  }

  if(victim_hit == 1)
    return;

  int evict_vline = 0;
  int min = victim[0].timestamp;

  //no free slots in victim cache,find LRU lines in victim cache   
  for(i = 1; i < 32; i++)
  { 
    assert(victim[i].valid == 1);
    if(victim[i].timestamp < min)
    {
      min = victim[i].timestamp;
      evict_vline = i;
    }
  }

  //evict LRU lines in victim cache, 
  victim[evict_vline].tag = address; //put evicted cache line in victim cache,
  victim[evict_vline].timestamp = 32;
  cache[index].tag = tag; //put new cache line to cache 

  *miss_count += 1;
}

//set cache used with victim cache 
void setcache_victim_access
(struct block **cache,struct block *victim,unsigned addr,int offsetbits,int indexbits,int nways,counter_t *miss_count)
{
  int index;//index for address
  int tag;// tag for address
  int way_num;
  int cache_hit = 0;//set associative cache hit 
  int victim_hit = 0; //victim cache hit
  int i;
  int vtag;//victim cache tag for addr

  index = (addr >> offsetbits) & ((1 << indexbits) - 1);
  tag = addr >> (offsetbits + indexbits);
  vtag = addr >> offsetbits;

  assert(index < pow(2,indexbits));

  //Check if it is a cache hit 
  for(way_num = 0; way_num < nways; way_num++)
  {
    if(cache[index][way_num].tag == tag && cache[index][way_num].valid == 1)
    {
      //set timestamp to maximum upon insert
      cache[index][way_num].timestamp = nways - 1; 
      cache_hit = 1;
    }
    else if(cache_hit == 0 && cache[index][way_num].valid == 0)//insert if there is available slots
    {
      *miss_count += 1; //a miss 
      cache[index][way_num].tag = tag;
      //set timestamp to maximum upon insert
      cache[index][way_num].timestamp = nways - 1;
      cache[index][way_num].valid = 1;
      return;
    }
    else if(cache[index][way_num].valid == 1)
    { //decrement timestamp value if it is not insert or hit
      cache[index][way_num].timestamp--;
    }
  }

  if(cache_hit == 1)
    return;
    
  int cache_evict_line = 0;
  int min = cache[index][0].timestamp;

  //find cache line with lowest timestamp value, which is the most least used line
  for(way_num = 1; way_num < nways; way_num++)
  {
    assert(cache[index][way_num].valid == 1);
    assert(cache[index][way_num].timestamp != min);
    if(cache[index][way_num].timestamp < min)
    {
      min = cache[index][way_num].timestamp;
      cache_evict_line = way_num;
    }
  }

  md_addr_t address = (cache[index][cache_evict_line].tag << indexbits) ^ index;//main cache evicted line tag in fully-associative cache

  //scan victim cache to check if there is a hit in victim cache  
  for(i = 0; i < 32;i++)
  {
    //a hit in victim cache, swap cache line from main cache and victim cache
    if(victim[i].tag == vtag && victim[i].valid == 1)
    {
      victim[i].tag = address; //put evicted main cache line into victim cache 
      victim[i].timestamp = 32;
      cache[index][cache_evict_line].tag = tag; 
      cache[index][cache_evict_line].timestamp = nways - 1; 
      victim_hit = 1;
    }
    //cold miss in victim cache, 
    else if(victim_hit == 0 && victim[i].valid == 0)
    {
      victim[i].tag = address; //move evicted main cache line to victim cache
      victim[i].valid = 1;
      victim[i].timestamp = 32;
      cache[index][cache_evict_line].tag = tag; //main cache stores new tag
      cache[index][cache_evict_line].timestamp = nways - 1; 
      *miss_count += 1;//a miss 
      return;
    }
    else if(victim[i].valid == 1)
    {
      victim[i].timestamp--;
    }
  }

  if(victim_hit)
    return;

  int evict_vline = 0;
  min = victim[0].timestamp;

  //no free slots in victim cache,find LRU lines in victim cache   
  for(i = 1; i < 32; i++)
  { 
    assert(victim[i].valid == 1);
    assert(victim[i].timestamp != min);
    if(victim[i].timestamp < min)
    {
      min = victim[i].timestamp;
      evict_vline = i;
    }
  }

  //evict LRU lines in victim cache 
  victim[evict_vline].tag = address; //put evicted cache line in victim cache,
  victim[evict_vline].timestamp = 32;
  cache[index][cache_evict_line].tag = tag;//put new cache line to cache 
  cache[index][cache_evict_line].timestamp = nways - 1; 
  *miss_count += 1;
}
/*Task 3 End*/

/*Task 4 Start*/
//prefetch cache block of addr 
void prefetch(struct block **cache,unsigned addr,int offsetbits,int indexbits,int nways)
{
  int index;//index for address
  int tag;// tag for address
  int way_num;
  int prefetch_hit = 0;

  index = (addr >> offsetbits) & ((1 << indexbits) - 1);
  tag = addr >> (offsetbits + indexbits);

  //Check if it is a cache hit 
  for(way_num = 0; way_num < nways; way_num++)
  {
    if(cache[index][way_num].tag == tag && cache[index][way_num].valid == 1)
    {
      //set timestamp to maximum upon insert
      cache[index][way_num].timestamp = nways - 1; 
      prefetch_hit = 1;
    }
    else if(prefetch_hit == 0 && cache[index][way_num].valid == 0)//insert if there is available slots
    {
      cache[index][way_num].tag = tag;
      //set timestamp to maximum upon insert
      cache[index][way_num].timestamp = nways - 1;
      cache[index][way_num].valid = 1;
      cache[index][way_num].prefetched = 1; //mark as prefetched block 
      g_total_prefetch++;
      return;
    }
    else if(cache[index][way_num].valid == 1)
    { //decrement timestamp value if it is not insert or hit
      cache[index][way_num].timestamp--;
    }
  }

  if(prefetch_hit == 1)
    return;
    

  int evict_line = 0;
  int min = cache[index][0].timestamp;

  //find cache line with lowest timestamp value, which is the most least used line
  for(way_num = 1; way_num < nways; way_num++)
  {
    assert(cache[index][way_num].valid == 1 && cache[index][way_num].timestamp != min);
    if(cache[index][way_num].timestamp < min)
    {
      min = cache[index][way_num].timestamp;
      evict_line = way_num;
    }
  }

  //no free slot, evict least used cache lines
  cache[index][evict_line].tag = tag;
  cache[index][evict_line].timestamp = nways - 1;
  cache[index][evict_line].prefetched = 1; //mark as prefetched block 
  g_total_prefetch++;
}

void setcache_prefetch(struct block **cache,unsigned addr,int offsetbits,int indexbits,int nways)
{
  int index;//index for address
  int tag;// tag for address
  int way_num;
  int cache_hit = 0;
  int hit;

  index = (addr >> offsetbits) & ((1 << indexbits) - 1);
  tag = addr >> (offsetbits + indexbits);

  //Check if it is a cache hit 
  for(way_num = 0; way_num < nways; way_num++)
  {
    if(cache[index][way_num].tag == tag && cache[index][way_num].valid == 1)
    {
      //set timestamp to maximum upon insert
      cache[index][way_num].timestamp = nways - 1; 
      cache_hit = 1;
      if(cache[index][way_num].prefetched == 1)
      {
          g_total_pref_hit++;
	        cache[index][way_num].prefetched = 0;
      }
    }
    else if(cache_hit == 0 && cache[index][way_num].valid == 0)//insert if there is available slots
    {
      cache[index][way_num].tag = tag;
      //set timestamp to maximum upon insert
      cache[index][way_num].timestamp = nways - 1;
      cache[index][way_num].valid = 1;
      cache[index][way_num].prefetched = 0; 
      g_total_miss++;
      prefetch(cache,addr + 32,offsetbits,indexbits,nways);
      return;
    }
    else if(cache[index][way_num].valid == 1)
    { //decrement timestamp value if it is not insert or hit
      cache[index][way_num].timestamp--;
    }
  }

  if(cache_hit == 1)
    return;
    

  int evict_line = 0;
  int min = cache[index][0].timestamp;

  //find cache line with lowest timestamp value, which is the most least used line
  for(way_num = 1; way_num < nways; way_num++)
  {
    assert(cache[index][way_num].valid == 1 && cache[index][way_num].timestamp != min);
    if(cache[index][way_num].timestamp < min)
    {
      min = cache[index][way_num].timestamp;
      evict_line = way_num;
    }
  }

  //no free slot, evict least used cache lines
  cache[index][evict_line].tag = tag;
  cache[index][evict_line].timestamp = nways - 1;
  cache[index][evict_line].prefetched = 0; 
  g_total_miss++;
  prefetch(cache,addr + 32,offsetbits,indexbits,nways);
}
/*Task 4 End*/

/* start simulation, program loaded, processor precise state initialized */
void
sim_main(void)
{
  md_inst_t inst;
  register md_addr_t addr;
  enum md_opcode op;
  register int is_write;
  enum md_fault_type fault;

  /*Task 1: Start*/ 
  unsigned inst_addr; //address of current fetched instruction 
  int i;
  //Create Direct-Mapped Cache with 512 Cache Lines
  struct block *dcache = (struct block*) calloc(512,sizeof(struct block)); 

  //Create 4-way set associate cache with 256 cache sets/rows
  struct block **set4_cache = (struct block**) calloc(256,sizeof(struct block*));
  for(i = 0; i < 256; i++)
    set4_cache[i] = (struct block*) calloc(4,sizeof(struct block));
  /*Task 1: End*/

  /*Task 2: Start*/
  //Create 8-way set associate cache with 64 cache sets/rows 
  struct block **set8_cache = (struct block**) calloc(64,sizeof(struct block*));
  for(i = 0; i < 64; i++)
    set8_cache[i] = (struct block*) calloc(8,sizeof(struct block));
  /*Task 2: End*/

  /*Task 3: Start*/
  //fully associative victim cache with 32 cache lines
  struct block *victim_d = (struct block*) calloc(32,sizeof(struct block));
  struct block *victim_s2 = (struct block*) calloc(32,sizeof(struct block));
  struct block *victim_s4 = (struct block*) calloc(32,sizeof(struct block));

  //Main cache: direct-mapped cache 
  struct block *dv_cache = (struct block*) calloc(512,sizeof(struct block));
  //Main cache:2-way set associate cache with 256 cache sets/rows,index bits 8
  struct block **sv2_cache = (struct block**) calloc(256,sizeof(struct block*));
  for(i = 0; i < 256; i++)
    sv2_cache[i] = (struct block*) calloc(2,sizeof(struct block));
  
  //Main Cache:4-way set associate cache with 128 cache sets/rows,index bits 7
  struct block **sv4_cache = (struct block**) calloc(128,sizeof(struct block*));
  for(i = 0; i < 128; i++)
    sv4_cache[i] = (struct block*) calloc(4,sizeof(struct block));
  /*Task 3: End*/

  /*Task 4 Start*/
  //Create 4-way set associate cache with 256 cache setts/rows
  struct block **s4_cache = (struct block**) calloc(256,sizeof(struct block*));
  for(i = 0; i < 256; i++)
    s4_cache[i] = (struct block*) calloc(4,sizeof(struct block));
  /*Task 4 End*/

  fprintf(stderr, "sim: ** starting functional simulation **\n");

  /* set up initial default next PC */
  regs.regs_NPC = regs.regs_PC + sizeof(md_inst_t);


  while (TRUE)
    {
      /* maintain $r0 semantics */
      regs.regs_R[MD_REG_ZERO] = 0;
#ifdef TARGET_ALPHA
      regs.regs_F.d[MD_REG_ZERO] = 0.0;
#endif /* TARGET_ALPHA */

      /*Task 1: Start*/
      inst_addr = regs.regs_PC;

      //access direct mapped cache with 6 bits offset, 9 bits index
      dcache_access(dcache,inst_addr,6,9);

      //access 4-way set associative cache with 5 bits offset, 8 bits index  
      setcache_access(set4_cache,inst_addr,5,8,4);

      /*Task 1: End*/

      /*Task 4 Start*/
      setcache_prefetch(s4_cache,inst_addr,5,8,4);
      /*Task 4 End*/

      /* get the next instruction to execute */
      MD_FETCH_INST(inst, mem, regs.regs_PC);

      /* keep an instruction count */
      sim_num_insn++;

      /* set default reference address and access mode */
      addr = 0; is_write = FALSE;

      /* set default fault - none */
      fault = md_fault_none;

      /* decode the instruction */
      MD_SET_OPCODE(op, inst);

      /* execute the instruction */
      switch (op)
	{
#define DEFINST(OP,MSK,NAME,OPFORM,RES,FLAGS,O1,O2,I1,I2,I3)		\
	case OP:							\
          SYMCAT(OP,_IMPL);						\
          break;
#define DEFLINK(OP,MSK,NAME,MASK,SHIFT)					\
        case OP:							\
          panic("attempted to execute a linking opcode");
#define CONNECT(OP)
#define DECLARE_FAULT(FAULT)						\
	  { fault = (FAULT); break; }
#include "machine.def"
	default:
	  panic("attempted to execute a bogus opcode");
      }

      if (fault != md_fault_none)
	fatal("fault (%d) detected @ 0x%08p", fault, regs.regs_PC);

      if (verbose)
	{
	  myfprintf(stderr, "%10n [xor: 0x%08x] @ 0x%08p: ",
		    sim_num_insn, md_xor_regs(&regs), regs.regs_PC);
	  md_print_insn(inst, regs.regs_PC, stderr);
	  if (MD_OP_FLAGS(op) & F_MEM)
	    myfprintf(stderr, "  mem: 0x%08p", addr);
	  fprintf(stderr, "\n");
	  /* fflush(stderr); */
	}

      if (MD_OP_FLAGS(op) & F_MEM)
	{
	  sim_num_refs++;

    /*Task 2 & Task 3 Start*/
	  if (MD_OP_FLAGS(op) & F_LOAD)
    {
      g_total_load++;
      //access 8-way set associative cache with 6 offset bits and 6 index bits
      setcache(set8_cache,addr,6,6,8,0);
      //direct-mapped cache with 6 bit offsets, 9 bits index
      dcache_victim_access(dv_cache,victim_d,addr,6,9,&g_total_load_dmiss);
      //2-way set associative cache with 6 bits offsets, 8 bits index 
      setcache_victim_access(sv2_cache,victim_s2,addr,6,8,2,&g_total_load_s2miss);
      //4-way set associative cache with 6 bits offsets, 7 bits index
      setcache_victim_access(sv4_cache,victim_s4,addr,6,7,4,&g_total_load_s4miss);
    }
    
	  if (MD_OP_FLAGS(op) & F_STORE)
    {
      g_total_store++;
	    is_write = TRUE;
      //access 8-way set associative cache with 6 offset bits and 6 index bits
      setcache(set8_cache,addr,6,6,8,1);
      //direct-mapped cache with 6 bit offsets, 9 bits index
      dcache_victim_access(dv_cache,victim_d,addr,6,9,&g_total_store_dmiss);
      //2-way set associative cache with 6 bits offsets, 8 bits index 
      setcache_victim_access(sv2_cache,victim_s2,addr,6,8,2,&g_total_store_s2miss);
      //4-way set associative cache with 6 bits offsets, 7 bits index
      setcache_victim_access(sv4_cache,victim_s4,addr,6,7,4,&g_total_store_s4miss);
    }
    /*Task 2 & Task 3 End*/

	}


      /* go to the next instruction */
      regs.regs_PC = regs.regs_NPC;
      regs.regs_NPC += sizeof(md_inst_t);

      /* finish early? */
      if (max_insts && sim_num_insn >= max_insts)
	return;
    }
}
