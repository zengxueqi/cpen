Your report goes here.

### Part 1
Set-associative cache may not always increase performance in this case. In fpppp benchmarks, set-associative cache performs worse than directed-map cache. But for most benchmarks, set-associative cache decreases instruction cache miss rate. Set-associative cache can reduce conflict problems. 

### Part 2
Write back ratio is higher than store miss rate but lower than load miss rate. Write back events are triggered by cache line eviction, so benchmarks may keep access same cache index. 

### Part 3
Victim cache drastically decreases the data cache miss rates in all benchmarks using smaller way numbers because victim cache provide extra associativity to for some cache lines. We may access some specific index more often, and fully-associative cache reduces these conflict. The cache line evicted from main cache will be put into victim cache. If there is a victim cache hit, this cache line will be brought back to main cache. This algorithm exploits the temporal locality of cache. If a cache line has been accessed recently, it has higher probability it will be accessed again. 

We can use 4-way set-associative cache and victim cache to achieve similar result when using 8-way set associative cache. So it can reduce the costs of more associativity. 

### Part 4
The prefetch cache decreases the miss rate and the prefetch success rate is very high. The simulator may execute instructions in order so prefetch helps a lot in this case. 


### fpppp benchmark
Fpppp has a highest miss rate on instruction cache. 4 way set-associative instruction cache has a even higher cache miss rate than direct-mapped instruction cache. So fpppp may not keeps access the same cache index with different tags. Since direct-mapped cache has a larger block size and smaller number of cache lines/blocks, it may access instruction address in small range(spatial locality). 

It also has the highest number of load and store instructions among all benchmarks ,and it has the lowest data cache miss rate. This is beacuse it may repeatly access same memory addresses in small range. 

The direct-mapped victim cache has a higher store miss than load miss. Store instruction may keeps access memory in a small range so it has more conflict misses. So set-associative victim cache can drastically decrease store miss rate.

fpppp has the highest prefetch success rate, and prefetch miss rate is lower than direct-mapped and set-associative instruction cache. This is because it always access small number of consecutive instructions(spatial locality). 

### gcc benchmark 
Gcc has a lower instruction cache miss rate but higher data cache miss rate than Fpppp.Gcc has lower number of load and store instruction Gcc may execute instructions in a small loop and execute them sequentially. Set-associative instruction cache has lower miss rate than directed-mapped instruction cache, so the instruction access tends to have more temporal locality in this case.

The write back ratio is lower than load miss, we may have several memory access at same cache index, which causes more cache line evictions and more write back events.

Gcc also has least performance on prefetching, so Gcc may not execute instructions in strict order. It may sometimes jump to instructions abruptly so prefetch won't help in this case. 

### go benchmark 
Go has the best performance in data cache and victim cache. It may access to same memory address repetitively. Go has the highest simulation speed since its cache miss rate is low. Go may always access the same instruction and data address(temporal locality).

Set-associative instruction cache performs worse than directed-mapped instruction cache since Go may have instructions access that always jumps to instruction address within direct-mapped cache block size but larger than set-associative cache block size.

### vpr benchmark 
vpr has the highest data cache miss rate and lowest instruction cache miss rate. It may keep access instruction address in small range, so set-associative instruction cache can decrease the miss rate. It may also access data cache for specifc cache index several times. 

vpr also has the highest write-back ratio, it may keeps refreshing memory. Since we use write-back and write-allocate-policy, it reduces the number of memory refrshes thus increasing the simulation speed. vpr also has a high simulation speed.

Prefetch reduces instruction cache miss rate significantly, which proves that gcc keeps execute instruction in sequential order. 




