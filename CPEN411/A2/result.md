Task 4: 

Task 4 CPI is worse than Task 2 CPI.
Since the clock frequency is doubled at 8-stage pipeline, 8-stage pipeline has better performance than 5-stage. 
But the speed up of 8-stage pipeline is not 2 times as double frequency should results in double speedup.The reason behind this as follows: 

1) If there exists dependency between non-branch instruction at ID/RF stage and load instruction at EX stage, 
  it causes 1 cycle stall for 5-stage pipeline and 3 cycles stall for 8-stage pipeline.
2) If there exists dependency between non-branch instruction at ID/RF stage and load instruction at MEM/DF stage, 
  it causes 0 cycle stall for 5-stage pipeline and 2 cycles stal for 8-stage pipeline.
3) If there exists dependency between non-branch instruction at ID/RF stage and load instruction at WB/DS stage, 
  it causes 0 cycle stall for 5-stage pipeline and 1 cycles stal for 8-stage pipeline.
4) If there exists dependency between branch instruction at ID/RF stage and load instruction at EX stage, 
  it causes 2 cycle stall for 5-stage pipeline and 4 cycles stall for 8-stage pipeline.
5) If there exists dependency between branch instruction at ID/RF stage and load instruction at MEM/DF stage, 
  it causes 1 cycle stall for 5-stage pipeline and 3 cycles stal for 8-stage pipeline.
6) If there exists dependency between branch instruction at ID/RF stage and load instruction at WB/DS stage, 
  it causes 0 cycle stall for 5-stage pipeline and 2 cycles stal for 8-stage pipeline.
7) If there exists dependency between branch instruction at ID/RF stage and non-load instruction at EX stage, 
  it causes 1 cycle stall for 5-stage pipeline and 1 cycles stall for 8-stage pipeline.
8) For branch misprediction, it causes 1 cycle stall for 5-stage pipeline and 2 cycles stall for 8-stage pipeline. 

I use the following table to show different stall cycles for same instructions sequence shown above. 

ID,EX,MEM,WB,Done,5-stage stall cycles,RF,EX,DF,DS,TC,8-stage stall cycles
Non-Branch,Load,NA,NA,1,Non-Branch,Load,NA,NA,NA,3
Non-Branch,NA,Load,NA,0,Non-Branch,NA,Load,NA,NA,2
Non-Branch,NA,NA,Load,0,Non-Branch,NA,NA,Load,NA,1
Branch,Load,NA,NA,2,Branch,Load,NA,NA,NA,4
Branch,NA,Load,NA,1,Branch,NA,Load,NA,NA,3
Branch,NA,NA,Load,0,Branch,NA,NA,Load,NA,2
NA,NA,NA,NA,0,Branch,NA,NA,NA,Load,1
Branch,Non-Load,NA,NA,1,Branch,Non-Load,NA,NA,NA,1

Since we have different stall cycles of same instruction sequence for 5-stage and 8-stage pipeline, the speed-up of task 4 doesn't correspond to double frequency.
8-stage pipeline seems to have more stall cycles than 5-stage pipeline. 

Task 5: 
The performance losing by implementing a realistic mul/div = (Task 5 CPI - Task 4 CPI) / Task 4 CPI = 1 / Task 5 Speed-up - 1
