## TASK 5 IMPLEMENTATION

I combine 2-bit gselect branch predictor and Task 4 gshare branch predictor with 2 bits GBHR. I create BHT with 4 tables(rows) and 2^15 entries(columns). Each entries stores 2 bit saturated counter value. I use middle 15 bits PC xor with 2 bits GBHR to select entries(columns). Then I use the value of GBHR to select which table(row) to use. 

The table entries update and GBHR update are the same as gshare Task 4 branch predictor.
