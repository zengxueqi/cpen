package com.s39236161.CPEN431.A3.CPEN431_2020_A3;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;

import com.google.protobuf.ByteString;

import ca.NetSysLab.ProtocolBuffers.KeyValueResponse;
import ca.NetSysLab.ProtocolBuffers.Message;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
	private ByteString key1 = Helper.GenerateKey();
	private ByteString value1 = Helper.GenerateKey();
	private ByteString key2 = Helper.GenerateKey();
	private ByteString value2 = Helper.GenerateKey();
	private ByteString key3 = Helper.GenerateKey();
	private ByteString value3 = Helper.GenerateKey();
	private ByteString key4 = Helper.GenerateKey();
	private ByteString value4 = Helper.GenerateKey();

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }
    
    public void testApp(){
    	assertTrue(true);
    }
    
//    public void testBasic() throws UnknownHostException, SocketException
//    {	
//    	TestBasic.testWipeOut();
//    	TestBasic.testPut(key1,value1,1);
//    	TestBasic.testGet(key1,value1,1);
//    	TestBasic.testPut(key2,value2,3);
//    	TestBasic.testGet(key2,value2,3);
//    	TestBasic.testGetError(value2);
//    }
//    
//    public void testRemove() throws UnknownHostException, SocketException
//    {
//    	TestBasic.testPut(key1,value1,1);
//    	TestBasic.testPut(key2,value2,3);
//    	TestBasic.testRemove(key1, 0);
//    	TestBasic.testRemove(key1, 1); //remove twice error
//    	TestBasic.testRemove(value1, 1); //error: remove non-existent key
//    	TestBasic.testRemove(key2, 0);
//    	TestBasic.testRemove(value2, 1);
//    }
//    
//    public void testWipeout() throws UnknownHostException, SocketException
//    {
//    	TestBasic.testPut(key3,value3,4);
//    	TestBasic.testGet(key3,value3, 4);
//    	TestBasic.testPut(key4,value4,5);
//    	TestBasic.testGet(key4, value4, 5);
//    	TestBasic.testWipeOut();
//    	TestBasic.testGetError(key3);
//    	TestBasic.testGetError(key4);
//    }
//    
//    public void testEmptyKeyValue() throws UnknownHostException, SocketException
//    {
//    	TestBasic.testPut(ByteString.EMPTY, value3, 2);
//    	TestBasic.testPut(key4, ByteString.EMPTY, 5);
//    	TestBasic.testGet(ByteString.EMPTY,value3, 2);
//    	TestBasic.testGet(key4, ByteString.EMPTY, 5);
//    }
//    
//    public void testPutPut() throws UnknownHostException, SocketException{
//    	TestBasic.testPut(key3, value3, 10);
//    	TestBasic.testPut(key3, value4, 8);
//    	TestBasic.testGet(key3, value4, 8);
//    	
//    	TestBasic.testInvalidCommand(10);
//    	TestBasic.testInvalidCommand(20);
//    }
//    
//    public void testInvalidKeyValue() throws UnknownHostException, SocketException
//    {
//    	TestBasic.testPut(null, value4, 2);
//    	TestBasic.testPut(key4, null, 5);
//    	TestBasic.testGetError(key4);
//    }
//    
//    public void testAdvance() throws UnknownHostException, SocketException{
//    	ByteString key = key3;
//    	ByteString value = value4;
//    	
//    	TestAdvance.testSameMsgID(key, value);
//    	TestBasic.testPut(key, value, 1);
//    }
//    
//    public void testOther() throws UnknownHostException, SocketException{
//    	TestBasic.testGetMembershipCount();
//    	TestBasic.testGetPID();
//    	TestBasic.testIsAlive();
//    }
//    
//    public void testStress() throws UnknownHostException, SocketException{
//    	TestAdvance.testStress();
//    	assertTrue(true);
//    }
//    
//    public void testShutdown() throws UnknownHostException, SocketException{
//    	TestBasic.testShutDown();
//    }
}
