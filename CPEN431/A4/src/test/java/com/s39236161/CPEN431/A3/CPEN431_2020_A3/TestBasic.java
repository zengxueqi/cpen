package com.s39236161.CPEN431.A3.CPEN431_2020_A3;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.zip.CRC32;

import com.google.protobuf.ByteString;

import ca.NetSysLab.ProtocolBuffers.KeyValueResponse;
import ca.NetSysLab.ProtocolBuffers.Message;
import junit.framework.Assert;

public class TestBasic {
	private static int portnum = 3000;

	private static boolean checkMsg(Message.Msg response, ByteString MessageID) {
		ByteString ServerID = response.getMessageID();
		byte[] array = ServerID.concat(response.getPayload()).toByteArray();
		CRC32 checksum = new CRC32();
		checksum.update(array);
		return (MessageID.equals(ServerID) && checksum.getValue() == response.getCheckSum());
	}

	public static void testPut(ByteString key, ByteString value, int version)
			throws UnknownHostException, SocketException {

		ByteString MessageID = Helper.GenerateMessageID();
		byte[] msg;

		if (key == null)
			msg = Helper.GenerateMsg(MessageID, 1);
		else if (value == null)
			msg = Helper.GenerateMsg(MessageID, key, 1);
		else
			msg = Helper.GenerateMsg(MessageID, key, value, version, 1);

		InetAddress address = InetAddress.getByName("localhost");

		byte[] rcvbuf = new byte[2000];
		DatagramSocket socket = new DatagramSocket();

		DatagramPacket sendpacket = new DatagramPacket(msg, msg.length, address, portnum);
		DatagramPacket rcvpacket = new DatagramPacket(rcvbuf, rcvbuf.length);

		int timeout = 100;

		for (int i = 0; i < 4; i++) {
			try {
				socket.send(sendpacket);
				socket.setSoTimeout(timeout);
				socket.receive(rcvpacket);

				byte[] res = new byte[rcvpacket.getLength()];
				System.arraycopy(rcvbuf, 0, res, 0, rcvpacket.getLength());

				// Parse ResponseMessage
				Message.Msg response = Message.Msg.parseFrom(res);

				if (!checkMsg(response, MessageID)) {
					timeout *= 2;
					System.out.println("Packet Corrupted");
				}

				KeyValueResponse.KVResponse reply = KeyValueResponse.KVResponse
						.parseFrom(response.getPayload().toByteArray());

				if (value == null || value.size() > 32)
					Assert.assertTrue(reply.getErrCode() == 7);
				else if (key == null || key.size() > 10000)
					Assert.assertTrue(reply.getErrCode() == 6);
				else
					Assert.assertTrue(reply.getErrCode() == 0);

				System.out.println("Put Error Code is" + reply.getErrCode());
				break;

			} catch (SocketTimeoutException e) {
				timeout *= 2;
				System.out.println("Socket Timeout");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				break;
			}
		}
		socket.close();
	}

	public static void testGet(ByteString key, ByteString value, int version)
			throws UnknownHostException, SocketException {
		ByteString MessageID = Helper.GenerateMessageID();

		byte[] msg = Helper.GenerateMsg(MessageID, key, 2);
		InetAddress address = InetAddress.getByName("localhost");

		byte[] rcvbuf = new byte[2000];
		DatagramSocket socket = new DatagramSocket();

		DatagramPacket sendpacket = new DatagramPacket(msg, msg.length, address, portnum);
		DatagramPacket rcvpacket = new DatagramPacket(rcvbuf, rcvbuf.length);

		int timeout = 100;

		for (int i = 0; i < 4; i++) {
			try {

				socket.send(sendpacket);
				socket.setSoTimeout(timeout);
				socket.receive(rcvpacket);

				byte[] res = new byte[rcvpacket.getLength()];
				System.arraycopy(rcvbuf, 0, res, 0, rcvpacket.getLength());

				// Parse ResponseMessage
				Message.Msg response = Message.Msg.parseFrom(res);
				if (!checkMsg(response, MessageID)) {
					timeout *= 2;
					System.out.println("Packet Corrupted");
					continue;
				}
				KeyValueResponse.KVResponse reply = KeyValueResponse.KVResponse
						.parseFrom(response.getPayload().toByteArray());

				Assert.assertTrue(reply.getErrCode() == 0);
				Assert.assertTrue(value.equals(reply.getValue()));
				Assert.assertTrue(reply.getVersion() == version);

				// System.out.println("Get Error code" + reply.getErrCode());
				break;
			} catch (SocketTimeoutException e) {
				System.out.println("Socket Timeout");
				timeout *= 2;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		socket.close();
	}

	public static void testRemove(ByteString key, int error) throws UnknownHostException, SocketException {
		ByteString MessageID = Helper.GenerateMessageID();

		byte[] msg = Helper.GenerateMsg(MessageID, key, 3);
		InetAddress address = InetAddress.getByName("localhost");

		byte[] rcvbuf = new byte[2000];
		DatagramSocket socket = new DatagramSocket();

		DatagramPacket sendpacket = new DatagramPacket(msg, msg.length, address, portnum);
		DatagramPacket rcvpacket = new DatagramPacket(rcvbuf, rcvbuf.length);
		int timeout = 100;
		for (int i = 0; i < 4; i++) {
			try {
				socket.send(sendpacket);
				socket.setSoTimeout(timeout);
				socket.receive(rcvpacket);

				byte[] res = new byte[rcvpacket.getLength()];
				System.arraycopy(rcvbuf, 0, res, 0, rcvpacket.getLength());

				// Parse ResponseMessage
				Message.Msg response = Message.Msg.parseFrom(res);
				if (!checkMsg(response, MessageID)) {
					timeout *= 2;
					System.out.println("Packet Corrupted");
					continue;
				}
				KeyValueResponse.KVResponse reply = KeyValueResponse.KVResponse
						.parseFrom(response.getPayload().toByteArray());

				Assert.assertTrue(reply.getErrCode() == error);
				break;
				// System.out.println("Remove Error Code: "+
				// reply.getErrCode());
			} catch (SocketTimeoutException e) {
				System.out.println("Socket Timeout");
				timeout *= 2;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		socket.close();
	}

	public static void testWipeOut() throws UnknownHostException, SocketException {
		ByteString MessageID = Helper.GenerateMessageID();

		byte[] msg = Helper.GenerateMsg(MessageID, 5);
		InetAddress address = InetAddress.getByName("localhost");

		byte[] rcvbuf = new byte[2000];
		DatagramSocket socket = new DatagramSocket();

		DatagramPacket sendpacket = new DatagramPacket(msg, msg.length, address, portnum);
		DatagramPacket rcvpacket = new DatagramPacket(rcvbuf, rcvbuf.length);

		int timeout = 100;
		for (int i = 0; i < 4; i++) {
			try {

				socket.send(sendpacket);
				socket.setSoTimeout(timeout);
				socket.receive(rcvpacket);

				byte[] res = new byte[rcvpacket.getLength()];
				System.arraycopy(rcvbuf, 0, res, 0, rcvpacket.getLength());

				// Parse ResponseMessage
				Message.Msg response = Message.Msg.parseFrom(res);
				if (!checkMsg(response, MessageID)) {
					timeout *= 2;
					System.out.println("Packet Corrupted");
					continue;
				}
				KeyValueResponse.KVResponse reply = KeyValueResponse.KVResponse
						.parseFrom(response.getPayload().toByteArray());

				Assert.assertTrue(reply.getErrCode() == 0);
				break;
			} catch (SocketTimeoutException e) {
				System.out.println("Socket Timeout");
				timeout *= 2;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		socket.close();
	}

	public static void testGetError(ByteString key) throws UnknownHostException, SocketException {
		ByteString MessageID = Helper.GenerateMessageID();
		byte[] msg;
		if (key == null)
			msg = Helper.GenerateMsg(MessageID, 2);
		else
			msg = Helper.GenerateMsg(MessageID, key, 2);

		InetAddress address = InetAddress.getByName("localhost");

		byte[] rcvbuf = new byte[2000];
		DatagramSocket socket = new DatagramSocket();

		DatagramPacket sendpacket = new DatagramPacket(msg, msg.length, address, portnum);
		DatagramPacket rcvpacket = new DatagramPacket(rcvbuf, rcvbuf.length);

		int timeout = 100;
		for (int i = 0; i < 4; i++) {
			try {

				socket.send(sendpacket);
				socket.setSoTimeout(timeout);
				socket.receive(rcvpacket);

				byte[] res = new byte[rcvpacket.getLength()];
				System.arraycopy(rcvbuf, 0, res, 0, rcvpacket.getLength());

				// Parse ResponseMessage
				Message.Msg response = Message.Msg.parseFrom(res);
				if (!checkMsg(response, MessageID)) {
					timeout *= 2;
					System.out.println("Packet Corrupted");
					continue;
				}
				KeyValueResponse.KVResponse reply = KeyValueResponse.KVResponse
						.parseFrom(response.getPayload().toByteArray());

				Assert.assertTrue(reply.getErrCode() == 1);
				break;
			} catch (SocketTimeoutException e) {
				System.out.println("Socket Timeout");
				timeout *= 2;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		socket.close();
	}

	public static void testInvalidCommand(int command) throws UnknownHostException, SocketException {
		ByteString MessageID = Helper.GenerateMessageID();

		byte[] msg = Helper.GenerateMsg(MessageID, command);
		InetAddress address = InetAddress.getByName("localhost");

		byte[] rcvbuf = new byte[2000];
		DatagramSocket socket = new DatagramSocket();

		DatagramPacket sendpacket = new DatagramPacket(msg, msg.length, address, portnum);
		DatagramPacket rcvpacket = new DatagramPacket(rcvbuf, rcvbuf.length);

		try {

			socket.send(sendpacket);
			socket.receive(rcvpacket);

			byte[] res = new byte[rcvpacket.getLength()];
			System.arraycopy(rcvbuf, 0, res, 0, rcvpacket.getLength());

			// Parse ResponseMessage
			Message.Msg response = Message.Msg.parseFrom(res);
			KeyValueResponse.KVResponse reply = KeyValueResponse.KVResponse
					.parseFrom(response.getPayload().toByteArray());

			Assert.assertTrue(reply.getErrCode() == 5);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		socket.close();
	}

	public static void testGetMembershipCount() throws UnknownHostException, SocketException {
		ByteString MessageID = Helper.GenerateMessageID();

		byte[] msg = Helper.GenerateMsg(MessageID, 8);
		InetAddress address = InetAddress.getByName("localhost");

		byte[] rcvbuf = new byte[2000];
		DatagramSocket socket = new DatagramSocket();

		DatagramPacket sendpacket = new DatagramPacket(msg, msg.length, address, portnum);
		DatagramPacket rcvpacket = new DatagramPacket(rcvbuf, rcvbuf.length);

		try {

			socket.send(sendpacket);
			socket.receive(rcvpacket);

			byte[] res = new byte[rcvpacket.getLength()];
			System.arraycopy(rcvbuf, 0, res, 0, rcvpacket.getLength());

			// Parse ResponseMessage
			Message.Msg response = Message.Msg.parseFrom(res);
			KeyValueResponse.KVResponse reply = KeyValueResponse.KVResponse
					.parseFrom(response.getPayload().toByteArray());

			Assert.assertTrue(reply.getErrCode() == 0);
			Assert.assertTrue(reply.getMembershipCount() == 1);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		socket.close();
	}

	public static void testGetPID() throws UnknownHostException, SocketException {
		ByteString MessageID = Helper.GenerateMessageID();

		byte[] msg = Helper.GenerateMsg(MessageID, 7);
		InetAddress address = InetAddress.getByName("localhost");

		byte[] rcvbuf = new byte[2000];
		DatagramSocket socket = new DatagramSocket();

		DatagramPacket sendpacket = new DatagramPacket(msg, msg.length, address, portnum);
		DatagramPacket rcvpacket = new DatagramPacket(rcvbuf, rcvbuf.length);

		try {

			socket.send(sendpacket);
			socket.receive(rcvpacket);

			byte[] res = new byte[rcvpacket.getLength()];
			System.arraycopy(rcvbuf, 0, res, 0, rcvpacket.getLength());

			// Parse ResponseMessage
			Message.Msg response = Message.Msg.parseFrom(res);
			KeyValueResponse.KVResponse reply = KeyValueResponse.KVResponse
					.parseFrom(response.getPayload().toByteArray());

			Assert.assertTrue(reply.getErrCode() == 0);
			Assert.assertTrue(reply.hasPid());

			System.out.println("PID is " + reply.getPid());

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		socket.close();
	}

	public static void testShutDown() throws UnknownHostException, SocketException {
		ByteString MessageID = Helper.GenerateMessageID();
		byte[] msg = Helper.GenerateMsg(MessageID, 4);
		InetAddress address = InetAddress.getByName("localhost");

		DatagramSocket socket = new DatagramSocket();
		DatagramPacket sendpacket = new DatagramPacket(msg, msg.length, address, portnum);
		try {
			socket.send(sendpacket);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		socket.close();
	}

	public static void testIsAlive() throws UnknownHostException, SocketException {
		ByteString MessageID = Helper.GenerateMessageID();

		byte[] msg = Helper.GenerateMsg(MessageID, 6);
		InetAddress address = InetAddress.getByName("localhost");
		byte[] rcvbuf = new byte[2000];
		DatagramSocket socket = new DatagramSocket();

		DatagramPacket sendpacket = new DatagramPacket(msg, msg.length, address, portnum);
		DatagramPacket rcvpacket = new DatagramPacket(rcvbuf, rcvbuf.length);

		try {

			socket.send(sendpacket);
			socket.receive(rcvpacket);

			byte[] res = new byte[rcvpacket.getLength()];
			System.arraycopy(rcvbuf, 0, res, 0, rcvpacket.getLength());

			// Parse ResponseMessage
			Message.Msg response = Message.Msg.parseFrom(res);
			KeyValueResponse.KVResponse reply = KeyValueResponse.KVResponse
					.parseFrom(response.getPayload().toByteArray());

			Assert.assertTrue(reply.getErrCode() == 0);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		socket.close();
	}
}
