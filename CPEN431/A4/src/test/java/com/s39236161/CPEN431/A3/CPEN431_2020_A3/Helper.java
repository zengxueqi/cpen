package com.s39236161.CPEN431.A3.CPEN431_2020_A3;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.Random;
import java.util.zip.CRC32;

import com.google.protobuf.ByteString;

import ca.NetSysLab.ProtocolBuffers.KeyValueRequest;
import ca.NetSysLab.ProtocolBuffers.Message;

public class Helper {
    public static int ubyte2int(byte x) {
        return ((int)x) & 0x000000FF;
    }
    
    public static String byteArrayToHexString(byte[] bytes) {
        StringBuffer buf=new StringBuffer();
        String       str;
        int val;

        for (int i=0; i<bytes.length; i++) {
            val = ubyte2int(bytes[i]);
            str = Integer.toHexString(val);
            while ( str.length() < 2 )
                str = "0" + str;
            buf.append( str );
        }
        return buf.toString().toUpperCase();
    }
    
    public static ByteString GenerateKey(){
    	byte[] key = new byte[16];
    	
        //Generate 8 bytes random number 
        Random randomo = new Random();
        byte[] rands = new byte[8];
        randomo.nextBytes(rands);
        
        //The time request was generated 
        ByteBuffer bb = ByteBuffer.allocate(8);
        bb.putLong(System.nanoTime());
        byte[] nanotime = bb.array();
        
        System.arraycopy(rands, 0, key, 0, 8);
        System.arraycopy(nanotime, 0, key, 8, 8);
        
        return ByteString.copyFrom(key);    
    }
    
	 public static ByteString GenerateMessageID() throws UnknownHostException{
	    	byte[] msg = new byte[16];
	        //get clientIP address
	        byte[] clientIP = InetAddress.getLocalHost().getHostAddress().getBytes();
	        byte[] ports = "8989".getBytes();
	        
	        //Generate 2 bytes random number 
	        Random randomo = new Random();
	        byte[] rands = new byte[2];
	        randomo.nextBytes(rands);
	        
	        //The time request was generated 
	        ByteBuffer bb = ByteBuffer.allocate(8);
	        bb.putLong(System.nanoTime());
	        byte[] nanotime = bb.array();
	        
	        //Form Request's unique ID 
	        System.arraycopy(clientIP, 0, msg, 0, 4);
	        System.arraycopy(ports, 0, msg, 4, 2);
	        System.arraycopy(rands, 0, msg, 6, 2);
	        System.arraycopy(nanotime, 0, msg, 8, 8);
	        
	        return ByteString.copyFrom(msg);
	    }
	    
	    public static byte[] GenerateMsg( ByteString MessageID, ByteString key,
	    		int command) throws UnknownHostException{
	    	KeyValueRequest.KVRequest request
	    				= KeyValueRequest.KVRequest.newBuilder().
		    			setCommand(command).
		    			setKey(key).
		    			build();
	    	
	    	ByteString payload = ByteString.copyFrom(request.toByteArray());
	    	
	    	return createMsg(MessageID,payload);
	    }
	    
	    public static byte[] GenerateMsg( ByteString MessageID, ByteString key,
	    		ByteString value,int version, int command) throws UnknownHostException{
	    	KeyValueRequest.KVRequest request
	    				= KeyValueRequest.KVRequest.newBuilder().
		    			setCommand(command).
		    			setKey(key).
		    			setValue(value).
		    			setVersion(version).
		    			build();
	    	
	    	ByteString payload = ByteString.copyFrom(request.toByteArray());
	    	
	    	return createMsg(MessageID,payload);
	    }
	    
	    public static byte[] GenerateMsg( ByteString MessageID,
	    		int command) throws UnknownHostException{
	    	KeyValueRequest.KVRequest request
	    				= KeyValueRequest.KVRequest.newBuilder().
		    			setCommand(command).
		    			build();
	    	
	    	ByteString payload = ByteString.copyFrom(request.toByteArray());
	    	
	    	return createMsg(MessageID,payload);
	    }
	    
	    private static byte[] createMsg(ByteString MessageID,ByteString payload) {
	    	
	    	CRC32 checksum = new CRC32();
	    	checksum.update(MessageID.concat(payload).toByteArray());
	    	
	        Message.Msg msg = 
	        		Message.Msg.newBuilder()
	        		.setMessageID(MessageID)
	        		.setPayload(payload)
	        		.setCheckSum(checksum.getValue())
	        		.build();
	        
	        return msg.toByteArray();
	    }
}
