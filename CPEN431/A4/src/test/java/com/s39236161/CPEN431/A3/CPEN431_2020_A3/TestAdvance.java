package com.s39236161.CPEN431.A3.CPEN431_2020_A3;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import com.google.protobuf.ByteString;

import ca.NetSysLab.ProtocolBuffers.KeyValueResponse;
import ca.NetSysLab.ProtocolBuffers.Message;
import junit.framework.Assert;

public class TestAdvance {
	private static int portnum = 3000;

	public static void testSameMsgID(ByteString key,ByteString value) throws UnknownHostException, SocketException {
		ByteString MessageID = Helper.GenerateMessageID();
		
		byte[] msg = Helper.GenerateMsg(MessageID,key,value,1, 1);

		InetAddress address = InetAddress.getByName("localhost");

		byte[] rcvbuf = new byte[2000];
		DatagramSocket socket = new DatagramSocket();

		DatagramPacket sendpacket = new DatagramPacket(msg, msg.length, address, portnum);
		DatagramPacket rcvpacket = new DatagramPacket(rcvbuf, rcvbuf.length);

		int timeout = 100;
		for(int i = 0; i < 3; i++){
			try {
	
				socket.send(sendpacket);
				socket.setSoTimeout(timeout);
				socket.receive(rcvpacket);
	
				byte[] res = new byte[rcvpacket.getLength()];
				System.arraycopy(rcvbuf, 0, res, 0, rcvpacket.getLength());
	
				// Parse ResponseMessage
				Message.Msg response = Message.Msg.parseFrom(res);
				KeyValueResponse.KVResponse reply = KeyValueResponse.KVResponse
						.parseFrom(response.getPayload().toByteArray());
	
				Assert.assertTrue(reply.getErrCode() == 0);
				
				ByteString key1 = Helper.GenerateKey();
				ByteString value1 = Helper.GenerateKey();
				byte[] msg1 = Helper.GenerateMsg(MessageID, key1,value1,1,1);
				sendpacket = new DatagramPacket(msg1, msg1.length, address, portnum);
				
			} catch (SocketTimeoutException e ){ 
				System.out.println("Socket Timeout");
				timeout *= 2;
			}
			catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return;
			}
		}
		socket.close();
	}
	
	public static void testStress() throws UnknownHostException, SocketException {
		InetAddress address = InetAddress.getByName("localhost");

		DatagramSocket socket = new DatagramSocket();
		byte[] rcvbuf = new byte[2000];
		DatagramPacket rcvpacket = new DatagramPacket(rcvbuf, rcvbuf.length);
		int i = 1;
		for(i = 10; i < 2000003;i++){
			ByteString MessageID = Helper.GenerateMessageID();
			ByteString key = ByteString.copyFrom(Integer.toString(i).getBytes());
			
			byte[] msg = Helper.GenerateMsg(MessageID,key,key,1,1);
			DatagramPacket sendpacket = new DatagramPacket(msg, msg.length, address, portnum);
			try {
				socket.send(sendpacket);
				if( i > 2000000){
					System.out.println("Here");
					socket.setSoTimeout(100);
					socket.receive(rcvpacket);
					
					byte[] res = new byte[rcvpacket.getLength()];
					System.arraycopy(rcvbuf, 0, res, 0, rcvpacket.getLength());
		
					// Parse ResponseMessage
					Message.Msg response = Message.Msg.parseFrom(res);
					KeyValueResponse.KVResponse reply = KeyValueResponse.KVResponse
							.parseFrom(response.getPayload().toByteArray());
		
//					Assert.assertTrue(reply.getErrCode() == 0);
					
					if(reply.getErrCode() != 0){
						System.out.println("Stress Test Failed, Error Code is " + reply.getErrCode());
						System.out.println("Failed at " + i + " Attempts");
						return;
					}
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("Failed at " + i + " Attempts");
				return;
			} 
		}

		socket.close();
	}
	

}
