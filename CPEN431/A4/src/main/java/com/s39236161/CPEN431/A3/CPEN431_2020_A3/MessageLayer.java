package com.s39236161.CPEN431.A3.CPEN431_2020_A3;

import java.util.concurrent.TimeUnit;
import java.util.zip.CRC32;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;

import ca.NetSysLab.ProtocolBuffers.KeyValueRequest;
import ca.NetSysLab.ProtocolBuffers.KeyValueResponse;
import ca.NetSysLab.ProtocolBuffers.KeyValueResponse.KVResponse;
import ca.NetSysLab.ProtocolBuffers.Message;

public class MessageLayer {
//	private Message.Msg message;
	//1MB
	
	private final static Cache<ByteString, KeyValueResponse.KVResponse> cache = CacheBuilder.newBuilder()
			.expireAfterWrite(5, TimeUnit.SECONDS).build();
//	private static final long threshold = (long) 4.5 * 1024 * 1024;
	
//	public static MessageLayer(Message.Msg msg) {
//		message = msg;
//	}
//	
	public static byte[] MessageHandler(Message.Msg message){
		ByteString MessageID = message.getMessageID();
		KeyValueResponse.KVResponse response = cache.getIfPresent(MessageID);

			// No Response Stored in Cache
			if (response == null) {
//				long mem = Runtime.getRuntime().freeMemory();
//				if (mem < threshold) {
//					System.gc();
//					response = KeyValueResponse.KVResponse.newBuilder().setErrCode(3).setOverloadWaitTime(5).build();
//				} else {
					try {
						// Parse PayLoad and Send to PayloadLayer
						KeyValueRequest.KVRequest request = KeyValueRequest.KVRequest
								.parseFrom(message.getPayload().toByteArray());

//						PayloadLayer payload = new PayloadLayer(request);
						response = PayloadLayer.handleKVRequest(request);
						
						// Store response into ResponseCache
						cache.put(MessageID, response);
					} catch (Exception e) {
						System.out.println("Failed to Parse Payload from Msg");
						e.printStackTrace();
						// response =
						// KeyValueResponse.KVResponse.newBuilder().setErrCode(4).build();
					}
//				}

				
			}
			return generateMsg(response,MessageID);
	}
	
//	private boolean hasMemory() {
//		long mem = Runtime.getRuntime().freeMemory();
//		if( mem > threshold ) 
//			return true;
//		System.gc();
//		return false;
//	}
	
	/*
	 * Generate Response Message
	 */
	private static byte[] generateMsg(KeyValueResponse.KVResponse res,ByteString MessageID) {
		CRC32 checksum = new CRC32();
		ByteString response = res.toByteString();
		
		byte[] array = MessageID.concat(response).toByteArray();
		checksum.update(array);
		return Message.Msg.newBuilder().setMessageID(MessageID).setPayload(response)
				.setCheckSum(checksum.getValue()).build().toByteArray();
		
//		return msg.toByteArray();
		
	}

	/*
	 * Check Message CheckSum is Valid
	 */
	public static boolean checksumValid(Message.Msg message) {
		long checksum = message.getCheckSum();
		ByteString MessageID = message.getMessageID();
		ByteString Payload = message.getPayload();

		byte[] array = MessageID.concat(Payload).toByteArray();
		CRC32 reschecksum = new CRC32();
		reschecksum.update(array);
		if (checksum == reschecksum.getValue())
			return true;
		else
			return false;
	}

//	private ByteString getMessageID(){
//		return this.message.getMessageID();
//	}
//	
//	private ByteString getPayload(){
//		return this.message.getPayload();
//	}
}
