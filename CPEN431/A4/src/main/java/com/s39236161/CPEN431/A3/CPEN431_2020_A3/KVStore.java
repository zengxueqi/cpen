package com.s39236161.CPEN431.A3.CPEN431_2020_A3;

import java.util.HashMap;

import com.google.protobuf.ByteString;

public class KVStore {

	public final static HashMap<ByteString,Value> map 
	= new HashMap<ByteString,Value>();

/*
* Clear All Contents in KeyValueStore
*/
public static void clear() {
//synchronized(map){
	map.clear();
//}
}

public static int size(){
	return map.size();
}

/*
* Insert new pairs of <Key,Value> into store
* Return Error Code
*/
public static void put( ByteString key, ByteString value,int version ) {
//synchronized(map){
	Value v = map.get(key);
	if(v != null){
		v.setValue(value, version);
	}
	else{
		v = new Value(value,version);
		map.put(key, v);
	}
//}
}

/*
* Get Value for Specific key
* Return null if no Value Stored for specific key
*/
public static Value get(ByteString key) {
 //synchronized(map){
	 return map.get(key);
// }
}


/*
* Check if KeyValueStore contains specific key
*/
public static boolean containsKey(ByteString key) {
// synchronized(map){
	 return map.containsKey(key);
// }
}

/*
* Remove Value for specific key
* Return null if no Value Stored for specific key
*/
public static Value remove(ByteString key) {
// synchronized(map){
	 return map.remove(key);
// }
}
}
