package com.s39236161.CPEN431.A3.CPEN431_2020_A3;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import ca.NetSysLab.ProtocolBuffers.Message;

/*
 * Comment data = null in APP.java 
 * Remove this keyword 
 * MessageLayer Hasmemory() function 
 * PayloadLayer handleKVRequest return 
 * PayloadLayer 2 * threshold in hasMemeory() function 
 */
public class App {

	private static Message.Msg message;

	public static void main(String[] args) throws SocketException {
		if (args.length != 1) {
			System.out.println("Invalid Args,Please include portnum");
			return;
		}
		DatagramSocket socket = new DatagramSocket(Integer.parseInt(args[0]));
		byte[] buf = new byte[15000];
		DatagramPacket packet = new DatagramPacket(buf, buf.length);
		

		while (true) {
			try {
				// Server Receives A Packet
				socket.receive(packet);
//				byte[] data = ByteString.copyFrom(packet.getData(),0,packet.getLength()).toByteArray();
				byte[] data = new byte[packet.getLength()];
				System.arraycopy(buf, 0, data, 0, packet.getLength());

				try {
					// Parse Message and Send to Message Layer
					message = Message.Msg.parseFrom(data);
					if (MessageLayer.checksumValid(message)) {
						byte[] sendbuf = MessageLayer.MessageHandler(message);
//						System.out.println("Length is: " + MessageLayer.length + "  " + MessageLayer.getLength());
						DatagramPacket sendpacket = new DatagramPacket(sendbuf, sendbuf.length, packet.getAddress(),
								packet.getPort());
						// Send Response back
						socket.send(sendpacket);
//						data = null;
					}
				} catch (InvalidProtocolBufferException e) {
					System.out.println("Invalid Protocol Buffer Exception for Message Object");
					continue; // ignore invalid message object
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out.println("Socket IO Exception");
				e.printStackTrace();
			}

		}

	}
}
/*
 * First jar in Home Folder 
 * Change threshold to 4.5MB 
 * PayloadLayer 2 * threshold 
 */

/*
 * Second jar in A4 folder 
 * Change Structure of MessageLayer,PayloadLayer, Remove new Keyword
 * Put Cache in MessageLayer, KVStore in PayloadLayer  
 */

/*
 * Third Jar in A4/fck  folder 
 * threshold to 4MB,threshold1 to 9MB in PayLoadLayer
 */
