package com.s39236161.CPEN431.A3.CPEN431_2020_A3;

import com.google.protobuf.ByteString;

/*
 * Value Objects that contain value and version number 
 * Used in KeyValueStore Class
 */
public class Value {
	private ByteString value;
	private int version;

	public Value(ByteString newvalue, int newversion) {
		// TODO Auto-generated constructor stub
		value = newvalue;
		version = newversion;
	}

	/*
	 * Get Value of Specific Version
	 */
	public ByteString getValue(int vversion) {
		if (vversion != 0 && version != vversion)
			return null;
		return value;
	}

	/*
	 * Get value of object
	 */
	public ByteString getValue() {
		return value;
	}

	/*
	 * Get version of object
	 */
	public int getVersion() {
		return version;
	}

	/*
	 * Change the value and version of object
	 */
	public void setValue(ByteString newvalue, int newversion) {
		value = newvalue;
		version = newversion;
	}
}
