package com.s39236161.CPEN431.A3.CPEN431_2020_A3;

import java.lang.management.ManagementFactory;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;

import ca.NetSysLab.ProtocolBuffers.KeyValueRequest;
import ca.NetSysLab.ProtocolBuffers.KeyValueResponse;
import ca.NetSysLab.ProtocolBuffers.KeyValueResponse.KVResponse.Builder;

public class PayloadLayer {
	private static final HashMap<ByteString, Value> KVStore = new HashMap<ByteString,Value>();
//	private KeyValueRequest.KVRequest request;
	private static final long threshold1 = (long) 9 * 1024 * 1024;
	private static final long threshold = (long) 4 * 1024 * 1024;

//	private static Builder response = KeyValueResponse.KVResponse.newBuilder().setErrCode(0);
	
//	public PayloadLayer(KeyValueRequest.KVRequest req){
//		request = req;
//	}
	
//	/*
//	 * Get Command of KeyValueRequest
//	 */
//	public int getCommand(){
//		return request.getCommand();
//	}
	
	/*
	 * Get Key of KeyValueRequest 
	 */
//	public ByteString getKey(){
//		if (request.hasKey() )
//			return request.getKey();
//		else 
//			return null;
//	}
//	
//	/*
//	 * Get Value of KeyValueRequest
//	 */
//	public ByteString getValue(){
//		if (request.hasValue())
//			return request.getValue();
//		else 
//			return null;
//	}
//	
//	/*
//	 * Get Version of KeyValueRequest
//	 */
//	public int getVersion(){
//		if (request.hasVersion() )
//			return request.getVersion();
//		else
//			return 0;
//	}
	
	/*
	 * Handle Different Operations of KVRequest
	 */
	public static KeyValueResponse.KVResponse handleKVRequest(KeyValueRequest.KVRequest request){
		ByteString key = request.getKey();
		int error = 0; // error code
		int command = request.getCommand();
		switch (command) {
		// Put
		case 1:
			if (!checkKey(key)) {
				error = 6; // invalid key
				break;
			}
			ByteString value = request.getValue();
			if (!checkValue(value)) {
				error = 7; // invalid value
				break;
			}
			// Out-Of-Memory Error
			if (!hasMemory()) {
				error = 2;
//				System.out.println("Cache size: " + RCache.size() + " KVStore size: " + KVStore.size());
				break;
			}
			Value v = KVStore.get(key);
			if(v != null){
				v.setValue(value, request.getVersion());
			}
			else{
				v = new Value(value,request.getVersion());
				KVStore.put(key, v);
			}
			break;
		// Get
		case 2:
			if (!checkKey(key)) {
				error = 6; // invalid key
				break;
			}

			Value val = KVStore.get(key);
			if (val == null) {
				error = 1;
				break;
			}
//			return response.setValue(val.getValue()).setVersion(val.getVersion()).build();
			return KeyValueResponse.KVResponse.newBuilder().setErrCode(0).setValue(val.getValue())
					.setVersion(val.getVersion()).build();
		// Remove
		case 3:
			if (!checkKey(key)) {
				error = 6; // invalid key
				break;
			}

			if (KVStore.remove(key) == null) {
				error = 1;
			}
			break;
		// ShutDown
		case 4:
			System.exit(0);
//			break;
		// WipeOut
		case 5:
			KVStore.clear();
			break;
		// IsAlive
		case 6:
			break;
		// GetPID
		case 7:
			int pid = getPID();
//			return response.setPid(pid).build()
			return KeyValueResponse.KVResponse.newBuilder().setErrCode(0).setPid(pid).build();
		// GetMembershipCount
		case 8:
//			return response.setMembershipCount(1).build();
			return KeyValueResponse.KVResponse.newBuilder().setErrCode(0).setMembershipCount(1).build();
		default:
			error = 5; // unrecognized command
			break;
		}
		return KeyValueResponse.KVResponse.newBuilder().setErrCode(error).build();
	}
	
	/*
	 * Get Available Heap Memory at Runtime
	 */
//	private boolean hasMemory(int size) {
//		long mem = Runtime.getRuntime().freeMemory();
//		if( size > 1000){
//			if(mem > threshold1)
//				return true;
//			System.gc();
//			return false;
//		}
//		if( mem > threshold ) 
//			return true;
//		System.gc();
//		return false;
//	}
	private static boolean hasMemory() {
		long mem = Runtime.getRuntime().freeMemory();
		if( mem > threshold1 ) 
			return true;
		System.gc();
		return (mem > threshold);
	}

	/*
	 * Get Process ID
	 */
	private static int getPID() {
		String vmName = ManagementFactory.getRuntimeMXBean().getName();
		return Integer.parseInt(vmName.substring(0,vmName.indexOf("@")));
	}

	private static boolean checkKey(ByteString key) {
		if (key == null)
			return false;
		return (key.size() <= 32);
	}

	private static boolean checkValue(ByteString value) {
		if (value == null)
			return false;
		return (value.size() <= 10000);
	}
}
