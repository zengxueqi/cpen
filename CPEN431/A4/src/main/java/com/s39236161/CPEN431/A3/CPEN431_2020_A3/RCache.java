package com.s39236161.CPEN431.A3.CPEN431_2020_A3;

import java.util.concurrent.TimeUnit;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.protobuf.ByteString;

import ca.NetSysLab.ProtocolBuffers.KeyValueResponse;

public class RCache {

	public final static Cache<ByteString, KeyValueResponse.KVResponse> cache = CacheBuilder.newBuilder()
			.expireAfterWrite(5, TimeUnit.SECONDS).build();

	public static int insert(ByteString id, KeyValueResponse.KVResponse response) {
		synchronized (cache) {
			try {
				cache.put(id, response);
				return 0;
			} catch (Exception e) {
				System.out.println("Cache Interal Error");
				return 3;
			}
		}
	}

	public static KeyValueResponse.KVResponse get(ByteString id) {
		synchronized (cache) {
			return cache.getIfPresent(id);
		}
	}
	
	public static KeyValueResponse.KVResponse getIfPresent(ByteString id) {
			return cache.getIfPresent(id);
	}
	
	public static void put(ByteString id, KeyValueResponse.KVResponse response) {
			cache.put(id, response);
	}
	
	public static long size(){
		return cache.size();
	}

}
