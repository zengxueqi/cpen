Name: Xueqi Zeng    
Student ID: 39236161   
Secrete Code: DEC771C47A540478942849AE5DE30094 

# How To Run this Project   
java -jar A2.jar <IP Address> <Port Number> <Student ID>  
java -jar A2.jar 34.209.99.236 43102 39236161  

# Sample Output  
Socket Timeout Reached!!! 100  
Packet Corrupted: ClientID and ServerID not Match. TimeOut: 200  
Packet Corrupted: Incorrect CheckSum TimeOut: 400  
Student ID: 39236161   
Secret Code Length: 16  
Secret Code: DEC771C47A540478942849AE5DE30094  

