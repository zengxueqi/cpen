package com.s39236161.CPEN431.A2.CPEN431_2020_A2;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.Random;
import java.util.zip.CRC32;

import com.google.protobuf.ByteString;
import ca.NetSysLab.ProtocolBuffers.RequestPayload;
import ca.NetSysLab.ProtocolBuffers.ResponsePayload;
import ca.NetSysLab.ProtocolBuffers.Message;

import com.s39236161.CPEN431.A2.CPEN431_2020_A2.StringUtils;

public class App 
{
	
    public static void main( String[] args ) throws IOException
    {
    	   final int MessageIDLength = 16;
    		
	       if (args.length != 3) {
	            System.out.println("Error, wrong argument lengths");
	            return;
	        }
	        
	        //Generate Unique MessageID
	        byte[] msgID = GenerateMessageID(args[1]);
	        ByteString ClientID = ByteString.copyFrom(msgID,0,MessageIDLength);
	        
	        //Create RequestPayload
	        RequestPayload.ReqPayload reqpayload = 
	        		RequestPayload.ReqPayload.newBuilder()
	        		.setStudentID(Integer.parseInt(args[2]))
	        		.build();     
	        byte[] payload = reqpayload.toByteArray();
	        
	        //Create Request Message Byte Array
	        byte[] reqmsg = new byte[MessageIDLength + payload.length];
	        System.arraycopy(msgID, 0, reqmsg, 0, MessageIDLength);
	        System.arraycopy(payload, 0, reqmsg, MessageIDLength, payload.length);
	   
	        CRC32 reqchecksum = new CRC32();
	        reqchecksum.update(reqmsg, 0, MessageIDLength + payload.length);
	        
	        Message.Msg request = 
	        		Message.Msg.newBuilder()
	        		.setMessageID(ClientID)
	        		.setPayload(ByteString.copyFrom(reqmsg, MessageIDLength, payload.length))
	        		.setCheckSum(reqchecksum.getValue())
	        		.build();
	        byte[] buf = request.toByteArray();
	        
	        DatagramSocket socket = new DatagramSocket();
	        InetAddress address = InetAddress.getByName(args[0]);
	        int portnum = Integer.parseInt(args[1]);
	        byte[] rcvbuf = new byte[256];

	        DatagramPacket sendpacket = new DatagramPacket(buf, buf.length, 
	        		address, portnum);
	        DatagramPacket rcvpacket = new DatagramPacket(rcvbuf, rcvbuf.length);
	        
	        int timeout = 100;

	        for (int i = 0; i < 4; i++) {
		        try {
		        	socket.send(sendpacket);
		        	socket.setSoTimeout(timeout);    
			        
		            socket.receive(rcvpacket);
		            byte[] res = new byte[rcvpacket.getLength()];
		            System.arraycopy(rcvbuf, 0, res, 0, rcvpacket.getLength());
		            
		            //Parse ResponseMessage 
		            Message.Msg response = Message.Msg.parseFrom(res);
		            ByteString ServerID = response.getMessageID();
		            long ResCS = response.getCheckSum();
		            
		            //Calculate ReplyMsg Checksum Using CRC32 
		            byte[] array = ServerID.concat(response.getPayload()).toByteArray();
			        CRC32 reschecksum = new CRC32();
			        reschecksum.update(array, 0, array.length);
			        
			        //Check Response ServerID Equals to ClientID and CheckSum is correct
		            if (ServerID.equals(ClientID) && ResCS == reschecksum.getValue()) {            	
		            	//parse secreteKey from responsePayload
		            	ResponsePayload.ResPayload resPayload = 
		            			ResponsePayload.ResPayload.parseFrom(response.getPayload().toByteArray());
		            	byte[] secretKey = resPayload.getSecretKey().toByteArray();
		            	
				        System.out.println("Student ID: " + args[2]);
				        System.out.println("Secret Code Length: " + secretKey.length);
				        System.out.println("Secret Code: " + StringUtils.byteArrayToHexString(secretKey));
		            	
				        return;
		            }
			        else {
			        	if (!ServerID.equals(ClientID)) {
			        		System.out.println("Packet Corrupted: ClientID and ServerID not Match. TimeOut: " + timeout );
			        	}
			        	if (ResCS != reschecksum.getValue())  {
			        		System.out.println("Packet Corrupted: Incorrect CheckSum TimeOut: " + timeout);
			        	}
			        	timeout *= 2;
			        	continue;
			        }
		        } catch (SocketTimeoutException e) {
	                System.out.println("Socket Timeout Reached!!! " + timeout);
	                timeout *= 2;
	                continue;
	            } catch (IOException e) {
		            // TODO Auto-generated catch block
		            e.printStackTrace();
		        }
	        }
	        
	        System.out.println("Failure: Re-try three times, no responses received");
    }
    
    
    public static byte[] GenerateMessageID(String port) throws UnknownHostException{
    	byte[] msg = new byte[16];
        //get clientIP address
        byte[] clientIP = InetAddress.getLocalHost().getHostAddress().getBytes();
        byte[] ports = port.getBytes();
        
        //Generate 2 bytes random number 
        Random randomo = new Random();
        byte[] rands = new byte[2];
        randomo.nextBytes(rands);
        
        //The time request was generated 
        ByteBuffer bb = ByteBuffer.allocate(8);
        bb.putLong(System.nanoTime());
        byte[] nanotime = bb.array();
        
        //Form Request's unique ID 
        System.arraycopy(clientIP, 0, msg, 0, 4);
        System.arraycopy(ports, 0, msg, 4, 2);
        System.arraycopy(rands, 0, msg, 6, 2);
        System.arraycopy(nanotime, 0, msg, 8, 8);
        
        return msg;
    }
    
    
}
