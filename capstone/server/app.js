var express = require('express')
var bodyParser = require('body-parser')
var app = express()
var pg = require('pg');
var inside = require('robust-point-in-polygon');

//change if you want to connect to different database 
var conString = "postgres://postgres:pb107@localhost:5432/postgres";

app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

var client = new pg.Client(conString);

// Database Connection 
client.connect(function(err){
  if(err){
    console.log("Database Connection Failed: " + err.stack);
  } else
    console.log("Connected to Database");
});

 

/*
* Return data whose location are inside polygon area 
*/
function getPolygonData(polygon,data){

  var array = []

  for(i = 0; i < data.length;i++){

    var point = [data[i].longitude,data[i].latitude]
    
    //Check if the point is inside polygon 
    if(inside(polygon,point) < 1)

      array.push(data[i]);

  }

  return array;

}

 

/*
* Draw Polygon to get CanaCode Table Information
* @param: Polygon Coordinates 
*/
app.post('/polygoncana', (req, res) => {

  var polygon = req.body.coordinates[0].geometry.coordinates[0];

  client.query("select * from canacode c, pclatlon p where c.postcode = p.pc",(err,result)=>{
    let array = getPolygonData(polygon,result.rows);

    res.setHeader('Content-Type', 'application/json');
    res.status(200).send(JSON.stringify(array));
  });
});

 

/*
* Get canacode.csv data 
* @ param: cluster ID 
* @ param: Niche ID
*/

app.post('/canacode', (req,res)=>{

  var cluster = req.body.cluster
  var niche = req.body.niche

  if ( cluster == undefined && niche == undefined){
    client.query("select * from canacode ",(err,result)=>{
      // if(err) throw err;
      res.setHeader('Content-Type', 'application/json');
      res.status(200).send(JSON.stringify(result.rows));
    })
  } else if( niche == undefined){
    client.query("select * from canacode where cluster like '%" + cluster +"%'",(err,result)=>{
      // if(err) throw err;
      res.setHeader('Content-Type', 'application/json');
      res.status(200).send(JSON.stringify(result.rows));
    })
  } else if( cluster == undefined){
    client.query("select * from canacode where niche like '%" + niche +"%'",(err,result)=>{
      // if(err) throw err;
      res.setHeader('Content-Type', 'application/json');
      res.status(200).send(JSON.stringify(result.rows));
    })
  } else {
    client.query("select * from canacode where cluster like '%" + cluster + "%' and niche like '%" + niche + "%'",(err,result)=>{
      // if(err) throw err;
      res.setHeader('Content-Type', 'application/json');
      res.status(200).send(JSON.stringify(result.rows));
    })
  }
}); 

 

/*
* Get DayTimePop2019.csv data 
*/
app.post('/daytimepop2019', (req,res)=>{

  client.query("select * from daytimepop2019",(err,result)=>{
    // if(err) throw err;
    res.setHeader('Content-Type', 'application/json');
    res.status(200).send(JSON.stringify(result.rows));
  })
});

 

/*
* Draw Polygon to View Daytimepop2019 Table 
*/
app.post('/polygondaytimepop2019', (req,res)=>{

  var polygon = req.body.coordinates[0].geometry.coordinates[0];
  var query = "select latitude,longitude,code,ecytotpopd,ecydaypop,ecyhomepop,ecyhom014,"
  + "ecyhom1564,ecyhom65p,ecyworkpop,ecywkpusp,ecywkpmob,ecywkphom from daytimepop2019,pclatlon "
  + "where daytimepop2019.code = pclatlon.pc ";

  client.query(query,(err,result)=>{
    // if(err) throw err;
    let array = getPolygonData(polygon,result.rows);
    console.log("pylygondaytimepop2019   length:"+array.length)
    res.setHeader('Content-Type', 'application/json');
    res.status(200).send(JSON.stringify(array));

  })

});

 

/*
* Get DayTimePop2018.csv data 
*/
app.post('/daytimepop2018', (req,res)=>{
  client.query("select * from daytimepop2018",(err,result)=>{
    //if(err) throw err;
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(result.rows));

  })

});

 

/*
* Draw Polygon to View Daytimepop2018 Table 
*/
app.post('/polygondaytimepop2018', (req,res)=>{

  var polygon = req.body.coordinates[0].geometry.coordinates[0];

  var query = "select latitude,longitude,code,ecytotpopd,ecydaypop,ecyhomepop,ecyhom014,"
  + "ecyhom1564,ecyhom65p,ecyworkpop,ecywkpusp,ecywkpmob,ecywkphom from daytimepop2018,pclatlon "
  + "where daytimepop2018.code = pclatlon.pc ";

  client.query(query,(err,result)=>{

    // if(err) throw err;
    let array = getPolygonData(polygon,result.rows);
    console.log("polygondaytimepop2018   length: "+array.length)
    res.setHeader('Content-Type', 'application/json');
    res.status(200).send(JSON.stringify(array));

  })

});

 

app.listen(3001,()=>console.log('Server Running on Port 3001'))