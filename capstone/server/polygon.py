# Python script to check if points is inside polygon 
 
import numpy as np 
import psycopg2
import json,ast 
import sys 
from shapely.geometry import Point 
from shapely.geometry import Polygon
 
connection = psycopg2.connect(user="postgres",
                                password="pb107",
                                host="localhost",
                                port="5432")
cursor = connection.cursor()
query = ("select c.postcode,hh_tot,cluster,niche,latitude,longitude " 
    "from canacode c, pclatlon p where c.postcode = p.pc")
 
cursor.execute(query)
result = cursor.fetchall()
res = []
polygon = Polygon([(-1000,1000),(-1000,-1000),(1000,1000),(1000,-1000)])
 
for row in result:
    point = Point(row[4],row[5])
    if polygon.contains(point):
        res.append(row)
 
print(len(res))
print(res)
 
cursor.close()
connection.close()


