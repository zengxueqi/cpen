var assert = require('assert');
const axios = require('axios')
var inside = require('robust-point-in-polygon');

var polygon = [
  [-123.18316876938498, 49.266546683565],
  [-123.13270032456063, 49.24884496902152],
  [-123.16943585922868, 49.23248204440509],
  [-123.18316876938498, 49.266546683565]
];

 

function testInsidePolygon(data){

  for(i = 0; i < data.length; i++){
    var point = [data[i].longitude,data[i].latitude]
    if(inside(polygon,point) == 1){
      console.log("Result Data not in Polygon")
      console.log("Test in Polygon Failed");
      return 0;
    }
  }

  console.log("Test Inside Polygon Passed");
  return 1;
}

function test1() {

  console.log("Test 1: Test Draw Polygon At Canacode Table ");
  axios.post('http://localhost:3001/polygoncana', {
    coordinates: [
      {                                         
        geometry: {
          coordinates: [polygon], type: 'Polygon'
        }
      }
    ]
  })
    .then((res) => {
      console.log(`Test 1 statusCode: ${res.status}`)
      console.log("Test 1 Received Data Length: " + res.data.length)
      assert(res.status == 200)
      assert(res.data.length == 862);
      console.log("Test 1: Test Inside Polygon");

      testInsidePolygon(res.data)
      console.log("Test 1 Passed");

      test2()
      return 1;

    })

    .catch((error) => {
      console.log("Test 1 Failed");
      console.error(error)
      return 0;
    })
};

 

function test2() {

  console.log("Test 2: Test Draw Polygon At Day Time Population Table 2018 ");
  axios.post('http://localhost:3001/polygondaytimepop2018', {
    coordinates: [
      {
        geometry: {
          coordinates: [polygon], 
        }
      }
    ]
  })

    .then((res) => {
      console.log(`Test 2 statusCode: ${res.status}`)
      console.log("Test 2 Received Data Length: " + res.data.length)
      assert(res.status == 200)
      assert(res.data.length == 850)
      console.log("Test 2: Test Inside Polygon");
      testInsidePolygon(res.data)
      console.log("Test 2 Passed");
      test3();
      return 1;

    })

    .catch((error) => {
      console.log("Test 2 Failed");
      console.error(error)
      return 0;

    })
};

 

function test3() {

  console.log("Test 3: Test Draw Polygon At Day Time Population Table 2019 ");
  axios.post('http://localhost:3001/polygondaytimepop2019', {
    coordinates: [
      {
        geometry: {
          coordinates: [polygon], type: 'Polygon'
        }
      }
    ]
  })
    .then((res) => {
      console.log(`Test 3: statusCode: ${res.status}`)
      console.log("Test 3 Received Data Length: " + res.data.length)
      assert(res.status == 200)
      console.log("Test 3: Test Inside Polygon");
      testInsidePolygon(res.data)
      console.log("Test 3 Passed");

    })
    .catch((error) => {
      console.log("Test 3 Failed");
      console.error(error)
    })
}

test1()