# conda activate capstone 
# conda install <package name>  
import psycopg2
import csv
 
try:
    connection = psycopg2.connect(user="postgres",
                                  password="pb107",
                                  host="localhost",
                                  port="5432")
    cursor = connection.cursor()
 
    with open('csvdata\ePCCF_Unique_201908.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')    
        next(csv_reader)
        for row in csv_reader:
            cursor.execute("INSERT INTO pclatlon VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(row[0],row[1],row[2],row[3],row[5],row[6],row[7],row[37],row[39],row[58],row[66],row[74]))
            connection.commit()
        
except (Exception, psycopg2.Error) as error :
    if(connection):
        print("Failed to insert record into mobile table", error)
 
finally:
    #closing database connection.
    if(connection):
        cursor.close()
        connection.close()
        print("PostgreSQL connection is closed")

