# conda activate capstone 
# conda install <package name>  
import psycopg2
import csv
 
try:
    connection = psycopg2.connect(user="postgres",
                                  password="pb107",
                                  host="localhost",
                                  port="5432")
    cursor = connection.cursor()
    insert_stmt = ("INSERT INTO daytimepop2018 "
                   "(code,geo,ecytotpopd,ecydaypop,ecyhomepop,ecyhom014,"
                   "ecyhom1564,ecyhom65p,ecyworkpop,ecywkpusp,ecywkpmob,ecywkphom,id)"
                   " VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)")
 
    with open('csvdata\DaytimePop2018_GEO.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')    
        next(csv_reader)
        count = 1
        # count = 853287
        # reader = list(csv_reader)
        # print(type(csv_reader))
        # for i in range(853288,len(reader)):
        #     row = reader[i]
        #     row.append(count)
        #     cursor.execute(insert_stmt,row)
        #     connection.commit()
        #     count += 1
        for row in csv_reader:
            row.append(count)
            cursor.execute(insert_stmt,row)
            connection.commit()
            count += 1
 
        cursor.close()
        connection.close()
        print("PostgreSQL connection is closed")
 
except (Exception, psycopg2.Error) as error :
    if(connection):
        print("Failed to insert record into mobile table", error)
 
finally:
    #closing database connection.
    if(connection):
        cursor.close()
        connection.close()
        print("PostgreSQL connection is closed")
