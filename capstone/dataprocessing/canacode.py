# conda activate capstone 
# conda install <package name>  
import psycopg2
import csv
 
try:
    connection = psycopg2.connect(user="postgres",
                                  password="pb107",
                                  host="localhost",
                                  port="5432")
    cursor = connection.cursor()
 
    with open('csvdata\CanaCode_PC.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')    
        next(csv_reader)
        count = 1
        for row in csv_reader:
            row.append(count)
            cursor.execute("INSERT INTO canacode(postcode,hh_tot,cluster,niche,id) VALUES (%s,%s, %s, %s, %s)",row)
            connection.commit()
            count += 1
 
except (Exception, psycopg2.Error) as error :
    if(connection):
        print("Failed to insert record into mobile table", error)
 
finally:
    #closing database connection.
    if(connection):
        cursor.close()
        connection.close()
        print("PostgreSQL connection is closed")


