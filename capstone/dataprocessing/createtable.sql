-- CanaCode_PC.csv 
create table canacode(
    id serial primary key,
    postcode varchar(10),
    hh_tot integer,
    cluster varchar(5),
    niche varchar(10));
-- hh_tot: Total Number of HouseHolds 
-- cluser: Cluster ID
-- niche: Niche ID 
-- For cluster about description, see cluster description in UBC Shared folder
 
-- DayTimePop_2019_GEO.csv
create table daytimepop2018(
    id serial primary key,
    code varchar(20), 
    geo varchar(10),
    ecytotpopd integer, 
    ecydaypop integer,
    ecyhomepop integer,
    ecyhom014 integer,
    ecyhom1564 integer,
    ecyhom65p integer,
    ecyworkpop integer,
    ecywkpusp integer,
    ecywkpmob integer,
    ecywkphom integer);
-- Variable list 
-- CODE         Code
-- GEO          Geographic Summarization Indicator
-- ECYTOTPOPD   Total Household Population
-- ECYDAYPOP    Total Daytime Population
-- ECYHOMEPOP   Total Daytime Population at Home
-- ECYHOM014    Total Daytime Population at Home Aged 0-14
-- ECYHOM1564   Total Daytime Population at Home Aged 15-64
-- ECYHOM65P    Total Daytime Population at Home Aged 65 and Over
-- ECYWORKPOP   Total Daytime Population at Work
-- ECYWKPUSP    Total Daytime Population at Work at Usual Place
-- ECYWKPMOB    Total Daytime Population at Work Mobile
-- ECYWKPHOM    Total Daytime Population at Work at Home
 
-- DayTimePop_2019_GEO.csv
-- same variable list as below 
create table daytimepop2019(
    id serial primary key,
    code varchar(20), 
    geo varchar(10),
    ecytotpopd integer, 
    ecydaypop integer,
    ecyhomepop integer,
    ecyhom014 integer,
    ecyhom1564 integer,
    ecyhom65p integer,
    ecyworkpop integer,
    ecywkpusp integer,
    ecywkpmob integer,
    ecywkphom integer);
 
-- PCCF_Unique2019.csv 
-- convert postcode to latitude and longitude 
create table pclatlon(
    pc varchar(10) primary key,
    latitude float,
    longitude float,
    commname varchar(100),
    retired integer,
    active integer,
    Business integer,
    prnamee varchar(100),
    prabb varchar(100),
    regnamee varchar(100),
    fed13namee varchar(100),
    ernamee varchar(100));
 
-- 1    PC          Postal Code
-- 2    LATITUDE    EA Enhanced Y-Coordinate
-- 3    LONGITUDE   EA Enhanced X-Coordinate
-- 4    COMMNAME    Community Name
-- 6    RETIRED     Retired Postal Code Indicator   Value: 0(no) / 1(yes)
-- 7    ACTIVE      Active Postal Code Indicator    Value: 0(no) / 1(yes)
-- 8    BUSINESS    Business Postal Code Indicator  Value: 0(no) / 1(yes)
-- 38   PRNAMEE     Province (2016) Name (English)
-- 40   PRABB       Province (2016) Abbreviation
-- 59   REGNAMEE    Region (2016) Name (English)
-- 67   FED13NAMEE  Federal Electoral District (2013) Name (English)
-- 75   ERNAMEE     Economic Region (2016) Name (English)



