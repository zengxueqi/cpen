# Postgres Databse Usage   
1) Open SQL Shell 
2) Choose Default Server[localhost], Post[5432],Username[postgres],password[pb107]
3) \q to exit postgres db 
 
Note: if postsql db requires initial password
open pg_hba.conf and change md5 to trust under ipv4 option 
 


# Run Python Script  
open anaconda power shell prompt   
conda activate capstone  
conda install <package name>  
 


# Run Machine Learning Script that need tensorflow library   
conda activate tensorflow_env  
 


# Populate Postgres Database 
cd dataprocessing  
run createtable.sql in sql command shell to create neccessary table in database   
python canacode.py      load CanaCode_PC.csv data into postgres database    
python daytimepop.py    load DaytimePop2018_GEO.csv data into database  
python postcode.py      load ePCCF_Unique_201908.csv into database  
 


# Run Server at Port 3001  
cd server   
node app.js      
var conString = "postgres://YourUserName:YourPassword@localhost:5432/YourDatabase";  
change conString format if you want to connect to different database 



# Run Website AT Port 3000  
cd FrontEnd  
npm i  
npm start   



# Postgres DB Command Cheatsheet  
\d  show all public tables 
\d <table name> show table attributes 




