#include "HSLAPixel.h"

HSLAPixel :: HSLAPixel(){
	s = 0;
	l = 1;
	a = 1;
}

HSLAPixel  :: HSLAPixel(double h,double s,double l){
	this->h = h;
	this->s = s;
	this->l = l;
}

HSLAPixel :: HSLAPixel(double h,double s,double l,double a){
	this->h = h;
	this->s = s;
	this->l = l;
	this->a = a;
}
