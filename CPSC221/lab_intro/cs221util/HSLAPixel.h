#ifndef HSLAPixel_H_
#define HSLAPixel_H_

class HSLAPixel{
public:
  HSLAPixel();
  HSLAPixel(double h,double s,double l);
  HSLAPixel(double h,double s,double l,double a);
  double h,s,l,a;
private:

};

#endif 