/**
 * @file graph_tools.cpp
 * This is where you will implement several functions that operate on graphs.
 * Be sure to thoroughly read the comments above each function, as they give
 *  hints and instructions on how to solve the problems.
 */

#include "graph_tools.h"
#include <algorithm>

/**
 * Finds the minimum edge weight in the Graph graph.
 *
 * @param graph - the graph to search
 * @return the minimum weighted edge
 *
 * @todo Label the minimum edge as "MIN". It will appear blue when
 *  graph.savePNG() is called in minweight_test.
 *
 * @note You must do a traversal.
 * @note You may use the STL stack and queue.
 * @note You may assume the graph is connected.
 *
 * @hint Initially label vertices and edges as unvisited.
 */
int GraphTools::findMinWeight(Graph& graph)
{
	/* your code here! */
	vector<Vertex> v = graph.getVertices();
	for(size_t i = 0; i < v.size(); i++){
		graph.setVertexLabel(v[i],"UNEXPLORED");
		vector<Vertex> adjacent = graph.getAdjacent(v[i]);
		for(size_t j = 0; j < adjacent.size(); j++){
			graph.setEdgeLabel(v[i],adjacent[j],"UNEXPLORED");
		}	
	}

	queue<Vertex> q;
	Vertex v1,v2;
	q.push(v[0]);
	graph.setVertexLabel(v[0],"VISITED");
	v1 = v[0];
	v2 = graph.getAdjacent(v1)[0];
	int weight = graph.getEdgeWeight(v1,v2);
	while(!q.empty()){
		Vertex curr = q.front();
		q.pop();
		vector<Vertex> adjacent = graph.getAdjacent(curr);
		for(size_t i = 0; i < adjacent.size(); i++){
			if(graph.getVertexLabel(adjacent[i]) == "UNEXPLORED"){
				graph.setEdgeLabel(curr,adjacent[i],"DISCOVERY");
				graph.setVertexLabel(adjacent[i],"VISITED");
				q.push(adjacent[i]);			
			}
			if(graph.getVertexLabel(adjacent[i]) == "VISITED"){
				graph.setEdgeLabel(curr,adjacent[i],"CROSS");
			}
			if(graph.getEdgeWeight(curr,adjacent[i]) <= weight){
				weight = graph.getEdgeWeight(curr,adjacent[i]);
				v1 = curr;	
				v2 = adjacent[i];
			}

		}
	}
	graph.setEdgeLabel(v1,v2,"MIN");
	return weight;
}

/**
 * Returns the shortest distance (in edges) between the Vertices
 *  start and end.
 *
 * @param graph - the graph to search
 * @param start - the vertex to start the search from
 * @param end - the vertex to find a path to
 * @return the minimum number of edges between start and end
 *
 * @todo Label each edge "MINPATH" if it is part of the minimum path
 *
 * @note Remember this is the shortest path in terms of edges,
 *  not edge weights.
 * @note Again, you may use the STL stack and queue.
 * @note You may also use the STL's unordered_map, but it is possible
 *  to solve this problem without it.
 *
 * @hint In order to draw (and correctly count) the edges between two
 *  vertices, you'll have to remember each vertex's parent somehow.
 */
int GraphTools::findShortestPath(Graph& graph, Vertex start, Vertex end)
{
	/* your code here! */
	vector<Vertex> v = graph.getVertices();
	for(size_t i = 0; i < v.size(); i++){
		graph.setVertexLabel(v[i],"UNEXPLORED");
		vector<Vertex> adjacent = graph.getAdjacent(v[i]);
		for(size_t j = 0; j < adjacent.size(); j++){
			graph.setEdgeLabel(v[i],adjacent[j],"UNEXPLORED");
		}
	}         
	queue<pair<Vertex,int>> q;
	unordered_map<Vertex,Vertex> predecessor;
	int distance = 0;
	q.push(make_pair(start,0));
	while(!q.empty()){
		pair<Vertex,int> curr = q.front();
		distance = curr.second + 1;
		q.pop();
		graph.setVertexLabel(curr.first, "VISITED");
		vector<Vertex> adjacent = graph.getAdjacent(curr.first);
		for(size_t j = 0; j < adjacent.size(); j++){
			if(adjacent[j] == end){
				predecessor.insert(make_pair(adjacent[j],curr.first));
				goto printMinPath;
			}
			else if(graph.getVertexLabel(adjacent[j]) == "UNEXPLORED"){
				graph.setEdgeLabel(curr.first,adjacent[j],"DISCOVERY");
				graph.setVertexLabel(adjacent[j],"VISITED");
				predecessor.insert(make_pair(adjacent[j],curr.first));
				q.push(make_pair(adjacent[j],distance));
			}
			else if(graph.getVertexLabel(adjacent[j]) == "VISITED")
				graph.setEdgeLabel(curr.first,adjacent[j],"CROSS");
		}
	}
	return -1;
printMinPath:
	Vertex v1 = end;
	while(v1 != start){
		Vertex v2 = predecessor[v1];
		graph.setEdgeLabel(v1,v2,"MINPATH");
		v1 = v2;
	}
	return distance;
}

/**
 * Finds a minimal spanning tree on a graph.
 *
 * @param graph - the graph to find the MST of
 *
 * @todo Label the edges of a minimal spanning tree as "MST"
 *  in the graph. They will appear blue when graph.savePNG() is called.
 *
 * @note Use your disjoint sets class given in dsets.cpp to help you with
 *  Kruskal's algorithm.
 *
 * @note You may call std::sort instead of creating a priority queue.
 */
void GraphTools::findMST(Graph& graph)
{
	vector<Vertex> v = graph.getVertices();
	for(size_t i = 0; i < v.size(); i++){
		graph.setVertexLabel(v[i],"UNEXPLORED");
		vector<Vertex> adjacent = graph.getAdjacent(v[i]);
		for(size_t j = 0; j < adjacent.size(); j++){
			graph.setEdgeLabel(adjacent[j],v[i],"UNEXPLORED");
		}	
	}
	DisjointSets ds;
	ds.addelements(v.size());
	vector<Edge> allEdges = graph.getEdges();
	std::sort(allEdges.begin(),allEdges.end(),allEdges);
	size_t count = 0;
	for(size_t n = 0; n < v.size() && count < allEdges.size(); n++){
	findMin:	
		Edge min = allEdges[count];
		int s = getIndex(v,min.source);
		int d = getIndex(v,min.dest);	
		if(ds.find(s) == ds.find(d) && n != 0){		
			count++;
			goto findMin;
		}
		ds.setunion(ds.find(s),ds.find(d));
		graph.setEdgeLabel(min.source,min.dest,"MST");
		count++;
	}
	/* your code here! */
}

int GraphTools::getIndex(vector<Vertex> v,Vertex a){
	for(size_t i = 0; i < v.size(); i++){
		if(v[i] == a)	
			return i;
	}
		return -1;
}
/**
 * Does a BFS of a graph, keeping track of the minimum
 *  weight edge seen so far.
 * @param g - the graph
 * @param start - the vertex to start the BFS from
 * @return the minimum weight edge
 */
Edge GraphTools::BFS(Graph& graph, Vertex start)
{
	queue<Vertex> q;
	graph.setVertexLabel(start, "VISITED");
	q.push(start);
	Edge min;
	min.weight = INT_MAX;

	while (!q.empty()) {
		Vertex v = q.front();
		q.pop();
		vector<Vertex> adj = graph.getAdjacent(v);
		for (size_t i = 0; i < adj.size(); ++i) {
			if (graph.getVertexLabel(adj[i]) == "UNEXPLORED") {
				graph.setEdgeLabel(v, adj[i], "DISCOVERY");
				graph.setVertexLabel(adj[i], "VISITED");
				q.push(adj[i]);
				int weight = graph.getEdgeWeight(v, adj[i]);
				if (weight < min.weight) {
					min.source = v;
					min.dest = adj[i];
					min.weight = weight;
				}
			} else if (graph.getEdgeLabel(v, adj[i]) == "UNEXPLORED") {
				graph.setEdgeLabel(v, adj[i], "CROSS");
				int weight = graph.getEdgeWeight(v, adj[i]);
				if (weight < min.weight) {
					min.source = v;
					min.dest = adj[i];
					min.weight = weight;
				}
			}
		}
	}
	return min;
}
