#include <cstdlib>
#include <cmath>
#include <iostream>
#include "cs221util/PNG.h"
#include "cs221util/HSLAPixel.h"

using namespace cs221util;
using namespace std;
//HSLAPixel* pixel = new HSLAPixel();
// sets up the output image
PNG* setupOutput(unsigned w, unsigned h) {
    PNG* image = new PNG(w, h);
    return image;
}

// Returns my favorite color
HSLAPixel* myFavoriteColor(double saturation) {
HSLAPixel* pixel = new HSLAPixel(170,saturation,0.5);//dynamic variable, use heap rather than stack   
//HSLAPixel*  pixel(-1, saturation, 0.5);
  //  pixel->s = saturation;
  //  pixel->h = 170;
    //pixel->l = 0.5;
	return pixel;
}

void sketchify(std::string inputFile, std::string outputFile) {
    // Load in.png
// PNG* original = NULL; cannot dereference a null pointer 
    PNG* original = new PNG();
    
    original->readFromFile(inputFile);
    unsigned width = original->width();
    unsigned height = original->height();
    // Create out.png
    PNG* output = setupOutput(width, height);
    // Load our favorite color to color the outline
    HSLAPixel* myPixel = myFavoriteColor(0.5);

    // Go over the whole image, and if a pixel differs from that to its upper
    // left, color it my favorite color in the output
    for (unsigned y = 1; y < height; y++) {
        for (unsigned x = 1; x < width; x++) {
            // Calculate the pixel difference
            HSLAPixel* prev = original->getPixel(x - 1, y - 1);
            HSLAPixel* curr = original->getPixel(x, y);
            double diff = std::fabs(curr->h - prev->h);
		HSLAPixel* temp = output->getPixel(x,y);
            // If the pixel is an edge pixel,
            // color the tput pixel with my favorite color
            if (diff > 20) {
		*temp = *myPixel;//change the position than temp points to 
//		temp = myPixel; change the value of temp, but do not change the value of output
//		 output->getPixel(x,y) = myPixel; cannot assign a value to a function 
		output->getPixel(x,y)->h = myPixel->h;
		output->getPixel(x,y)->s = myPixel->s;
		output->getPixel(x,y)->l = myPixel->l;
		
            }
        }
    }

    // Save the output file
    output->writeToFile(outputFile);

    // Clean up memory
    delete myPixel;
    delete output;
    delete original;
}
