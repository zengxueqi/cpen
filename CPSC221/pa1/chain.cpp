#include "chain.h"
#include "chain_given.cpp"
#include <stdlib.h>   
// PA1 functions you need to implement:

/*
 * Deconstructor: Destory current chain, make sure no memory leak 
 */ 
Chain::~Chain(){ 
	clear();
	head_ = NULL;
	tail_ = NULL;
	length_ = 0;
}

/*
 * Insert a new node at the beginning of the chain
 */
void Chain::insertFront(const Block & ndata){
	Node* newNode = new Node(ndata);
	if(empty()){
		tail_ = head_ = newNode;
	}
	else{
		newNode->next = head_;
		head_->prev = newNode;
		head_ = newNode;
	}
	length_++;
	/*your code here*/
}

/*
 * Insert a new node at the back of the chain
 */
void Chain::insertBack(const Block & ndata){
	/*your code here*/
	Node* newNode = new Node(ndata);
	if(empty()){
		head_ = tail_ = newNode;
	}
	else{
		newNode->prev = tail_;
		tail_->next = newNode;
		tail_ = newNode;
	}
	length_++;
}

/*
 * Return a pointer to the block data at the nth index of the Chain.
 */
const Block* Chain::getBlock(int index) {
	/*your code here*/
	return &(walk(head_,index)->data);
}

/*
 *  Remove the item at the front of the Chain.
 */
void Chain::removeFront() {
	/*your code here*/
	if(empty()){
		return;
	}
	else if(length_ == 1){
		Node* curr = head_;
		delete(curr);
		head_ = NULL;
		tail_ = NULL;
	}
	else{
		Node* prevHead = head_;
		head_ = head_->next;
		head_->prev = NULL;
		if(length_ == 2){
			head_->next = tail_->prev = NULL;
		}
		delete(prevHead);
		prevHead = NULL;
	}
	length_--;
}		

/*
 * Remove the item at the back of the Chain
 */
void Chain::removeBack() {
	if(empty()){
		return;
	}
	else if(length_ == 1){
		Node* curr = tail_;
		delete(curr);
		tail_ = head_ = NULL;
	}
	else{
		Node* prevTail = tail_;
		tail_ = tail_->prev;
		tail_->next = NULL;
		if(length_ == 2){
			head_->next = tail_->prev = NULL;
		}
		delete(prevTail);
		prevTail = NULL;
	}
	length_--;
	/*your code here*/
}

/*
 * Replaces the nth node in the Chain with a new node.
 */
void Chain::replaceBlock(int index, const Block& newB) {
	if(empty() || index >= length_ || index < 0 ){
		return;
	}
	Node* curr = head_;
	Node* newNode = new Node(newB);
	if(length_ == 1){
		head_ = tail_ = newNode;
	}
	else if(index == 0){
		Node* next = curr->next;
		next->prev = newNode;
		newNode->next = next;
		head_ = newNode;
		head_->prev = NULL;
	}
	else if(index == length_ - 1){
		curr = tail_;
		Node* prev = tail_->prev;	
		prev->next = newNode;
		newNode->prev = prev;
		tail_ = newNode;
		tail_->next = NULL;
	}
	else{
		curr = walk(curr,index);
		Node* prev = curr->prev;
		Node* next = curr->next;
		prev->next = newNode;
		newNode->prev = prev;
		newNode->next = next;
		next->prev = newNode;
	}
	delete(curr);
	curr = NULL;
	/*your code here*/
}

/*
 * Swap node at adjacent location, 
 * Assume that index of node1 < index of node 2
 */
void Chain::swap_Adjacent(Node* & node1,Node* & node2){
	Node* prev = node1->prev;
	Node* next = node2->next;
	if(prev){
		prev->next = node2;
		node2->prev = prev;
	}
	else{
		head_ = node2;
		head_->prev = NULL;
	}

	if(next){
		next->prev = node1;
		node1->next = next;
	}
	else{
		tail_ = node1;
		tail_->next = NULL;
	}
	node2->next = node1;
	node1->prev = node2;
}

/*
 * Swap node at non-adjacent position
 */
void Chain::swap_helper(Node* & node1,Node* & node2){
        Node* prev1 = node1->prev;
        Node* next1 = node1->next;
        Node* prev2 = node2->prev;
        Node* next2 = node2->next;
 	if(prev1){
        	prev1->next = node2;
        	node2->prev = prev1;
	}
	else{
		head_ = node2;
		head_->prev = NULL;
	}

	if(next1){
		node2->next = next1;
        	next1->prev = node2;
 	}
	else{
		tail_ = node2;
		tail_->next = NULL;
	}

	if(prev2){
        	prev2->next = node1;
        	node1->prev = prev2;
        } 
	else{
		head_ = node1;
		head_->prev = NULL;
	}

	if(next2){
		node1->next = next2;
        	next2->prev = node1;
	}
	else{
		tail_ = node1;
		tail_->next = NULL;
	}
}

/*
 * Swap the node in pos1 with the node in pos2
 */
void Chain::swap(int pos1, int pos2){
	/*your code here*/
	if(pos1 >= length_ || pos2 >= length_ || pos1 < 0 || pos2 < 0 || pos1 == pos2 || length_ < 2)
		return;
	Node* node1 = head_;
	Node* node2 = head_;
	node1 = walk(node1,pos1);
	node2 = walk(node2,pos2);
	if(abs(pos1 - pos2) == 1){
		if(pos1 < pos2)
			swap_Adjacent(node1,node2);
		else
			swap_Adjacent(node2,node1);
	}
	else{
			swap_helper(node1,node2);
	}
}

/*
 * Helper function: swap 2 nodes between 2 chains 
 */
void Chain::helper(Node* node1, Node* node2,Chain &other){
	 Node* prev1 = node1->prev;
         Node* prev2 = node2->prev;
         Node* next1 = node1->next;
         Node* next2 = node2->next;
         if(!next1){
                 prev1->next = node2;
                 node2->prev = prev1;
                 tail_ = node2;
                 tail_->next = NULL;
         }
         else{   
                 prev1->next = node2;
                 node2->prev = prev1;
                 next1->prev = node2;
                 node2->next = next1;
         }       
         if(!next2){
                 prev2->next = node1;
                 node1->prev = prev2;
                 other.tail_ = node1;
                 other.tail_->next = NULL;
         }
         else{
                 prev2->next = node1;
                 node1->prev = prev2;
                 next2->prev = node1;
                 node1->next = next2;
         }
}

/*
 * Swap the nodes in every second locations between the current Chain and the other Chain.
 */
void Chain::checkeredSwap(Chain &other) {
	/*your code here*/
	if(other.length_ < 2 || length_ < 2 || other.length_ != length_) 
		return;
	for(int i = 1; i < min(length_,other.length_);i += 2){
		Node* node1 = walk(head_,i);
		Node* node2 = walk(other.head_,i);
		helper(node1,node2,other);
	}
}

void Chain::moveToBack(int startPos, int len){
	if(startPos + len >= length_ || startPos < 0 || len <= 0 ||
	 startPos >= length_ - 1 || len >= length_ || empty())
		return;
	Node* start = head_;
	start = walk(start,startPos);//start of chain to be moved 
	Node* end = head_;
	end = walk(end,startPos + len - 1);//end of the chain to be moved 
	if(startPos == 0){ //update head of the chain 
		Node* newHead = end->next;
		head_ = newHead;
		head_->prev = NULL;
	}
	else{
		Node* prev = start->prev;
		Node* next = end->next;
		prev->next = next;
		next->prev = prev;
	}
	start->prev = tail_;
	tail_->next = start;
	tail_ = end;
	tail_->next = NULL;	
	/*your code here*/
}

/*
 * Move k nodes from the front of the Chain to the end
 */
void Chain::rotate(int k){
	/*your code here*/
	if(k <= 0 || k >= length_ || empty() || length_ <= 1)
		return;
	moveToBack(0,k);
}


