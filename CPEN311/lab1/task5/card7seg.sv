module card7seg(input [3:0] SW, output [6:0] HEX0);
   // your code goes here
	reg [6:0] out;
	assign HEX0 = out;
	always @(*)
		case(SW)
			4'd1: out = 7'b000_1000;
			4'd2: out = 7'b010_0100;
			4'd3: out = 7'b011_0000;
			4'd4: out = 7'b001_1001;
			4'd5: out = 7'b001_0010;
			4'd6: out = 7'b0000_010;
			4'd7: out = 7'b1111_000;
			4'd8: out = 7'b0;
			4'd9: out = 7'b0010_000;
			4'd10: out = 7'b1000_000;
			4'd11: out = 7'b110_0001;
			4'd12: out = 7'b001_1000;
			4'd13: out = 7'b000_1001;
			default: out = 7'b111_1111;
		endcase
endmodule

