module tb_statemachine();

// Your testbench goes here. Make sure your tests exercise the entire design
// in the .sv file.  Note that in our tests the simulator will exit after
// 10,000 ticks (equivalent to "initial #10000 $finish();").
logic slow_clock;
logic resetb;
logic [3:0] dscore;
logic [3:0] pscore;
logic [3:0] pcard3;
logic load_pcard1;
logic load_pcard2;
logic load_pcard3;
logic load_dcard1;
logic load_dcard2;
logic load_dcard3;
logic player_win_light;
logic dealer_win_light;

statemachine dut(.*);

wire [7:0] test = {load_pcard1,load_pcard2,load_pcard3,load_dcard1,load_dcard2,load_dcard3,player_win_light,dealer_win_light};

initial begin 
    slow_clock = 0;
    forever #5 slow_clock = ~slow_clock;
end 

initial begin 
    resetb = 1'b0;
    forever #150 resetb = ~resetb;
end

initial begin 
    #150;//reset
    
    /*Test1: player and dealer do not pick up 3rd card,player wins*/
    pscore = 4'd8;
    dscore = 4'd2;
    assert(load_pcard1 === 1'b1);//P1
    assert(test === 8'b100_000_00);
    #10;
    assert(load_dcard1 === 1'b1);//D1
    assert(test === 8'b000_100_00);
    #10;
    assert(load_pcard2 === 1'b1);//P2
    assert(test === 8'b010_000_00);
    #10;
    assert(load_dcard2 === 1'b1);//D2
    assert(test === 8'b000_010_00);
    #10;
    assert(player_win_light === 1'b1 && dealer_win_light === 1'b0);//Finish
    assert(test === 8'b10);
    #60;
    assert(test === 8'b10);
    #200;

    /*Test2: player and dealer do not pick up 3rd card,dealer wins*/
    pscore = 4'd8;
    dscore = 4'd9;
    assert(load_pcard1 === 1'b1);//P1
    assert(test === 8'b100_000_00);
    #10;
    assert(load_dcard1 === 1'b1);//D1
    assert(test === 8'b000_100_00);
    #10;
    assert(load_pcard2 === 1'b1);//P2
    assert(test === 8'b010_000_00);
    #10;
    assert(load_dcard2 === 1'b1);//D2
    assert(test === 8'b000_010_00);
    #10;
    assert(player_win_light === 1'b0 && dealer_win_light === 1'b1);//Finish
    assert(test === 8'b01);
    #60;
    assert(test === 8'b01);
    #200;

    /*Test3: player and dealer pick up 3rd card*/
    pscore = 4'd5;
    dscore = 4'd6;
    pcard3 = 4'd6;
    assert(load_pcard1 === 1'b1);//P1
    assert(test === 8'b100_000_00);
    #10;
    assert(load_dcard1 === 1'b1);//D1
    assert(test === 8'b000_100_00);
    #10;
    assert(load_pcard2 === 1'b1);//P2
    assert(test === 8'b010_000_00);
    #10;
    assert(load_dcard2 === 1'b1);//D2
    assert(test === 8'b000_010_00);
    #10;
    assert(load_pcard3 === 1'b1 && load_dcard3 === 1'b0);//P3
    assert(test === 8'b001_000_00);
    #5;
    pscore = 4'd1;
    #5;
    assert(load_dcard3 === 1'b1 && load_pcard3 === 1'b0);//D3
    assert(test === 8'b000_001_00);
    #5;
    dscore = 4'd6;
    #5;
    assert(dealer_win_light === 1'b1 && player_win_light === 1'b0);//Finish
    assert(test === 8'b01);
    #40;
    assert(test === 8'b01);
    #200;

    /*Test4: only player picks up 3rd card*/
    pscore = 4'd3;
    dscore = 4'd4;
    pcard3 = 4'd1;
    assert(load_pcard1 === 1'b1);//P1
    assert(test === 8'b100_000_00);
    #10;
    assert(load_dcard1 === 1'b1);//D1
    assert(test === 8'b000_100_00);
    #10;
    assert(load_pcard2 === 1'b1);//P2
    assert(test === 8'b010_000_00);
    #10;
    assert(load_dcard2 === 1'b1);//D2
    assert(test === 8'b000_010_00);
    #10;
    assert(load_pcard3 === 1'b1 && load_dcard3 === 1'b0);//P3
    assert(test === 8'b001_000_00);
    #5;
    pscore = 4'd4;
    #5;
    assert(dealer_win_light === 1'b1 && player_win_light === 1'b1);//Finish
    assert(test === 8'b11);
    #50;
    assert(test === 8'b11);
    #200;

    /*Test5: both player and dealer can't pick up 3rd card*/
    pscore = 4'd7;
    dscore = 4'd6;
    assert(load_pcard1 === 1'b1);//P1
    assert(test === 8'b100_000_00);
    #10;
    assert(load_dcard1 === 1'b1);//D1
    assert(test === 8'b000_100_00);
    #10;
    assert(load_pcard2 === 1'b1);//P2
    assert(test === 8'b010_000_00);
    #10;
    assert(load_dcard2 === 1'b1);//D2
    assert(test === 8'b000_010_00);
    #10;
    assert(player_win_light === 1'b1 && dealer_win_light === 1'b0);//Finish
    assert(test === 8'b10);
    #60;
    assert(test === 8'b10);
    #200;

    /*Test6: only dealer picks up 3rd card*/
    pscore = 4'd7;
    dscore = 4'd4;
    assert(load_pcard1 === 1'b1);//P1
    assert(test === 8'b100_000_00);
    #10;
    assert(load_dcard1 === 1'b1);//D1
    assert(test === 8'b000_100_00);
    #10;
    assert(load_pcard2 === 1'b1);//P2
    assert(test === 8'b010_000_00);
    #10;
    assert(load_dcard2 === 1'b1);//D2
    assert(test === 8'b000_010_00);
    #10;
    assert(load_dcard3 === 1'b1 && load_pcard3 === 1'b0);//D3
    assert(test === 8'b000_001_00);
    #5;
    dscore = 4'd7;
    #5;
    assert(dealer_win_light === 1'b1 && player_win_light === 1'b1);//Finish
    assert(test === 8'b11);
    #50;
    assert(test === 8'b11);
    #200;

    /*Test7: player picks up 3rd card and wins,dealer can't pick up 3rd card  */
    pscore = 4'd5;
    dscore = 4'd4;
    pcard3 = 4'd3;
    assert(load_pcard1 === 1'b1);//P1
    assert(test === 8'b100_000_00);
    #10;
    assert(load_dcard1 === 1'b1);//D1
    assert(test === 8'b000_100_00);
    #10;
    assert(load_pcard2 === 1'b1);//P2
    assert(test === 8'b010_000_00);
    #10;
    assert(load_dcard2 === 1'b1);//D2
    assert(test === 8'b000_010_00);
    #10;
    assert(load_pcard3 === 1'b1 && load_dcard3 === 1'b0);//P3
    assert(test === 8'b001_000_00);
    #5;
    pscore = 4'd8;
    #5;
    assert(player_win_light === 1'b1 && dealer_win_light === 1'b0);//Finish
    assert(test === 8'b10); 
    #50;
    assert(test === 8'b10); 
    $stop;
end

endmodule

