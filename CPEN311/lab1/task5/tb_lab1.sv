`define n0 7'b111_1111
`define n2 7'b010_0100
`define n3 7'b011_0000
`define n5 7'b001_0010
`define n8 7'b0
`define n10 7'b1000_000
`define n13 7'b000_1001
module tb_lab1();

// Your testbench goes here. Make sure your tests exercise the entire design
// in the .sv file.  Note that in our tests the simulator will exit after
// 100,000 ticks (equivalent to "initial #100000 $finish();").
logic CLOCK_50;
logic [3:0] KEY;
logic [9:0] LEDR;
logic [6:0] HEX5;
logic [6:0] HEX4;
logic [6:0] HEX3;
logic [6:0] HEX2;
logic [6:0] HEX1;
logic [6:0] HEX0;

lab1 dut(.*);

initial begin
    KEY[0] = 1'b0;
    forever #5 KEY[0] = ~KEY[0];
end

initial begin
    CLOCK_50 = 1'b0;
    forever #1 CLOCK_50 = ~CLOCK_50;
end

initial begin 
    KEY[3] = 1'b0;
    #10;
    KEY[3] = 1'b1;
    assert(HEX0 === `n0 && HEX1 === `n0 && HEX2 === `n0 && HEX3 === `n0 && HEX4 === `n0 && HEX5 === `n0);//test reset 
    assert(LEDR === 10'b0);
    #10;
    assert(HEX0 === `n3 && HEX1 === `n0 && HEX2 === `n0 && HEX3 === `n0 && HEX4 === `n0 && HEX5 === `n0);//load pcard1
    assert(LEDR === 10'b11);
    #10;
    assert(HEX0 === `n3 && HEX1 === `n0 && HEX2 === `n0 && HEX3 === `n8 && HEX4 === `n0 && HEX5 === `n0);//load dcard1
    assert(LEDR === 10'b0010_0000_11);
    #10;
    assert(HEX0 === `n3 && HEX1 === `n13 && HEX2 === `n0 && HEX3 === `n8 && HEX4 === `n0 && HEX5 === `n0);//load pcard2
    assert(LEDR === 10'b0010_0000_11);
    #10;
    assert(HEX0 === `n3 && HEX1 === `n13 && HEX2 === `n0 && HEX3 === `n8 && HEX4 === `n5 && HEX5 === `n0);//load dcard2
    assert(LEDR === 10'b0000_1100_11);
    #10;
    assert(HEX0 === `n3 && HEX1 === `n13 && HEX2 === `n10 && HEX3 === `n8 && HEX4 === `n5 && HEX5 === `n0);//load pcard3
    assert(LEDR === 10'b0000_1100_11);
    #10;
    assert(HEX0 === `n3 && HEX1 === `n13 && HEX2 === `n10 && HEX3 === `n8 && HEX4 === `n5 && HEX5 === `n2);//load dcard3
    assert(LEDR === 10'b1001_0100_11);
    #20;
    assert(LEDR === 10'b1001_0100_11);
    $stop;
end 

endmodule

