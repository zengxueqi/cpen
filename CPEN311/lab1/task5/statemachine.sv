`define Start 4'b0111 
`define P1 4'b0001 
`define D1 4'b0010 
`define P2 4'b0100 
`define P3 4'b1101
`define D3 4'b1110 
`define D2 4'b1100
`define Finish 4'b0000

module statemachine(input slow_clock, input resetb,
                    input [3:0] dscore, input [3:0] pscore, input [3:0] pcard3,
                    output load_pcard1, output load_pcard2,output load_pcard3,
                    output load_dcard1, output load_dcard2, output load_dcard3,
                    output player_win_light, output dealer_win_light);
wire [3:0] present_state, state_next_reset, state_next;
reg [11:0] next;
wire [2:0] canwin;//decide who can win the game 
wire canpickD3;//decide dealer can pick dcard3 after the player pick up pcard3

wire canpickP3 = (pscore <= 5) ? 1 : 0;//decide player can pick up pcard3
wire takeD3 = (dscore <= 5) ? 1 : 0;//decide dealer can pick dcard3
pickD3 fck(dscore,pcard3,canpickD3); //decide dealer can pick dcard3 after the player pick up pcard3
win fuc(dscore,pscore,canwin);

//following code is state machine 
vDFF #(4) STATE(slow_clock,state_next_reset,present_state);
assign state_next_reset = resetb ? state_next : `Start;

    always @(*) begin //     3          1       1       1
        casex({present_state,canwin,canpickP3,canpickD3,takeD3})
            //dealer and player get first 2 cards
            {`Start,6'bx}: next = {`P1,3'b100,3'b000,2'b0};
            {`P1,6'bx}: next = {`D1,3'b000,3'b100,2'b0};
            {`D1,6'bx}: next = {`P2,3'b010,3'b000,2'b0};
            {`P2,6'bx}: next = {`D2,3'b000,3'b010,2'b0};
           
            //dealer or player can win after they get 2 cards
            {`D2,3'b011,3'bx}: next = {`Finish,3'b0,3'b0,2'b11};
            {`D2,3'b010,3'bx}: next = {`Finish,3'b0,3'b0,2'b10};
            {`D2,3'b001,3'bx}: next = {`Finish,3'b0,3'b0,2'b01};
            
            //player picks pcard3 if pscore <= 5, set load_pcard3 = 1
            {`D2,3'bx,3'b1xx}: next = {`P3,3'b001,3'b000,2'b0};
            
            //player can win after picking up pcard3
            {`P3,3'b010,3'bx}: next = {`Finish,3'b0,3'b0,2'b10};
            
            //dealer can pick up dcard3 after player picks pcard3,set load_dcard3 = 1
            {`P3,3'bx,3'bx1x}: next = {`D3,3'b0,3'b001,2'b0};
            
            //dealer picks dcard3 when pscore = 6 or 7 and dscore <= 5
            {`D2,3'bx,3'bxx1}: next = {`D3,3'b0,3'b001,2'b0};
            
            //game is over, decide who can win
            {4'b11xx,3'bx11,3'bx}: next = {`Finish,6'b0,2'b11};
            {4'b11xx,3'bx01,3'bx}: next = {`Finish,6'b0,2'b01};
            {4'b11xx,3'bx10,3'bx}: next = {`Finish,6'b0,2'b10};
            {`Finish,3'bx11,3'bx}: next = {`Finish,6'b0,2'b11};
            {`Finish,3'bx01,3'bx}: next = {`Finish,6'b0,2'b01};
            {`Finish,3'bx10,3'bx}: next = {`Finish,6'b0,2'b10};
        endcase
    end

    assign {state_next,load_pcard1,load_pcard2,load_pcard3,load_dcard1,load_dcard2,load_dcard3,
        player_win_light,dealer_win_light} = next;
endmodule

/*
 * D filp-flop for state machine state changes  
 */
module vDFF(clk,in,out);
parameter n = 4;
input clk;
input [n-1:0] in;
output reg [n-1:0] out;
    always @(posedge clk)
        out <= in;
endmodule

    /*
    * deside who wins the game
    * result = 3'bx10: player wins
    * result = 3'bx01: dealer wins 
    * result = 3'bx11: a tie 
    */
module win(input [3:0] dscore, input [3:0] pscore,output reg [2:0] result);
   always @(*) begin 
       if(dscore == 4'd8 || dscore == 4'd9 || pscore == 4'd8 || pscore == 4'd9) begin
           if(pscore > dscore)
               result = 3'b010;
           else if(pscore < dscore)
               result = 3'b001;
           else 
               result = 3'b011;
       end
       else begin
           if(pscore > dscore)
               result = 3'b110;
           else if(pscore < dscore)
               result = 3'b101;
           else 
               result = 3'b111;
       end 
   end 
endmodule 

/*
 * Check if the dealer can pick up the third card after player picks up pcard3 
 * result = 1 means dealer can pick up the third card 
 */
module pickD3(input [3:0] dscore, input [3:0] pcard3,output reg result);
  always @(*) begin 
      if(dscore == 4'd6 && (pcard3 == 4'd6 || pcard3 == 4'd7))
          result = 1'b1;
      else if(dscore == 4'd5 && pcard3 >= 4'd4 && pcard3 <= 4'd7)
          result = 1'b1;
      else if(dscore == 4'd4 && pcard3 >= 4'd2 && pcard3 <= 4'd7)
          result = 1'b1;
      else if (dscore == 4'd3 && pcard3 != 4'd8)
          result = 1'b1;
      else if(dscore == 4'd0 || dscore == 4'd1 || dscore == 4'd2)
          result = 1'b1;
      else 
          result = 1'b0;
  end
endmodule

