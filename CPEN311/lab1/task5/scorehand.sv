module scorehand(input [3:0] card1, input [3:0] card2, input [3:0] card3, output [3:0] total);

// The code describing scorehand will go here.  Remember this is a combinational
// block. The function is described in the handout.  Be sure to review the section
// on representing numbers in the lecture notes.
wire [3:0] newcard1,newcard2,newcard3;
adjust_card C1(card1,newcard1);
adjust_card C2(card2,newcard2);
adjust_card C3(card3,newcard3);
assign total = ((newcard1 + newcard2 + newcard3) % 10);
endmodule

/*
 * Change Tens, Jacks, Queens, and Kings cards to value 0, otherwise it is the card value 
 */
module adjust_card(input [3:0] card,output reg [3:0] newcard);
	always @(*) begin
		if(card < 4'd10)
			newcard = card;
		else
			newcard = 4'b0;
	end
endmodule 
