module tb_scorehand();

// Your testbench goes here. Make sure your tests exercise the entire design
// in the .sv file.  Note that in our tests the simulator will exit after
// 10,000 ticks (equivalent to "initial #10000 $finish();").
logic [3:0] card1;
logic [3:0] card2;
logic [3:0] card3;
logic [3:0] total;	

scorehand dut(.*);

initial begin
    card1 = 4'd2;
    card2 = 4'd5;
    card3 = 4'd0;
    #10;
    assert(total === 4'd7);
    card1 = 4'd3;
    card2 = 4'd5;
    card3 = 4'd5;
    #10;
    assert(total === 4'd3);
    card1 = 4'd12;
    card2 = 4'd14;
    card3 = 4'd1;
    #10;
    assert(total === 4'd1);
    $stop;
end

endmodule

