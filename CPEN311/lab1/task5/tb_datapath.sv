`define N3 7'b011_0000
`define check 7'b111_1111
module tb_datapath();

// Your testbench goes here. Make sure your tests exercise the entire design
// in the .sv file.  Note that in our tests the simulator will exit after
// 10,000 ticks (equivalent to "initial #10000 $finish();").
logic slow_clock; 
logic fast_clock;
logic resetb;
logic load_pcard1;
logic load_pcard2;
logic load_pcard3;
logic load_dcard1;
logic load_dcard2;
logic load_dcard3;
logic [3:0] pcard3_out;
logic [3:0] pscore_out;
logic [3:0] dscore_out;
logic [6:0] HEX5;
logic [6:0] HEX4;
logic [6:0] HEX3;
logic [6:0] HEX2;
logic [6:0] HEX1;
logic [6:0] HEX0;

datapath dut(.*);

initial begin 
    slow_clock = 1'b0;
    forever #5 slow_clock = ~slow_clock;
end

initial begin 
    fast_clock = 1'b0;
    forever #1 fast_clock = ~fast_clock;
end 

initial begin
    resetb = 1'b0;
    forever #10 resetb = ~resetb;
end

initial begin 
    #10;//reset

    /*Test1: load pcard1*/
    load_pcard1 = 1'b1;
    #10;
    assert(pscore_out === 4'd3 && HEX0 === `N3);
    assert(HEX1 === `check && HEX2 === `check && HEX3 === `check && HEX4 === `check && HEX5 === `check && dscore_out === 4'd0);
    load_pcard1 = 1'b0;
    #10;

    /*Test2: load dcard1*/
    load_dcard1 = 1'b1;
    #10;
    assert(dscore_out === 4'd3 && HEX3 === `N3);
     assert(HEX1 === `check && HEX2 === `check && HEX0 === `check && HEX4 === `check && HEX5 === `check && pscore_out === 4'd0);
    load_dcard1 = 1'b0;
    #10;
 
    /*Test3: load pcard2*/   
    load_pcard2 = 1'b1;
    #10;
    assert(pscore_out === 4'd3 && HEX1 === `N3);
    assert(HEX0 === `check && HEX2 === `check && HEX3 === `check && HEX4 === `check && HEX5 === `check && dscore_out === 4'd0);
    load_pcard2 = 1'b0;
    #10;

    /*Test4: load dcard2*/    
    load_dcard2 = 1'b1;
    #10;
    assert(dscore_out === 4'd3 && HEX4 === `N3);
    assert(HEX1 === `check && HEX2 === `check && HEX3 === `check && HEX0 === `check && HEX5 === `check && pscore_out === 4'd0);
    load_dcard2 = 1'b0;
    #10;

    /*Test5: load dcard3*/    
    load_dcard3 = 1'b1;
    #10;
    assert(dscore_out === 4'd3 && HEX5 === `N3);
    assert(HEX1 === `check && HEX2 === `check && HEX3 === `check && HEX4 === `check && HEX0 === `check && pscore_out === 4'd0);
    load_dcard3 = 1'b0;
    #10;
 
    /*Test6: load pcard3*/   
    load_pcard3 = 1'b1;
    #10;
    assert(pscore_out === 4'd3 && HEX2 === `N3 && pcard3_out === 4'd3);
    assert(HEX1 === `check && HEX0 === `check && HEX3 === `check && HEX4 === `check && HEX5 === `check && dscore_out === 4'd0);
    load_pcard3 = 1'b0;
    #10;
    
    /*Test7: load pcard1,pcard2,pcard3,dcard1,dcard2,dcard3*/
    load_pcard1 = 1'b1;
    load_pcard2 = 1'b1;
    load_pcard3 = 1'b1;
    load_dcard1 = 1'b1;
    load_dcard2 = 1'b1;
    load_dcard3 = 1'b1;
    #10;
    assert(pscore_out === 4'd9 && pcard3_out === 4'd3 && dscore_out === 4'd9);
    assert(HEX0 === `N3 && HEX1 === `N3 && HEX2 === `N3 && HEX4 === `N3 && HEX5 === `N3 && HEX3 === `N3);
    $stop;
end

endmodule

