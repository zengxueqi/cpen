module tb_card7seg();
logic [3:0] SW;
logic [6:0] HEX0;

card7seg dut(.*);

	initial begin
		SW = 4'd0;
		#10;
        assert(HEX0 === 7'b111_1111);
		SW = 4'd1;
		#10;
        assert(HEX0 == 7'b000_1000);
		SW = 4'd2;
		#10;
        assert(HEX0 === 7'b010_0100);
        SW = 4'd3;
        #10;
        assert(HEX0 === 7'b011_0000);
        SW = 4'd4;
        #10;
        assert(HEX0 === 7'b001_1001);
        SW = 4'd5;
        #10;
        assert(HEX0 === 7'b001_0010);
        SW = 4'd6;
        #10;
        assert(HEX0 === 7'b0000_010);
        SW = 4'd7;
        #10;
        assert(HEX0 === 7'b1111_000);
        SW = 4'd8;
        #10;
        assert(HEX0 === 7'b0);
        SW = 4'd9;
        #10;
        assert(HEX0 === 7'b0010_000);
        SW = 4'd10;
        #10;
        assert(HEX0 === 7'b1000_000);
        SW = 4'd11;
        #10;
        assert(HEX0 === 7'b110_0001);
        SW = 4'd12;
        #10;
        assert(HEX0 === 7'b001_1000);
        SW = 4'd13;
        #10;
        assert(HEX0 === 7'b000_1001);
        SW = 4'd14;
        #10;
        assert(HEX0 === 7'b111_1111);
        SW = 4'd15;
        #10;
        assert(HEX0 === 7'b111_1111);
        $stop;
    end
// Your testbench goes here. Make sure your tests exercise the entire design
// in the .sv file.  Note that in our tests the simulator will exit after
// 10,000 ticks (equivalent to "initial #10000 $finish();").
						
endmodule

