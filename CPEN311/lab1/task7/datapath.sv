module datapath(input slow_clock, input fast_clock, input resetb,
                input load_pcard1, input load_pcard2, input load_pcard3,
                input load_dcard1, input load_dcard2, input load_dcard3,
                output [3:0] pcard3_out,
                output [3:0] pscore_out, output [3:0] dscore_out,
                output[6:0] HEX5, output[6:0] HEX4, output[6:0] HEX3,
                output[6:0] HEX2, output[6:0] HEX1, output[6:0] HEX0);
wire [3:0] new_card;
wire [3:0] dcard1,dcard2,dcard3;
wire [3:0] pcard1,pcard2;		
// The code describing your datapath will go here.  Your datapath 
// will hierarchically instantiate six card7seg blocks, two scorehand
// blocks, and a dealcard block.  The registers may either be instatiated
// or included as sequential always blocks directly in this file.
//
// Follow the block diagram in the Lab 1 handout closely as you write this code.
dealcard DC(fast_clock,resetb,new_card);
reg4 Pcard1(resetb,new_card,load_pcard1,slow_clock,pcard1);
card7seg p1(pcard1,HEX0);
reg4 Pcard2(resetb,new_card,load_pcard2,slow_clock,pcard2);
card7seg p2(pcard2,HEX1);
reg4 Pcard3(resetb,new_card,load_pcard3,slow_clock,pcard3_out);
card7seg p3(pcard3_out,HEX2);
reg4 Dcard1(resetb,new_card,load_dcard1,slow_clock,dcard1);
card7seg d1(dcard1,HEX3);
reg4 Dcard2(resetb,new_card,load_dcard2,slow_clock,dcard2);
card7seg d2(dcard2,HEX4);
reg4 Dcard3(resetb,new_card,load_dcard3,slow_clock,dcard3);
card7seg d3(dcard3,HEX5);

scorehand P(pcard1,pcard2,pcard3_out,pscore_out);
scorehand D(dcard1,dcard2,dcard3,dscore_out);
endmodule

module reg4(input reset,input [3:0] in,input load, input clk,output reg [3:0] out) ;
	always_ff @(posedge clk) begin
	    if(reset == 1'b1 && load == 1'b1)
            out <= in;
        else if(reset == 1'b0)
	        out <= 4'b0;
	end 
endmodule

