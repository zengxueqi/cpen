//print decimal value on hex display 
`define N0 7'b100_0000
`define N1 7'b1111_001
`define N2 7'b0100_100
`define N3 7'b011_0000
`define N4 7'b0011_001
`define N5 7'b0010_010
`define N7 7'b1111_000
`define N12 7'b1000_110
`define N13 7'b0100_001
`define N14 7'b0000_110
`define N15 7'b0001_110

`define n0 7'b111_1111
`define n2 7'b010_0100
`define n3 7'b011_0000
`define n5 7'b001_0010
`define n8 7'b0
`define n10 7'b1000_000
`define n13 7'b000_1001

module tb_lab1();
logic CLOCK_50;
logic [3:0] KEY;
logic [9:0] LEDR;
logic [6:0] HEX5;
logic [6:0] HEX4;
logic [6:0] HEX3;
logic [6:0] HEX2;
logic [6:0] HEX1;
logic [6:0] HEX0;
logic [9:0] SW;
wire guess = (SW[9] === LEDR[9] && SW[8] === LEDR[8]);
lab1 dut(.*);

initial begin
    KEY[0] = 1'b0;
    forever #5 KEY[0] = ~KEY[0];
end

initial begin
    CLOCK_50 = 1'b0;
    forever #1 CLOCK_50 = ~CLOCK_50;
end

initial begin 
    KEY[3] = 1'b0;
    forever #100 KEY[3] = ~KEY[3];
end

initial begin 
    KEY[2] = 1'b0;
    KEY[1] = 1'b1;
    #100; 
    assert(HEX0 === `n0 && HEX1 === `n0 && HEX2 === `n0 && HEX3 === `n0 && HEX4 === `n0 && HEX5 === `n0);//test reset 
    assert(LEDR === 10'b0);
    /*Test1: User guesses wrong*/
    KEY[2] = 1'b1;
    SW = 10'b11_0001_1100;
    #10;
    assert(HEX0 === `n3 && HEX1 === `n0 && HEX2 === `n0 && HEX3 === `n0 && HEX4 === `n0 && HEX5 === `n0);//load pcard1
    assert(LEDR === 10'b11);
    #10;
    assert(HEX0 === `n3 && HEX1 === `n0 && HEX2 === `n0 && HEX3 === `n8 && HEX4 === `n0 && HEX5 === `n0);//load dcard1
    assert(LEDR === 10'b0010_0000_11);
    #10;
    assert(HEX0 === `n3 && HEX1 === `n13 && HEX2 === `n0 && HEX3 === `n8 && HEX4 === `n0 && HEX5 === `n0);//load pcard2
    assert(LEDR === 10'b0010_0000_11);
    #10;
    assert(HEX0 === `n3 && HEX1 === `n13 && HEX2 === `n0 && HEX3 === `n8 && HEX4 === `n5 && HEX5 === `n0);//load dcard2
    assert(LEDR === 10'b0000_1100_11);
    #10;
    assert(HEX0 === `n3 && HEX1 === `n13 && HEX2 === `n10 && HEX3 === `n8 && HEX4 === `n5 && HEX5 === `n0);//load pcard3
    assert(LEDR === 10'b0000_1100_11);
    #10;
    assert(HEX0 === `n3 && HEX1 === `n13 && HEX2 === `n10 && HEX3 === `n8 && HEX4 === `n5 && HEX5 === `n2);//load dcard3
    assert(LEDR === 10'b1001_0100_11);
    #30;
    assert(guess === 1'b0);
    /*show and caculate total money after the game finishes */
    KEY[1] = 1'b0;
    #2;
    assert(HEX5 === `N0 && HEX4 === `N0 && HEX3 === `N0 && HEX2 === `N0);//show total money user have, now is 0 
    assert(HEX1 === `N1 && HEX0 === `N12);//hex0,1 show the bet
    assert(LEDR === 10'b1001_0100_11);
    #5;
    /*subtract bet from total money*/
    assert(HEX5 === `N15 && HEX4 === `N15 && HEX3 === `N14 && HEX2 === `N4);
    assert(LEDR === 10'b1001_0100_11);
    #3;
    KEY[1] = 1'b1;
    #100;
    assert(HEX0 === `n0 && HEX1 === `n0 && HEX2 === `n0 && HEX3 === `n0 && HEX4 === `n0 && HEX5 === `n0);//test reset 
    assert(LEDR === 10'b0);

    /*Test2: User guesses wrong*/
    SW = 10'b01_0001_1111;
    #10;
    assert(HEX0 === `n3 && HEX1 === `n0 && HEX2 === `n0 && HEX3 === `n0 && HEX4 === `n0 && HEX5 === `n0);
    assert(LEDR === 10'b11);
    #10;
    assert(HEX0 === `n3 && HEX1 === `n0 && HEX2 === `n0 && HEX3 === `n8 && HEX4 === `n0 && HEX5 === `n0);
    assert(LEDR === 10'b0010_0000_11);
    #10;
    assert(HEX0 === `n3 && HEX1 === `n13 && HEX2 === `n0 && HEX3 === `n8 && HEX4 === `n0 && HEX5 === `n0);
    assert(LEDR === 10'b0010_0000_11);
    #10;
    assert(HEX0 === `n3 && HEX1 === `n13 && HEX2 === `n0 && HEX3 === `n8 && HEX4 === `n5 && HEX5 === `n0);
    assert(LEDR === 10'b0000_1100_11);
    #10;
    assert(HEX0 === `n3 && HEX1 === `n13 && HEX2 === `n10 && HEX3 === `n8 && HEX4 === `n5 && HEX5 === `n0);
    assert(LEDR === 10'b0000_1100_11);
    #10;
    assert(HEX0 === `n3 && HEX1 === `n13 && HEX2 === `n10 && HEX3 === `n8 && HEX4 === `n5 && HEX5 === `n2);
    assert(LEDR === 10'b1001_0100_11);
    #30;
    assert(guess === 1'b0);
    /*show and caculate total money after the game finishes */
    KEY[1] = 1'b0;//get total money
    #2;
    assert(HEX5 === `N15 && HEX4 === `N15 && HEX3 === `N14 && HEX2 === `N4);
    assert(HEX1 === `N1 && HEX0 === `N15);
    assert(LEDR === 10'b1001_0100_11);
    #5;
    /*subtract bet from total money*/
    assert(HEX5 === `N15 && HEX4 === `N15 && HEX3 === `N12 && HEX2 === `N5);
    assert(LEDR === 10'b1001_0100_11);
    #3;
    KEY[1] = 1'b1;
    #100;
    assert(HEX0 === `n0 && HEX1 === `n0 && HEX2 === `n0 && HEX3 === `n0 && HEX4 === `n0 && HEX5 === `n0);//test reset 
    assert(LEDR === 10'b0);

    /*Test3: User guesses correct*/
    SW = 10'b10_0001_0010;
    #10;
    assert(HEX0 === `n3 && HEX1 === `n0 && HEX2 === `n0 && HEX3 === `n0 && HEX4 === `n0 && HEX5 === `n0);//load pcard1
    assert(LEDR === 10'b11);
    #10;
    assert(HEX0 === `n3 && HEX1 === `n0 && HEX2 === `n0 && HEX3 === `n8 && HEX4 === `n0 && HEX5 === `n0);//load dcard1
    assert(LEDR === 10'b0010_0000_11);
    #10;
    assert(HEX0 === `n3 && HEX1 === `n13 && HEX2 === `n0 && HEX3 === `n8 && HEX4 === `n0 && HEX5 === `n0);//load pcard2
    assert(LEDR === 10'b0010_0000_11);
    #10;
    assert(HEX0 === `n3 && HEX1 === `n13 && HEX2 === `n0 && HEX3 === `n8 && HEX4 === `n5 && HEX5 === `n0);//load dcard2
    assert(LEDR === 10'b0000_1100_11);
    #10;
    assert(HEX0 === `n3 && HEX1 === `n13 && HEX2 === `n10 && HEX3 === `n8 && HEX4 === `n5 && HEX5 === `n0);//load pcard3
    assert(LEDR === 10'b0000_1100_11);
    #10;
    assert(HEX0 === `n3 && HEX1 === `n13 && HEX2 === `n10 && HEX3 === `n8 && HEX4 === `n5 && HEX5 === `n2);//load dcard3
    assert(LEDR === 10'b1001_0100_11);
    #30;
    assert(guess === 1'b1);
    /*show and caculate total money after the game finishes */
    KEY[1] = 1'b0;//get total money 
    #2;
    assert(HEX5 === `N15 && HEX4 === `N15 && HEX3 === `N12 && HEX2 === `N5);
    assert(HEX1 === `N1 && HEX0 === `N2);
    assert(LEDR === 10'b1001_0100_11);
    #5;
    /*Add bet to total money*/
    assert(HEX5 === `N15 && HEX4 === `N15 && HEX3 === `N13 && HEX2 === `N7);
    assert(LEDR === 10'b1001_0100_11);
    #3;
    $stop;
end

endmodule

