// This module contains a Verilog description of the top level module
// Assuming you don't modify the inputs and outputs of the various submodules,
// you should not have to modify anything in this file.

/*
 * Press KEY[3] to start the game  
 * Press KEY[2] to reset the total money user have 
 * Press KEY[1] to see the bet and total money
 * HEX5,4,3,2 display the total money user has, HEX0,1 display the bet value 
 * KEY[0] acts as a clk 
 */
module lab1(input CLOCK_50, input[3:0] KEY, output[9:0] LEDR,
            output[6:0] HEX5, output[6:0] HEX4, output[6:0] HEX3,
            output[6:0] HEX2, output[6:0] HEX1, output[6:0] HEX0,
            input [9:0] SW);
wire fast_clock, slow_clock, resetb;
wire load_pcard1, load_pcard2, load_pcard3;
wire load_dcard1, load_dcard2, load_dcard3;
wire [3:0] pscore, dscore;
wire [3:0] pcard3;

wire [9:0] bet;
wire [15:0] total_money;
wire [6:0] hex0_dp,hex1_dp,hex2_dp,hex3_dp,hex4_dp,hex5_dp,
        hex0_bet,hex1_bet,hex2_money,hex3_money,hex4_money,hex5_money;

assign resetb = KEY[3];
assign slow_clock = KEY[0];
assign fast_clock = CLOCK_50;

vDFF #(10) readBet(CLOCK_50,SW,bet);//read input from switch 
sseg B1(bet[3:0],hex0_bet);
sseg B2(bet[7:4],hex1_bet);

assign HEX0 = KEY[1] ? hex0_dp : hex0_bet;
assign HEX1 = KEY[1] ? hex1_dp : hex1_bet;
assign HEX2 = KEY[1] ? hex2_dp : hex2_money;
assign HEX3 = KEY[1] ? hex3_dp : hex3_money;
assign HEX4 = KEY[1] ? hex4_dp : hex4_money;
assign HEX5 = KEY[1] ? hex5_dp : hex5_money;

wire win = (bet[9] == LEDR[9] && bet[8] == LEDR[8]) ? 1 : 0;//win or lose the game 
wire load = (LEDR[9] == 1'b1 || LEDR[8] == 1'b1) ? 1 : 0;
updateMoney UM(bet[7:0],win,load,KEY[2:0],total_money);//update total money 

sseg M1(total_money[3:0],hex2_money);
sseg M2(total_money[7:4],hex3_money);
sseg M3(total_money[11:8],hex4_money);
sseg M4(total_money[15:12],hex5_money);

datapath dp(.slow_clock(slow_clock),
            .fast_clock(fast_clock),
            .resetb(resetb),
            .load_pcard1(load_pcard1),
            .load_pcard2(load_pcard2),
            .load_pcard3(load_pcard3),
            .load_dcard1(load_dcard1),
            .load_dcard2(load_dcard2),
            .load_dcard3(load_dcard3),
            .dscore_out(dscore),
            .pscore_out(pscore),
            .pcard3_out(pcard3),
            .HEX5(hex5_dp),
            .HEX4(hex4_dp),
            .HEX3(hex3_dp),
            .HEX2(hex2_dp),
            .HEX1(hex1_dp),
            .HEX0(hex0_dp));

assign LEDR[3:0] = pscore;
assign LEDR[7:4] = dscore;

statemachine sm(.slow_clock(slow_clock),
                .resetb(resetb),
                .dscore(dscore),
                .pscore(pscore),
                .pcard3(pcard3),
                .load_pcard1(load_pcard1),
                .load_pcard2(load_pcard2),
                .load_pcard3(load_pcard3),
                .load_dcard1(load_dcard1),
                .load_dcard2(load_dcard2),
                .load_dcard3(load_dcard3),
                .player_win_light(LEDR[8]),
                .dealer_win_light(LEDR[9]));
endmodule

/*
*  Update total value the user have after the game finishes
*  Press KEY[0] and KEY[1] together to update total money
*  Press KEY[0] and KEY[2] together to reset total money 
*/
module updateMoney(input [7:0] bet,input win,input load,input [2:0] KEY,output reg [15:0] total_money);
wire clk = KEY[0];
wire reset = KEY[2];
wire [15:0] bet_value = {8'b0,bet};

   always_ff @(posedge clk) begin
        if(reset == 1'b1 && load == 1'b1 && KEY[1] == 1'b0) 
            total_money <= win ? (total_money + bet_value) : (total_money - bet_value);
        else if(reset == 1'b0) 
            total_money <= 16'b0;
    end
endmodule

/*
* Print hex value on 7 segment display 
*/
module sseg(input [3:0] in,output reg [6:0] segs);
    always @* begin
        case(in)
              4'd0: segs = 7'b1000_000;
              4'd1: segs = 7'b1111_001;
              4'd2: segs = 7'b0100_100;
              4'd3: segs = 7'b0110_000;
              4'd4: segs = 7'b0011_001;
              4'd5: segs = 7'b0010_010;
              4'd6: segs = 7'b0000_010;
              4'd7: segs = 7'b1111_000;
              4'd8: segs = 7'b0;
              4'd9: segs = 7'b0010_000;
              4'd10: segs = 7'b0001_000;
              4'd11: segs = 7'b0000_011;
              4'd12: segs = 7'b1000_110;
              4'd13: segs = 7'b0100_001;
              4'd14: segs = 7'b0000_110;
              4'd15: segs = 7'b0001_110;
              default: segs = 7'b1111_111;
      endcase
  end
endmodule

