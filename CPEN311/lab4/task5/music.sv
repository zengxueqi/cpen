module music(input CLOCK_50, input CLOCK2_50, input [3:0] KEY, input [9:0] SW,
             input AUD_DACLRCK, input AUD_ADCLRCK, input AUD_BCLK, input AUD_ADCDAT,
             inout FPGA_I2C_SDAT, output FPGA_I2C_SCLK, output AUD_DACDAT, output AUD_XCK,
             output [6:0] HEX0, output [6:0] HEX1, output [6:0] HEX2,
             output [6:0] HEX3, output [6:0] HEX4, output [6:0] HEX5,
             output [9:0] LEDR);
			
// signals that are used to communicate with the audio core
// DO NOT alter these -- we will use them to test your design

reg read_ready, write_ready, write_s;
reg [15:0] writedata_left, writedata_right;
reg [15:0] readdata_left, readdata_right;	
wire reset, read_s;
assign reset = ~(KEY[3]);
// signals that are used to communicate with the flash core
// DO NOT alter these -- we will use them to test your design

reg flash_mem_read;
reg flash_mem_waitrequest;
reg [22:0] flash_mem_address;
reg [31:0] flash_mem_readdata;
reg flash_mem_readdatavalid;
reg [3:0] flash_mem_byteenable;
reg rst_n, clk;

assign clk = CLOCK_50;
assign rst_n = KEY[3];

logic [22:0] index; 
logic start_read, start_write;
logic [1:0] write_sample1,write_sample2;
reg [31:0] data;

// DO NOT alter the instance names or port names below -- we will use them to test your design

clock_generator my_clock_gen(CLOCK2_50, reset, AUD_XCK);
audio_and_video_config cfg(CLOCK_50, reset, FPGA_I2C_SDAT, FPGA_I2C_SCLK);
audio_codec codec(CLOCK_50,reset,read_s,write_s,writedata_left, writedata_right,AUD_ADCDAT,AUD_BCLK,AUD_ADCLRCK,AUD_DACLRCK,
            read_ready, write_ready,readdata_left, readdata_right,AUD_DACDAT);
flash flash_inst(.clk_clk(clk), .reset_reset_n(rst_n), .flash_mem_write(1'b0), .flash_mem_burstcount(1'b1),
                 .flash_mem_waitrequest(flash_mem_waitrequest), .flash_mem_read(flash_mem_read), .flash_mem_address(flash_mem_address),
                 .flash_mem_readdata(flash_mem_readdata), .flash_mem_readdatavalid(flash_mem_readdatavalid), 
                 .flash_mem_byteenable(flash_mem_byteenable), .flash_mem_writedata());

assign flash_mem_byteenable = 4'b1111;
assign read_s = 1'b0;

    always @(posedge clk, negedge rst_n) begin 
        if(!rst_n) begin index = 23'b0; flash_mem_read = 0; flash_mem_address = 23'b0; start_read = 0; start_write = 0;
             write_s = 0; write_sample1 = 2'b00; write_sample2 = 2'b11; end 
        else begin 
            if(start_write) begin 
                if(write_ready && write_sample1 == 2'b00) begin writedata_left = $signed(data[15:0]) / 64; writedata_right = $signed(data[15:0]) / 64; 
                        write_s = 1; write_sample1 = 2'b01; end 
                else if(write_sample1 == 2'b01 && !write_ready) begin write_s = 0; write_sample1 = 2'b11; write_sample2 = 2'b00; end 
                else if(write_ready && write_sample2 == 2'b00) begin writedata_left = $signed(data[31:16]) / 64; writedata_right = $signed(data[31:16]) / 64;
                         write_s = 1; write_sample2 = 2'b01; end 
                else if(write_sample2 == 2'b01 && !write_ready) begin write_s = 0; start_write = 0; write_sample1 = 2'b00; write_sample2 = 2'b11; end 
            end 
            else if(!flash_mem_waitrequest && !start_read && !start_write) begin if(index == 23'h100000) index = 23'b0;
                 flash_mem_address = index; flash_mem_read = 1; start_read = 1; end 
            else if(start_read && flash_mem_readdatavalid && !start_write) begin flash_mem_read = 0; start_read = 0; start_write = 1; 
                    data = flash_mem_readdata; index = index + 23'b1; end 
        end 
    end  

endmodule: music
