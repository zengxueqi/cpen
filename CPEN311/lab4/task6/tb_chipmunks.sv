`timescale 1ps / 1ps

module tb_chipmunks();
logic CLOCK_50, CLOCK2_50; logic [3:0] KEY; logic [9:0] SW;
logic AUD_DACLRCK, AUD_ADCLRCK, AUD_BCLK, AUD_ADCDAT;
wire FPGA_I2C_SDAT;
wire FPGA_I2C_SCLK;
logic AUD_DACDAT,AUD_XCK;
logic [6:0] HEX0,HEX1,HEX2,HEX3,HEX4,HEX5;
logic [9:0] LEDR;

chipmunks dut(.*);

initial begin
    CLOCK_50 = 1'b0;
    forever #1 CLOCK_50 = ~CLOCK_50;
end

initial begin 
    KEY[3] = 0; SW[1:0] = 2'b00; #5;
    assert(dut.read_s === 0 && dut.flash_mem_byteenable === 4'b1111);
    assert(dut.index === 23'b0 && dut.flash_mem_read === 0 && dut.flash_mem_address === 23'b0);
    assert(dut.start_read === 0 && dut.start_write === 0);
    assert(dut.write_s === 0 && dut.write_sample1 === 2'b00 && dut.write_sample2 === 2'b11);
    KEY[3] = 1;
  
    wait (dut.flash_mem_waitrequest == 0);
    @(posedge CLOCK_50);
    assert(dut.flash_mem_address === 23'b0 && dut.flash_mem_read === 1 && dut.start_read === 1);  
    wait (dut.flash_mem_readdatavalid == 1);
    @(posedge CLOCK_50);
    assert(dut.flash_mem_read === 0 && dut.index === 23'b1 && dut.flag === 3'b0);
    assert(dut.start_read === 0 && dut.start_write === 1 && dut.data === 32'hFFFF_0000); 

    wait (dut.write_ready == 1'b1);
    @(posedge CLOCK_50);
    assert(dut.writedata_left === 16'b0 && dut.writedata_right === 16'b0 && dut.write_sample1 === 2'b01 && dut.write_s === 1); 
 
    force dut.write_ready = 1'b0; 
    @(posedge CLOCK_50); 
    assert(dut.write_s === 0 && dut.write_sample1 === 2'b11 && dut.write_sample2 === 2'b00);

    force dut.write_ready = 1'b1; 
    @(posedge CLOCK_50); 
    assert(dut.writedata_left === 16'b0&& dut.writedata_right === 16'b0 && dut.write_s === 1 && dut.write_sample2 === 2'b01);

    force dut.write_ready = 1'b0; 
    @(posedge CLOCK_50); 
    assert(dut.start_write === 0 && dut.write_s === 0 && dut.start_write === 0 && dut.write_sample1 === 2'b00 && dut.write_sample2 === 2'b11); 

    force dut.start_read = 1; force dut.flash_mem_readdatavalid = 1; force dut.start_write = 0; SW[1:0] = 2'b10;
    @(posedge CLOCK_50);  assert(dut.flag === 3'b011);  SW[1:0] = 2'b01;
    @(posedge CLOCK_50);  assert(dut.flag === 3'b100);  release dut.start_read; release dut.flash_mem_readdatavalid; release dut.start_write;

    force dut.start_write = 1;
    @(posedge CLOCK_50);
    force dut.write_sample1 = 2'b01; force dut.write_ready = 0; force dut.flag = 3'b100;
    release dut.start_write; release dut.write_sample1; 
    @(posedge CLOCK_50);
    assert(dut.start_write === 0 && dut.write_sample1 === 2'b00 && dut.write_sample2 === 2'b11); 
    
    @(posedge CLOCK_50);
    force dut.start_write = 1; force dut.write_sample1 = 2'b01; force dut.write_ready = 0; force dut.flag = 3'b011;
    release dut.start_write; release dut.write_sample1; release dut.flag;
    @(posedge CLOCK_50);
    assert(dut.write_sample1 === 2'b00 && dut.flag === 3'b010);

    @(posedge CLOCK_50);
    force dut.start_write = 1; force dut.write_sample2 = 2'b01; force dut.write_ready = 0; force dut.flag = 3'b010;
    release dut.start_write; release dut.write_sample2; release dut.flag;
    @(posedge CLOCK_50);
    assert(dut.write_sample2 === 2'b00 && dut.flag === 3'b0);

    force dut.start_read = 0; force dut.start_write = 0; force dut.index = 23'h100_000;
    wait (dut.flash_mem_waitrequest == 0);
    @(posedge CLOCK_50);  release dut.index;
    @(posedge CLOCK_50); 
    assert(dut.index === 23'b0);    
    $display("tb_chipmunks done");
    $stop;
end 
endmodule: tb_chipmunks

module flash(input logic clk_clk, input logic reset_reset_n,
             input logic flash_mem_write, input logic flash_mem_burstcount,
             output logic flash_mem_waitrequest, input logic flash_mem_read,
             input logic [22:0] flash_mem_address, output logic [31:0] flash_mem_readdata,
             output logic flash_mem_readdatavalid, input logic [3:0] flash_mem_byteenable,
             input logic [31:0] flash_mem_writedata);

logic [1:0] flag;
logic [2:0] count;

    always @(posedge clk_clk,negedge reset_reset_n) begin
        if(!reset_reset_n) begin flash_mem_waitrequest = 1; count = 3'b0; end 
        else begin 
            if(count < 3'd3) flash_mem_waitrequest = 0; 
            else flash_mem_waitrequest = 1; 
            count = count + 3'b1; 
        end 
    end 

    always @(posedge clk_clk,negedge reset_reset_n) begin 
        if(!reset_reset_n) begin flash_mem_readdatavalid = 0; flash_mem_readdata = 32'b0; flag = 2'b0; end 
        else begin 
            if(flag == 2'b01) begin flash_mem_readdata = {16'hFFFF,flash_mem_address[15:0]}; flash_mem_readdatavalid = 1; flag = 2'b10; end 
            else if(flag == 2'b10) begin flash_mem_readdatavalid = 0; flag = 2'b11; end
            else if(flash_mem_read && !flash_mem_waitrequest) begin flag = 2'b01; end 
        end 
    end 

endmodule: flash

