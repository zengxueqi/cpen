`timescale 1ps / 1ps
module tb_flash_reader();
logic CLOCK_50; logic [3:0] KEY; logic [9:0] SW;
logic [6:0] HEX0,HEX1,HEX2,HEX3,HEX4,HEX5;
logic [9:0] LEDR;

flash_reader dut(.*);

initial begin
    CLOCK_50 = 1'b0;
    forever #1 CLOCK_50 = ~CLOCK_50;
end

initial begin 
    KEY[3] = 0; #5; 
    assert(dut.addr === 8'b0 && dut.wrdata === 16'b0 && dut.wren === 0 && dut.index === 23'b0 && dut.start_read === 0 && dut.start_write === 0);
    assert(dut.flash_mem_address === 23'b0 && dut.flash_data === 16'b0 && dut.flash_mem_read === 0);
    KEY[3] = 1;// #5;
    wait (dut.flash_mem_waitrequest == 0);
    @(posedge CLOCK_50);
    assert(dut.flash_mem_address === 23'b0 && dut.flash_mem_read === 1 && dut.start_read === 1 && dut.wren === 0);// #2; 
    wait (dut.flash_mem_readdatavalid == 1);
    @(posedge CLOCK_50);
    assert(dut.start_read === 0 && dut.start_write === 1 && dut.flash_data === 16'hFFFF);
    assert(dut.wren === 1 && dut.addr === 8'b0 && dut.wrdata === 16'b0); //#2;
    @(posedge CLOCK_50);
    assert(dut.start_write === 0 && dut.wren === 1 && dut.addr === 8'b1 && dut.wrdata === 16'hFFFF && dut.index === 23'b1); //#10;

    wait (dut.flash_mem_waitrequest == 0);
    @(posedge CLOCK_50);
    assert(dut.flash_mem_address === 23'b1 && dut.flash_mem_read === 1 && dut.start_read === 1 && dut.wren === 0); //#2; 
    wait (dut.flash_mem_readdatavalid == 1);
    @(posedge CLOCK_50);
    assert(dut.start_read === 0 && dut.start_write === 1 && dut.flash_data === 16'hFFFF);
    assert(dut.wren === 1 && dut.addr === 8'd2 && dut.wrdata === 16'b1); //#2;
    @(posedge CLOCK_50);
    assert(dut.start_write === 0 && dut.wren === 1 && dut.addr === 8'd3 && dut.wrdata === 16'hFFFF && dut.index === 23'd2);// #2;
    
    force dut.index = 23'd128;// #4;
    @(posedge CLOCK_50);
    assert(dut.wren === 0 && dut.flash_mem_read === 0); #100;
    assert(dut.wren === 0 && dut.flash_mem_read === 0);
    $display("flash reader test done");
    $stop;
end 
endmodule: tb_flash_reader

module flash(input logic clk_clk, input logic reset_reset_n,
             input logic flash_mem_write, input logic flash_mem_burstcount,
             output logic flash_mem_waitrequest, input logic flash_mem_read,
             input logic [22:0] flash_mem_address, output logic [31:0] flash_mem_readdata,
             output logic flash_mem_readdatavalid, input logic [3:0] flash_mem_byteenable,
             input logic [31:0] flash_mem_writedata);

logic [1:0] flag;
logic [2:0] count;

    always @(posedge clk_clk,negedge reset_reset_n) begin
        if(!reset_reset_n) begin flash_mem_waitrequest = 1; count = 3'b0; end 
        else begin 
            if(count < 3'd3) flash_mem_waitrequest = 0; 
            else flash_mem_waitrequest = 1; 
            count = count + 3'b1; 
        end 
    end 

    always @(posedge clk_clk,negedge reset_reset_n) begin 
        if(!reset_reset_n) begin flash_mem_readdatavalid = 0; flash_mem_readdata = 32'b0; flag = 2'b0; end 
        else begin 
            if(flag == 2'b01) begin flash_mem_readdata = {16'hFFFF,flash_mem_address[15:0]}; flash_mem_readdatavalid = 1; flag = 2'b10; end 
            else if(flag == 2'b10) begin flash_mem_readdatavalid = 0; flag = 2'b11; end
            else if(flash_mem_read && !flash_mem_waitrequest) begin flag = 2'b01; end 
        end 
    end 

endmodule: flash
