module flash_reader(input logic CLOCK_50, input logic [3:0] KEY, input logic [9:0] SW,
                    output logic [6:0] HEX0, output logic [6:0] HEX1, output logic [6:0] HEX2,
                    output logic [6:0] HEX3, output logic [6:0] HEX4, output logic [6:0] HEX5,
                    output logic [9:0] LEDR);

// You may use the SW/HEX/LEDR ports for debugging. DO NOT delete or rename any ports or signals.

logic clk, rst_n;

assign clk = CLOCK_50;
assign rst_n = KEY[3];

logic flash_mem_read; //Input: read control to memory
logic flash_mem_waitrequest; //Output: waitrequest control from memory
logic flash_mem_readdatavalid;//Output: memory read data is available

logic [22:0] flash_mem_address;
logic [31:0] flash_mem_readdata;//Output: read data bus from memory First sample: lower 16 bits, Second sample: upper 16 bits
logic [3:0] flash_mem_byteenable;

logic [15:0] wrdata,rddata;
logic [7:0] addr;
logic [15:0] flash_data;
logic wren,start_read,start_write;

logic [22:0] index;

//only read from flash 
flash flash_inst(.clk_clk(clk), .reset_reset_n(rst_n), .flash_mem_write(1'b0), .flash_mem_burstcount(1'b1),
                 .flash_mem_waitrequest(flash_mem_waitrequest), .flash_mem_read(flash_mem_read), .flash_mem_address(flash_mem_address),
                 .flash_mem_readdata(flash_mem_readdata), .flash_mem_readdatavalid(flash_mem_readdatavalid), 
                 .flash_mem_byteenable(flash_mem_byteenable), .flash_mem_writedata());

s_mem samples(.address(addr),.clock(CLOCK_50),.data(wrdata),.wren(wren),.q(rddata));
assign flash_mem_byteenable = 4'b1111;

    always @(posedge clk,negedge rst_n) begin 
        if(!rst_n) begin addr = 8'b0; wrdata = 16'b0; wren = 0; index = 23'b0; start_read = 0; start_write = 0; flash_data = 16'b0;
                  flash_mem_address = 23'b0; flash_mem_read = 0; end  
        else if(index < 23'd128) begin wren = 0;
            if(!flash_mem_waitrequest && !start_read && !start_write) begin flash_mem_address = index; flash_mem_read = 1; start_read = 1; end 
            else if(flash_mem_readdatavalid && start_read && !start_write) begin flash_mem_read = 0;
                start_write = 1; start_read = 0; flash_data = flash_mem_readdata[31:16];
                wren = 1; addr = 2 * index[7:0]; wrdata = flash_mem_readdata[15:0]; end 
            else if(start_write) begin start_write = 0; wren = 1; addr = 2 * index[7:0] + 8'b1; wrdata = flash_data; index = index + 23'b1; end 
        end 
        else begin wren = 0;end 
    end 

endmodule: flash_reader
