
`define delay_period 23'd44000//period is 0.5s and has 44000 data samples 
`define data_length 23'h200_000

module flanger(input CLOCK_50, input CLOCK2_50, input [3:0] KEY, input [9:0] SW,
               input AUD_DACLRCK, input AUD_ADCLRCK, input AUD_BCLK, input AUD_ADCDAT,
               inout FPGA_I2C_SDAT, output FPGA_I2C_SCLK, output AUD_DACDAT, output AUD_XCK,
               output [6:0] HEX0, output [6:0] HEX1, output [6:0] HEX2,
               output [6:0] HEX3, output [6:0] HEX4, output [6:0] HEX5,
               output [9:0] LEDR);
			
// signals that are used to communicate with the audio core
// DO NOT alter these -- we will use them to test your design

reg read_ready, write_ready, write_s;
reg [15:0] writedata_left, writedata_right;
reg [15:0] readdata_left, readdata_right;	
wire reset, read_s;

// signals that are used to communicate with the flash core
// DO NOT alter these -- we will use them to test your design

reg flash_mem_read;
reg flash_mem_waitrequest;
reg [22:0] flash_mem_address;
reg [31:0] flash_mem_readdata;
reg flash_mem_readdatavalid;
reg [3:0] flash_mem_byteenable;
reg rst_n, clk;

assign reset = ~(KEY[3]);
assign clk = CLOCK_50;
assign rst_n = KEY[3];

logic [22:0] index,cursor,read_addr,delay_value;
logic start_read, start_write,read_flag;
logic [1:0] write_sample1,write_sample2;
logic signed [15:0] sum1,sum2;
logic [15:0] temp,delay_index;
logic [2:0] flag; 
reg [15:0] data[0:22000];

// DO NOT alter the instance names or port names below -- we will use them to test your design

clock_generator my_clock_gen(CLOCK2_50, reset, AUD_XCK);
audio_and_video_config cfg(CLOCK_50, reset, FPGA_I2C_SDAT, FPGA_I2C_SCLK);
audio_codec codec(CLOCK_50,reset,read_s,write_s,writedata_left, writedata_right,AUD_ADCDAT,AUD_BCLK,AUD_ADCLRCK,AUD_DACLRCK,read_ready,
                 write_ready,readdata_left, readdata_right,AUD_DACDAT);

flash flash_inst(.clk_clk(clk), .reset_reset_n(rst_n), .flash_mem_write(1'b0), .flash_mem_burstcount(1'b1),
                 .flash_mem_waitrequest(flash_mem_waitrequest), .flash_mem_read(flash_mem_read), .flash_mem_address(flash_mem_address),
                 .flash_mem_readdata(flash_mem_readdata), .flash_mem_readdatavalid(flash_mem_readdatavalid), 
                 .flash_mem_byteenable(flash_mem_byteenable), .flash_mem_writedata());

// your code for the rest of this task here

assign flash_mem_byteenable = 4'b1111;
assign read_s = 1'b0;

    initial $readmemh("LUT.memh",data);//lookup table for y = cos(x*pi/22000) * 1600 + 2750

    always @(posedge clk, negedge rst_n) begin
        if(!rst_n) begin index = 23'b0; flash_mem_read = 0; flash_mem_address = 23'b0; start_read = 0; start_write = 0;
             write_s = 0; write_sample1 = 2'b00; write_sample2 = 2'b11; flag = 3'b0;
             cursor = 23'b0; sum1 = 16'b0; sum2 = 16'b0; read_flag = 0; read_addr = 32'b0; end
        else begin
            if(start_write) begin //write buffer state 
                if(write_ready && write_sample1 == 2'b00) begin writedata_left = $signed(sum1) / 2; writedata_right = writedata_left;
                        write_s = 1; write_sample1 = 2'b01; end
                else if(write_sample1 == 2'b01 && !write_ready) begin write_s = 0;
                    if(flag == 3'b100) begin start_write = 0;  write_sample1 = 2'b00; write_sample2 = 2'b11; 
                            sum1 = 16'b0; sum2 = 16'b0; read_flag = 0; end//exit start_write state, reset 
                    else if(flag == 3'b011) begin write_sample1 = 2'b00; flag = 3'b010; end
                    else begin write_sample1 = 2'b11; write_sample2 = 2'b00; end
                end
                else if(write_ready && write_sample2 == 2'b00) begin writedata_left = $signed(sum2) / 2; writedata_right = writedata_left;
                         write_s = 1; write_sample2 = 2'b01; end
                else if(write_sample2 == 2'b01 && !write_ready) begin write_s = 0;
                    if(flag == 3'b010) begin write_sample2 = 2'b00; flag = 3'b0; end
                    else begin  write_sample1 = 2'b00; write_sample2 = 2'b11; start_write = 0;
                         sum1 = 16'b0; sum2 = 16'b0; read_flag = 0; end//exit start_write state,reset
                end
            end //send read request to flash mem state 
            else if(!flash_mem_waitrequest && !start_read && !start_write) begin if(index == `data_length / 2) index = 23'b0;
                 flash_mem_address = read_flag ? read_addr : index; flash_mem_read = 1; start_read = 1; end
                 //read flash mem data state 
             else if(start_read && flash_mem_readdatavalid && !start_write) begin flash_mem_read = 0; start_read = 0; 
                    if(read_flag == 0) begin sum1 = sum1 + $signed(flash_mem_readdata[15:0]) / 64;
                                             sum2 = sum2 + $signed(flash_mem_readdata[31:16]) / 64; end 
                    else if(read_flag == 1) begin //read the delayed sound 
                        if(delay_value == 2 * index) begin 
                            if(cursor[0] == 0) sum1 = sum1 + $signed(flash_mem_readdata[15:0]) / 80;
                            else sum1 = sum1 + $signed(flash_mem_readdata[31:16]) / 80;       
                        end  
                        if(delay_value == 2 * index + 23'b1) begin
                           if(cursor[0] == 0) sum2 = sum2 + $signed(flash_mem_readdata[15:0]) / 80; 
                            else sum2 = sum2 + $signed(flash_mem_readdata[31:16]) / 80;       
                        end 
                        cursor = cursor + 23'b1; 
                        if(cursor == `data_length) cursor = 23'b0; 
                    end
                    temp = cursor % `delay_period;
                    delay_index = temp >= `delay_period / 2 ? `delay_period - temp : temp;
                    delay_value = ({7'b0,data[delay_index]} + cursor) % `data_length; 

                    if(delay_value == 2 * index + 23'b1 || delay_value == 2 * index) begin read_addr = cursor / 2; read_flag = 1; end 
                    else begin start_write = 1; index = index + 23'b1; read_flag = 0; end //don't read delayed sound, start write  

                    if(SW[1:0] == 2'b10) flag = 3'b011;//half the speed
                    else if(SW[1:0] == 2'b01) flag = 3'b100;//play twice fast
                    else flag = 3'b0;
            end
        end
    end


endmodule: flanger
