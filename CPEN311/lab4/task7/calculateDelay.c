/*
 * C function used to calculate the delay time for each sample 
 * Copy the output of console to LUT.memh to create lookup table of cosine function 
 */
#include <stdio.h>
#include <math.h>
#define M_PI acos(-1.0)
int main(){
   for(double x = 0; x <= 22000.0; x = x + 1.0){
       double temp = x * M_PI / 22000.0;
       double value = 1650.0 * cos(temp) + 2750.0;
       double delay = round(value);
        printf("%X   ",(int)delay);
       if((int)x % 10 == 9)
        	printf("\n");
   }
    return 0;
}

