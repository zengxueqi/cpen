module tb_fillscreen();
logic clk;
logic rst_n;
logic [2:0] colour;
logic start;
logic done;
logic [7:0] vga_x;
logic [6:0] vga_y;
logic [2:0] vga_colour;
logic vga_plot;

fillscreen dut(.*);

task test_fillscreen;
	for(int x = 0; x < 160; x++) begin 
		for(int y = 0; y < 120;y++) begin 
			assert(tb_fillscreen.dut.vga_x === x);
			assert(tb_fillscreen.dut.vga_y === y);
			assert(tb_fillscreen.dut.vga_colour === x % 8);
			assert(tb_fillscreen.dut.vga_plot === 1);
			#2;
		end
	end
endtask;

initial begin 
    clk = 1'b0;
    forever #1 clk = ~clk;
end

initial begin
	rst_n = 0;start = 1;#5; rst_n = 1;//test reset 
	assert(tb_fillscreen.dut.done === 0 && tb_fillscreen.dut.vga_plot === 0);
        assert(vga_colour === 3'b0 && vga_x === 8'b0 && vga_y === 7'b111_1111); #2;

	test_fillscreen;//test fill screen 

	assert(tb_fillscreen.dut.done === 1 && tb_fillscreen.dut.vga_plot === 0); #100; assert(tb_fillscreen.dut.done === 1 && tb_fillscreen.dut.vga_plot === 0);//done
    $display("fill screen test done");
    $stop;
end
// Your testbench goes here. Our toplevel will give up after 1,000,000 ticks.

endmodule: tb_fillscreen
