module task2(input logic CLOCK_50, input logic [3:0] KEY, // KEY[3] is async active-low reset
             input logic [9:0] SW, output logic [9:0] LEDR,
             output logic [6:0] HEX0, output logic [6:0] HEX1, output logic [6:0] HEX2,
             output logic [6:0] HEX3, output logic [6:0] HEX4, output logic [6:0] HEX5,
             output logic [7:0] VGA_R, output logic [7:0] VGA_G, output logic [7:0] VGA_B,
             output logic VGA_HS, output logic VGA_VS, output logic VGA_CLK);
        
        wire [2:0] vga_colour;
        wire start,done,vga_plot;
        wire [7:0] vga_x;
        wire [6:0] vga_y;

        wire [9:0] VGA_R_10,VGA_G_10,VGA_B_10;
        wire VGA_BLANK,VGA_SYNC;

        assign VGA_R = VGA_R_10[9:2];
        assign VGA_G = VGA_G_10[9:2];
        assign VGA_B = VGA_B_10[9:2];

      reset2 R2(.clk(CLOCK_50),
		.rst(KEY[3]),
		.done(done),
		.start(start));

          fillscreen fs(.clk(CLOCK_50),
                        .rst_n(KEY[3]),
                        .colour(SW[2:0]),
                        .start(start),
                        .done(done),
                        .vga_x(vga_x),
                        .vga_y(vga_y),
                        .vga_colour(vga_colour),
                        .vga_plot(vga_plot));

         vga_adapter #(.RESOLUTION("160x120")) 
                 vga_u0(.resetn(KEY[3]),
                        .clock(CLOCK_50),
                        .colour(vga_colour),
                        .x(vga_x),
                        .y(vga_y),
                        .plot(vga_plot),
                        .VGA_R(VGA_R_10),
                        .VGA_G(VGA_G_10),
                        .VGA_B(VGA_B_10),
                        .VGA_HS(VGA_HS),
                        .VGA_VS(VGA_VS),
                        .VGA_BLANK(VGA_BLANK),
                        .VGA_SYNC(VGA_SYNC),
                        .VGA_CLK(VGA_CLK));
endmodule: task2

module reset2(input clk,input rst,input done,output logic start);
	always @(posedge clk) begin 
		if(!rst) 
			start = 1'b0;
		else if(rst && !done) 
			start = 1'b1;
		else if(done)
			start = 1'b0;
	end
endmodule 

