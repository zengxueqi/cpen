module fillscreen(input logic clk, input logic rst_n, input logic [2:0] colour,
                  input logic start, output logic done,
                  output logic [7:0] vga_x, output logic [6:0] vga_y,
                  output logic [2:0] vga_colour, output logic vga_plot);
     // fill the screen
     assign done = (vga_x == 8'd159 && vga_y == 7'd119);

     always @(posedge clk,negedge rst_n) begin 
        if(!rst_n) begin vga_plot = 1'b0; vga_x = 8'b0; vga_y = 7'b111_1111; vga_colour = 3'b0; end
        else if(start && ~done) begin 
             if(vga_y == 7'd119) begin vga_y = 7'b0; vga_x = vga_x + 8'b1; end //increment vga_x after inner loop finishes 
             else begin vga_y = vga_y + 7'b1; end //increment vga_y 
                 vga_plot = 1'b1;
                 vga_colour = vga_x % 8;
        end
        else if(done) begin vga_plot = 1'b0; end //not plotting if finishing filling the whole screen 
     end
endmodule


