module tb_task2();
logic CLOCK_50;
logic [3:0] KEY; // KEY[3] is async active-low reset
logic [9:0] SW;
logic [9:0] LEDR;
logic [6:0] HEX0;
logic [6:0] HEX1;
logic [6:0] HEX2;
logic [6:0] HEX3;
logic [6:0] HEX4;
logic [6:0] HEX5;
logic [7:0] VGA_R;
logic [7:0] VGA_G;
logic [7:0] VGA_B;
logic VGA_HS;
logic VGA_VS;
logic VGA_CLK;

task2 dut(.*);

task test_fillscreen;
	for(int x = 0; x < 160; x++) begin 
		for(int y = 0; y < 120;y++) begin 
			assert(tb_task2.dut.fs.start === 1);
			assert(tb_task2.dut.vga_u0.x === x);
			assert(tb_task2.dut.vga_u0.y === y);
			assert(tb_task2.dut.vga_u0.colour === x % 8);
			assert(tb_task2.dut.vga_u0.plot === 1);
			#2;
		end
	end
endtask;
	
initial begin 
    CLOCK_50 = 1'b0;
    forever #1 CLOCK_50 = ~CLOCK_50;
end 

initial begin 	
	KEY[3] = 0; #5; KEY[3] = 1;//test reset 
	assert(tb_task2.dut.vga_u0.plot === 0); assert(tb_task2.dut.fs.start === 0); assert(tb_task2.dut.fs.done === 0); #2;
	assert(tb_task2.dut.fs.start === 1);#2;
	test_fillscreen;//test fill screen 
    assert(tb_task2.dut.fs.done === 1);
	assert(tb_task2.dut.vga_u0.plot === 0);//finish plotting 
    assert(tb_task2.dut.fs.start === 0);//de-assert start = 0
    #100; assert(tb_task2.dut.fs.done === 1); assert(tb_task2.dut.vga_u0.plot === 0); assert(tb_task2.dut.fs.start === 0);
    $display("task2 test done");
    $stop;
end

endmodule: tb_task2
