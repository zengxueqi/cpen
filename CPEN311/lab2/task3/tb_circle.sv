module tb_circle();

// Your testbench goes here. Our toplevel will give up after 1,000,000 ticks.
logic clk;
logic rst_n;
logic [2:0] colour;
logic [7:0] centre_x;
logic [6:0] centre_y;
logic [7:0] radius;
logic start;
logic done;
logic [7:0] vga_x;
logic [6:0] vga_y;
logic [2:0] vga_colour;
logic vga_plot;

logic [15:0] offset_y,offset_x,crit;

circle dut(.*);

task test_circle;
    logic [9:0] x,y;

    while(offset_y <= offset_x) begin
    	/*Test octant 1*/
    	assert(dut.count % 8 - 1 === 1);
    	x = centre_x + offset_x; y = centre_y + offset_y; #2;
    	if(x > 10'd159 || y > 10'd119) assert(vga_plot === 0);
    	else assert(vga_x === x[7:0] && vga_colour === colour && vga_y === y[6:0] && vga_plot === 1); #8;
    	/*Test octant 2*/
	assert(dut.count % 8 - 1 === 2);
    	x = centre_x + offset_y; y = centre_y + offset_x; #2;
    	if(x > 10'd159 || y > 10'd119) assert(vga_plot === 0);
    	else assert(vga_x === x[7:0] && vga_colour === colour && vga_y === y[6:0] && vga_plot === 1); #8;
    	/*Test octant 3*/
	assert(dut.count % 8 - 1 === 3);
    	x = centre_x - offset_y; y = centre_y + offset_x; #2;
    	if(x > 10'd159 || y > 10'd119) assert(vga_plot === 0);
    	else assert(vga_x === x[7:0] && vga_colour === colour && vga_y === y[6:0] && vga_plot === 1); #8;
    	/*Test octant 4*/
	assert(dut.count % 8 - 1 === 4);
    	x = centre_x - offset_x; y = centre_y + offset_y; #2;
    	if(x > 10'd159 || y > 10'd119) assert(vga_plot === 0);
    	else assert(vga_x === x[7:0] && vga_colour === colour && vga_y === y[6:0] && vga_plot === 1); #8;
    	/*Test octant 5*/
	assert(dut.count % 8 - 1 === 5);
    	x = centre_x - offset_x; y = centre_y - offset_y; #2;
    	if(x > 10'd159 || y > 10'd119) assert(vga_plot === 0);
    	else assert(vga_x === x[7:0] && vga_colour === colour && vga_y === y[6:0] && vga_plot === 1); #8;
    	/*Test octant 6*/
	assert(dut.count % 8 - 1 === 6);
    	x = centre_x - offset_y; y = centre_y - offset_x; #2;
    	if(x > 10'd159 || y > 10'd119) assert(vga_plot === 0);
    	else assert(vga_x === x[7:0] && vga_colour === colour && vga_y === y[6:0] && vga_plot === 1); #8;
    	/*Test octant 7*/
	assert(dut.count % 8 === 0);
    	x = centre_x + offset_x; y = centre_y - offset_y; #2;
    	if(x > 10'd159 || y > 10'd119) assert(vga_plot === 0);
    	else assert(vga_x === x[7:0] && vga_colour === colour && vga_y === y[6:0] && vga_plot === 1); #8;
    	/*Test octant 8*/
	assert(dut.count % 8 - 1 === 0);
    	x = centre_x + offset_y; y = centre_y - offset_x; #2;
    	if(x > 10'd159 || y > 10'd119) assert(vga_plot === 0);
    	else assert(vga_x === x[7:0] && vga_colour === colour && vga_y === y[6:0] && vga_plot === 1); #2;
    	offset_y = offset_y + 1; #2; assert(offset_y === dut.offset_y);
    	if(crit == 0 || crit[15] == 1) begin crit = crit + 2 * offset_y + 1;  #2; assert(crit === dut.crit);end
    	else begin offset_x = offset_x - 1; crit = crit + 2 * (offset_y - offset_x) + 1; #2;
        	assert(offset_x === dut.offset_x && offset_y === dut.offset_y); end #2;
    end
endtask 

initial begin 
    clk = 0;
    forever #5 clk = ~clk;
end

initial begin 
    /* Test 1: centre_x = 80, centre_y = 60, radius = 40,colour = 3'b10*/
    rst_n = 1'b0; start = 1'b1; centre_x = 8'd80; centre_y = 7'd60; radius = 8'd40; colour = 3'b10; #20;
    offset_x = radius; offset_y = 0; crit = 1 - radius; #10;
    assert(dut.count === 16'b1 && dut.offset_y === 16'b0 && dut.offset_x === radius && dut.crit === crit);
    assert(done === 1'b0 && vga_plot === 0);//test reset
    rst_n = 1'b1; #10;

    test_circle();//test circle 

    assert(done === 1'b1 && vga_plot === 1'b0); #100; assert(done === 1'b1 && vga_plot === 1'b0);
  
    /* Test 2: centre_x = 80, centre_y = 60, radius = 100,colour = 3'b100 */
    rst_n = 0; centre_x = 8'd80; centre_y = 7'd60; radius = 8'd100; colour = 3'b100; #40;
    assert(done === 1'b0 && vga_plot === 0);//test reset
    rst_n = 1'b1; offset_x = radius; offset_y = 0; crit = 1 - radius;#10;

    test_circle();//test circle 

    assert(done === 1'b1 && vga_plot === 1'b0);#100;assert(done === 1'b1 && vga_plot === 1'b0);
    
    /*Test 3: centre_x = 0, centre_y = 0, radius = 40,colour = 3'b11 */
    rst_n = 0; centre_x = 8'd0; centre_y = 7'd0; radius = 8'd40;colour = 3'b11;#40;
    assert(done === 1'b0 && vga_plot === 0);//test reset
    rst_n = 1'b1;offset_x = radius;offset_y = 0;crit = 1 - radius;#10;

    test_circle();//test circle 

    assert(done === 1'b1 && vga_plot === 1'b0);#100;assert(done === 1'b1 && vga_plot === 1'b0);
	
    /*Test 4: centre_x = 150, centre_y = 0,*/
    rst_n = 0; centre_x = 8'd150; centre_y = 7'd0; radius = 8'd40; colour = 3'b11; #40;
    assert(done === 1'b0 && vga_plot === 0);//test reset
    rst_n = 1'b1; offset_x = radius; offset_y = 0; crit = 1 - radius; #10;

    test_circle();//test circle 

    assert(done === 1'b1 && vga_plot === 1'b0); #100; assert(done === 1'b1 && vga_plot === 1'b0);
	
    /*Test 5: centre_x = 0, centre_y = 100,*/
    rst_n = 0; centre_x = 8'd0;centre_y = 7'd100; radius = 8'd40; colour = 3'b11; #40;
    assert(done === 1'b0 && vga_plot === 0);//test reset
    rst_n = 1'b1;offset_x = radius;offset_y = 0;crit = 1 - radius; #10;

    test_circle();//test circle 

    assert(done === 1'b1 && vga_plot === 1'b0);#100; assert(done === 1'b1 && vga_plot === 1'b0);
	
    /*Test 6: centre_x = 150, centre_y = 100,*/
    rst_n = 0;centre_x = 8'd150;centre_y = 7'd100;radius = 8'd40; colour = 3'b11;#40;
    assert(done === 1'b0 && vga_plot === 0);//test reset
    rst_n = 1'b1;offset_x = radius;offset_y = 0;crit = 1 - radius;#10;

    test_circle();//test circle 

    assert(done === 1'b1 && vga_plot === 1'b0); #100; assert(done === 1'b1 && vga_plot === 1'b0);
    $display("circle test done");
    $stop;
end
endmodule: tb_circle
