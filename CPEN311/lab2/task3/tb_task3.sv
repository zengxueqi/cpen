module tb_task3();

// Your testbench goes here. Our toplevel will give up after 1,000,000 ticks.
logic CLOCK_50;
logic [3:0] KEY; // KEY[3] is async active-low reset
logic [9:0] SW;
logic [9:0] LEDR;
logic [6:0] HEX0;
logic [6:0] HEX1;
logic [6:0] HEX2;
logic [6:0] HEX3;
logic [6:0] HEX4;
logic [6:0] HEX5;
logic [7:0] VGA_R;
logic [7:0] VGA_G;
logic [7:0] VGA_B;
logic VGA_HS;
logic VGA_VS;
logic VGA_CLK;

logic [7:0] centre_x;
logic [6:0] centre_y;
logic [7:0] radius;
logic [7:0] offset_y,offset_x,crit;
logic [2:0] colour;

task3 dut(.*);

task test_clearscreen;
	for(int x = 0; x < 160; x++) begin
		for(int y = 0; y < 120;y++) begin
			assert(tb_task3.dut.vga_u0.x === x);
			assert(tb_task3.dut.vga_u0.y === y);
            		assert(tb_task3.dut.vga_u0.colour === 3'b0);
			assert(tb_task3.dut.vga_u0.plot === 1);
           		assert(dut.vga_x === dut.vga_x_cs);
           		assert(dut.vga_y === dut.vga_y_cs);
            		assert(dut.vga_colour === dut.vga_colour_cs);
            		assert(dut.vga_plot === dut.vga_plot_cs);
			#10;
		end
	end
endtask;

task test_vga_mux;
    assert(dut.vga_x === dut.vga_x_c);
    assert(dut.vga_colour === dut.vga_colour_c);
    assert(dut.vga_plot === dut.vga_plot_c);
    assert(dut.vga_y === dut.vga_y_c);
endtask;

task test_circle;
    logic [9:0] x,y;
    logic test;
    assign test = (tb_task3.dut.vga_u0.x === x[7:0] && tb_task3.dut.vga_u0.colour === colour && 
                   tb_task3.dut.vga_u0.y === y[6:0] && tb_task3.dut.vga_u0.plot === 1);

    while(offset_y <= offset_x) begin
	 /* Test octant 1*/
        test_vga_mux;
    	x = centre_x + offset_x; y = centre_y + offset_y; #2;
    	if(x > 10'd159 || y > 10'd119) assert(tb_task3.dut.vga_u0.plot === 0);
        else assert(test === 1); #8;
    	/* Test octant 2*/
        test_vga_mux;
    	x = centre_x + offset_y; y = centre_y + offset_x; #2;
    	if(x > 10'd159 || y > 10'd119) assert(tb_task3.dut.vga_u0.plot === 0);
        else assert(test === 1); #8;
    	/* Test octant 3*/
        test_vga_mux;
    	x = centre_x - offset_y; y = centre_y + offset_x; #2;
    	if(x > 10'd159 || y > 10'd119) assert(tb_task3.dut.vga_u0.plot === 0);
        else assert(test === 1); #8;
	/* Test octant 4*/
        test_vga_mux;
       	x = centre_x - offset_x; y = centre_y + offset_y; #2;
    	if(x > 10'd159 || y > 10'd119) assert(tb_task3.dut.vga_u0.plot === 0);
        else assert(test === 1); #8;
    	/* Test octant 5*/
        test_vga_mux;
    	x = centre_x - offset_x; y = centre_y - offset_y; #2;
    	if(x > 10'd159 || y > 10'd119) assert(tb_task3.dut.vga_u0.plot === 0);
        else assert(test === 1); #8;
    	/* Test octant 6*/
        test_vga_mux;
	    x = centre_x - offset_y; y = centre_y - offset_x; #2;
    	if(x > 10'd159 || y > 10'd119) assert(tb_task3.dut.vga_u0.plot === 0);
        else assert(test === 1); #8;
    	/* Test octant 7*/
        test_vga_mux;
    	x = centre_x + offset_x; y = centre_y - offset_y; #2;
    	if(x > 10'd159 || y > 10'd119) assert(tb_task3.dut.vga_u0.plot === 0);
        else assert(test === 1); #8;
    	/* Test octant 8*/
        test_vga_mux;
    	x = centre_x + offset_y; y = centre_y - offset_x; #2;
    	if(x > 10'd159 || y > 10'd119) assert(tb_task3.dut.vga_u0.plot === 0);
        else assert(test === 1); #2;
    	offset_y = offset_y + 1; #2;
    	if(crit == 0 || crit[7] == 1) begin crit = crit + 2 * offset_y + 1; end
    	else begin offset_x = offset_x - 1; crit = crit + 2 * (offset_y - offset_x) + 1; end #4;
    end
endtask

initial begin 
    CLOCK_50 = 1'b0;
    forever #5 CLOCK_50 = ~CLOCK_50;
end 

initial begin 
    KEY[3] = 1'b0; #40;
    assert(tb_task3.dut.start_cs === 1 && tb_task3.dut.start_c === 0);
    assert(tb_task3.dut.done_cs === 0 && tb_task3.dut.done_c === 0);//test reset
    KEY[3] = 1'b1;
    centre_x = tb_task3.dut.centre_x; centre_y = tb_task3.dut.centre_y;radius = tb_task3.dut.radius; colour = tb_task3.dut.colour; #10;
    offset_x = radius; offset_y = 0; crit = 1 - radius;
    //test clear screen
    test_clearscreen;
    //clear screen done
    assert(tb_task3.dut.done_cs === 1); assert(tb_task3.dut.start_cs === 0); assert(tb_task3.dut.start_c === 1); #10;
    test_circle();//test circle 
    //draw circle done
    assert(tb_task3.dut.done_c === 1);assert(tb_task3.dut.start_c === 0); assert(tb_task3.dut.vga_u0.plot === 0); #100;
    assert(tb_task3.dut.done_c === 1); assert(tb_task3.dut.start_c === 0); assert(tb_task3.dut.vga_u0.plot === 0);
    $display("task3 test done");
    $stop;
end

endmodule: tb_task3
