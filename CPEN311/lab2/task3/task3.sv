module task3(input logic CLOCK_50, input logic [3:0] KEY, // KEY[3] is async active-low reset
             input logic [9:0] SW, output logic [9:0] LEDR,
             output logic [6:0] HEX0, output logic [6:0] HEX1, output logic [6:0] HEX2,
             output logic [6:0] HEX3, output logic [6:0] HEX4, output logic [6:0] HEX5,
             output logic [7:0] VGA_R, output logic [7:0] VGA_G, output logic [7:0] VGA_B,
             output logic VGA_HS, output logic VGA_VS, output logic VGA_CLK);
        
         wire [7:0] centre_x,vga_x_c,radius,vga_x_cs,vga_x;
         wire [6:0] centre_y,vga_y_c,vga_y_cs,vga_y;
         wire [2:0] colour,vga_colour,vga_colour_cs,vga_colour_c;
         wire start_c,done_c,vga_plot_c,start_cs,done_cs,vga_plot_cs,vga_plot;

        wire [9:0] VGA_R_10,VGA_G_10,VGA_B_10;
        wire VGA_BLANK,VGA_SYNC;

        assign VGA_R = VGA_R_10[9:2];
        assign VGA_G = VGA_G_10[9:2];
        assign VGA_B = VGA_B_10[9:2];
        
       reset3 R3(.clk(CLOCK_50),
                .rst(KEY[3]),
	        	.done_c(done_c),
                .done_cs(done_cs),
                .start_c(start_c),
                .start_cs(start_cs));

        assign vga_x = start_cs ? vga_x_cs : vga_x_c;
        assign vga_y = start_cs ? vga_y_cs : vga_y_c;
        assign vga_colour = start_cs ? vga_colour_cs : vga_colour_c;
        assign vga_plot = start_cs ? vga_plot_cs : vga_plot_c;
        
        assign centre_x = 8'd80;
        assign centre_y = 7'd60;
        assign radius = 8'd40;
        assign colour = 3'b10;

          circle fk(.clk(CLOCK_50),
                    .rst_n(KEY[3]),
                    .colour(colour),
                    .centre_x(centre_x),
                    .centre_y(centre_y),
                    .radius(radius),
                    .start(start_c),
                    .done(done_c),
                    .vga_x(vga_x_c),
                    .vga_y(vga_y_c),
                    .vga_colour(vga_colour_c),
                    .vga_plot(vga_plot_c));

         clearscreen cs(.clk(CLOCK_50),
                        .rst_n(KEY[3]),
                        .colour(SW[2:0]),
                        .start(start_cs),
                        .done(done_cs),
                        .vga_x(vga_x_cs),
                        .vga_y(vga_y_cs),
                        .vga_colour(vga_colour_cs),
                        .vga_plot(vga_plot_cs));

         vga_adapter #(.RESOLUTION("160x120")) 
                 vga_u0(.resetn(KEY[3]),
                        .clock(CLOCK_50),
                        .colour(vga_colour),
                        .x(vga_x),
                        .y(vga_y),
                        .plot(vga_plot),
                        .VGA_R(VGA_R_10),
                        .VGA_G(VGA_G_10),
                        .VGA_B(VGA_B_10),
                        .VGA_HS(VGA_HS),
                        .VGA_VS(VGA_VS),
                        .VGA_BLANK(VGA_BLANK),
                        .VGA_SYNC(VGA_SYNC),
                        .VGA_CLK(VGA_CLK));

endmodule: task3

module reset3(input clk,input rst,input done_c,input done_cs,output logic start_c,output logic start_cs);
    always @(posedge clk) begin 
        if(!rst) begin
            start_cs = 1;
            start_c = 0;
        end
	else if(rst && done_c) begin 
	    start_c = 0;
        end
        else if(rst && done_cs) begin 
            start_cs = 0;
            start_c = 1;
        end
    end
endmodule

