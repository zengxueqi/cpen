module tb_clearscreen();

logic clk;
logic rst_n;
logic [2:0] colour;
logic start;
logic done;
logic [7:0] vga_x;
logic [6:0] vga_y;
logic [2:0] vga_colour;
logic vga_plot;

clearscreen dut(.*);

task counter_test;
	for(int x = 0; x < 160; x++) begin 
		for(int y = 0; y < 120;y++) begin 
			assert(tb_clearscreen.dut.vga_x === x);
			assert(tb_clearscreen.dut.vga_y === y);
			assert(tb_clearscreen.dut.vga_plot === 1);
			#2;
		end
	end
endtask;

initial begin
    clk = 1'b0;
    forever #1 clk = ~clk;
end

initial begin
    rst_n = 0; start = 1;#5; rst_n = 1;//test reset
    assert(vga_x === 8'b0 && vga_y === 7'b111_1111); assert(tb_clearscreen.dut.done === 0 && tb_clearscreen.dut.vga_plot === 0); #2;
    counter_test;
    assert(tb_clearscreen.dut.done === 1 && tb_clearscreen.dut.vga_plot === 0);//done
    $stop;
end
// Your testbench goes here. Our toplevel will give up after 1,000,000 ticks.

endmodule: tb_clearscreen
