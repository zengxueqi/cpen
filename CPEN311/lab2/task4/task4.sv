module task4(input logic CLOCK_50, input logic [3:0] KEY, // KEY[3] is async active-low reset
             input logic [9:0] SW, output logic [9:0] LEDR,
             output logic [6:0] HEX0, output logic [6:0] HEX1, output logic [6:0] HEX2,
             output logic [6:0] HEX3, output logic [6:0] HEX4, output logic [6:0] HEX5,
             output logic [7:0] VGA_R, output logic [7:0] VGA_G, output logic [7:0] VGA_B,
             output logic VGA_HS, output logic VGA_VS, output logic VGA_CLK);
  
          wire [7:0] centre_x,vga_x_r,diameter,vga_x_cs,vga_x;
         wire [6:0] centre_y,vga_y_r,vga_y_cs,vga_y;
         wire [2:0] colour,vga_colour,vga_colour_cs,vga_colour_r;
         wire start_r,done_r,vga_plot_r,start_cs,done_cs,vga_plot_cs,vga_plot;

        wire [9:0] VGA_R_10,VGA_G_10,VGA_B_10;
        wire VGA_BLANK,VGA_SYNC;

        assign VGA_R = VGA_R_10[9:2];
        assign VGA_G = VGA_G_10[9:2];
        assign VGA_B = VGA_B_10[9:2];

        reset4 R4(.clk(CLOCK_50),
                .rst(KEY[3]),
		.done_r(done_r),
                .done_cs(done_cs),
                .start_r(start_r),
                .start_cs(start_cs));

        assign vga_x = start_cs ? vga_x_cs : vga_x_r;
        assign vga_y = start_cs ? vga_y_cs : vga_y_r;
        assign vga_colour = start_cs ? vga_colour_cs : vga_colour_r;
        assign vga_plot = start_cs ? vga_plot_cs : vga_plot_r;

        assign centre_x = 8'd80;
        assign centre_y = 7'd60;
        assign diameter = 8'd80;
        assign colour = 3'b10;

        reuleaux fk(.clk(CLOCK_50),
                    .rst_n(KEY[3]),
                    .colour(colour),
                    .centre_x(centre_x),
                    .centre_y(centre_y),
                    .diameter(diameter),
                    .start(start_r),
                    .done(done_r),
                    .vga_x(vga_x_r),
                    .vga_y(vga_y_r),
                    .vga_colour(vga_colour_r),
                    .vga_plot(vga_plot_r));

         clearscreen cs(.clk(CLOCK_50),
                        .rst_n(KEY[3]),
                        .colour(SW[2:0]),
                        .start(start_cs),
                        .done(done_cs),
                        .vga_x(vga_x_cs),
                        .vga_y(vga_y_cs),
                        .vga_colour(vga_colour_cs),
                        .vga_plot(vga_plot_cs));

         vga_adapter #(.RESOLUTION("160x120")) 
                 vga_u0(.resetn(KEY[3]),
                        .clock(CLOCK_50),
                        .colour(vga_colour),
                        .x(vga_x),
                        .y(vga_y),
                        .plot(vga_plot),
                        .VGA_R(VGA_R_10),
                        .VGA_G(VGA_G_10),
                        .VGA_B(VGA_B_10),
                        .VGA_HS(VGA_HS),
                        .VGA_VS(VGA_VS),
                        .VGA_BLANK(VGA_BLANK),
                        .VGA_SYNC(VGA_SYNC),
                        .VGA_CLK(VGA_CLK));

endmodule: task4

module reset4(input clk,input rst,input done_r,input done_cs,output logic start_r,output logic start_cs);
    always @(posedge clk) begin
        if(!rst) begin
            start_cs = 1;
            start_r = 0;
        end
	else if(done_r) begin 
		start_r = 0;
	end
        else if(done_cs) begin
            start_cs = 0;
            start_r = 1;
        end
    end
    endmodule

