/*
 * Clear screen to black before starting drawing circles 
 */
module clearscreen(input logic clk, input logic rst_n, input logic [2:0] colour,
                  input logic start, output logic done,
                  output logic [7:0] vga_x, output logic [6:0] vga_y,
                  output logic [2:0] vga_colour, output logic vga_plot);
     assign done = (vga_x == 8'd159 && vga_y == 7'd119);
     assign vga_colour = 3'b0;

     always @(posedge clk,negedge rst_n) begin 
        if(!rst_n) begin vga_plot = 1'b0; vga_x = 8'b0; vga_y = 7'b111_1111; end
        else if(start && ~done) begin 
             if(vga_y == 7'd119) begin vga_y = 7'b0; vga_x = vga_x + 8'b1;end
             else begin vga_y = vga_y + 7'b1; end
                 vga_plot = 1'b1;
        end
        else if(done) begin vga_plot = 1'b0; end
     end
endmodule

