module tb_task4();

// Your testbench goes here. Our toplevel will give up after 1,000,000 ticks.
logic CLOCK_50;
logic [3:0] KEY; // KEY[3] is async active-low reset
logic [9:0] SW;
logic [9:0] LEDR;
logic [6:0] HEX0;
logic [6:0] HEX1;
logic [6:0] HEX2;
logic [6:0] HEX3;
logic [6:0] HEX4;
logic [6:0] HEX5;
logic [7:0] VGA_R;
logic [7:0] VGA_G;
logic [7:0] VGA_B;
logic VGA_HS;
logic VGA_VS;
logic VGA_CLK;

logic [15:0] offset_y,offset_x,crit,offset_y1,offset_x1,crit1,offset_x2,offset_y2,crit2;
logic [7:0] diameter;
logic [2:0] colour;

task4 dut(.*);

task test_clearscreen;
	for(int x = 0; x < 160; x++) begin
        	for(int y = 0; y < 120;y++) begin
			assert(tb_task4.dut.vga_u0.x === x);
			assert(tb_task4.dut.vga_u0.y === y);
            		assert(tb_task4.dut.vga_u0.colour === 3'b0);
			assert(tb_task4.dut.vga_u0.plot === 1);
            		assert(dut.vga_x === dut.vga_x_cs);
            		assert(dut.vga_y === dut.vga_y_cs);
            		assert(dut.vga_colour === dut.vga_colour_cs);
            		assert(dut.vga_plot === dut.vga_plot_cs);
			#10;
		end
    	end
endtask;

task test_vga_mux;
    assert(dut.vga_x === dut.vga_x_r);
    assert(dut.vga_colour === dut.vga_colour_r);
    assert(dut.vga_plot === dut.vga_plot_r);
    assert(dut.vga_y === dut.vga_y_r);
endtask;

task test_bottom_arc;
    logic [9:0] x,y;
    logic test;
    assign test = (x === tb_task4.dut.vga_u0.x && y === tb_task4.dut.vga_u0.y && tb_task4.dut.vga_u0.plot === 1 && dut.vga_u0.colour === colour);

    while(!dut.fk.count30_done) begin
        test_vga_mux;
	x = tb_task4.dut.fk.centre_x_c1 - offset_y1;y = tb_task4.dut.fk.centre_y_c1 + offset_x1; #2;
        assert(test === 1);
        offset_y1 = offset_y1 + 1; #2;
        if(crit1[15] == 1 || crit1 == 16'b0) begin crit1 = crit1 + 2 * offset_y1 + 1;  end
        else begin offset_x1 = offset_x1 - 1; crit1 = crit1 + 2 * (offset_y1 - offset_x1) + 1; end #6;
    
        test_vga_mux;
        x = tb_task4.dut.fk.centre_x_c1 + offset_y1; y = tb_task4.dut.fk.centre_y_c1 + offset_x1; #2;
        assert(test === 1); #8;
    end
endtask

task test_full_octant;
    logic [9:0] x,y;
    logic test;
    assign test = (x === tb_task4.dut.vga_u0.x && y === tb_task4.dut.vga_u0.y && tb_task4.dut.vga_u0.plot === 1 && dut.vga_u0.colour === colour);

    while(offset_y <= offset_x) begin
        test_vga_mux;
        x = tb_task4.dut.fk.centre_x_c2 + offset_x; y = tb_task4.dut.fk.centre_y_c2 - offset_y; #2;
        assert(test === 1);#8;
        test_vga_mux;
        x = tb_task4.dut.fk.centre_x_c3 - offset_x; y = tb_task4.dut.fk.centre_y_c3 - offset_y; #2;
        assert(test === 1);
        offset_y = offset_y + 1; #2;
        if(crit[15] == 1 || crit == 16'b0) begin crit = crit + 2 * offset_y + 1;end
        else begin offset_x = offset_x - 1; crit = crit + 2 * (offset_y - offset_x) + 1; end #6;
    end
endtask

initial begin 
    CLOCK_50 = 1'b0;
    forever #5 CLOCK_50 = ~CLOCK_50;
end 

initial begin 
    KEY[3] = 1'b0; #40; //test reset
    assert(tb_task4.dut.start_cs === 1 && tb_task4.dut.start_r === 0);
    assert(tb_task4.dut.done_cs === 0 && tb_task4.dut.done_r === 0);
    diameter = tb_task4.dut.diameter; colour = tb_task4.dut.colour; KEY[3] = 1'b1; #10;

    test_clearscreen;//test clear screen

    assert(tb_task4.dut.done_cs === 1); assert(tb_task4.dut.start_cs === 0);assert(tb_task4.dut.start_r === 1);

    offset_x1 = diameter; offset_y1 = 0;crit1 = 1 - diameter;offset_x = diameter;offset_y = 0; crit = 1 - diameter; #10;
    test_bottom_arc();#10; test_full_octant();#1000;
    assert(tb_task4.dut.done_r === 1); assert(tb_task4.dut.start_r === 0); assert(tb_task4.dut.vga_u0.plot === 0);
    $display("task4 tb done");
    $stop;
end

endmodule: tb_task4

