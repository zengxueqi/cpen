module tb_reuleaux();

// Your testbench goes here. Our toplevel will give up after 1,000,000 ticks.
logic clk;
logic rst_n;
logic [2:0] colour;
logic [7:0] centre_x;
logic [6:0] centre_y;
logic [7:0] diameter;
logic start;
logic done;
logic [7:0] vga_x;
logic [6:0] vga_y;
logic [2:0] vga_colour;
logic vga_plot;

logic [15:0] offset;
logic [15:0] offset_y,offset_x,crit,offset_y1,offset_x1,crit1,offset_x2,offset_y2,crit2;
logic [15:0] centre_x_c1,centre_x_c2,centre_x_c3;
logic [15:0] centre_y_c1,centre_y_c2,centre_y_c3;

reuleaux dut(.*);

initial begin 
    clk = 0;
    forever #5 clk = ~clk;
end

task test_bottom_arc;
    logic [9:0] x,y;
    logic test;
    assign test = (x === tb_reuleaux.dut.vga_x && y === tb_reuleaux.dut.vga_y && tb_reuleaux.dut.vga_plot === 1 && vga_colour === colour);

    while(!dut.count30_done) begin 
        assert(dut.count30 % 2 === 1);
	    x = tb_reuleaux.dut.centre_x_c1 - offset_y1; y = tb_reuleaux.dut.centre_y_c1 + offset_x1; #2; 
        if(x > 10'd159 || y > 10'd119) assert(vga_plot === 0);
        else assert(test === 1);
        offset_y1 = offset_y1 + 1; #2; assert(offset_y1 === dut.offset_y1);
        if(crit1[15] == 1 || crit1 == 16'b0) begin crit1 = crit1 + 2 * offset_y1 + 1; #2; assert(dut.crit1 === crit1); end
        else begin offset_x1 = offset_x1 - 1; crit1 = crit1 + 2 * (offset_y1 - offset_x1) + 1; #2;
            assert(dut.offset_x1 === offset_x1 && dut.crit1 === crit1); end #2;
        if(x === dut.centre_x_c2) assert(dut.done_octant2 === 1);#2;
        assert(dut.count30 % 2 === 0);
        x = tb_reuleaux.dut.centre_x_c1 + offset_y1; y = tb_reuleaux.dut.centre_y_c1 + offset_x1; #2;
        if(x > 10'd159 || y > 10'd119) assert(vga_plot === 0);
        else assert(test === 1); #2;
        if(x === dut.centre_x_c3) assert(dut.done_octant3 === 1); #6;
    end
endtask

task test_full_octant;
    logic [9:0] x,y;
    logic test;
    assign test = (x === tb_reuleaux.dut.vga_x && y === tb_reuleaux.dut.vga_y && tb_reuleaux.dut.vga_plot === 1 && vga_colour === colour);
   
    while(offset_y <= offset_x) begin
        assert(dut.count45 % 2 === 1);
        x = tb_reuleaux.dut.centre_x_c2 + offset_x; y = tb_reuleaux.dut.centre_y_c2 - offset_y; #2; 
        if(x > 10'd159 || y > 10'd119) assert(vga_plot === 0);
        else assert(test === 1);#8;
        assert(dut.count45 % 2 === 0);
        x = tb_reuleaux.dut.centre_x_c3 - offset_x; y = tb_reuleaux.dut.centre_y_c3 - offset_y; #2;
        if(x > 10'd159 || y > 10'd119) assert(vga_plot === 0);
        else assert(test === 1);
        offset_y = offset_y + 1; #2; assert(offset_y === dut.offset_y);
        if(crit[15] == 1 || crit == 16'b0) begin crit = crit + 2 * offset_y + 1; #2; assert(dut.crit === crit);end
        else begin offset_x = offset_x - 1; crit = crit + 2 * (offset_y - offset_x) + 1; #2;
            assert(dut.offset_x === offset_x && dut.crit === crit); end #2;
		if(dut.centre_x_c2 + offset_y == dut.centre_x_c3 - offset_y && dut.centre_y_c2 - offset_x == dut.centre_y_c3 - offset_x)
			begin offset_x2 = offset_x; offset_y2 = offset_y; crit2 = crit; end #2;
    end
endtask

task test_15_degree;    
    logic [9:0] x,y;
    logic test;
    assign test = (x === tb_reuleaux.dut.vga_x && y === tb_reuleaux.dut.vga_y && tb_reuleaux.dut.vga_plot === 1 && vga_colour === colour);
	
	assert(offset_x2 == dut.offset_x2 && offset_y2 == dut.offset_y2 && crit2 == dut.crit2);
    while(offset_y2 <= offset_x2) begin 
        assert(dut.count15 % 2 === 1);
		x = dut.centre_x_c2 + offset_y2; y = dut.centre_y_c2 - offset_x2; #2;
        if(x > 10'd159 || y > 10'd119) assert(vga_plot === 0);
        else assert(test === 1); #8;
        assert(dut.count15 % 2 === 0);
		x = dut.centre_x_c3 - offset_y2; y = dut.centre_y_c3 - offset_x2; #2;
        if(x > 10'd159 || y > 10'd119) assert(vga_plot === 0);
        else assert(test === 1);
        offset_y2 = offset_y2 + 1; #2; assert(offset_y2 === dut.offset_y2);
        if(crit2[15] == 1 || crit2 == 16'b0) begin crit2 = crit2 + 2 * offset_y2 + 1; #2; assert(crit2 === dut.crit2); end
        else begin offset_x2 = offset_x2 - 1; crit2 = crit2 + 2 * (offset_y2 - offset_x2) + 1; #2;
            assert(dut.offset_x2 === offset_x2 && dut.crit2 === crit2); end #4;
    end
endtask

task test_reuleaux;
	assert(done === 1'b0); #10;
    assert(tb_reuleaux.dut.count30_done === 0); test_bottom_arc(); assert(tb_reuleaux.dut.count30_done === 1); #10;
    assert(tb_reuleaux.dut.count45_done === 0); test_full_octant(); assert(tb_reuleaux.dut.count45_done === 1);
	assert(tb_reuleaux.dut.count15_done === 0); test_15_degree(); assert(tb_reuleaux.dut.count15_done === 1);
    assert(done === 1'b1 && vga_plot === 1'b0); #100; assert(done === 1'b1 && vga_plot === 1'b0);
endtask 

task test_reset;
	offset = diameter * diameter; #10; offset = $sqrt(offset / 3);
	offset_x1 = diameter; offset_y1 = 0; crit1 = 1 - diameter;
	offset_x = diameter; offset_y = 0; crit = 1 - diameter; crit2 = 1 - diameter; #10;
	centre_x_c1 = centre_x; centre_y_c1 = centre_y - offset;
    centre_x_c2 = centre_x - diameter / 2; centre_y_c2 = centre_y + offset / 2;
    centre_x_c3 = centre_x + diameter / 2; centre_y_c3 = centre_y + offset / 2; #10;

    //test coordinates of 3 endpoints of triangle 
    assert(offset === dut.offset);
    assert(dut.centre_x_c1 === centre_x_c1);
    assert(dut.centre_y_c1 === centre_y_c1);
    assert(dut.centre_x_c2 === centre_x_c2);
    assert(dut.centre_y_c2 === centre_y_c2);
    assert(dut.centre_x_c3 === centre_x_c3);
    assert(dut.centre_y_c3 === centre_y_c3);
  
    //test reset 
    assert(dut.offset_y === 16'b0 && dut.offset_x === diameter && dut.crit === crit && dut.crit1 === crit1 && dut.crit2 === crit2);
    assert(dut.done_octant2 === 0 && dut.done_octant3 === 0 && dut.y30 === 8'b0);
    assert(dut.offset_y1 === 16'b0 && dut.offset_x1 === diameter && dut.offset_y2 === 16'b0 && dut.offset_x2 === diameter);
    assert(dut.vga_plot === 1'b0 && dut.count45 === 16'b0 && dut.count30 === 16'b0 && dut.count15 === 16'b0);
endtask

initial begin 
	/*Test 1: centre_x = 80,centre_y = 60, diameter = 40*/
    rst_n = 1'b1; centre_x = 8'd80; centre_y = 7'd60; diameter = 8'd40; colour = 3'b10; start = 1'b1; #10;
	rst_n = 1'b0;  test_reset(); rst_n = 1'b1; test_reuleaux();
	
	/*Test 2: centre_x = 80,centre_y = 60, diameter = 140*/
	rst_n = 1'b1; centre_x = 8'd80; centre_y = 7'd60; diameter = 8'd140; colour = 3'b10; start = 1'b1; #10;
	rst_n = 1'b0;  test_reset(); rst_n = 1'b1; test_reuleaux();
	
	/*Test 3: centre_x = 10,centre_y = 10, diameter = 80*/
	rst_n = 1'b1; centre_x = 8'd10; centre_y = 7'd10; diameter = 8'd80; colour = 3'b10; start = 1'b1; #10;
	rst_n = 1'b0;  test_reset(); rst_n = 1'b1; test_reuleaux();

	/*Test 4: centre_x = 150,centre_y = 10, diameter = 80*/
	rst_n = 1'b1; centre_x = 8'd150; centre_y = 7'd10; diameter = 8'd80; colour = 3'b10; start = 1'b1; #10;
	rst_n = 1'b0;  test_reset(); rst_n = 1'b1; test_reuleaux();
	
	/*Test 5: centre_x = 150,centre_y = 100, diameter = 80*/
	rst_n = 1'b1; centre_x = 8'd150; centre_y = 7'd100; diameter = 8'd80; colour = 3'b10; start = 1'b1; #10;
	rst_n = 1'b0;  test_reset(); rst_n = 1'b1; test_reuleaux();
    $display("reuleaux test done");
    $stop;
end

endmodule: tb_reuleaux

