module reuleaux(input logic clk, input logic rst_n, input logic [2:0] colour,
                input logic [7:0] centre_x, input logic [6:0] centre_y, input logic [7:0] diameter,
                input logic start, output logic done,
                output logic [7:0] vga_x, output logic [6:0] vga_y,
                output logic [2:0] vga_colour, output logic vga_plot);
     // draw the Reuleaux triangle
     logic [15:0] centre_x_c1,centre_x_c2,centre_x_c3;
     logic [15:0] centre_y_c1,centre_y_c2,centre_y_c3;
     logic count45_done,count30_done,count15_done,done_octant2,done_octant3;
     logic [15:0] count45,count30,count15;
     logic [15:0] offset_y,offset_x,crit,offset_y1,offset_x1,crit1,offset_x2,offset_y2,crit2;
     logic [7:0] offset, sqrt3,y30;
     logic [15:0] temp;
     logic signed [9:0] x,y;

     assign sqrt3 = 8'b100101;//0.578 = 1 / sqrt(3)
     assign temp = sqrt3 * diameter;
     assign offset = temp[5] ? temp[13:6] + 1 : temp[13:6];//calculate diameter / sqrt(3)
	 
     assign count45_done = (offset_y > offset_x);
     assign count30_done = (done_octant2 && done_octant3);
     assign count15_done = (offset_y2 > offset_x2);

     assign centre_x_c1 = centre_x; 
     assign centre_y_c1 = centre_y - offset;//circle 1
     assign centre_x_c2 = centre_x - diameter / 2; 
     assign centre_y_c2 = centre_y + offset / 2;//circle 2
     assign centre_x_c3 = centre_x + diameter / 2; 
     assign centre_y_c3 = centre_y + offset / 2;//circle 3

     assign vga_colour = colour;
     assign vga_x = x[7:0];
     assign vga_y = y[6:0];
     assign done = (count15_done && count30_done && count45_done);

     always @(posedge clk,negedge rst_n) begin 
        if(!rst_n) begin offset_y = 16'b0; offset_x = diameter; crit = 1 - diameter; 
                done_octant2 = 0;done_octant3 = 0; y30 = 8'b0;
                offset_y1 = 16'b0;offset_x1 = diameter; crit1 = 1 - diameter;
		offset_y2 = 16'b0;offset_x2 = diameter; crit2 = 1 - diameter;
                vga_plot = 1'b0; count45 = 16'b0; count30 = 16'b0; count15 = 16'b0; 
        end

        /*Step1: Draw 2/3 octant (30 degree):Octant 2 && Octant 3 of circle 1*/
		else if(start && !count30_done) begin 
            if(count30 % 2 == 1) begin x = centre_x_c1 + offset_y1; y = centre_y_c1 + offset_x1;
				if(centre_x_c3 == x ) done_octant3 = 1; end 
            else begin x = centre_x_c1 - offset_y1; y = centre_y_c1 + offset_x1; offset_y1 = offset_y1 + 1;
				if(x == centre_x_c2) done_octant2 = 1;
                if(crit1[15] == 1 || crit1 == 16'b0) begin crit1 = crit1 + 2 * offset_y1 + 1; end 
                else begin offset_x1 = offset_x1 - 1; crit1 = crit1 + 2 * (offset_y1 - offset_x1) + 1; end
            end
		    count30 = count30 + 1;//increment the counter
            /*Get the start y coordinate of full octant in step2*/
            y30 = y[7:0]; 

            if(x > 10'd159 || y > 10'd119 || $signed(x) < 0 || $signed(y) < 0) vga_plot = 1'b0;
            else  vga_plot = 1'b1;
        end

        /*Step2: Draw a full octant:(1)Octant 7 of circle 2.(2)Octant 5 of circle 3*/
        else if(start && !count45_done) begin
	        if(count45 % 2 == 0) begin x = centre_x_c2 + offset_x; y = centre_y_c2 - offset_y; end//octant 7,circle 2
            else begin x = centre_x_c3 - offset_x; y = centre_y_c3 - offset_y; offset_y = offset_y + 1;//ostant 5,circle 3
                if(crit[15] == 1 || crit == 16'b0) begin crit = crit + 2 * offset_y + 1;end
                else begin offset_x = offset_x - 1; crit = crit + 2 * (offset_y - offset_x) + 1; end
            end
            count45 = count45 + 1;//increment the counter
            /*Get offset & crit of 1/3 octant in step3*/
			if(centre_x_c2 + offset_y == centre_x_c3 - offset_y && centre_y_c2 - offset_x == centre_y_c3 - offset_x)
			begin offset_x2 = offset_x; offset_y2 = offset_y; crit2 = crit; end 

            if(x > 10'd159 || y > 10'd119 || $signed(x) < 0 || $signed(y) < 0 || y > y30) vga_plot = 1'b0;
            else vga_plot = 1'b1;
        end

        /* Step3: Draw 1/3 octant (15 degree): (1) octant 8 of circle 2, (2) octant 6 of circle 3 */
	    else if(start && !count15_done) begin 
	        if(count15 % 2 == 0) begin x = centre_x_c2 + offset_y2; y = centre_y_c2 - offset_x2; end//octant 8,circle 2
            else begin x = centre_x_c3 - offset_y2; y = centre_y_c3 - offset_x2; offset_y2 = offset_y2 + 1; //octant 6,circle 3
                if(crit2[15] == 1 || crit2 == 16'b0) begin crit2 = crit2 + 2 * offset_y2 + 1; end 
                else begin offset_x2 = offset_x2 - 1; crit2 = crit2 + 2 * (offset_y2 - offset_x2) + 1; end
            end 
	        count15 = count15 + 1;//increment the counter
		    if(x > 10'd159 || y > 10'd119 || $signed(x) < 0 || $signed(y) < 0) vga_plot = 1'b0;
            else vga_plot = 1'b1;
    	end
        else if(done) vga_plot = 1'b0;
    end 
endmodule
