module tb_ball();
logic CLOCK_50;
logic [3:0] KEY; // KEY[3] is async active-low reset
logic [9:0] SW;
logic [9:0] LEDR;
logic [6:0] HEX0;
logic [6:0] HEX1;
logic [6:0] HEX2;
logic [6:0] HEX3;
logic [6:0] HEX4;
logic [6:0] HEX5;
logic [7:0] VGA_R;
logic [7:0] VGA_G;
logic [7:0] VGA_B;
logic VGA_HS;
logic VGA_VS;
logic VGA_CLK;

ball dut(.*);

initial begin 
    CLOCK_50 = 1'b0;
    forever #1 CLOCK_50 = ~CLOCK_50;
end 

task vga_mux;
    input [7:0] vga_x;
    input [6:0] vga_y;
    input [2:0] vga_colour;
    input vga_plot;
        assert(dut.vga_x === vga_x);
        assert(dut.vga_y === vga_y);
        assert(dut.vga_colour === vga_colour);
        assert(dut.vga_plot === vga_plot);
endtask;

initial begin 
    KEY[3] = 1'b0;
    #10;//test reset 
    assert(dut.start_cs === 1 && dut.start_circle === 0 && dut.start_clear === 0 && dut.rst_circle === 0 && dut.rst_clear === 0);
    assert(dut.centre_x === 8'd80 && dut.centre_y === 7'd60 && dut.state === 2'b10 && dut.lock === 0);
    assert(dut.delay_circle === 20'b1 && dut.delay_start === 28'b1);
    KEY[3] = 1'b1;
    #2;
    //vga start clear screen
    vga_mux(dut.vga_x_cs,dut.vga_y_cs,dut.vga_colour_cs,dut.vga_plot_cs);

    //test delay_start counter 
    force dut.delay_start = 28'b1111_1111_1111_1111_1111_1111_1111; release dut.delay_start; #2;
    assert(dut.done_delay_start === 1);

    force dut.done_cs = 1; force dut.done_delay_start = 1; force dut.delay_circle = 20'b1111_1111_1111_1111_1111;
    release dut.delay_circle; #2;
    assert(dut.done_delay_circle === 1);
    assert(dut.start_circle === 1 && dut.rst_circle === 1 && dut.start_cs === 0 && dut.lock === 1);
    
    //vga start clear circle 
    force dut.done_circle = 1; force dut.done_delay_circle = 1; #2;
    assert(dut.rst_circle === 0 && dut.start_circle === 0 && dut.rst_clear === 1 && dut.start_clear === 1); #2;
    vga_mux(dut.vga_x_clear,dut.vga_y_clear,dut.vga_colour_clear,dut.vga_plot_clear);
    release dut.done_circle; release dut.done_delay_circle;
    
    //vga start draw circle 
    force dut.done_clear = 1; #2;
    assert(dut.rst_clear === 0 && dut.start_clear === 0 && dut.rst_circle === 1 && dut.start_circle === 1); #2;
    vga_mux(dut.vga_x_circle,dut.vga_y_circle,dut.vga_colour_circle,dut.vga_plot_circle);
    
    //test state = 2'b10
    force dut.state = 2'b10; force dut.centre_x = 8'd139;  force dut.centre_y = 7'd20;
    release dut.centre_x; release dut.centre_y; release dut.state; #2;
    assert(dut.state === 2'b01 && dut.centre_x === 8'd138 && dut.centre_y === 7'd21);//x,y    
    force dut.centre_x = 8'd139; force dut.state = 2'b10;release dut.centre_x;release dut.state;#2;
    assert(dut.state === 2'b00 && dut.centre_x === 8'd138 && dut.centre_y === 7'd20);//x    
    force dut.state = 2'b10;release dut.state;#2;
    assert(dut.state === 2'b11 && dut.centre_x === 8'd139 && dut.centre_y === 7'd21);//y   
    force dut.centre_x = 8'd30;force dut.state = 2'b10; release dut.centre_x; release dut.state;#2;
    assert(dut.centre_x === 8'd31 && dut.centre_y === 7'd20);

    //test state = 2'b00
    force dut.state = 2'b00; release dut.state; #2;
    assert(dut.state === 2'b01 && dut.centre_x === 8'd30 && dut.centre_y === 7'd21);//y
    force dut.centre_x = 8'd20;force dut.centre_y = 7'd20; force dut.state = 2'b00;
    release dut.centre_x;release dut.centre_y; release dut.state; #2;
    assert(dut.state === 2'b11 && dut.centre_x === 8'd21 && dut.centre_y === 7'd21);//x,y   
    force dut.centre_x = 8'd20; force dut.state = 2'b00; release dut.state; release dut.centre_x;#2;
    assert(dut.state === 2'b10 && dut.centre_x === 8'd21 && dut.centre_y === 7'd20);//x   
    force dut.centre_y = 7'd30;force dut.state = 2'b00; release dut.centre_y;release dut.state; #2;
    assert(dut.centre_x === 8'd20 && dut.centre_y === 7'd29);

    //test state = 2'b11
    force dut.state = 2'b11; #2;
    assert(dut.centre_x === 8'd21 && dut.centre_y === 7'd30);
    force dut.centre_x = 8'd139; force dut.centre_y = 7'd99; release dut.centre_x; release dut.centre_y;release dut.state; #2;
    assert(dut.state === 2'b00 && dut.centre_x === 8'd138 && dut.centre_y === 7'd98);//x,y
    force dut.centre_x = 8'd139; force dut.state = 2'b11; release dut.state;release dut.centre_x; #2;
    assert(dut.state === 2'b01 && dut.centre_x === 8'd138 && dut.centre_y === 7'd99);//x
    force dut.state = 2'b11; release dut.state; #2;
    assert(dut.state === 2'b10 && dut.centre_x === 8'd139 && dut.centre_y === 7'd98);//y

    //test state = 2'01
    force dut.state = 2'b01;#2;
    assert(dut.centre_x === 8'd138 && dut.centre_y === 7'd99); #2;
    assert(dut.centre_x === 8'd137 && dut.centre_y === 7'd98);//y    
    force dut.centre_x = 8'd20; release dut.centre_x;#2;
    assert(dut.centre_x === 8'd21 && dut.centre_y === 7'd99);//x   
    force dut.centre_x = 8'd20;release dut.centre_x; #2;
    assert(dut.centre_x === 8'd21 && dut.centre_y === 7'd98);//x,y

    $display("ball tb done");
    $stop;
end

endmodule: tb_ball
