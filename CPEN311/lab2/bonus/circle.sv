module circle(input logic clk, input logic rst_n, input logic [2:0] colour,
              input logic [7:0] centre_x, input logic [6:0] centre_y, input logic [7:0] radius,
              input logic start, output logic done,
              output logic [7:0] vga_x, output logic [6:0] vga_y,
              output logic [2:0] vga_colour, output logic vga_plot);
     // draw the circle
    logic [7:0] offset_y,offset_x,crit;
    logic [15:0] count;
    logic signed [9:0] x,y;

    assign done = (offset_y > offset_x);
    assign vga_colour = colour; 
    assign vga_x = x[7:0];
    assign vga_y = y[6:0];
   
    always @(posedge clk,negedge rst_n) begin 
        if(!rst_n) begin count = 16'b1; vga_plot = 1'b0; offset_y = 8'b0;offset_x = radius;crit = 1 - radius; end 
        else if(start && !done) begin 
            if(count % 8 == 1) begin x = centre_x + offset_x; y = centre_y + offset_y; end//octant 1
            else if(count % 8 == 2) begin x = centre_x + offset_y; y = centre_y + offset_x; end//octant 2
            else if(count % 8 == 4) begin x = centre_x - offset_x; y = centre_y + offset_y; end//octant 4
            else if(count % 8 == 3) begin x = centre_x - offset_y; y = centre_y + offset_x; end//octant 3
            else if(count % 8 == 5) begin x = centre_x - offset_x; y = centre_y - offset_y; end//octant 5
            else if(count % 8 == 6) begin x = centre_x - offset_y; y = centre_y - offset_x; end//octant 6
            else if(count % 8 == 7) begin x = centre_x + offset_x; y = centre_y - offset_y; end//octant 7
            else begin x = centre_x + offset_y; y = centre_y - offset_x; offset_y = offset_y + 1;//octant 8
                if(crit[7] == 1 || crit == 8'b0) begin  crit = crit + 2 * offset_y + 1; end //update offset every 8 cycles 
                else begin offset_x = offset_x - 1; crit = crit + 2 * (offset_y - offset_x) + 1; end
            end
            count = count + 1;//increment the counter
            if(x > 10'd159 || y > 10'd119 || $signed(x) < 0 || $signed(y) < 0) vga_plot = 1'b0;//not plotting if pixel is out-of-range
            else begin vga_plot = 1'b1; end
        end
        else if(done) begin vga_plot = 1'b0; end//not plotting if finish drawing the circle 
    end
endmodule

