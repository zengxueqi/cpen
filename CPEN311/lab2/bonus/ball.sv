module ball(input logic CLOCK_50, input logic [3:0] KEY, // KEY[3] is async active-low reset
            input logic [9:0] SW, output logic [9:0] LEDR,
            output logic [6:0] HEX0, output logic [6:0] HEX1, output logic [6:0] HEX2,
            output logic [6:0] HEX3, output logic [6:0] HEX4, output logic [6:0] HEX5,
            output logic [7:0] VGA_R, output logic [7:0] VGA_G, output logic [7:0] VGA_B,
            output logic VGA_HS, output logic VGA_VS, output logic VGA_CLK);
  
    // instantiate and connect the VGA adapter and your module

         logic [7:0] centre_x,vga_x_clear,vga_x_circle,radius,vga_x_cs,vga_x;
         logic [6:0] centre_y,vga_y_clear,vga_y_circle,vga_y_cs,vga_y;
         logic [2:0] vga_colour,vga_colour_cs,vga_colour_clear,vga_colour_circle;
         logic start_clear,done_clear,vga_plot_clear,rst_clear;
         logic start_circle,done_cricle,vga_plot_circle,rst_circle;
         logic start_cs,done_cs,vga_plot_cs,vga_plot;

         logic lock,done_delay_circle,done_delay_start;
         logic [1:0] state;//state[1] represents x's state, 1 means x++,0 means x--,
         logic [19:0] delay_circle;//delay 2^20 clock cycles each time after finish drawing a cycle 
			logic [27:0] delay_start;//delay 2^28 clock cycles before the monitor turns on
			
        wire [9:0] VGA_R_10,VGA_G_10,VGA_B_10;
        wire VGA_BLANK,VGA_SYNC;

        assign VGA_R = VGA_R_10[9:2];
        assign VGA_G = VGA_G_10[9:2];
        assign VGA_B = VGA_B_10[9:2];

        assign radius = 8'd20;
        assign vga_x = start_cs ? vga_x_cs : (start_clear ? vga_x_clear : vga_x_circle);
        assign vga_y = start_cs ? vga_y_cs : (start_clear ? vga_y_clear : vga_y_circle);
        assign vga_colour = start_cs ? vga_colour_cs : (start_clear ? vga_colour_clear : vga_colour_circle);
        assign vga_plot = start_cs ? vga_plot_cs : (start_clear ? vga_plot_clear : vga_plot_circle);

       circle clear(.clk(CLOCK_50),
                    .rst_n(rst_clear),
                    .colour(3'b000),
                    .centre_x(centre_x),
                    .centre_y(centre_y),
                    .radius(radius),
                    .start(start_clear),
                    .done(done_clear),
                    .vga_x(vga_x_clear),
                    .vga_y(vga_y_clear),
                    .vga_colour(vga_colour_clear),
                    .vga_plot(vga_plot_clear));

        circle draw(.clk(CLOCK_50),
                    .rst_n(rst_circle),
                    .colour(3'b10),
                    .centre_x(centre_x),
                    .centre_y(centre_y),
                    .radius(radius),
                    .start(start_circle),
                    .done(done_circle),
                    .vga_x(vga_x_circle),
                    .vga_y(vga_y_circle),
                    .vga_colour(vga_colour_circle),
                    .vga_plot(vga_plot_circle));

         clearscreen cs(.clk(CLOCK_50),
                        .rst_n(KEY[3]),
                        .colour(SW[2:0]),
                        .start(start_cs),
                        .done(done_cs),
                        .vga_x(vga_x_cs),
                        .vga_y(vga_y_cs),
                        .vga_colour(vga_colour_cs),
                        .vga_plot(vga_plot_cs));

         vga_adapter #(.RESOLUTION("160x120"))
                 vga_u0(.resetn(KEY[3]),
                        .clock(CLOCK_50),
                        .colour(vga_colour),
                        .x(vga_x),
                        .y(vga_y),
                        .plot(vga_plot),
                        .VGA_R(VGA_R_10),
                        .VGA_G(VGA_G_10),
                        .VGA_B(VGA_B_10),
                        .VGA_HS(VGA_HS),
                        .VGA_VS(VGA_VS),
                        .VGA_BLANK(VGA_BLANK),
                        .VGA_SYNC(VGA_SYNC),
                        .VGA_CLK(VGA_CLK));

        assign done_delay_circle = (delay_circle == 20'b0);
		  assign done_delay_start = (delay_start == 28'b0);
		  
        always_ff @(posedge CLOCK_50) begin 
            if(!KEY[3]) begin delay_circle <= 20'b1; delay_start <= 28'b1; end 
            else if(done_delay_start) delay_circle <= delay_circle + 20'b1;
				else delay_start <= delay_start + 28'b1; 
        end 

        always_ff @(posedge CLOCK_50) begin 
            if(!KEY[3]) begin start_cs <= 1; start_circle <= 0; start_clear <= 0; rst_circle <= 0; rst_clear <= 0; 
								centre_x <= 8'd80; centre_y <= 7'd60; state <= 2'b10;lock <= 0; end 
				else if(done_cs && !lock && done_delay_start) begin start_circle <= 1; rst_circle <= 1;
						start_cs <= 0;lock <= 1; end //lock this branch, go to this branch only once 
            else if(done_circle && done_delay_circle) begin rst_circle <= 0; start_circle <= 0; //reset circle 
						rst_clear <= 1; start_clear <= 1; end //start clear
            else if(done_clear) begin rst_clear <= 0; start_clear <= 0; //reset clear
                case(state)
                     2'b10: begin 
                        if(centre_x == 8'd139 && centre_y == 7'd20) begin state <= 2'b01;centre_y <= centre_y + 7'b1;centre_x <= centre_x - 8'b1; end 
                        else if(centre_x == 8'd139) begin state[1] <= ~state[1]; centre_x <= centre_x - 8'b1; centre_y <= centre_y - 7'b1; end 
                        else if(centre_y == 7'd20) begin state[0] <= ~state[0]; centre_x <= centre_x + 8'b1; centre_y <= centre_y + 7'b1; end 
                        else begin centre_x <= centre_x + 8'b1; centre_y <= centre_y - 7'b1; end 
                     end
                    2'b01: begin 
                        if(centre_x == 8'd20 && centre_y == 7'd99) begin state <= 2'b10;centre_x <= centre_x + 8'b1; centre_y <= centre_y - 7'b1;end 
                        else if(centre_x == 8'd20) begin state[1] <= ~state[1]; centre_x <= centre_x + 8'b1; centre_y <= centre_y + 7'b1;end
                        else if(centre_y == 7'd99) begin state[0] <= ~state[0]; centre_x <= centre_x - 8'b1; centre_y <= centre_y - 7'b1;end 
                        else begin centre_x <= centre_x - 8'b1; centre_y <= centre_y + 7'b1; end 
                    end
                    2'b11: begin 
                        if(centre_x == 8'd139 && centre_y == 7'd99) begin state <= 2'b00;centre_x <= centre_x - 8'b1;centre_y <= centre_y - 7'b1; end 
                        else if(centre_x == 8'd139) begin state[1] <= ~state[1]; centre_x <= centre_x - 8'b1;centre_y <= centre_y + 7'b1; end 
                        else if(centre_y == 7'd99) begin state[0] <= ~state[0]; centre_x <= centre_x + 8'b1;centre_y <= centre_y - 7'b1; end 
                        else begin centre_x <= centre_x + 8'b1; centre_y <= centre_y + 7'b1; end 
                    end 
                    2'b00: begin 
                        if(centre_x == 8'd20 && centre_y == 7'd20) begin state <= 2'b11; centre_x <= centre_x + 8'b1; centre_y <= centre_y + 7'b1; end 
                        else if(centre_x == 8'd20) begin state[1] <= ~state[1]; centre_x <= centre_x + 8'b1; centre_y <= centre_y - 7'b1; end 
                        else if(centre_y == 7'd20) begin state[0] <= ~state[0]; centre_x <= centre_x - 8'b1; centre_y <= centre_y + 7'b1; end 
                        else begin centre_x <= centre_x - 8'b1; centre_y <= centre_y - 7'b1; end 
                    end
                endcase
                rst_circle <= 1; 
                start_circle <= 1; //start circle 
            end
        end
		  
endmodule: ball

