module arc4(input logic clk, input logic rst_n,
            input logic en, output logic rdy,
            input logic [23:0] key,
            output logic [7:0] ct_addr, input logic [7:0] ct_rddata,
            output logic [7:0] pt_addr, input logic [7:0] pt_rddata, output logic [7:0] pt_wrdata, output logic pt_wren);

    logic en_init,rdy_init,wren_init,en_ksa,rdy_ksa,wren_ksa,en_prga,rdy_prga,wren_prga,wren;
    logic lock_assert_en_prga,lock_assert_en_ksa;
    logic done_init,done_ksa,start_init,start_ksa;
    logic [7:0] addr,wrdata,addr_init,wrdata_init,addr_ksa,wrdata_ksa,addr_prga,wrdata_prga;
    logic [7:0] q;

    assign en_init = en;

     init i(.clk(clk),
            .rst_n(rst_n),
            .en(en_init),
            .rdy(rdy_init),
            .addr(addr_init),
            .wrdata(wrdata_init),
            .wren(wren_init));

         ksa k(.clk(clk), 
               .rst_n(rst_n),
               .en(en_ksa),
               .rdy(rdy_ksa),
               .key(key),
               .addr(addr_ksa), 
               .rddata(q), 
               .wrdata(wrdata_ksa), 
               .wren(wren_ksa),
	        	.done(done_ksa));

        prga p(.clk(clk), 
               .rst_n(rst_n),
               .en(en_prga), 
               .rdy(rdy_prga),
               .key(key),
               .s_addr(addr_prga), 
               .s_rddata(q), 
               .s_wrdata(wrdata_prga), 
               .s_wren(wren_prga),
               .ct_addr(ct_addr), 
               .ct_rddata(ct_rddata),
               .pt_addr(pt_addr),
               .pt_rddata(pt_rddata),
               .pt_wrdata(pt_wrdata),
               .pt_wren(pt_wren));

    s_mem s(.address(addr),
            .clock(clk),
            .data(wrdata),
            .wren(wren),
            .q(q));

        assign wren = start_init ? wren_init : (start_ksa ? wren_ksa : wren_prga);
        assign addr = start_init ? addr_init : (start_ksa ? addr_ksa : addr_prga);
        assign wrdata = start_init ? wrdata_init : (start_ksa ? wrdata_ksa : wrdata_prga);

        assign done_init = (addr_init == 8'd255);

        //control rdy 
        always @(posedge clk,negedge rst_n) begin  
                if(!rst_n) rdy = 1; 
                else if(en) rdy = 0;  
                else if(rdy_prga && pt_addr != 8'b0) rdy = 1; 
        end 

         always @(posedge clk,negedge rst_n) begin 
            if(!rst_n) begin en_ksa = 0; en_prga = 0; start_init = 1; start_ksa = 0;
                 lock_assert_en_ksa = 0; lock_assert_en_prga = 0; end 
            else if(done_init && !lock_assert_en_ksa && rdy_ksa) begin en_ksa = 1; lock_assert_en_ksa = 1; start_ksa = 1;start_init = 0;end 
            else if(done_ksa && !lock_assert_en_prga && rdy_prga) begin en_prga = 1;lock_assert_en_prga = 1; start_ksa = 0; end 
            else begin en_prga = 0; en_ksa = 0; end  
        end
endmodule: arc4
