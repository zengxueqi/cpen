module task3(input logic CLOCK_50, input logic [3:0] KEY, input logic [9:0] SW,
             output logic [6:0] HEX0, output logic [6:0] HEX1, output logic [6:0] HEX2,
             output logic [6:0] HEX3, output logic [6:0] HEX4, output logic [6:0] HEX5,
             output logic [9:0] LEDR);

         logic [7:0] wrdata_ct,addr_pt,wrdata_pt,addr_ct;
         logic wren_ct,wren_pt;
         logic [7:0] q_ct,q_pt;
         logic en,rdy;
         logic lock_assert_en;

  ct_mem ct(.address(addr_ct),
            .clock(CLOCK_50),
            .data(wrdata_ct),
            .wren(1'b0),
            .q(q_ct));

  pt_mem pt(.address(addr_pt),
            .clock(CLOCK_50),
            .data(wrdata_pt),
            .wren(wren_pt),
            .q(q_pt));

     arc4 a4(.clk(CLOCK_50),
             .rst_n(KEY[3]),
             .en(en), 
             .rdy(rdy),
             .key({14'b0,SW}),
             .ct_addr(addr_ct),
             .ct_rddata(q_ct),
             .pt_addr(addr_pt), 
             .pt_rddata(q_pt),
             .pt_wrdata(wrdata_pt),
             .pt_wren(wren_pt));
        
        always @(posedge CLOCK_50,negedge KEY[3]) begin 
        if(!KEY[3]) begin en = 0; lock_assert_en = 0; end
        else if(rdy && !lock_assert_en) begin en = 1; lock_assert_en = 1; end
        else en = 0;
    end 

//assign en = rdy;
endmodule: task3
