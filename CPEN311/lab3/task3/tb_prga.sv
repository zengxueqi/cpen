module tb_prga();
logic clk,rst_n,en,rdy;
logic [23:0] key;
logic [7:0] s_addr,s_rddata,s_wrdata,ct_addr,ct_rddata,pt_addr,pt_rddata,pt_wrdata;
logic s_wren,pt_wren;

prga dut(.*);

initial begin 
    clk = 0;
    forever #1 clk = ~clk;
end 

initial begin 
    rst_n = 0; #5;
    assert(rdy === 1 && dut.start === 0 && dut.i == 8'b0 && dut.j === 8'b0 && dut.k === 8'b0 && dut.length === 8'b0 && dut.done_length === 0 && dut.count === 16'b0);
    assert(dut.s_addr === 8'b0 && dut.s_wrdata === 8'b0 && dut.s_wren === 0 && dut.ct_addr == 8'b0 && pt_addr === 8'b0 && pt_wrdata === 8'b0 && pt_wren === 0);

    rst_n = 1; en = 1; #1; s_rddata = 8'b0; ct_rddata = 8'd26;
    assert(dut.start ===1 && rdy === 0);
    assert(dut.count === 16'b1 && dut.k === 8'b1 && dut.i === 8'b1 && s_addr === 8'b1 && s_wren === 0 && pt_wren === 0); en = 0; #2;
    assert(dut.count === 16'd2 && dut.k === 8'b1 && dut.i === 8'b1 && s_addr === 8'b1); #2;
    assert(dut.count === 16'd3 && dut.k === 8'b1 && dut.i === 8'b1 && s_addr === 8'b1); #2;
    assert(dut.count === 16'd4 && dut.Si === 8'b0 && dut.temp_j === 10'b0 && dut.j === 8'b0 && s_addr === 8'b0); 
    assert(dut.done_length === 1 && dut.length === 8'd26 && pt_wrdata === 8'd26 && pt_wren === 1); #2;
    assert(dut.count === 16'd5 && s_wren === 0 && pt_wren === 0); #2;
    assert(dut.count === 16'd6 && s_wren === 0 && pt_wren === 0); s_rddata = 8'd45; #2;
    assert(dut.count === 16'd7 && dut.Sj === 8'd45 && s_addr === 8'b1 && s_wrdata === 8'd45 && s_wren === 1 && s_wren === 1 && dut.index_s === 10'd45); #2;
    assert(dut.count === 16'd8 && s_addr === dut.j && s_wrdata === dut.Si && s_wren === 1); #2;
    assert(dut.count === 16'd9 && s_addr === 8'd45 && ct_addr === 8'b1); #2;
    assert(dut.count === 16'd10 && s_wren === 0 && pt_wren === 0); #2;
    assert(dut.count === 16'd11 && s_wren === 0 && pt_wren === 0); s_rddata = 8'b0; ct_rddata = 8'b0; #2;
    assert(dut.count === 16'd12 && s_wren === 0 && pt_wren === 1 && pt_addr === 8'b1 && pt_wrdata === 8'd0); #2;
    force dut.k = 8'd27; #2;
    assert(dut.done === 1 && rdy === 1 && s_wren === 0 && pt_wren === 0);

    $display("prga test done");
    $stop;
end 
endmodule: tb_prga
