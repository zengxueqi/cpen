`timescale 1ps / 1ps
module tb_arc4();
logic clk,rst_n,en,rdy;
logic [23:0] key;
logic [7:0] s_addr,s_rddata,s_wrdata,ct_addr,ct_rddata,pt_addr,pt_rddata,pt_wrdata;
logic s_wren,pt_wren;

arc4 dut(.*);

initial begin 
    clk = 0;
    forever #1 clk = ~clk;
end

initial begin 
    rst_n = 0; key = 24'b1010_1111_0101_1111; #5;
    assert(rdy === 1 && dut.en_ksa === 0 && dut.en_prga === 0 && dut.start_init === 1 && dut.start_ksa === 0);
    assert(dut.lock_assert_en_ksa === 0 && dut.lock_assert_en_prga === 0);

    rst_n = 1; en = 1; #1;
    assert(dut.en_init === 1); #1; en = 0;#100;
    assert(dut.wren === dut.wren_init && dut.addr === dut.addr_init && dut.wrdata === dut.wrdata_init);
    assert(dut.en_ksa === 0 && dut.en_prga === 0);

    force dut.done_init = 1; #2;
    assert(dut.lock_assert_en_ksa === 1 && dut.en_ksa === 1 && dut.lock_assert_en_ksa === 1 && dut.start_ksa === 1 && dut.start_init === 0); #100;
    assert(dut.wren === dut.wren_ksa && dut.addr === dut.addr_ksa && dut.wrdata === dut.wrdata_ksa);
    release dut.done_init; #100;
    assert(dut.wren === dut.wren_ksa && dut.addr === dut.addr_ksa && dut.wrdata === dut.wrdata_ksa);
    assert(dut.en_ksa === 0 && dut.en_prga === 0);
    force dut.done_ksa = 1; #2;
    assert(dut.en_prga === 1 && dut.lock_assert_en_prga === 1 && dut.start_ksa === 0); 
    release dut.done_ksa; #100;
    assert(dut.wren === dut.wren_prga && dut.addr === dut.addr_prga && dut.wrdata === dut.wrdata_prga);
    assert(dut.en_ksa === 0 && dut.en_prga === 0); #1000;

    force dut.rdy_prga = 1; force dut.pt_addr = 8'h8; #2;
    assert(rdy === 1);
    $display("arc4 test done");
    $stop;
end
endmodule: tb_arc4
