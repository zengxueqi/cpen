`timescale 1ps / 1ps
module tb_task3();
logic CLOCK_50;
logic [3:0] KEY;
logic [9:0] SW;
logic [6:0] HEX0;
logic [6:0] HEX1;
logic [6:0] HEX2;
logic [6:0] HEX3;
logic [6:0] HEX4;
logic [6:0] HEX5;
logic [9:0] LEDR;
logic [7:0] length;
logic [7:0] data [0:255];
task3 dut(.*);

task memory_check;
     $readmemh("out.memh",data); #10;
     length = data[0];
     for(int i = 0; i <= length; i++) begin 
        assert(dut.pt.altsyncram_component.m_default.altsyncram_inst.mem_data[i] === data[i]); #1;
    end
endtask 

initial begin 
    CLOCK_50 = 1'b0;
    forever #1 CLOCK_50 = ~CLOCK_50;
end 
// Your testbench goes here.
initial begin 
    $readmemh("test2.memh", dut.ct.altsyncram_component.m_default.altsyncram_inst.mem_data); #10;
    SW = 10'h18;
    KEY[3] = 0; #5;
    assert (dut.en === 0 && dut.lock_assert_en === 0);
    KEY[3] = 1; #2;
    assert(dut.en === 1 && dut.lock_assert_en === 1); #2;
    assert(dut.en === 0);
    #4500;
    memory_check;
    $stop;
end 

endmodule: tb_task3
