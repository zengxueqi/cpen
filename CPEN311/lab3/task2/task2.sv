module task2(input logic CLOCK_50, input logic [3:0] KEY, input logic [9:0] SW,
             output logic [6:0] HEX0, output logic [6:0] HEX1, output logic [6:0] HEX2,
             output logic [6:0] HEX3, output logic [6:0] HEX4, output logic [6:0] HEX5,
             output logic [9:0] LEDR);

    logic en_init,rdy_init,wren_init,en_ksa,rdy_ksa,wren_ksa,wren; 
    logic lock_assert_en_init,lock_assert_en_ksa;
    logic done_init;
    logic [7:0] addr,wrdata,addr_init,wrdata_init,addr_ksa,wrdata_ksa;
    logic [7:0] q;

    init fk(.clk(CLOCK_50),
            .rst_n(KEY[3]),
            .en(en_init),
            .rdy(rdy_init),
            .addr(addr_init),
            .wrdata(wrdata_init),
            .wren(wren_init));

    ksa ksa_fk(.clk(CLOCK_50), 
               .rst_n(KEY[3]),
               .en(en_ksa),
               .rdy(rdy_ksa),
               .key({14'b0,SW[9:0]}),
               .addr(addr_ksa), 
               .rddata(q), 
               .wrdata(wrdata_ksa), 
               .wren(wren_ksa));

    s_mem s(.address(addr),
            .clock(CLOCK_50),
            .data(wrdata),
            .wren(wren),
            .q(q));

        assign wren = done_init ? wren_ksa : wren_init;
        assign addr = done_init ? addr_ksa : addr_init;
        assign wrdata = done_init ? wrdata_ksa : wrdata_init;

        assign done_init = (wren_init == 0 && addr_init == 8'd255);
        
	always @(posedge CLOCK_50,negedge KEY[3]) begin 
        if(!KEY[3]) begin en_init = 0; en_ksa = 0; lock_assert_en_ksa = 0; lock_assert_en_init = 0; end 
        else if(rdy_init && !lock_assert_en_init) begin en_init = 1; lock_assert_en_init = 1; end//enable init
        else if(done_init && rdy_ksa && !lock_assert_en_ksa) begin en_ksa = 1; lock_assert_en_ksa = 1; end //enable ksa
        else begin en_ksa = 0; en_init = 0; end 
    end
endmodule: task2
