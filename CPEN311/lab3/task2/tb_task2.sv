`timescale 1ps / 1ps
module tb_task2();
logic CLOCK_50;
logic [3:0] KEY;
logic [9:0] SW;
logic [6:0] HEX0;
logic [6:0] HEX1;
logic [6:0] HEX2;
logic [6:0] HEX3;
logic [6:0] HEX4;
logic [6:0] HEX5;
logic [9:0] LEDR;
logic [7:0] length;
logic [7:0] data [0:255];

task2 dut(.*);

task memory_check;
     $readmemh("check.memh",data); #10;
     for(int i = 0; i <= 255; i++) begin 
        assert(dut.s.altsyncram_component.m_default.altsyncram_inst.mem_data[i] === data[i]); #1;
    end
endtask 

initial begin 
    CLOCK_50 = 1'b0;
    forever #1 CLOCK_50 = ~CLOCK_50;
end 
// Your testbench goes here.
initial begin 
    KEY[3] = 0; SW = 10'b11_0011_1100; #10; //test reset 
    assert(dut.en_init === 0 && dut.en_ksa === 0 && dut.lock_assert_en_ksa === 0 && dut.lock_assert_en_init === 0 && dut.done_init === 0);
    assert(dut.rdy_init === 1 && dut.rdy_ksa === 1);
    
    KEY[3] = 1; #2; //start init
    assert(dut.en_init === 1 && dut.lock_assert_en_init === 1 && dut.rdy_init === 1);#2;
    assert(dut.en_init === 0 && dut.rdy_init === 0); 
    assert(dut.wren === dut.wren_init && dut.addr === dut.addr_init && dut.wrdata === dut.wrdata_init); #100;
    assert(dut.wren === dut.wren_init && dut.addr === dut.addr_init && dut.wrdata === dut.wrdata_init);
    assert(dut.en_init === 0 && dut.rdy_init === 0); #412;

    //finish init, start ksa
    assert(dut.done_init === 1); #2;
    assert(dut.en_ksa === 1 && dut.rdy_ksa === 1 && dut.lock_assert_en_ksa === 1); #2;
    assert(dut.en_ksa === 0 && dut.rdy_ksa === 0 && dut.en_init === 0); 
    assert(dut.wren === dut.wren_ksa && dut.addr === dut.addr_ksa && dut.wrdata === dut.wrdata_ksa); #100;
    assert(dut.wren === dut.wren_ksa && dut.addr === dut.addr_ksa && dut.wrdata === dut.wrdata_ksa); #100;

    #10000;
    memory_check;
    $display("task2 test done");
    $stop;
end 
endmodule: tb_task2
