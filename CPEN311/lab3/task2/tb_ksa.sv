module tb_ksa();
logic clk,rst_n,en,rdy,wren;
logic [23:0] key;
logic [7:0] addr,rddata,wrdata;

ksa dut(.*);

initial begin 
    clk = 0;
    forever #1 clk = ~clk;
end 

initial begin 
    rst_n = 0; key = 24'b0; en = 0; #5;//test reset 
    assert(dut.start === 0 && rdy === 1);
    assert(dut.i === 8'b1111_1111 && dut.j === 8'b0 && wren === 0 && addr === 8'b1111 && wrdata === 8'b0);
    assert(dut.count === 16'b0 && dut.Si === 8'b0 && dut.Sj === 8'b0 && dut.temp_j === 10'b0);

    rst_n = 1; en = 1; #1; 
    assert(rdy === 0 && dut.start === 1); 
    assert(dut.count % 5 === 1 && dut.i === 8'b0 && dut.wren === 0 && addr === 8'b0); #2; en = 0;
    assert(dut.count % 5 === 2); rddata = 8'b0; #2;
    assert(dut.count % 5 === 3 && dut.Si === rddata && wren === 0 && dut.temp_j === 10'b0 && dut.j === 8'b0 && addr === dut.j); #2;
    assert(dut.count % 5 === 4 && wrdata === dut.Si && addr === dut.j && wren === 1); #2;
    assert(dut.count % 5 === 0 && wrdata === 8'b0 && addr === 8'b0 && wren === 1); #2;

    force dut.i = 8'b0; release dut.i; assert(dut.Key === key[23:16]); #2;
    force dut.i = 8'b1; release dut.i; assert(dut.Key === key[15:8]); #2;
    force dut.i = 8'd2; release dut.i; assert(dut.Key === key[7:0]); #2;

    force dut.count = 16'd4; force dut.i = 8'd255; release dut.count; release dut.i; #2;
    assert(dut.done === 1); #2;
    assert(wren === 0 && rdy === 1);
    $display("ksa test done");
    $stop;
end

endmodule: tb_ksa
