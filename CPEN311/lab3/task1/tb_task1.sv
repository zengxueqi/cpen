`timescale 1ps / 1ps

module tb_task1();
logic CLOCK_50;
logic [3:0] KEY;
logic [9:0] SW;
logic [6:0] HEX0;
logic [6:0] HEX1;
logic [6:0] HEX2;
logic [6:0] HEX3;
logic [6:0] HEX4;
logic [6:0] HEX5;
logic [9:0] LEDR;

task1 dut(.*);

initial begin 
    CLOCK_50 = 1'b1;
    forever #1 CLOCK_50 = ~CLOCK_50;
end 

initial begin
	KEY[3] = 0; #10; 
    assert(dut.en === 0 && dut.lock_assert_en === 0);
    KEY[3] = 1; #2;
    assert(dut.en === 1 && dut.lock_assert_en === 1);#2;
    assert(dut.en === 0 && dut.fk.rdy === 0);
    for(int i = 0; i <= 255;i++) begin 
        assert(dut.addr === i && dut.wrdata === i && dut.wren === 1); #2;
    end
    assert(dut.rdy === 1 && dut.wren === 0);
    $display("task1 test done");
    $stop;
end

// Your testbench goes here.

endmodule: tb_task1
