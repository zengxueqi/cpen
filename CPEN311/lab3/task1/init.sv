module init(input logic clk, input logic rst_n,
            input logic en, output logic rdy,
            output logic [7:0] addr, output logic [7:0] wrdata, output logic wren);
    logic start,done;
    assign done = (addr == 8'd255);

    //control start initialize memory 
    always @(posedge clk,negedge rst_n) begin 
        if(!rst_n) begin start = 0; rdy = 1;end
        else if(en) begin start = 1; rdy = 0;end
        else if(done) rdy = 1; 
    end 

    //initialize memory
    always @(posedge clk,negedge rst_n) begin 
        if(!rst_n) begin addr = 8'b0; wrdata = 8'b1111_1111; wren = 0; end 
        else if(start && !done) begin wrdata = wrdata + 8'b1; addr = wrdata; wren = 1; end
        else if(done) begin wren = 0; end
    end

endmodule: init
