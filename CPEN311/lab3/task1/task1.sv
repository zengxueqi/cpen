module task1(input logic CLOCK_50, input logic [3:0] KEY, input logic [9:0] SW,
             output logic [6:0] HEX0, output logic [6:0] HEX1, output logic [6:0] HEX2,
             output logic [6:0] HEX3, output logic [6:0] HEX4, output logic [6:0] HEX5,
             output logic [9:0] LEDR);

    logic en,rdy,wren;
    logic lock_assert_en;
    logic [7:0] addr,wrdata;
    logic [7:0] q;

    init fk(.clk(CLOCK_50), 
            .rst_n(KEY[3]),
            .en(en), 
            .rdy(rdy),
            .addr(addr), 
            .wrdata(wrdata), 
            .wren(wren));

    s_mem s(.address(addr),
            .clock(CLOCK_50),
            .data(wrdata),
            .wren(wren),
            .q(q));

       always @(posedge CLOCK_50,negedge KEY[3]) begin 
        if(!KEY[3]) begin en = 0; lock_assert_en = 0; end
        else if(rdy && !lock_assert_en) begin en = 1; lock_assert_en = 1; end
        else en = 0; 
    end 

endmodule: task1



