module tb_init();

// Your testbench goes here.
logic clk;
logic rst_n;
logic en;
logic rdy;
logic [7:0] addr;
logic [7:0] wrdata;
logic wren;

init dut(.*);

initial begin 
    clk = 1'b0;
    forever #1 clk = ~clk;
end

initial begin 
    rst_n = 0;
    #5;
    assert(dut.start === 0 && dut.done === 0 && rdy === 1);
    assert(addr === 8'b0 && wrdata == 8'b1111_1111 && wren === 0);
    rst_n = 1; en = 1; #1; 
    assert(dut.start === 1 && rdy === 0); #1;
    assert(wrdata === 8'b0 && addr === 8'b0 && wren === 1);#2; en = 0;
    assert(wrdata === 8'b1 && addr === 8'b1 && wren === 1);#2;
    force dut.wrdata = 8'd254; release dut.wrdata; #2;
    assert(wrdata === 8'd255 && addr === 8'd255 && wren === 1 && dut.done === 1);
    #2; 
    assert(rdy === 1 && wren === 0);
    $display("init test done");
    $stop;
end
endmodule: tb_init
