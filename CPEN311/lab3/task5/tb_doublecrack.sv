`timescale 1ps / 1ps

module tb_doublecrack();
logic clk,rst_n,en,rdy,key_valid;
logic [23:0] key;
logic [7:0] ct_addr,ct_rddata;

doublecrack dut(.*);

initial begin 
    clk = 0;
    forever #1 clk = ~clk;
end 

initial begin 
    force dut.key_valid_c1 = 1; force dut.key_c1 = 24'h89; force dut.key_c2 = 24'h34; #1;
    assert(dut.key_valid === 1 && dut.key === 24'h89); #1; force dut.done = 1; #1;
    assert(rdy === 1); #1;
    
    force dut.key_valid_c1 = 0; #1;
    assert(key === dut.key_c2); #1;
    
    force dut.rdy_c1 = 1; force dut.rdy_c2 = 1; #1;
    assert(rdy === 1); 
    
    force dut.rdy_c1 = 0; force dut.rdy_c2 = 0; #1;
    assert(rdy === 0);
    release dut.key_valid_c1; release dut.key_valid_c2; release dut.key_c1; release dut.key_c2;
    release dut.done; release dut.rdy_c1; release dut.rdy_c2;

    /*Test copy ct mem*/
    rst_n = 0; ct_rddata = 8'h12; #5; 
    assert(dut.index === 8'b0 && dut.ct_addr_copy === 8'b0 && dut.ct2_wrdata === 8'b0 && dut.ct2_wren === 0 && dut.done_copy === 0);
    rst_n = 1; en = 1; #1; 
    assert(dut.count === 8'b1 && dut.ct_addr_copy === 8'b0 && dut.ct2_wren === 0); #1; en = 0; #1;
    assert(dut.count === 8'd2 && dut.ct2_wren === 0); #2;
    assert(dut.length === 8'h12);
    assert(dut.count === 8'd3 && dut.ct2_wren === 1 && dut.ct2_wrdata === 8'h12 && dut.index === 8'b1); #2;
    assert(ct_addr === dut.ct_addr_copy && dut.ct2_addr === dut.ct_addr_copy);  #20;
    assert(ct_addr === dut.ct_addr_copy && dut.ct2_addr === dut.ct_addr_copy);  #200;

    assert(dut.done_copy === 1 && dut.ct2_wren === 0);
    assert(ct_addr === dut.ct_addr_c1 && dut.ct2_addr === dut.ct_addr_c2); #1000;
    assert(ct_addr === dut.ct_addr_c1 && dut.ct2_addr === dut.ct_addr_c2); #1000;
    assert(ct_addr === dut.ct_addr_c1 && dut.ct2_addr === dut.ct_addr_c2); 

    $display("doublecrack test done");
    $stop;
end
endmodule: tb_doublecrack
