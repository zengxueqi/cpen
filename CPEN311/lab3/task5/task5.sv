module task5(input logic CLOCK_50, input logic [3:0] KEY, input logic [9:0] SW,
             output logic [6:0] HEX0, output logic [6:0] HEX1, output logic [6:0] HEX2,
             output logic [6:0] HEX3, output logic [6:0] HEX4, output logic [6:0] HEX5,
             output logic [9:0] LEDR);

        logic [7:0] ct_addr, ct_rddata,ct_wrdata;
        logic en,rdy,key_valid,wren;
        logic [23:0] key;
        logic lock_assert_en;

  ct_mem ct(.address(ct_addr),
            .clock(CLOCK_50),
            .data(ct_wrdata),
            .wren(wren),
            .q(ct_rddata));

    doublecrack dc(.clk(CLOCK_50),.rst_n(KEY[3]),.en(en),.rdy(rdy),.key(key),.key_valid(key_valid),
             .ct_addr(ct_addr), .ct_rddata(ct_rddata));

    sseg R1(key[3:0],rdy,key_valid,HEX0);
    sseg R2(key[7:4],rdy,key_valid,HEX1);
    sseg R3(key[11:8],rdy,key_valid,HEX2);
    sseg R4(key[15:12],rdy,key_valid,HEX3);
    sseg R5(key[19:16],rdy,key_valid,HEX4);
    sseg R6(key[23:20],rdy,key_valid,HEX5);

    always @(posedge CLOCK_50,negedge KEY[3]) begin 
        if(!KEY[3]) begin en = 0; lock_assert_en = 0;end 
        else if(!lock_assert_en) begin en = 1; lock_assert_en = 1; end
        else en = 0; 
    end 
endmodule: task5

module sseg(input [3:0] in,input rdy,input key_valid,output reg [6:0] segs);
    always @* begin
        if(!rdy) segs = 7'b1111_111;
        else if(!key_valid && rdy) segs = 7'b0111_111;
        else begin
            case(in)
                 4'd0: segs = 7'b1000_000;
                 4'd1: segs = 7'b1111_001;
                 4'd2: segs = 7'b0100_100;
                 4'd3: segs = 7'b0110_000;
                 4'd4: segs = 7'b0011_001;
                 4'd5: segs = 7'b0010_010;
                 4'd6: segs = 7'b0000_010;
                 4'd7: segs = 7'b1111_000;
                 4'd8: segs = 7'b0;
                 4'd9: segs = 7'b0010_000;
                 4'd10: segs = 7'b0001_000;
                 4'd11: segs = 7'b0000_011;
                 4'd12: segs = 7'b1000_110;
                 4'd13: segs = 7'b0100_001;
                 4'd14: segs = 7'b0000_110;
                 4'd15: segs = 7'b0001_110;
                 default: segs = 7'b1111_111;
            endcase
        end
    end
    endmodule

