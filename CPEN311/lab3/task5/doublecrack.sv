module doublecrack(input logic clk, input logic rst_n,
             input logic en, output logic rdy,
             output logic [23:0] key, output logic key_valid,
             output logic [7:0] ct_addr, input logic [7:0] ct_rddata);

   logic rdy_c1,rdy_c2,key_valid_c1,key_valid_c2,pt_wren,ct2_wren,done; 
   logic [7:0] ct_addr_c1,ct_addr_c2,ct_addr_copy;//c1,c2 addr
   logic [7:0] ct2_rddata,ct2_addr,ct2_wrdata;//ct2 module
   logic [7:0] pt_addr,pt_rddata,pt_wrdata;//pt module 
   logic [23:0] key_c1,key_c2;
   logic [7:0]data_c1[0:255];
   logic [7:0]data_c2[0:255];

   logic [7:0] index,length,count;
   logic done_copy;

    // this memory must have the length-prefixed plaintext if key_valid
    pt_mem pt(.address(pt_addr),.clock(clk),.data(pt_wrdata),.wren(pt_wren),.q(pt_rddata));

    s_mem ct2(.address(ct2_addr),.clock(clk),.data(ct2_wrdata),.wren(ct2_wren),.q(ct2_rddata));//ct memory for crack c2

    // for this task only, you may ADD ports to crack
    assign ct_addr = done_copy ? ct_addr_c1 : ct_addr_copy;
    assign ct2_addr = done_copy ? ct_addr_c2 : ct_addr_copy;

    crack c1(.clk(clk),.rst_n(rst_n),.en(en),.rdy(rdy_c1),.key(key_c1),.key_valid(key_valid_c1),
             .ct_addr(ct_addr_c1), .ct_rddata(ct_rddata),.data(data_c1),.state(1'b0),.other_done(rdy_c2));//connect to ct_mem in task5.sv 
    crack c2(.clk(clk),.rst_n(rst_n),.en(en),.rdy(rdy_c2),.key(key_c2),.key_valid(key_valid_c2),
             .ct_addr(ct_addr_c2), .ct_rddata(ct2_rddata),.data(data_c2),.state(1'b1),.other_done(rdy_c1));//connect to ct2 mem

    write_pt fuck(clk,rst_n,key_valid_c1,key_valid_c2,data_c1,data_c2,pt_addr,pt_wrdata,pt_wren,done);
    
    assign key_valid = (key_valid_c1 || key_valid_c2);
    assign key = key_valid_c1 ? key_c1 : key_c2;

    always @(*) begin 
        if(key_valid_c1 || key_valid_c2) rdy = done;
        else if(rdy_c1 && rdy_c2) rdy = 1;
        else rdy = 0;
    end 

    assign done_copy = (index != 8'b0 && index - 8'b1 == length && count % 3 == 1);

    //copy content to ct2 module 
    always @(posedge clk,negedge rst_n) begin
        if(!rst_n) begin index = 8'b0; ct_addr_copy = 8'b0; ct2_wrdata = 8'b0; ct2_wren = 0; count = 8'b0; end 
        else if(!done_copy) begin ct2_wren = 0; 
            if(count % 3 == 0) begin ct_addr_copy = index; end 
            else if(count % 3 == 2) begin 
                if(index == 8'b0) begin length = ct_rddata; end 
                ct2_wren = 1; ct2_wrdata = ct_rddata; index = index + 8'b1;//write ct[index] to ct2 mem
            end 
            count = count + 8'b1;
        end
        else begin ct2_wren = 0; ct2_wrdata = 8'b0; end 
    end

endmodule: doublecrack


module write_pt(input logic clk,input logic rst_n,input logic key_valid_c1,input logic key_valid_c2,
            input logic [7:0]data_c1[0:255],input logic [7:0]data_c2[0:255],
            output logic [7:0] pt_addr,output logic [7:0] pt_wrdata,output logic pt_wren,output logic done);
   logic [7:0] index,length;
   logic done_length;
   assign done = (index == length && done_length);
    always @(posedge clk) begin 
        if(!rst_n) begin index = 8'b0; done_length = 0; pt_wren = 0; pt_addr = 8'b0; pt_wrdata = 8'b0; end 
        else if( (key_valid_c1 || key_valid_c2) && !done) begin
            if(!done_length) begin length = data_c1[0]; pt_addr = 8'b0; pt_wrdata = length; done_length = 1; pt_wren = 1; end 
            else begin index = index + 8'b1; pt_addr = index; pt_wrdata = key_valid_c1 ? data_c1[index] : data_c2[index]; pt_wren = 1; end 
        end
        else pt_wren = 0;
    end 
	 
endmodule



