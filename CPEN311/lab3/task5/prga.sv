module prga(input logic clk, input logic rst_n,
            input logic en, output logic rdy,
            input logic [23:0] key,
            output logic [7:0] s_addr, input logic [7:0] s_rddata, output logic [7:0] s_wrdata, output logic s_wren,
            output logic [7:0] ct_addr, input logic [7:0] ct_rddata,
            output logic [7:0] pt_addr, input logic [7:0] pt_rddata, output logic [7:0] pt_wrdata, output logic pt_wren);
    logic lock_assert_rdy;
    logic start,done,done_length;
    logic [9:0] temp_j,index_s;
    logic [7:0] Si,Sj,length,i,j,k;
    logic [15:0] count;

    assign done = (k - 8'b1 == length && done_length);

    //control start prga
    always @(posedge clk,negedge rst_n) begin 
        if(!rst_n) begin start = 0; rdy = 1; end
        else if(en) begin start = 1; rdy = 0; end
        else if(done) rdy = 1;
    end

    always @(posedge clk,negedge rst_n) begin
        if(!rst_n) begin i = 8'b0; j = 8'b0; k = 8'b0; length = 8'b0; done_length = 0; count = 16'b0;
                    s_addr = 8'b0; s_wrdata = 8'b0; s_wren = 0; ct_addr = 8'b0; pt_addr = 8'b0; pt_wrdata = 8'b0; pt_wren = 0; end 
        else if(start && !done) begin s_wren = 0; pt_wren = 0;
            if(count % 12 == 0) begin k = k + 8'b1; i = (i + 8'b1) % 256; s_addr = i; end //read s[i],length 
            else if(count % 12 == 3) begin Si = s_rddata; temp_j = ({2'b0,j} + {2'b0,Si}) % 256; j = temp_j[7:0]; s_addr = j; //read s[j]
 		if(!done_length) begin length = ct_rddata; done_length = 1; pt_wrdata = length; pt_wren = 1; end end //write length to pt
            else if(count % 12 == 6) begin Sj = s_rddata; s_addr = i; s_wrdata = Sj; s_wren = 1; index_s = ({2'b0,Si} + {2'b0,Sj}) % 256; end //write s[j] to location i 
            else if(count % 12 == 7) begin s_addr = j; s_wrdata = Si; s_wren = 1; end //write s[i] to location j
            else if(count % 12 == 8) begin s_addr = index_s[7:0]; ct_addr = k; end //read s[s[i]+s[j]%256],read ct[k]
            else if(count % 12 == 11) begin pt_wrdata = s_rddata ^ ct_rddata; pt_addr = k; pt_wren = 1; end //write plaintext
            count = count + 16'b1;
        end    
        else if(done) begin s_wren = 0; pt_wren = 0;end

    end
endmodule: prga
