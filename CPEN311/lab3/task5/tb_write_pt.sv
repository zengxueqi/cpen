module tb_write_pt();
logic clk,rst_n,key_valid_c1,key_valid_c2;
logic [7:0] data_c1[0:255];
logic [7:0] data_c2[0:255];
logic [7:0] pt_addr,pt_wrdata;
logic pt_wren,done;

write_pt dut(.*);

initial begin
    clk = 0;
    forever #1 clk = ~clk;
end

initial begin
    rst_n = 0; #5;  data_c1[0] = 8'h67; data_c1[1] = 8'h45; data_c1[2] = 8'h34;
    assert(dut.index === 8'b0 && dut.done_length === 0 && pt_wren === 0 && pt_addr === 8'b0 && dut.pt_wrdata === 8'b0 && done === 0);
    rst_n = 1; force dut.key_valid_c1 = 1; #2;
    assert(dut.done_length === 1 && dut.length === 8'h67 && pt_addr === 8'b0 && pt_wren === 1 && pt_wrdata === 8'h67); #2;
    assert(dut.index === 8'b1 && pt_addr === 8'b1 && pt_wrdata === 8'h45 && pt_wren === 1); #2;
    assert(dut.index === 8'd2 && pt_addr === 8'd2 && pt_wrdata === 8'h34 && pt_wren === 1); #2;
    force dut.index = 8'h67; #2;
    assert(done === 1 && pt_wren === 0);
    $display("write_pt test done");
    $stop;
end 
endmodule: tb_write_pt

