module tb_sseg();
logic [3:0] in;
logic rdy,key_valid;
logic [6:0] segs;

sseg dut(.*);

initial begin 
    rdy = 0; #1;
    assert(segs === 7'b1111_111); rdy = 1; key_valid = 0; #1;
    assert(segs === 7'b0111_111); key_valid = 1; in = 4'd0; #1;
    assert(segs === 7'b1000_000); in = 4'd1; #1;
    assert(segs === 7'b1111_001); in = 4'd2; #1;
    assert(segs === 7'b0100_100); in = 4'd3; #1;
    assert(segs === 7'b0110_000); in = 4'd4; #1;
    assert(segs === 7'b0011_001); in = 4'd5; #1;
    assert(segs === 7'b0010_010); in = 4'd6; #1;
    assert(segs === 7'b010); in = 4'd7; #1;
    assert(segs === 7'b1111_000); in = 4'd8; #1;
    assert(segs === 7'b0); in = 4'd9; #1; 
    assert(segs === 7'b0010_000); in = 4'd10; #1;
    assert(segs === 7'b0001_000); in = 4'd11; #1;
    assert(segs === 7'b11); in = 4'd12; #1;
    assert(segs === 7'b1000_110); in = 4'd13; #1;
    assert(segs === 7'b0100_001); in = 4'd14; #1;
    assert(segs === 7'b110); in = 4'd15; #1;
    assert(segs === 7'b0001_110);
    $display("sseg test done");
    $stop;
end 
endmodule: tb_sseg 

