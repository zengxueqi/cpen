`timescale 1ps / 1ps
module tb_crack();
logic clk,rst_n,en,rdy,key_valid;
logic [23:0] key;
logic [7:0] ct_addr,ct_rddata;

crack dut(.*);

initial begin 
    clk = 0;
    forever #1 clk = ~clk;
end 

initial begin 
    rst_n = 0; #5;
    assert(dut.start === 0 && dut.en_arc === 0 && dut.lock_assert_en_arc === 0);
    assert(key === 24'b0 && rdy === 0 && key_valid === 0 && dut.length === 8'b0 && dut.done_length === 0);
    assert(dut.rst_arc === 0 && dut.lock_assert_rst_arc === 1 && dut.lock_deassert_rst_arc === 0 && dut.done_check_pt === 0);
    assert(dut.en_arc === 0 && dut.lock_assert_en_arc === 0);

   rst_n = 1; en = 1; #1;//start
   assert(dut.start === 1 && dut.rst_arc === 1 && dut.lock_deassert_rst_arc === 1); 
   #1; en = 0; #2;
   assert(dut.en_arc === 1 && dut.lock_assert_en_arc === 1);

   force dut.pt_wren = 1; force dut.pt_addr = 8'b0; force dut.pt_wrdata = 8'h56; #2;
    assert(dut.en_arc === 0 && dut.lock_assert_en_arc === 1);
    assert(dut.length === 8'h56); #4;//length branch

    force dut.pt_addr = 8'h12; force dut.pt_wrdata = 8'h90; force dut.pt_wren = 1; #2; //invalid key 
    assert(dut.done_check_pt === 1); #2;
    assert(dut.lock_assert_rst_arc === 0 && dut.lock_deassert_rst_arc === 0 && dut.done_check_pt === 0 && dut.key === 24'b1); #2;//done_check_pt branch
    assert(dut.rst_arc === 0 && dut.lock_assert_rst_arc === 1); #4;

    force dut.done_check_pt = 1; force dut.key = 24'hFFFFFF; release dut.key; #2;
    assert(rdy === 1 && key_valid === 0);//cannot find key 
    release dut.pt_wren; release dut.pt_addr; release dut.pt_wrdata; release dut.done_check_pt;

    rst_n = 0; #4; rst_n = 1; en = 1; #2; en = 0; #10; 
    force dut.pt_addr = 8'h56;force dut.length = 8'h56; force dut.pt_wren = 1;force dut.pt_wrdata = 8'h67;  #2;
    assert(key_valid === 1 && rdy === 1); #2;
    $display("crack test done");
    $stop;
end
endmodule: tb_crack
