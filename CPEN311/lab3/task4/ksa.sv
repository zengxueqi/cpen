module ksa(input logic clk, input logic rst_n,
           input logic en, output logic rdy,
           input logic [23:0] key,
           output logic [7:0] addr, input logic [7:0] rddata, output logic [7:0] wrdata, output logic wren,output logic done);

    logic start;
    logic [7:0] Si,i,j;
    logic [9:0] temp_j;
    logic [15:0] count;
    logic [7:0] Key;
    assign done = (addr == 8'd255 && count % 5 == 0);

    always @(*) begin 
        case(i % 3)
        0: Key = key[23:16];
        1: Key = key[15:8];
        2: Key = key[7:0];
        default: Key = 8'b0;
        endcase
    end

    //control start ksa stuff
    always @(posedge clk,negedge rst_n) begin 
        if(!rst_n) begin start = 0; rdy = 1;end
        else if(en) begin start = 1; rdy = 0;end
        else if(done) rdy = 1; 
    end

   //do ksa stuff  
    always @(posedge clk,negedge rst_n) begin 
        if(!rst_n) begin i = 8'b1111_1111; j = 8'b0; wren = 0; addr = 8'b1111; wrdata = 8'b0;
                count = 16'b0; Si = 8'b0; temp_j = 10'b0; end  
        else if(start && !done) begin 
            if(count % 5 == 0) begin i = i + 8'b1; wren = 0; addr = i;end //read s[i]
            else if(count % 5 == 2) begin Si = rddata; wren = 0; temp_j = ({2'b0,j} + {2'b0,Si} + {2'b0,Key}) % 10'd256; j = temp_j[7:0]; addr = j; end//read s[j]
            else if(count % 5 == 3) begin wrdata = Si; addr = j; wren = 1; end //write S[i] to location j 
            else if(count % 5 == 4) begin wrdata = rddata; addr = i; wren = 1; end //write S[j] to location i 
            count = count + 16'b1;
        end
        else if(done) begin wren = 0; end 
    end
endmodule: ksa
