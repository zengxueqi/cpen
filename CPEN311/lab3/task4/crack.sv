module crack(input logic clk, input logic rst_n,
             input logic en, output logic rdy,
             output logic [23:0] key, output logic key_valid,
             output logic [7:0] ct_addr, input logic [7:0] ct_rddata);

    logic [7:0] pt_addr,pt_wrdata,pt_rddata,length;
    logic pt_wren,start,done_check_pt,done_length;
    logic rst_arc,en_arc,rdy_arc;
    logic lock_assert_rst_arc,lock_deassert_rst_arc,lock_assert_en_arc;

    // this memory must have the length-prefixed plaintext if key_valid
    pt_mem pt(.address(pt_addr),.clock(clk),.data(pt_wrdata),.wren(pt_wren),.q(pt_rddata));

    arc4 a4(.clk(clk),.rst_n(rst_arc),.en(en_arc),.rdy(rdy_arc),.key(key),
            .ct_addr(ct_addr),.ct_rddata(ct_rddata),
            .pt_addr(pt_addr),.pt_rddata(pt_rddata),.pt_wrdata(pt_wrdata),.pt_wren(pt_wren));

    always @(posedge clk,negedge rst_n) begin
        if(!rst_n) begin start = 0; end
        else if(en) begin start = 1; end
    end
    
    //control en_arc 
    always @(posedge clk,negedge rst_arc) begin 
        if(!rst_arc) begin en_arc = 0; lock_assert_en_arc = 0; end 
        else if(!lock_assert_en_arc && rdy_arc) begin en_arc = 1; lock_assert_en_arc = 1; end 
        else begin en_arc = 0; lock_assert_en_arc = 1; end 
    end 

     always @(posedge clk,negedge rst_n) begin
        if(!rst_n) begin key = 24'b0; rdy = 0; key_valid = 0; length = 8'b0; done_length = 0;
                rst_arc = 0; lock_assert_rst_arc = 1; lock_deassert_rst_arc = 0; done_check_pt = 0;  end 
        else if(start && !rdy) begin 
            if(!lock_assert_rst_arc) begin rst_arc = 0; lock_assert_rst_arc = 1; end //reset arc
            else if(!lock_deassert_rst_arc) begin rst_arc = 1; lock_deassert_rst_arc = 1; end //de-assert rst_arc 
            else if(done_check_pt) begin lock_assert_rst_arc = 0; lock_deassert_rst_arc = 0; 
                        done_check_pt = 0; key = key + 24'b1; if(key == 24'b0) rdy = 1; end //start reset to check a new key
            else if(pt_wren == 1 && pt_addr == 8'b0) begin length = pt_wrdata; end  
            else if(pt_wren == 1 && pt_addr != 8'b0 && (pt_wrdata < 8'h20 || pt_wrdata > 8'h7E)) done_check_pt = 1;//this key is invalid
            else if(pt_wren == 1 && pt_addr == length) begin key_valid = 1; rdy = 1;end  
        end
     end

endmodule: crack
